.. meta::
	:author: Laurence Osete
	:description: Cas d'étude de problèmes juridiques liés à l'usage du numérique en situation de classe.
	:keywords: éducation, usages du numérique, Université Grenoble Alpes, Inspé, Inspé ressources, étude de cas
	:http-equiv=Content-Type: text/html; charset=UTF-8


.. _index:

===========
Cas d'étude
===========

.. admonition:: Information

	* **Responsable éditorial** : Laurence Osete, Inspé, Univ. Grenoble Alpes.

	* **Résumé** : Cette page indexe les différentes études de cas juridiques. 

	* **Dernière mise à jour** : |today|.

	* **Licence** : Sauf mention contraire spécifiée sur le document, tous les documents de ce site sont placés sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.


Cas d'étude en primaire (matrenelle ou élémentaire)
===================================================

* `Cas 1 <cas1_PE.html>`_
* `Cas 2 <cas2_PE.html>`_
* `Cas 3 <cas3_PE.html>`_
* `Cas 4 <cas4_PE.html>`_
* `Cas 5 <cas5_PE.html>`_
* `Cas 6 <cas6_PE.html>`_
