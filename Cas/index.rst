.. meta::
	:author: Laurence Osete
	:description: Cas d'étude de problèmes juridiques liés à l'usage du numérique en situation de classe.
	:keywords: éducation, usages du numérique, Université Grenoble Alpes, Inspé, Inspé ressources, étude de cas
	:http-equiv=Content-Type: text/html; charset=UTF-8


.. _index:

===========
Cas d'étude
===========

.. admonition:: Information

	* **Responsable éditorial** : Laurence Osete, Inspé, Univ. Grenoble Alpes.

	* **Résumé** : Cette page indexe les différentes études de cas juridiques. 

	* **Dernière mise à jour** : |today|.

	* **Licence** : Sauf mention contraire spécifiée sur le document, tous les documents de ce site sont placés sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.


Cas d'étude au secondaire (collège ou lycée)
============================================


* `Cas 1 : protection des données personnelles lors d'usage de services en ligne (dont ENT) <cas1_SD.html>`_
* `Cas 2 : protection des données personnelles stockées par les enseignants <cas2_SD.html>`_
* `Cas 3 : protection des données personnelles, cas particulier des photos d'élèves <cas3_SD.html>`_
* `Cas 4 : droit d'auteur des productions d'élèves <cas4_SD.html>`_
* `Cas 5 : droit d'auteur sur les ressources utilisées en classe <cas5_SD.html>`_
* `Cas 6 : cyber-violence en classe <cas6_SD.html>`_

Cas d'étude en primaire (matrenelle ou élémentaire)
===================================================

.. * `Cas 1 <cas1_PE.html>`_
.. * `Cas 2 <cas2_PE.html>`_
.. * `Cas 3 <cas3_PE.html>`_
.. * `Cas 4 <cas4_PE.html>`_
.. * `Cas 5 <cas5_PE.html>`_
.. * `Cas 6 <cas6_PE.html>`_

Cas d'étude pour l'encadrement éducatif
=======================================

.. * `Cas 1 <cas1_EE.html>`_
.. * `Cas 2 <cas2_EE.html>`_
.. * `Cas 3 <cas3_EE.html>`_
.. * `Cas 4 <cas4_EE.html>`_
.. * `Cas 5 <cas5_EE.html>`_
.. * `Cas 6 <cas6_EE.html>`_