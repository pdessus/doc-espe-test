.. _class-card:

**********************************************************************
Class-Card, un jeu de rôles pour simuler des situations d'enseignement
**********************************************************************


.. index::
	single: auteurs; Dessus, Philippe
	single: auteurs; Chabert, Julie
	single: auteurs; Pernin, Jean-Philippe
	single: auteurs; Wanlin, Philippe


.. admonition:: Informations

	* **Auteurs** : `Philippe Dessus <http://pdessus.fr/>`_, LaRAC & Inspé, Univ. Grenoble Alpes, Julie Chabert, Jean-Philippe Pernin, LIG, Univ. Grenoble Alpes, & Philippe Wanlin, Univ. de Genève, Suisse.

	* **Date de création** : Février 2021.

	* **Date de publication** : |today|.

	* **Statut du document** : En cours.

	* **Résumé** :  Ce Document présente *Class-Card*, un jeu sérieux de jeu de rôles simulant une grande partie du processus d'enseignement (de la planification au débriefing). Ce jeu se joue à 2 ou 3 personnes et une phase complète dure 2 h. Les exemples donnés sont adaptés à l'école élémentaire, mais une transposition à d'autres niveaux est possible. Voir Dessus *et al*. (2020) pour plus d'informations sur ce jeu.

	* **Voir aussi** : Le :ref:`rech-educ:conpa`, présentant un jeu sérieux pour trouver une problématique de recherche.
	
	* **Matériel à télécharger** : :download:`images/docs-class-card.pdf`.
	
	* **Matériel nécessaire** : Des feuilles A3 vierges, des stylos de différentes couleurs, des notes repositionnables, des plans de séances d'enseignement (un plan est fourni dans le matériel à télécharger).
	
	* **Note** : Le masculin a été utilisé pour alléger la lecture du Document. Nous n'oublions pas que la majorité des enseignants des premier et second degré en France sont des enseignantes.
	
	* **Remerciements** : Nous remercions vivement Macha Klajnbaum pour le design graphique des cartes et jetons ; Hélène Gondrand pour avoir supervisé le travail du test de *Class-Card* ; le Pôle Grenoble Cognition pour avoir financé le stage ayant permis la conception et le test du jeu.
	
	* **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.

	* **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.

Introduction
************

L'activité d'enseignement comprend classiquement les phases de planification (pré-activité), de travail en présence des élèves (inter-activité) et de bilan post-activité. L'entraînement à ces différentes phases est crucial pour la formation initiale des enseignants, et pourtant, hormis les stages, il existe assez peu de moyens de les y préparer, hormis les stages en établissement ou écoles, et la vidéo-formation. 

Ce Document décrit *Class-Card* (Dessus, Chabert, Pernin, & Wanlin, 2020), un jeu sérieux de rôles "sur plateau" reprenant une grande partie de ce processus : elle permet à un “*Enseignant*”, à partir d'un squelette de leçon qu'il doit analyser, de planifier les différents événements auxquels lui-même et ses élèves vont devoir être confrontés, puis de les simuler devant le "*Maître de jeu*", qui pourra, quand il le jugera utile, introduire des aléas auxquels l'enseignant devra réagir.

Les exemples donnés sont adaptés à l'école élémentaire, mais une transposition à d'autres niveaux est possible sans difficultés.

Note
****

Les joueurs sont évoqués en italiques ; les <cartes et jetons> sont entourées de signe inférieur-supérieur.

But du jeu
**********

*Class-Card* est joué par au moins 2 joueurs (3 si les deux joueurs sont des enseignants débutants et qu'un formateur intervient dans la phase de Discussion-débriefing).  L'un des joueurs a le rôle de l'*Enseignant*, l'autre le *Maître du jeu* (ce dernier rôle peut aussi être joué par un formateur). Optionnellement, un formateur peut mener la phase de Discussion-débriefing, dans le rôle du *Discutant*.

*Class-Card* est censé aborder un assez grand nombre de connaissances et compétences en lien avec l'enseignement et peut être joué par des étudiants ou professeurs-stagiaires en formation professionnelle, ou toute autre personne désireuse de réfléchir à cette question.


Phases du jeu
************* 

Un tour de jeu dure environ 2 h et comprend les phases suivantes. Lorsqu'un tour complet est réalisé le *Maître du jeu* et l'*Enseignant* échangent leurs rôles pour un deuxième tour, s'ils en ont le temps.

0. Installation (env. 20 min)
-----------------------------

Le but de cette phase est d'installer le matériel de jeu et de faire en sorte que les participants comprennent les règles du jeu. Le *Maître du jeu* présente à l'*Enseignant* le jeu, son but, ses règles, son matériel et ses phases. Pour cela, il donne la <Carte-rôle Enseignant> à l'*Enseignant* et garde pour lui la <Carte-rôle Maître du jeu>. Ensuite l'*Enseignant* lit en détail le plan de leçon, qui va être le propos principal de la phase suivante. Ce plan de leçon peut être soit imposé par le formateur, soit choisi par l'*Enseignant* parmi plusieurs.

1. Préparation (env. 40 min)
----------------------------

Pendant cette phase l'*Enseignant* doit réaliser un scénario de séance inspirée par le plan de leçon. Le *Maître du jeu* a pour rôle de clarifier les règles du jeu à la demande de l'*Enseignant*. Deux sous-phases sont tour à tour réalisées, la Scénarisation et l'Explication.

**1.1. Scénarisation de la leçon (20 min)**. Tout d'abord, l'*Enseignant* lit très attentivement le plan de leçon à la fois pour le comprendre et pour en réaliser une scénarisation plus précise, sur une feuille A3, qu'il organisera le mieux possible (par exemple en séparant les différentes phases de la leçon). Pour mieux caractériser les événements qui vont se dérouler dans sa leçon, il pourra utiliser des <Cartes d'actions d'apprentissage> (6 cartes) pour les élèves, et les <jetons d'actions de l'enseignant> pour lui-même (13 jetons). Des annotations sur la feuille et sur des notes repositionnables vont permettre de détailler le mieux possible le décours de la leçon et la mémorisation des points importants (voir Figure 1 ci-dessous). Cette sous-phase est terminée quand l'*Enseignant* pense avoir décrit le Scénario de la leçon, c'est-à-dire de détailler ce que lui et ses “élèves” auront à faire pendant la leçon. Il est à noter qu'à la fin de cette phase le plan de leçon initial sera retiré. La Carte <Critères d'un scénario> pourra l'aider à rien oublier d'important.

**1.2. Explication (env. 20 min)**. L'*Enseignant* explique quels sont les principales parties du Scénario de leçon, ce que lui et ses élèves devront faire. Le *Maître du jeu* peut demander des précisions si des points sont insuffisamment clairs, précisions qui peuvent être ajoutées au Scénario. Ensuite, le *Maître du jeu* pioche de 3 à 5 <Cartes événements> qui seront utilisées dans la phase suivante.

2. Simulation (env. 30 min)
---------------------------

Cette phase simule le Scénario de la manière la plus authentique possible et représente la phase interactive de l'enseignement. Ici, l'*Enseignant* explique à voix haute ce qu'il ferait tout au long de la leçon, en disant à haute voix les consignes comme si ses élèves étaient en face de lui. Il peut aussi expliquer, le cas échéant, ce que les élèves sont censés faire à leur tour. Il peut à tout moment lire les indications qu'il a écrites sur le Scénario (feuille A3) pendant la phase précédente. 

À tout moment de la leçon, le *Maître* du jeu peut sélectionner une <Carte événement> et interrompre l'*Enseignant* en posant sur la table (face recto), pour simuler un incident en veillant qu'il est approprié à l'activité en cours. En même temps, il pose un <jeton “Point d'interrogation”> à l'endroit du scénario où la carte apparaît, pour pouvoir en discuter dans la Phase 3. L'*Enseignant* doit y réagir spontanément, en expliquant ce qu'il ferait. Le verso de la <Carte événement> peut ensuite être lu par l'*Enseignant* car il contient quelques éléments de réflexion sur l'événement. Ensuite, la leçon reprend son cours jusqu'à ce que le contenu du Scénario soit expliqué en entier, le *Maître de jeu* pouvant, lorsqu'il le juge utile, interrompre encore la leçon par d'autres <Cartes événements>.

Un exemple d'utilisation du plateau de jeu est donné dans la Figure 1 ci-dessous. Le plateau du haut correspond à l'état du jeu dans la phase 1 (Préparation). On peut noter : - la séparation verticale entre les différentes phases de la leçon ; - les différentes <Cartes d'actions d'apprentissage> posées dans chaque phase ; - à droite, les <Cartes-rôle> expliquant le rôle de chaque joueur.

Le plateau du bas montre les différentes <Cartes événements> apposées par le *Maître du jeu* au cours de la simulation de la leçon.

.. image:: /images/plateau-class-card.jpg
   :alt: plateau Class-Card
   :scale: 10%

3. Discussion-débriefing (env. 30 min)
--------------------------------------

Ensuite se met en place une phase de Discussion-débriefing, correspondant à la phase de post-activité de l'enseignement, dans laquelle l'*Enseignant* et le *Maître du jeu* reviennent sur les principales décisions prises pendant la leçon, discutent de possibles alternatives, etc. Les questions suivantes peuvent aider à déclencher des analyses : “Quels problèmes sont apparus ? Quelles décisions avez-vous prises pour les régler ? Quels problèmes ont été difficiles à régler ? Que modifieriez-vous dans cette leçon si vous aviez à la mener à nouveau ? Qu'avez-vous appris pendant cette session ? Si les deux joueurs sont des enseignants débutants, un formateur peut intervenir pour mener le débat en tant que *Discutant*. 


Matériel
********

Voici la présentation du matériel, phase par phase. Le fichier PDF contenant l'ensemble des documents est à télécharger ici : :download:`images/docs-class-card.pdf`.

0. Installation
---------------

- Des feuilles A3 pour écrire la préparation
- Des notes repositionnables pour compléter les éléments de la préparation
- Des stylos de différentes couleurs
- Une ou plusieurs plans de cours (pp. 1-2 du Document à télécharger)
- Des <Cartes-rôle> pour le Maître du jeu et l'Enseignant, décrivant leurs tâches dans les 3 grandes phases du jeu (Préparation, Simulation, Discussion)(p. 3 du Document)

1. Préparation
--------------

- un jeu de 6 <Cartes actions d'apprentissage> (p. 4 du Document)
- un jeu de 13 <Jetons actions de l'Enseignant> (p. 5 du Document), copié en au moins 3 exemplaires. Voici le rôle des jetons :

    + les 4 jetons bleus permettent de spécifier les modalités de travail des élèves (individuel ou en groupes)
    + les 7 jetons jaunes permettent de spécifier le rôle de l'*Enseignant*
    + le jeton "?" est utilisé lorsqu'une <carte événement> est abattue
- la <Carte Critères d'un scénario> (p. 6 du Document)


2. Simulation
-------------

- un jeu de 21 <Cartes événement> (pp. 7-11 du Document)


Fondements théoriques
*********************

Les fondements théoriques de ce jeu sont inspirés des travaux suivants. 

Le découpage en phases pré-, inter- et post-actives est classique et notamment décrit dans Clark et Peterson (1986).

Les cartes “actions d'apprentissage” reprennent la catégorisation de Bloom et ses collègues (1969).

Les jetons de description des actions de l'enseignant sont inspirés du travail de Merrill (2013).

La description des événements scolaires et des moyens d'intervenir sur les incidents est reprise du *Classroom Assessment Scoring System*, de Pianta et ses collègues ; on pourra consulter :ref:`cours-class:index`.

Références
**********

* Bloom, B. S., Englehart, M. D., Furst, E. J., Hill, W. H., & Krathwohl, D. R. (1969). *Taxonomie des objectifs pédagogiques (M. Lavallée, Trans.  Vol. 1 : Domaine cognitif)*. Montréal: Éducation Nouvelle.

* Clark, C. M., & Peterson, P. L. (1986). Teachers' thought processes. In M. C. Wittrock (Ed.), *Handbook of Research on Teaching* (3rd ed., pp. 255–296). New York: Mac Millan.

* Dessus, P., Chabert, J., Pernin, J.-P., & Wanlin, P. (2020). Class-Card: a role-playing simulation of instructional experiences for pre-service teachers. In I. Marfisi-Schottman, I., F. Bellotti, L. Hamon, & R. Klemke (Eds.), *9th Int. Conf. Games and learning alliance (GALA 2020)*(pp. 283–293). Cham: Springer, LNCS 12517. [`Article en PDF <https://hal.archives-ouvertes.fr/hal-03035427>`_] 

* Merrill, M. D. (2013). *First principles of instruction*. San Francisco: Pfeiffer.

* Pianta, R. C., La Paro, K. M., & Hamre, B. K. (2008). *Classroom assessment scoring system: Manual K-3*. Baltimore: Brookes.
 
 