.. _principes_educatifs:

***********************************
Réfléchir à ses principes éducatifs
***********************************

.. index::
    single: auteurs; Dessus, Philippe
    single: auteurs; Besse, Émilie

.. admonition:: Information

	* **Auteurs** : `Philippe Dessus <http://pdessus.fr/>`_, Inspé & LaRAC, Univ. Grenoble Alpes. Le quizz a été réalisé par Emilie Besse, projet `ReFlexPro <http://www.idefi-reflexpro.fr>`_.

	* **Date de création** : Mars 2003, mis à jour en Janvier 2006.

	* **Date de publication** : |today|.

	* **Statut du document** : Terminé. 

	* **Résumé** : L'objectif de ce document est de faire réfléchir l'enseignant à certains buts de son métier, certaines valeurs, et de lui montrer qu'ils peuvent être incompatibles les uns les autres et/ou conduire à des dilemmes. Aucune prescription ne sera donnée, mais plutôt une série d'exercices de réflexion.

	* **Voir aussi** : :doc:`sciedu-ateliers:atelier-metier`, qui contient divers exercices de réflexion sur cette question.
	
	* **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.

	**Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.


Ce que l'on sait
================

L'article premier de la loi d'\ `orientation sur l'éducation de 1989 <http://daniel.calin.free.fr/textoff/loi_1989.html>`_ mentionne l'éducation comme "première priorité nationale" et rappelle que le système éducatif est "conçu et organisé en fonction des élèves et des étudiants". Ces deux points sont aisés à comprendre, sinon à appliquer. En revanche, les deux phrases suivantes posent quelques problèmes.

  "Il [le système éducatif] contribue à l'égalité des chances. Le droit à l'éducation est garanti à chacun afin de lui permettre de développer sa personnalité, d'élever son niveau de formation initiale et continue, de s'insérer dans la vie sociale et professionnelle, d'exercer sa citoyenneté."

Le premier point, celui de l'égalité des chances, est déjà problématique. En effet, il ne dit pas comment l'enseignant doit se comporter pour contribuer à cela. Faut-il qu'il se consacre à chaque élève également (égalité) ? Faut-il au contraire qu'il se consacre *plus* aux élèves qui en ont *plus besoin* (équité) ? Gaudet et Lapointe :cite:`gaudet02` définissent précisément ces deux termes : égalité  signifie "distribution à chacun de la même quantité de biens" alors qu'équité signifie  "dépassement du principe d'égalité pour compenser certaines inégalités (sociales, individuelles, etc.)". 

Mais la question n'est pas encore réglée. Dans quels cas l'enseignant doit être "égalitaire" ? faire preuve d'équité ? Par exemple, les aspects liés au comportement des élèves seront plus à même d'être traités d'un point de vue d'égalité que ceux concernant l'apprentissage. De plus, comme certains auteurs l'ont noté (voir notamment :cite:`dubet02,dessus03,egan97`), certains des buts ci-dessus sont incompatibles, soit entre eux, soit avec l'autre but que doit remplir tout enseignant, celui d'instruire ses élèves. Par exemple, développer la personnalité de l'élève est incompatible avec l'idée de l'insérer dans sa future vie sociale (voir ci-dessous la section Analyse des pratiques, ex. 1 pour une réflexion sur les autres contradictions).

Dubet (:cite:`dubet02`, p. 93 *et sq*.), dans son analyse du métier d'enseignant (du primaire et du secondaire), montre que le système éducatif a longtemps ignoré l'enfant (à éduquer) au profit de l'élève (à instruire). Les psychologues du début du 20 :sup:`e` siècle ont été les   premiers à prendre en compte l'enfant, derrière l'élève difficile ou agité. Plus récemment (et surtout depuis la Loi d'orientation ci-dessus), arrive l'enfant aux côtés de l'élève, et complique le travail de l'enseignant, car, toujours d'après Dubet, un enfant qui souffre devient un élève en difficulté, *et vice versa*.

Ce que l'on peut faire
======================

Il nous semble que l'une des meilleures choses à faire concernant les principes éducatifs est d'y réfléchir, tout en étant conscient des dilemmes et paradoxes  qu'ils contiennent. C'est pourquoi ce document ne contient aucune prescription, mais une section "Analyse des pratiques", plus importante, ci-dessous.

On peut y réfléchir de trois manières : à propos de ses propres pratiques, à propos de celles de ses collègues (ou plutôt, celles que l'on pense être de ses collègues), à propos de celles auxquelles on a été témoin en tant qu'élève.

Quizz
=====

.. eqt:: Education-1

	**Question 1. 	Selon Gaudet et Lapointe**

	A) :eqt:`C` `L'équité signifie qu'il faut compenser certaines inégalités`
	B) :eqt:`I` `L'équité signifie que l'on donne la même quantité de bien à tout le monde`
	C) :eqt:`I` `L'égalité signifie qu'il faut compenser certaines inégalités`
	D) :eqt:`I` `L'égalité ainsi que l'équité signifie qu'il faut donner la même quantité de bien à tout le monde`

.. eqt:: Education-2

	**Question 2. 	L'article 1er de la Loi d'orientation sur l'éducation de 1989 rappelle que le système éducatif est conçu et organisé**

	A) :eqt:`I` `En fonction des enseignants`
	B) :eqt:`I` `En fonction de la politique de l'Éducation nationale`
	C) :eqt:`C` `En fonction des élèves`
	D) :eqt:`I` `En fonction des grands mouvements éducatifs`

.. eqt:: Education-3

	**Question 3. 	Les psychologues du XXe siècle**

	A) :eqt:`I` `Ont été les premiers à parler d'équité envers les élèves`
	B) :eqt:`C` `Ont été les premiers à prendre en compte l'enfant derrière l'élève en difficulté`
	C) :eqt:`I` `Préconisaient de ne pas prendre en compte l'enfant mais l'élève`
	D) :eqt:`I` `Ont été les premiers à parler d'égalité envers les élèves`


Analyse des pratiques
=====================

#. Dans quelles situations traiteriez-vous vos élèves à égalité ? Dans quelles autres mettriez-vous en place un état d'équité entre eux ?

#. Approfondissons ici les autres contradictions présentées ci-dessus (Dubet & Egan). Le but de socialisation (inculquer des normes, croyances, contraintes sociales) peut être incompatible avec celui de faire acquérir à l'élève des connaissances rationnelles, scientifiques, sur des contenus. De même, laisser chaque élève se développer selon ses propres aspirations (le développement prime les connaissances)  peut être contradictoire avec celui de lui inculquer des connaissances rationnelles (les connaissances priment le développement).

#. Reprendre les différentes conceptions ci-dessus et les classer selon qu'elles s'adressent à l'enfant ou à l'élève. Ajouter à la liste trois des vôtres propres.

#. Pour une situation donnée, analysez-la selon trois points de vue différents. Vous-même en tant qu'enseignant. En vous mettant à la place d'un collègue. Vous-même en tant qu'élève. Repérez les éventuels décalages liés aux valeurs et buts éducatifs.


Références
==========

.. bibliography::
 :cited:
 :style: apa
