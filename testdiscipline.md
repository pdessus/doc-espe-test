(questionnaire-discipline)=

# Atelier -- Questionnaire sur la gestion de la discipline

```{index} single: auteurs; Dessus, Philippe
```

:::{admonition} Informations
- **Auteur** : [Philippe Dessus](http://pdessus.fr/), Inspé & LaRAC, Univ. Grenoble Alpes.
- **Date de création** : Mars 2002.
- **Date de publication** : {sub-ref}`today`.
- **Statut du document** : Terminé.
- **Résumé** : Ce questionnaire permet de mettre à l'épreuve ses opinions sur la gestion de la discipline à l'école. Il permet notamment de se rendre compte de ses priorités quant à la gestion de la classe. Pense-t-on qu'il faut être autoritaire, permissif, intimidant, favorisant le groupe, etc. ? Ce test est une traduction adaptée de Weber (1994, p. 269-272, test de   maîtrise).
- **Voir aussi** : Document {ref}`discipline`.
- **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.
- **Licence** : Document placé sous licence *Creative Commons* : [BY-NC-SA](http://creativecommons.org/licenses/by-nc-sa/3.0/fr/).
:::

## Tâche

- Entourez les numéros des 10 items pour lesquels vous êtes parfaitements en accord.
- Barrez les numéros des 10 items  pour lesquels vous êtes en désaccord total.
- Ensuite, déterminez l'attitude qui correspond au plus grand nombre d'items (voir réponses ci-dessous).

## Questionnaire

01. Un des rôles centraux de l'enseignant est de maintenir l'ordre et la discipline dans la classe par le contrôle du comportement des élèves.
02. Le climat de la classe influence beaucoup l'apprentissage et l'enseignant influence la nature de ce climat.
03. Il faut féliciter les comportements acceptables des élèves  et éviter de féliciter ceux qui sont inacceptables.
04. Un programme d'enseignement individualisé peut éliminer la plupart des problèmes de gestion de la classe.
05. Lorsqu'il règle un problème, l'enseignant doit prendre en compte la  situation de l'élève, pas son caractère ou sa personnalité.
06. Ne pas donner de limites aux élèves car cela les empêche d'atteindre  leur propre potentiel.
07. Utiliser le système des conséquences logiques plutôt que les  punitions classiques, afin d'éviter les conséquences néfastes de ces  dernières (c'est-à-dire choisir une punition qui permette à l'élève  de réparer le préjudice commis, ou qu'il comprenne directement la  relation entre le comportement inadéquat et la punition, alors que  les punitions "classiques" sont sans lien direct avec le comportement  inadéquat, p.ex. faire des lignes. Voir document [Sanction](sanction.md) pour plus  de détails).
08. Gérer sa classe efficacement, c'est contrôler chaque élève, même par  l'usage de la coercition si nécessaire.
09. Il faut toujours être aimable et ferme avec les élèves, car la  cohérence dans l'attitude est importante.
10. L'enseignant doit aider les élèves à comprendre, accepter et suivre  les règles établies dans la classe.
11. Un système de bons points ou de notes bien conçu est un  moyen  efficace de promouvoir les comportements appropriés des élèves.
12. L'enseignant devrait être tolérant à toutes formes de comportements  d'élèves.
13. Les élèves doivent prendre conscience des conséquences matérielles et  logiques de leurs actes, sauf si elles les mettent en danger  physique.
14. L'enseignant doit faire comprendre aux élèves qu'il est toujours  pleinement conscient de tout qui se passe dans sa classe.
15. L'enseignant doit établir et maintenir des relations positives avec  ses élèves.
16. L'utilisation appropriée de punitions est un moyen efficace de  gestion de la classe.
17. On doit délivrer félicitations et récompenses individuellement, en  tenant compte de la personnalité de chaque élève.
18. Gérer de manière appropriée le travail des élèves fait décroître la  frustration et l'ennui des élèves, et fait maintenir la discipline.
19. L'usage approprié de réprimandes est un moyen efficace de maintenir  la discipline.
20. L'enseignant doit respecter les élève et les aider à développer une  forme d'auto-discipline.
21. Ne jamais punir un élève sans être persuadé de sa culpabilité.
22. L'enseignant doit aider la classe à développer ses propres normes et  renforcer la cohésion de ses membres.
23. L'enseignant doit tenir compte du fait que, en classe, des  comportements inappropriés peuvent aussi être appris.
24. Lorsqu'un élève a un comportement inapproprié, c'est souvent parce  qu'on lui a donné un travail inapproprié.
25. La manière dont l'enseignant communique aux élèves est d'une    importance capitale.
26. L'enseignant doit recourir aux sarcasmes ou à la moquerie avec  précaution, et seulement après que de bonnes relations avec les  élèves ont été instaurées.
27. L'enseignant doit toujours avoir un comportement exemplaire devant  les élèves.
28. L'enseignant doit observer et questionner les élèves afin de  justifier d'éventuelles récompenses.
29. L'utilisation de réunions de classe est un moyen efficace de résoudre  certains problèmes de discipline.
30. Lorsqu'il règle des problèmes de discipline, l'enseignant doit  pouvoir faire la différence entre le comportement et la personne.
31. La plupart des problèmes de discipline peuvent être évités par des  stratégies d'enseignement appropriées.
32. Il est important d'aider les élèves à développer des capacités de  communication et de travail en groupe.
33. Gérer efficacement une classe, c'est rien d'autre que faire preuve de  bon sens.
34. L'utilisation appropriée de sarcasmes et moqueries est un moyen  efficace de maintenir la discipline dans la classe.
35. Une des tâches les plus importantes de l'enseignant est d'utiliser des stratégies d'enseignement variées afin de maintenir la discipline dans sa classe.
36. Il est important d'établir et de maintenir des règles de comportement dans la classe.
37. L'enseignant doit être aimable mais ferme avec les élèves dès le début de l'année, car il est plus facile de relâcher le contrôle que de l'imposer lorsqu'on l'a perdu.
38. L'enseignant peut augmenter la cohésion de ses élèves en leur faisant comprendre que l'appartenance à un tel groupe est valorisante et agréable.
39. La discipline est favorisée lorsque les élèves trouvent les leçons intéressantes et motivantes.
40. La tolérance, la sollicitude et le sens pratique sont des qualités importantes pour gérer la classe efficacement.
41. Il est important de favoriser un environnement physique et psychologique dans lequel les élèves sont complètement libres de dire  et faire ce qu'ils désirent.
42. Des leçons bien préparées sont un moyen efficace de maintenir la discipline dans la classe.
43. On peut renforcer des comportements adéquats en faisant en sorte que les récompenses et punitions soient individuelles.
44. Une gestion efficace de la classe passe principalement par la capacité de l'enseignant à sanctionner les éventuels comportements inadaptés des élèves.
45. L'enseignant doit assumer la resposabilité du contrôle du comportement de ses élèves.
46. Une des tâches principales de l'enseignant est d'essayer un grand nombre de trucs et de ficelles pour prévenir ou régler des problèmes de discipline.
47. Il faut reconnaître qu'un comportement encouragé a des chances de se reproduire, alors qu'un comportement non encouragé a des chances de disparaître.
48. L'enseignant doit préparer et utiliser des activités adéquates afin de favoriser des comportements adéquats chez ses élèves.
49. Une gestion efficace de la classe passe par l'établissement et le maintien de son contrôle par l'enseignant.
50. Gérer efficacement sa classe, c'est l'aider à résoudre un grand nombre de ses problèmes par elle-même.

## Analyse des réponses

Chaque item ci-dessus renvoie à une attitude à propos de la gestion de la discipline. Déterminez celle que vous avez le plus approuvée, celle que vous avez le plus rejetée.

- *AU : autoritaire*. Vous pensez que le comportement des élèves peut   être contrôlé par l'enseignant, parce que ce dernier est le mieux   placé pour connaître ce qui est le mieux pour l'élève. Vous pensez que   le travail de l'enseignant, c'est en grande partie établir et   maintenir des règles, donner des directives.
- *CS : climat social*. Vous pensez que le plus important est que les   élèves se sentent bien dans votre classe, et que les relations   interélèves et élèves-enseignant se déroulent au mieux. Il convient   donc que l'enseignant soit tolérant, conciliant, exprime de la   sollicitude envers ses élèves.
- *MC : modification du comportement*. Vous pensez que, si un élève se comporte de manière inadéquate, c'est parce que personne ne lui a montré que c'était une manière inadéquate. Il existe donc selon vous des manières d'enseigner des comportements adéquats, en récompensant ces derniers, plutôt que de sanctionner les comportements inadéquats.
- *ME : méthodes d'enseignement.* Vous pensez qu'un enseignant qui prépare et utilise les méthodes d'enseignement appropriées afin de faire des cours intéressants  n'a pas ou peu de problèmes de discipline.
- *PE : permissif.* L'idée, ici, est de maximiser la liberté de l'élève, afin de lui permettre un développement optimal.
- *IN : intimidation*. Comme l'attitude autoritaire, cette attitude suppose que le comportement des élèves peut être contrôlé par l'enseignant. En revanche, ce contrôle passe plutôt par l'utilisation du sarcasme, la moquerie, voire la force ou la peur.
- *TR : trucs et ficelles*. Vous pensez que la gestion de la discipline en classe est affaire de trucs, de ficelles, bref, qu'avec du bon sens, on   règle la plupart des problèmes de discipline qui surviennent. Il suffit donc que l'enseignant soit attentif à ce qui se passe dans sa classe et règle au coup par coup les problèmes qui surviennent.
- *GG: Gestion de groupe*. L'apprentissage au sein d'un groupe est pour vous l'idée la plus importante à favoriser. C'est à l'enseignant de créer et maintenir une ambiance de travail collaborative, sans pour autant qu'il soit le leader incontesté du groupe. La création de réunions où la classe tente de résoudre ses propres problèmes est aussi favorisée.

## Réponses

Repérez les catégories des items que vous avez le plus choisis et rejetés :

- 1. AU, 2. CS, 3. MC, 4. ME, 5. CS, 6. PE, 7. CS, 8. IN, 9. TR, 10. AU,
- 11. MC, 12. PE, 13. CS, 14. GG, 15. CS, 16. IN, 17. MC, 18. ME, 19. AU, 20. CS
- 21. TR, 22. GG, 23. MC, 24. ME, 25. CS, 26. TR, 27. TR, 28. MC, 29. GG, 30. CS,
- 31. ME, 32. GG, 33. TR, 34. IN, 35. ME, 36. AU, 37. TR, 38. GG, 39. ME, 40. CS,
- 41. PE, 42. ME, 43. MC, 44. IN, 45. AU, 46. TR, 47. MC, 48. ME, 49. AU, 50. GG.

## Analyse de pratiques

1. Passez le test, puis revenez aux documents {ref}`discipline` et sur les [sanctions](sanction.md) pour comprendre de quelle manière peut se gérer la discipline dans votre classe.
2. Tous les "profils" d'enseignant ci-dessous vous paraissent-ils efficaces pour maintenir la discpline ? Pourquoi ?
3. Vous pouvez également passer un test plus général sur le métier d'enseignant (voir Doc. {ref}`metier_aphorismes`.

## Références

Weber, W. A. (1994). Classroom management. In J. M. Cooper (Ed.), *Classroom teaching skills* (pp. 233-280). Lexington: Heath and Co.
