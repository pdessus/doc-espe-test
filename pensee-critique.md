(pensee-critique)=

# Pensée critique et enseignement

```{index} single: auteurs; Dessus, Philippe
```

:::{admonition} Information
- **Auteur** : [Philippe Dessus](http://pdessus.fr), LaRAC & Inspé, Univ. Grenoble Alpes.
- **Date de création** : Juillet 2018.
- **Date de modification** : {sub-ref}`today`.
- **Statut** : En travaux.
- **Résumé** : Ce document donne quelques éléments à propos de la pensée critique.
- **Voir aussi** : Document {doc}`fake-news`.
- **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.
- **Licence** : Document placé sous licence *Creative Commons* : [BY-NC-SA](http://creativecommons.org/licenses/by-nc-sa/3.0/fr/).
:::

## Introduction

L'instruction consiste à enseigner des connaissances, mais aussi à donner aux apprenants une certaine autonomie dans leur acquisition.

Le problème, comme notamment les psychologues développementaux l'ont expliqué (Piaget, Bachelard, et plus récemment Giordan ou Shtulman), est que les connaissances nouvelles n'effacent pas les anciennes, potentiellement erronées : les différentes connaissances peuvent coexister et entrer en compétition.

## Qu'est-ce que la pensée critique ?

Très peu d'éducateurs seraient opposés à l'idée d'apprendre à leurs élèves à penser de manière critique, c'est-à-dire logiquement, en pesant les différents aspects d'un problème, en inférant des conclusions valides à partir des faits à leur disposition. En bref, penser comme un scientifique. Penser de manière critique est utile dans les nombreuses situations de la vie professionnelle et de tous les jours, où l'on a sans cesse à peser le pour et le contre, décider rapidement, etc.

Plusieurs problèmes surviennent dès lors qu'on essaie d'aller plus loin. Le premier est déjà de définir de plus près ce que serait cette "pensée critique", tant de nombreux termes y sont rattachés : jugement critique, esprit critique... (voir  Ensuite, de savoir si elle peut s'enseigner indépendamment du domaine considéré. Enfin, de savoir si *vraiment* les scientifiques pensent bien de manière raisonnée.

La pensée critique est une pratique évaluative fondée sur une démarche réflexive, autocritique et autocorrectrice impliquant le recours à différentes ressources (connaissances, habiletés de pensée, attitudes, personnes, informations, matériel) dans le but de déterminer ce qu’il y a raisonnablement lieu de croire (au sens épistémologique) ou de faire (aux sens méthodologique et éthique) en considérant attentivement les critères de choix et les diversités contextuelles. (Giroux *et al*., 2011).

## Sites

- [Cortecs](http://cortecs.org)
- [L'atelier médiation critique](www.atelier-mediation-critique.com)
- [Penser critique](https://www.penser-critique.be)
- [Conférences de D. Pestre sur l'ignorance](https://ignostudies.hypotheses.org/category/entretien-audio)
