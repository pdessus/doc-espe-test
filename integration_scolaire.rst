.. _integration_scolaire:

==============================================
Les dispositifs d'inclusion scolaire en France
==============================================

.. index::
	single: auteurs; Dessus, Philippe
	single: auteurs; Javellas, Renaud
	single: auteurs; Besse, Émilie

.. admonition:: Informations

	* **Auteurs** : Renaud Javellas, Inspé, Univ. Grenoble Alpes & `Philippe Dessus <http://pdessus.fr/>`_, Inspé & LaRAC, Univ. Grenoble Alpes. Le quizz a été réalisé par Émilie Besse, projet `ReflexPro <http://www.idefi-reflexpro.fr>`_.
	
	* **Date de création** : Novembre 2002, mis à jour en août 2017.
	
	* **Date de publication** : |today|.
	
	* **Statut du document** : Terminé
	
	* **Résumé** :  Ce document présente des informations sur les différents dispositifs d'inclusion scolaire et d'aide aux élèves en situation de grande difficulté d’apprentissage et de handicap.

	* **Voir aussi** : Les documents : :ref:`peda_projet`, :ref:`difficultes_ecrit`, et :ref:`difficultes-TIC`.
	
	 * **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.
	
	* **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.


Connaître les dispositifs ASH 
=============================

Les différentes structures et dispositifs d'inclusion scolaire et de scolarisation des élèves handicapés (ASH) en France sont très nombreux, complexes, et changent régulièrement, ce qui rend difficile la réalisation d'un panorama complet. De plus, ils dépendent de trois ministères différents (éducation nationale, santé et affaires sociales, justice). Plus largement, ils font partie du champ de l'éducation prioritaire, dans lequel une sous-partie s'occupe des élèves à besoins éducatifs particuliers (comme les élèves nouvellement arrivés, les enfants du voyage, les élèves en difficulté sociale ou encore les enfants intellectuellement précoces/à haut potentiel, les élèves malades, les élèves déscolarisés et bien sûr les élèves  en situation de handicap, avec ou sans handicap reconnu,  et les élèves ayant de grandes difficultés durables d'apprentissage). Au sein de cette sous-partie figure l'ASH.

La figure ci-dessous indique pour chaque catégorie d’âge de scolarité (école 1\ :sup:`er` degré, collège et lycée 2\ :sup:`nd`, degré et pré-adultes ou adultes) les différentes structures de prise en charge potentielle.

**Figure 1** – Les différents dispositifs d'aide et d'intégration scolaire.

.. image:: /images/struct-ash.jpg
	:scale: 50 %
	:align: center


Il est important de noter que tous les élèves reconnus handicapés ne sont pas scolarisés dans des établissements spécialisés. Sur l’ensemble de l’effectif des enfants reconnus handicapés, env. 25 % des enfants sont pris en charge en établissements spécialisés médico-sociaux, hospitaliers ou judiciaires, env. 25 % sont scolarisés en dispositifs spécialisés collectifs (ULIS, SEGPA, etc.) de l’Éducation nationale et env. 50 %  en classe ordinaire de l’Éducation nationale, à titre individuel, avec ou sans AVS, à temps complet ou partiel. A la rentrée 2016-2017,  env. 280 000 élèves reconnus comme handicapés  étaient scolarisés dans l'éducation nationale.

Les enfants des secteurs spécialisés médico-sociaux, hospitaliers ou judiciaires  sont pris en charge au sein de leurs établissements par l’équipe enseignante de l’Unité d’Enseignement (UE). Ils peuvent aussi bénéficier d'une scolarisation partielle en milieu ordinaire, qu’on appelle alors scolarité partagée, ou d’une scolarisation dans une classe dite externalisée (dans les locaux de l’éducation nationale pour être en contact avec leurs pairs). 

Quoi qu'il en soit, le parcours des élèves dans ces différentes structures n'est pas figé, et ces dernières sont perméables. Un élève peut très bien en changer, même en cours d'année, selon l'évolution de ses difficultés. On  sort de la logique de filière qui prédominait jusqu’à présent au profit d’une logique de parcours, souple, dynamique, déterminé en fonction des besoins évolutifs de l’enfant.

Plusieurs projets individuels d’élève sont proposés par l’Éducation nationale pour faciliter la scolarisation. Les élèves qui risquent de ne pas maîtriser certaines connaissances et compétences attendues à la fin d’un cycle d’enseignement peuvent bénéficier d’un Programme Personnalisé de Réussite Éducative (PPRE – Circ. du 25/08/2006). Les élèves identifiés avec un trouble d’apprentissage grave et durable, sans reconnaissance de handicap, peuvent bénéficier d’un Plan d’Accompagnement Personnalisé (PAP – Circ. du 22/01/2015). Les élèves reconnus handicapés bénéficient d’un Projet Personnalisé de Scolarisation (PPS - Art. D. 351-5 du code de l'éducation). Enfin, les élèves atteints de maladie chronique, d’allergie et d’intolérance alimentaire peuvent bénéficier d’un Projet d’Accueil Individualisé (PAI – Circ. du 08/09/2003).

Les différents programmes, projets et plans
===========================================

La principale source de ce qui suit est le document `Répondre aux besoins particuliers des élèves, du MENSR <http://cache.media.eduscol.education.fr/file/Handicap/41/0/ecole_inclusive_dossier_extrait_QPPQ_376117_378410.pdf>`_

* **Programme personnalisé de réussite éducative (PPRE)**. Décret `2005-1014 du 24.8.05 <http://www.education.gouv.fr/bo/2005/31/MENE0501635D.htm>`_

  * *Personnes impliquées* : Demande faite par le directeur d’école ou le chef d’établissement à l’initiative des équipes pédagogiques , discussion avec la famille, présentation à l’élève et mise en œuvre par les enseignants dans le cadre ordinaire de la classe avec concours éventuel du RASED ou des équipes d’accompagnement des élèves allophones arrivants.
  
  * *Objectifs *: Organiser l’accompagnement pédagogique différencié de l’élève tout au long du cycle afin de lui permettre de surmonter les difficultés rencontrées et de progresser dans ses apprentissages, renforcer la cohérence entre les actions entreprises pour aider l’élève.  
  
  * *Public concerné* : Tout élève (dont éventuellement des élèves allophones arrivants ou des élèves à haut potentiel en échec scolaire) ayant de grandes difficultés scolaires, estimées non ponctuelles, qui risquent de ne pas maîtriser certaines connaissances et compétences attendues à la fin d’un cycle d’enseignement. 
  
  * *Actions* : “Le programme personnalisé de réussite éducative consiste en un plan coordonné d’actions, conçues pour répondre aux difficultés d’un élève, formalisé dans un document qui en précise les objectifs, les modalités, les échéances et les modes d’évaluation. [...]” . Le plan coordonne les actions conçues pour répondre aux difficultés que rencontre l’élève, allant de l’accompagnement pédagogique différencié conduit en classe par son ou ses enseignants, aux aides spécialisées ou complémentaires (travail en petit groupe, renforcements, aides aux devoirs etc...) . Dans certains départements français, des « PPRE passerelle » sont mis en œuvre pour faciliter la transition entre primaire et secondaire.  Voir `guide <http://eduscol.education.fr/cid50680/les-programmes-personnalises-de-reussite-educative-ppre.html>`_ 

* **Projet d'accueil individualisé (PAI)**. Art. D. 351-9 du code de l’éducation, `BO n° 34 18.9.03 <http://www.education.gouv.fr/bo/2003/34/MENE0300417C.htm>`_

  * *Personnes impliquées* : Demande faite par la famille (ou par le chef d’établissement en accord avec la famille, rédaction par le médecin scolaire (ou de PMI), transmission à tout personnel en charge de l’élève.
  
  * *Objectifs* : Permettre une scolarité ordinaire en facilitant l’accueil de l’enfant ou de l’adolescent malade à l’école, préciser le rôle de chacun, dans le cadre de ses compétences, pour les aménagements spécifiques liés à l’état de santé.

  * *Public concerné* : Pour les élèves atteints de maladie chronique, d’allergie et d’intolérance alimentaire. 
  
  * *Actions* : Le PAI définit l’organisation entre les différents partenaires (enseignants, médecins, personnels de santé, équipe éducative, restauration, etc.). Il précise les traitements médicaux et/ou les régimes spécifiques durant les temps scolaires et périscolaires, des aménagements spécifiques dans la classe ou la vie scolaire et le protocole d’urgence à suivre en cas de problème. Si la situation nécessite un aménagement particulier, voir PPS ci-dessous. 

* **Plan d’accompagnement personnalisé (PAP)**. Art. D. 311-13 du code de l’éducation,  `BOEN n°5 du 29 janvier 2015 <http://www.education.gouv.fr/pid25535/bulletin_officiel.html?cid_bo=85550>`_.

	* *Personnes impliquées* : Demande faite par l’équipe éducative ou la famille avec l’accord de l’autre partie, évaluation par le médecin scolaire et les partenaires de santé, élaboration par l’équipe éducative et la famille, mise en œuvre par les enseignants au sein de la classe ordinaire.

	* *Objectifs* : Définir les aménagements pédagogiques dont bénéficie l’élève, sur la base d’un modèle national, pour répondre aux difficultés scolaires et éviter la rupture des aménagements entre chaque classe.

	* *Public concerné* : les élèves présentant des difficultés scolaires durables en raison d’un trouble des apprentissages. 

	* *Actions* : La visée est exclusivement pédagogique. Le PAP est formalisé par un document écrit normé indiquant les difficultés de l’élève et ses points d’appui, les conséquences du trouble sur les apprentissages (avec avis du médecin scolaire) et les préconisations d’aménagements pédagogiques à sélectionner parmi un ensemble d’items, en fonction des besoins. C’est aussi un outil de suivi des aménagements, organisé en fonction des cycles de la maternelle jusqu’au lycée.

* **Projet personnalisé de scolarisation (PPS)** Art. D. 351-5 du code de l’éducation, `Décret n° 2005-1752 du 30-12-2005 <http://www.education.gouv.fr/bo/2006/10/MENE0502666D.htm>`_.

  * *Personnes impliquées* : Demande faite par la famille, évaluation par l’Équipe Pluridisciplinaire d’Évaluation EPE de la Maison Départementale des Personnes Handicapées MDPH, décision de reconnaissance par la Commission des Droits et de l’Autonomie des Personnes Handicapées CDAPH, suivi par l’enseignant-référent de scolarité et mise en œuvre par l’équipe éducative ainsi que tous les partenaires impliqués (soins, sociaux, de transport, etc...).
  
  * *Objectifs* : organiser le déroulement de la scolarité de l’élève reconnu handicapé et assurer la cohérence, la qualité des accompagnements et des aides nécessaires à partir d’une évaluation globale de la situation et des besoins de l’élève (Art. `L-112-2 du Code de l'éducation <https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071191&idArticle=LEGIARTI000006524375&dateTexte=&categorieLien=cid>`_).
  
  * *Public concerné* :  les élèves présentant un handicap (trouble de la santé invalidant) reconnu au sens de l’Art. 2 de la loi de 2005 : « toute limitation d’activité ou restriction de participation à la vie en société subie dans son environnement par une personne en raison d’une altération substantielle, durable ou définitive d’une ou plusieurs fonctions physiques, sensorielles, mentales, cognitives, psychiques, d’un polyhandicap ou d’un trouble de santé invalidant ».

  * *Actions* : Le PPS est à la fois la feuille de route du parcours de scolarisation de l’enfant en situation de handicap (il « détermine et coordonne » les modalités de déroulement de la scolarité et les actions pédagogiques, éducatives, sociales, médicales et paramédicales répondant aux besoins de l’élève.) et un outil de suivi qui court sur la totalité du parcours de scolarisation. Il fait l’objet d’un suivi annuel (ou plus selon les besoins) par l’équipe de suivi de la scolarisation (ESS). Il est révisable au moins à chaque changement de cycle. “Un projet personnalisé de scolarisation définit les modalités de déroulement de la scolarité et les actions pédagogiques, psychologiques, éducatives, sociales, médicales et paramédicales répondant aux besoins particuliers des élèves présentant un handicap.”
    

Répertoire des principaux sigles
================================

* AEMO : Action Éducative en Milieu Ouvert 
* AESH : Accompagnant des Élèves en Situation de Handicap
* AP : Aide Personnalisée
* ASE : Aide sociale à l’Enfance
* AVS : Auxiliaire de Vie Scolaire
* CAMSP : Centre d’Action Médico-Sociale Précoce
* CATTP : Centre d’Accueil Thérapeutique à Temps Partiel
* CDAPH : Commission des Droits et de l’Autonomie des Personnes Handicapées
* CDOEASD : Commission Départementale d’Orientation vers les Enseignements Adaptés du Second Degré
* CEF : Centre Éducatif Fermé
* CLAD : Classe d’Adaptation
* CMP :  Centre Médico-Psychologique
* CMPP : Centre Médico-Psycho-Pédagogique
* EREA : Établissement régional d'Enseignement Adapté 
* EPE : Équipe Pluridisciplinaire d’Évaluation
* ESS : Équipe de Suivi de la Scolarisation
* IA-DASEN : Inspecteur(trice) d’académie – Directeur(trice) Académique des Services de l’Éducation Nationale
* IEM : Institut d’Éducation Motrice
* IEN : Inspecteur(trice) de l’Éducation Nationale
* IGEN : Inspection Générale de l’Éducation Nationale
* IME :  Institut Médico-Educatif
* IMP : Institut Médico-Pédagogique
* IMPRO : Institut Médico-Professionnel
* INJS : Institut National des Jeunes Sourds
* ITEP : Institut Thérapeutique Éducatif et Pédagogique 
* LEA : Lycée d'Enseignement Adapté
* MDPH : Maison Départementale des Personnes Handicapées
* MECS : Maison d’Enfants à Caractère Social
* MECSan : Maison d’Enfants à Caractère Sanitaire
* MENSR : Ministère de l’Éducation Nationale, de l’enseignement Supérieur et de la Recherche
* PAP : Plan d’Accompagnement Personnalisé
* PIA : Projet Individualisé d’Accompagnement
* PIAS : Projet Individuel d’Adaptation Scolaire
* PPA : Plan Personnalisé d’Accompagnement
* PPC : Plan Personnalisé de Compensation
* PPI : Projet Pédagogique Individualisé
* PPRE : Projet Personnalisé de Réussite Éducative
* PPS : Projet Personnalisé de Scolarisation
* RASED : Réseau d’Aides Spécialisées aux Élèves en Difficulté
* SEGPA : Section d’Enseignement Général et Professionnel Adapté
* ULIS : Unité Localisée pour l’Inclusion Scolaire
* UE : Unité d’Enseignement
* UEM : Unité d’Enseignement en Maternelle

Quizz
=====

.. eqt:: Integration_scolaire-1

	**Question 1. 	Que signifie PAI ?**

	A) :eqt:`C` `Projet d'Accueil Individualisé`
	B) :eqt:`I` `Parcours en Autonomie Individuelle`
	C) :eqt:`I` `Projet d'Autonime Innovant`
	D) :eqt:`I` `Parcours d'Accueil Innovant`

.. eqt:: Integration_scolaire-2

	**Question 2. 	Que signifie PPS ?**

	A) :eqt:`I` `Parcours Personnel au Secondaire`
	B) :eqt:`C` `Projet Personnalisé de Scolarisation`
	C) :eqt:`I` `Parcours pour les Professionnels de la Santé`
	D) :eqt:`I` `Projet en Pédagogie et Santé`

.. eqt:: Integration_scolaire-3

	**Question 3. 	De quel ministère dépendent les établissements spécialisés non médicaux ?**

	A) :eqt:`I` `Du ministère de l'éducation`
	B) :eqt:`I` `Du ministère de la santé`
	C) :eqt:`C` `Du ministère de la justice`
	D) :eqt:`I` `Du ministère de l'enseignement supérieur`


Références
==========

* `Répondre aux besoins particuliers des élèves, du MENSR <http://cache.media.eduscol.education.fr/file/Handicap/41/0/ecole_inclusive_dossier_extrait_QPPQ_376117_378410.pdf>`_

* `Guide des PPRE <http://eduscol.education.fr/cid50680/les-programmes-personnalises-de-reussite-educative-ppre.html>`_
