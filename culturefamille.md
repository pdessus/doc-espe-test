(culture-familiale)=

# Culture familiale et culture scolaire

```{index} single: auteurs; Dessus, Philippe single: auteurs; Besse, Émilie
```

:::{admonition} Information
- **Auteurs** : [Philippe Dessus](http://pdessus.fr/), Inspé & LaRAC, Univ. Grenoble Alpes.  Le quizz a été réalisé par Émilie Besse, projet [ReflexPro](http://www.idefi-reflexpro.fr).
- **Date de création** : Octobre 2004.
- **Date de publication** : {sub-ref}`today`.
- **Statut du document** : Terminé.
- **Résumé** : Comment et pourquoi le rôle de l'école est avant tout culturel ? Quelles différences y a-t-il entre la culture familiale et la culture scolaire ?
- **Voir aussi** : {ref}`metier-eleve`.
- **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.
- **Licence** : Document placé sous licence *Creative Commons* : [BY-NC-SA](http://creativecommons.org/licenses/by-nc-sa/3.0/fr/).
:::

## Ce que l'on sait

### La culture donne forme à l'esprit

Ce texte n'est pas l'occasion de faire un énième exposé sur les différences entre nature et culture : on en trouve de très bons dans tous les ouvrages de philosophie de l'éducation {cite}`reboul81,reboul83`. L'idée, ici, est de partir de définitions de la culture utilisée par des psychologues pour en venir à  des préoccupations d'éducation. Tout d'abord, il est assez évident que l'école (et l'éducation) est un moyen pour l'humain   d'enseigner sa culture (i.e., ce qu'il n'acquiert pas de manière innée). Mais qu'est-ce que la culture ?

La définition de Bruner {cite}`bruner91` est intéressante : "la culture donne forme à l'esprit" en donnant une signification à son action, et, plus précisément : "\[c'est\] une sorte de boîte à outils, où l'homme trouve les prothèses dont il a besoin pour dépasser et parfois redéfinir les "limites naturelles" de   son fonctionnement. Tous les outils humains, qu'ils soient matériels   ou intellectuels, répondent à ce besoin." (*id*., p. 36) La culture serait donc, entre autres, un ensemble d'outils cognitifs permettant   de dépasser certaines "limites naturelles", comme  : se souvenir de plus de sept éléments, arriver à compter jusqu'à plus de dix, mémoriser des histoires qui contiennent des aspects sociaux ou moraux (contes), etc (pour plus de renseignements sur les outils cognitifs, voir {cite}`dessus03`).

### Du sens commun

Une partie de la culture est nommée "le sens commun", ou encore "psychologie populaire" ou naïve. Il s'agit "d'un ensemble de   descriptions, plus ou moins reliées les unes aux autres, plus ou moins normatives, qui nous disent, entre autres choses, comment "fonctionnent" les hommes, à quoi ressemblent notre esprit et celui des autres, comment on doit agir dans des situations précises, quels sont les différents modes de vie possibles et comment il faut s'y conformer" ({cite}`bruner91` p. 49) Pour cet auteur, ce sens commun a une forme : il est socialement véhiculé par des narrations (contes, mythes). Certains auteurs ont avancé qu'un des rôles de l'école était justement d'aller contre le sens commun, et  de privilégier, non pas la pensée narrative, mais la pensée logico-scientifique ou  paradigmatique {cite}`bruner00`. Cette pensée a pour but de décrire et d'expliquer le monde qui nous entoure, en s'appuyant sur des hypothèses et des preuves, et a été forgée par des disciplines dites scientifiques (logique, mathématiques, sciences). Le Tableau I ci-dessous, et surtout les points 6 et 7, permettent de bien comprendre la différence entre éducation familiale et scolaire.

### Transmission culturelle en famille et à l'école

L'école serait donc, comme la famille, une "niche de développement" {cite}`bril88`, organisée de manière à favoriser certains apprentissages. Ces mêmes auteurs distinguent l'éducation informelle, familiale, de l'éducation formelle, véhiculée par l'école. Selon le Tableau I suivant, qui compare ces deux types d'éducation, il est donc normal que ces types d'éducation diffèrent. Pour autant, certains   milieux familiaux préparent mieux à l'éducation formelle que d'autres (voir plus bas).

**Tableau I. Différences entre éducation formelle et informelle** ({cite}`bril88` p. 156)

```{eval-rst}
+--------------------------------------+--------------------------------------+
| Education informelle                 | Education formelle                   |
+--------------------------------------+--------------------------------------+
| #. Activités intégrées à la vie      | #. Activités séparées du contexte de |
|    courante                          |    la vie courante                   |
| #. L'enseigné est responsable de ses | #. L'enseignant est responsable de   |
|    acquisitions (théoriques et       |    la transmission des acquisitions  |
|    pratiques)                        |    (théoriques et pratiques)         |
| #. Apprentissage personnalisé : les  | #. Apprentissage impersonnel : les   |
|    personnes de l'entourage sont les |    maître ne sont normalement pas    |
|    enseignants                       |    les parents                       |
| #. Peu ou pas de programmes          | #. Pédagogie et programmes           |
|    explicites                        |    explicites                        |
| #. Mise en valeur du maintien de la  | #. Mise en valeur du changement et   |
|    continuité et de la tradition     |    de la disponibilité               |
| #. Apprentissage par observation et  | #. Apprentissage par échanges        |
|    imitation ; pas de questionnement |    verbaux et questionnements        |
| #. Apprentissage par démonstration   | #. Apprentissage par présentation    |
| #. Motivation trouvée dans la        |    verbale des principes généraux    |
|    contribution sociale des          | #. Motivations sociales moins fortes |
|    débutants, leur participation au  |                                      |
|    monde adulte ; grande continuité  |                                      |
|    avec le jeu                       |                                      |
+--------------------------------------+--------------------------------------+
```

## Ce que l'on peut faire

Par définition, les aspects culturels étant implicites, il est difficile pour l'enseignant de comprendre et éventuellement de prendre en compte les différences interculturelles des élèves de sa classe. Là aussi, nous ne pourrons traiter ici qu'un aspect particulier, celui du rapport au savoir des élèves. Charlot {cite}`charlot99` s'intéresse à l'échec scolaire, et le considère comme quelque chose qui peut arriver à un élève, un moment de son histoire lié à des événements, ruptures, dérives, etc., sans pour autant que ce soit une caractéristique héritée de son groupe social. Sans entrer trop dans les détails, Charlot et son équipe ont montré des différences individuelles importantes dans les représentations des buts de l'école.

Les élèves de milieu populaire pensent que l'important, à l'école, n'est pas d'apprendre mais d'aller "le plus loin possible" : ils visent la moyenne et adoptent des stratégies, des rythmes de travail pour travailler *a minima*. Dès le CP, ces élèves caractérisent le "bon élève", non pas comme quelqu'un qui apprend, mais quelqu'un qui se   comporte correctement (arrive à l'heure, est poli, lève le doigt,   écouter la maîtresse). Ils attribuent leurs échecs de manière externe : "on ne m'a pas appris".

Les élèves de milieu plus favorisé, s'ils adoptent aussi parfois de telles stratégies, trouvent dans le savoir un intérêt pour lui-même : ils écoutent la leçon, ont des attributions internes "j'ai appris". Pour Charlot et ses collègues, il pourrait être possible que l'école influe positivement sur le rapport au savoir des élèves en échec scolaire, bien qu'elle ne le fasse pas si souvent. En effet, les messages des enseignants à leur encontre entraîne des effets parfois paradoxaux :

- "Si tu ne travailles pas, tu ne passeras pas" fait que l'élève va travailler davantage pour passer, et comprend qu'on va à l'école seulement pour passer dans la classe supérieure.
- "Apprends cela, c'est utile pour plus tard" fait que l'élève est conforté dans une vision utilitariste de la connaissance.
- "Apprends parce que ça me fera plaisir, à moi et à tes parents" fait   apprendre les enfants pour  la relation et non pour le savoir en lui-même.

## Quizz

```{eval-rst}
.. eqt:: Culturefamille-1

        **Question 1. Laquelle des caractéristiques suivantes est l'une de l'éducation formelle ?**

        A) :eqt:`I` `Mise en valeur du maintien de la continuité et de la tradition`
        B) :eqt:`C` `Apprentissage par échanges verbaux et questionnements`
        C) :eqt:`I` `Activités intégrées à la vie courante`
        D) :eqt:`I` `Apprentissage par démonstration`
```

```{eval-rst}
.. eqt:: Culturefamille-2

        **Question 2. Laquelle des caractéristiques suivantes est l'une de l'éducation informelle ?**

        A) :eqt:`I` `Motivations sociales moins fortes`
        B) :eqt:`I` `Activités séparées du contexte de la vie courante`
        C) :eqt:`I` `Mise en valeur du changement et de la disponibilité`
        D) :eqt:`C` `Apprentissage par observation et imitation`
```

```{eval-rst}
.. eqt:: Culturefamille-3

        **Question 3. Laquelle des caractéristiques suivantes est l'une de l'éducation informelle ?**

        A) :eqt:`I` `Apprentissage par échanges verbaux et questionnements`
        B) :eqt:`I` `Mise en valeur du changement et de la disponibilité`
        C) :eqt:`C` `Activités intégrées à la vie courante`
        D) :eqt:`I` `Activités séparées du contexte de la vie courante`

```

## Analyse des pratiques

1. Faire verbaliser des élèves en échec à propos de leur rapport au savoir : "qu'est-ce qui est important pour toi, à l'école ?, à quoi ça sert d'apprendre, de comprendre, de travailler ?" "comment ça s'est passé, l'école, depuis que tu y vas ?" (se reporter au guide d'entretien dans Charlot {cite}`charlot99b`, pp. 378-379).

## Références

```{bibliography}
:filter: docname in docnames
```
