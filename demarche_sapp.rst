.. _demarche_sapp:

***************************************************************
La démarche des Séminaires d'analyse des pratiques pédagogiques
***************************************************************

.. index::
    single: auteurs; Dessus, Philippe

.. admonition:: Informations

   * **Auteur** : `Philippe Dessus <http://pdessus.fr/>`_, Inspé & LaRAC, Univ. Grenoble Alpes.

   * **Date de création** : 1999.

   * **Date de publication** : |today|.

   * **Statut du document** : Terminé. 

   * **Résumé** : Ce document détaille décrit la démarche pédagogique utilisée dans les Séminaires d'analyse des pratiques pédagogiques mise en place à l'IUFM de Grenoble à partir de 1999, ainsi que les travaux liés.

   * **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.
   
   * **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.

Présentation
============

Ce séminaire a été mis en place en octobre 1999 dans les IUFM de Grenoble et Chambéry, dans les formations de Professeurs stagiaires de collèges et lycées 2\ :sup:`e` année (PLC2). Originellement nommé SAPEA (Séminaire d'analyse des pratiques d'enseignement/apprentissage, il a, depuis la rentrée 2007, pris le nom de SAPP). S'ils ont disparu du plan de formation de l'IUFM depuis la rentrée 2010, ce site continue d'exister et d'être alimenté pour d'autres cours que les SAPP.

C'est un lieu d'échange et de collaboration entre stagiaires de disciplines différentes en vue de faciliter la résolution de problèmes d'enseignement et d'apprentissage : l'interdisciplinarité permettant à chacun de se confronter aux objectifs et méthodologies des autres disciplines, aux différences et aux convergences dans la mise en œuvre en classe.

La formule du séminaire est basée sur la confrontation entre professeurs-stagiaires à partir de leurs pratiques de classe. 

Le SAPP regroupe des stagiaires de disciplines différentes. L'analyse stimule les échanges interdisciplinaires des professeurs-stagiaires sur leurs pratiques professionnelles.

Le SAPP est un séminaire qui fonctionne selon une *exploration outillée des pratiques*. Il a pour but une aide à la compréhension et à la décision des activités d'enseignement/apprentissage.

Le travail des professeurs-stagiaires pourra être étayé par des documents-ressources proposés par les formateurs.

Le séminaire se situe en dehors de toute évaluation institutionnelle.

Les propos tenus au sein du groupe reposent sur la confidentialité et le respect de la parole de chacun des membres.

Travaux
=======

Les sessions du séminaire SAPEA/SAPP ont fait l'objet des communications suivantes (ordre chronologique inverse) :

* Dessus, P. (2008). Qu'a-t-on besoin de savoir pour animer des séances d'analyse de pratiques ? *Journée de formation de formateurs à l'analyse de pratiques*. Voiron : Inspection académique de l'Isère, 7 février. [`Dias en PDF <../fofaem08.pdf>`_]

* Campanale, F. (2007).  Analyse réflexive et autoévaluation dans la formation des enseignants : quelles relations ? In A. Jorro (Ed.), *Evaluation et développement professionnel* (pp. 55-76). Paris : L'Harmattan.

* Dessus, P. (2003). *Quelques caractéristiques des Ateliers d'analyse des pratiques*. Grenoble : IUFM de Grenoble, journées de formation de formateurs AEM, 3-4 février. [`Dias en PDF <http://webcom.upmf-grenoble.fr/sciedu/pdessus/sapea/aem03.PDF>`_]

* Campanale, F. (2003). *Questions de procédures*. Grenoble : IUFM de Grenoble, journées de formation de formateurs AEM, 3-4 février. [`Dias en PDF <http://webcom.upmf-grenoble.fr/sciedu/pdessus/sapea/procAEM.PDF>`_] [`Texte en PDF <http://webcom.upmf-grenoble.fr/sciedu/pdessus/sapea/procAEMtxt.PDF>`_] 

* Campanale, F., & Dessus, P. (2002). Séminaire d'analyse collaborative des pratiques : décrire des situations pour faire évoluer des pratiques. *4e Colloque Inter-IUFM "Formation des enseignants et professionnalité"*. Bordeaux : IUFM d'Aquitaine, 15-17 avril. [`Texte en PDF <http://webcom.upmf-grenoble.fr/sciedu/pdessus/bordeaux02.PDF>`_] [`Site <http://www.aquitaine.iufm.fr/colloque2002/>`_]

* Barthélémy, A., Campanale, F., Dessus, P. (2002). *Présentation générale des SAPEA*. Grenoble : IUFM de Grenoble, session d'information pour les nouveaux formateurs SAPEA, 9 octobre. [`Dias en PDF <http://webcom.upmf-grenoble.fr/sciedu/pdessus/sapea/nform.PDF>`_] 

* Dessus, P., & Campanale, F. (2000, 2-4 octobre). Séminaires d'analyse des pratiques à l'IUFM : théorie de la pratique et pratique de la théorie. *Colloque « Les pratiques dans l'enseignement supérieur »*. Toulouse : AECSE CREFI. [`Texte en PDF <http://webcom.upmf-grenoble.fr/sciedu/pdessus/aecse00.pdf>`_]

