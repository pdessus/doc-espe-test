.. _ens_preuves:

**********************************
L'éducation fondée par les preuves
**********************************

.. Lire
.. https://twitter.com/CarlessDavid/status/1316696605794287616?s=20
.. {Howard-Jones, 2020 #22575;IJzerman, 2020 #22572}
.. {Rømer, 2018 #21902}
.. {Coe, 2020 #22480}
.. https://olc-wordpress-assets.s3.amazonaws.com/uploads/2019/10/Neuromyths-Betts-et-al.-September-2019.pdf
.. https://circlcenter.org/evidence-centered-design/
.. https://docs.google.com/spreadsheets/d/1qEqE45gyxaPt1mqTE1V3JLkLq290iCvFVJdMWEgHR9E/edit?usp=sharing
.. https://cdn2.hubspot.net/hubfs/2366135/Manifesto.pdf
.. https://hal.archives-ouvertes.fr/tel-02461323/document
.. https://eppi.ioe.ac.uk/cms/Default.aspx?tabid=62
.. https://blogs.mediapart.fr/gerardsensevyinspe-bretagnefr/blog/220620/quelle-conception-de-la-recherche-en-education


.. Sources de preuves {White, 2020 #22607, 30}
	- données
	- études
	- revues
	- bases de données
	- Cartes de preuves
	- Portails de preuves
	- Plateformes de preuves
	- Guidelines


.. index::
	single: auteurs; Dessus, Philippe
	single: auteurs; Jeune, Nathanaël
	single: auteurs; Atal, Ignacio


.. admonition:: Informations

	* **Auteurs** : Nathanaël Jeune, CRI, Univ. de Paris, `Philippe Dessus <http://pdessus.fr/>`_, LaRAC & Inspé, Univ. Grenoble Alpes, & Ignacio Atal, CRI, Univ. de Paris.

	* **Date de création** : Octobre 2020.

	* **Date de publication** : |today|.

	* **Statut du document** : En cours.

	* **Résumé** :  Ce Document donne quelques informations sur le courant de recherche de l'“éducation fondée sur les preuves”.

	* **Voir aussi** : Le Document :ref:`res_ens_preuves` qui liste des sites du domaine et le Document :ref:`appr_visible` qui détaille quelques résultats issus de l'éducation fondée/informée par les preuves.

	* **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.

	* **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.


Introduction
============

Le courant de l'”éducation fondée sur les preuves” (EFP, ou *evidence-based education*, ou, plus largement, *evidence-based practice*), part du principe que l'on peut tirer parti du nombre important de résultats de recherche scientifiquement prouvés dans un domaine (ici, l'éducation) pour que les acteurs concernés (ils peuvent être des administrateurs, des personnes politiques, et bien sûr des enseignants) prennent les décisions les mieux informées possible, et donc les plus efficaces.

Ce courant a débuté dans les champs de la médecine et du commerce :cite:`eraut04` dans les années 1990, et s'est ensuite transféré à d'autres domaines, dont celui de l'éducation.

Réfléchir à ce courant amène de nombreuses questions. La première est “Sur quoi donc d'autre voudriez-vous fonder l'éducation ? des croyances ? vous n'y pensez pas ?”. La seconde est “Bon, d'accord, sur des preuves, mais comment distinguer une bonne preuve d'une mauvaise ?”. La troisième est "Efficace, mais pour quoi faire ? une pratique donnée peut-elle être bonne *en soi* ?". La quatrième est : “À quelles sources puis-je me fier ? Vais-je devoir lire de gros articles en anglais et avoir de solides connaissances en statistiques pour améliorer ma pratique ?”. La cinquième est “Bon, d'accord, celle-ci est bonne, et je sais pour quoi je vais l'utiliser, mais comment vais-je faire dans ma classe ?”. La sixième et dernière est : “Au fait, quelles preuves avons-nous que l'éducation fondée sur des preuves marche ?”.

Les questions
=============

Ce document donne quelques pistes à propos de ces différentes questions. Tout d'abord, commençons par quelques précisions, question par question :

1. **“Sur quoi donc d'autre voudriez-vous fonder l'éducation ? des croyances ? vous n'y pensez pas ?”** Effectivement, l'EFP peut paraître en opposition avec les croyances, les perceptions ou les intuitions des enseignants eux-mêmes... et laisse penser qu'avant l'émergence de ce courant, les enseignants fondaient leurs décisions sur l'intuition, (voire des pratiques divinatoires...). Toutefois, est-il raisonnable de penser que la *seule* source de preuves utilisable pour fonder la pratique des enseignants est tirée de travaux de recherche ? Ne peut-on pas aussi compter comme preuve sa propre expérience, sa réflexivité, les réactions des élèves ? (Mockler & Stacey, 2019). Levin et O'Donnell :cite:`levin99` montrent aussi que c'est le processus lui-même de la recherche en éducation qui pose un problème, en suivant le principe ESP (Examiner, sélectionner, prescrire) : *examiner* des processus, en *sélectionner* certains fondés sur ces observations, et *prescrire* la méthode,  plutôt que de suivre un processus scientifique de test d'hypothèse fondé sur des théories.

2. **"Bon, d'accord, sur des preuves, mais comment distinguer une bonne preuve d'une mauvaise ?"** Les preuves, ici, sont des preuves scientifiques, issues de la littérature de recherche. Quand on parcourt des articles de recherche en éducation, on peut avoir d'assez bonnes idées des pratiques pouvant être efficaces, ou non, dans les contextes dans lesquels ces recherches ont été faites, bien sûr. Toutefois, il faut être prudent car certaines preuves peuvent avoir une meilleure validité externe que d'autres (*i.e.*, être plus généralisables à des contextes différents de ceux où les recherches ont été faites). Nous verrons plus loin cette hiérarchie des preuves.

3. **"Efficace, mais pour quoi faire ? une pratique donnée peut-elle être bonne *en soi* ?"** Effectivement, il est sans doute nécessaire de se fier à des données probantes plutôt que de simples intuitions, mais pour faire quoi ? :cite:`biesta10`), est-ce que n'importe quelle pratique pédagogique, pour peu qu'elle utilise une partie qui a été validée scientifiquement, est nécessairement acceptable ? D'une part, la variabilité des effets possibles de l'intervention peut amener des différences de résultats importants (ce que Biesta :cite:`biesta20` nomme "le déficit d'efficacité") ; d'autre part, il est nécessaire de tenir compte des valeurs éducatives que l'on poursuit à travers ces pratiques. En effet, un résultat de recherche donné peut entraîner de nombreuses décisions différentes, et ce seront les valeurs qui guideront les décideurs. De Bruyckere et ses collègues :cite:`debruyckere20` donnent un exemple intéressant à ce sujet : si des recherches fondées sur les preuves montrent que, dans un intervalle de nombre d'élèves donné, la taille des classes n'influence pas la qualité de l'apprentissage, alors on peut en venir à décider de licencier des enseignants.

4. **“À quelles sources puis-je me fier ?** Cette question est reliée à l'accessibilité de la littérature sur l'EFP, le plus souvent lisible dans des revues en anglais en accès payant, ce qui limite quelque peu leur disponibilité. Et s'il existe de nombreux sites qui présentent des informations librement accessibles, il n'en reste pas moins qu'ils sont pour la plupart du temps en anglais, et dans un format souvent peu lisible.

5. **"Bon, d'accord, celle-ci est bonne, et je sais pour quoi je vais l'utiliser, mais comment vais-je faire dans ma classe ?”** Il y a aussi sans doute un écart de spécification entre la pratique pédagogique "générique", telle qu'elle est précisée dans les articles de recherche (notamment, les méta-analyses), et la pratique "contextualisée", telle qu'elle peut être organisée dans les classes. Cet écart a été repéré comme faisant des différences importantes et tout un courant de recherche s'en est emparé (la science de l'implémentation :cite:`kelly12`). Cela a aussi amené certains :cite:`nelson17` à parler plutôt de pratique *inspirée* par les preuves, c'est-à-dire, en tenant aussi compte du contexte, mais aussi de l'expérience de l'enseignant

6. **"“Au fait, quelles preuves avons-nous que l'éducation fondée sur des preuves marche ?”** Il semble y avoir très peu de preuves que l'EFP se soit appliqué à soi-même ce qu'elle réclame de faire : les études comparatives comparant des groupes d'enseignants recourant à l'EFP à d'autres n'y recourant pas ne paraissent pas très nombreuses (voir toutefois “`The literacy octopus <https://educationendowmentfoundation.org.uk/projects-and-evaluation/projects/the-literacy-octopus-communicating-and-engaging-with-research/>`_” : la pieuvre littéraire, de l'EEF.

Alors, les pratiques qu'on utilise pour enseigner ou éduquer sont-elles si éloignées des preuves scientifiques pour qu'on ait besoin d'insister sur les liens entre science et pratique ? D'autres parlent plutôt de "pratique *informée* par les preuves" (*evidence-informed practice*) :cite:`nelson17,biesta20`. Le problème est double : il est loin d'être sûr que la pratique que l'on veut utiliser ou le problème que l'on veut résoudre ait déjà fait l'objet d'une recherche expérimentale évaluant l'efficacité de telle ou telle pratique sur un nombre suffisant d'élèves et, d'autre part, même si ce test existait, les conclusions pourraient-elles être appliquées trait pour trait dans notre contexte ?

Ce que l'on sait
================

Qu'est-que l'EFP ?
------------------

Comme les recherches fondées sur les preuves des autres domaines, il est difficile d'avoir une définition précise de ce que serait l'EFP. Bouffard et Reid :cite:`bouffard12` signalent que sa préoccupation principale est de déterminer “ce qui marche *relativement*” (*i.e.*, ce qui est *plus ou moins* efficace), plutôt que d'établir une vérité intrinsèque (*i.e.*, ce) : réaliser des interventions pédagogiques pour lesquelles il y a suffisamment de preuves établies qu'elles "marchent", autrement dit, d'établir des comparaisons entre diverses interventions fondées sur les preuves, les premières permettent des résultats significativement meilleurs et durables.

Cela suppose, bien sûr, un accès à ces preuves. Contrairement aux médecins, qui disposent d'un vocabulaire scientifique commun pour fonder leurs preuves (biologie, anatomie, etc.), les enseignants ne disposent pas de telles bases : les disciplines potentiellement contributives (éducation, psychologie, philosophie) ne sont pas organisées autour de documentations cumulatives de phénomènes éducatifs :cite:`hargreaves97,saussez09`, même s'il existe des tentatives pour en constituer :cite:`hattie08`. De plus, les relations causales entre traitement (essentiellement pharmacologique) et résultat sont un peu plus simples dans le cas de la médecine que de la pédagogie :cite:`hammersley04`. Enfin, toujours d'après Hammersley, les relations enseignant-élèves sont pour la plupart du temps collectives, ce qui n'est pas le cas en médecine. Le Tableau I ci-dessous recense les différences entre les deux milieux (médecine et scolaire).


**Tableau I -** Les différences entre milieux médicaux et scolaires concernant l'approche "fondée sur les preuves".

+---------------+-----------------+-----------------------------+
|               | Médical         | Scolaire                    |
+===============+=================+=============================+
| Ouverture     | Milieu contrôlé | Milieu peu contrôlé, ouvert |
+---------------+-----------------+-----------------------------+
| Interventions | Individuelles   | Collectives                 |
+---------------+-----------------+-----------------------------+
| Rôle du       | Bien défini     | Plus vague                  |
| jugement      |                 |                             |
+---------------+-----------------+-----------------------------+
| Niveau de     | Bien documenté  | Pas toujours bien documenté |
| preuve        |                 |                             |
+---------------+-----------------+-----------------------------+

.. IA: Ce tableau est à nuancer, la médecine fondée sur les preuves s’est aussi attelée à apporter des preuves sur des intervensions collectives en milieu peu contrôlé

De plus, comme le signale :cite:`hammersley04`, se centrer sur “ce qui marche” met en avant une vue très technique du métier : comme s'il y avait une manière simple et directe de distinguer, dans la pratique pédagogique, ce qui marche (*i.e.*, ce qui est efficace) de ce qui ne marche pas (*i.e.*, ce qui l'est moins). Si c'était le cas, il y aurait certainement moins de mythes tenaces (voir notamment :ref:`mythes_education`).

Politique et EFP
----------------

On a expliqué en détail :cite:`saussez09` comment le courant des professions fondées sur les preuves a pu émerger politiquement. Il part d'idées difficilement contestables : éviter le gaspillage de fonds publics, faire en sorte que les professionnels rendent des comptes régulièrement (*accountability*, ou imputabilité), travailler le plus efficacement possible.

Mais, d'un autre côté, ce courant marque l'arrivée d'une politique libérale (d'abord aux États-Unis d'Amérique et Grande-Bretagne) de l'“État gestionnaire” (par opposition à l'“État providence”), où l'État confie de plus en plus au privé ou semi-privé des prestations dont il avait l'entière charge auparavant :cite:`saussez09`, dans un supposé contexte de “rationalisation” : l'État fonctionnerait comme une entreprise privée. Comme l'indique :cite:`hammersley04`, toutefois, l'émergence de l'EFP doit aussi à un courant de la gauche prônant l'idée que tout citoyen sache à quoi ses impôts sont dévolus.

Une autre manière de considérer les choses est de se placer, comme :cite:`campbell91` dans une “société experimentatrice” (*experimenting society*), dans laquelle on teste de manière expérimentale les effets de diverses innovations. Une idée voisine de celle développée par :cite:`latour84` lorsqu'il montre comment Pasteur, aidé par les hygiénistes, a non seulement amené son laboratoire dans les fermes, mais aussi “pasteurisé la France” en changeant les pratiques des agriculteurs.

Une telle transposition de cet esprit expérimentateur est peut-être difficile à réaliser (certains pensent qu'il n'est pas souhaitable) dans un contexte scolaire ; une bonne raison est qu'il implique que l'enseignant se comporte également devant tous ses élèves, pour ne pas fausser l'expérimentation, alors que l'une de ses fonctions est de proposer une différenciation selon le niveau des élèves :cite:`stenhouse81`.

Quoi qu'il en soit, comprendre ce qu'est une EFP à des fins *personnelles* de développement professionnel peut être fructueux.


Compréhension de l'EFP par les praticiens
-----------------------------------------

Un autre argument est à prendre en compte, relié à la question 6 (“Quelles preuves avons-nous que l'éducation fondée sur des preuves marche ?”. Les enseignants lisant et utilisant des méthodes issues de l'EFP sont-ils moins perméables aux mythes, d'une part, et des enseignants plus efficaces, d'autre part ? Concernant la première question, une étude récente :cite:`betts19` (p. 22) montre que les enseignants lisant des travaux de psychologie, éducation et neurosciences sont moins enclins à croire à des principes dont la recherche a montré l'invalidité (mythes).

La hiérarchie des preuves
-------------------------

Toutes les preuves se valent donc ? Peut-on établir une hiérarchie ? Le niveau de preuve attaché à une étude donnée est sa capacité à répondre à la question que l'étude vise et les recommandations qui vont en être issues auront une force, une validité dont l'évaluation correspond à ce niveau de preuve.

Il existe de nombreuses manières de représenter une hiérarchie de preuves, et peu s'accordent :cite:`bouffard12` ; de plus, les travaux faisant état de telles hiérarchies n'expliquent en général pas comment elles ont été construites. Enfin, toujours en reprenant Bouffard et Reid, ces hiérarchies amènent à penser que certains types de recherches sont scientifiquement supérieurs à d'autres, et donc le rejet de certaines pratiques qui ont tout de même pu amener des connaissances intéressantes (il est connu que Jean Piaget menait souvent des études avec un nombre réduit de participants).

La hiérarchie suivante, par niveau de force décroissant, est couramment présenté. Il faut bien sûr noter que le soin avec lequel les différents travaux sont menés influe sur la force de la preuve (une méta-analyse mal menée pourra avoir une force moindre qu'une recherche par observation très pertinente :cite:`djul17`) :

1. **Revues systématiques et méta-analyses** : Travaux qui recensent, sans *a priori* d'exclusion, le plus grand nombre d'études sur une problématique donnée.
2. **Essais contrôlés randomisés** : Études qui répartissent les participants aléatoirement en 2 groupes, l'un recevant le traitement, l'autre n'étant pas exposé au traitement (*e.g.*, méthode pédagogique innovante), mais à un traitement de contrôle (*e.g.*, une méthode pédagogique standard).
3. **Recherche observationnelle** : Données recueillies (ou observations menées)  par des chercheurs entraînés, décrites et analysées pour élaborer de nouvelles hypothèses ou recherches.
4. **Avis d'experts et anecdotes** : Recueil de l'avis d'experts, anecdotes recueillies dans des contextes spécifiques.

.. Présentons tout de même une pyramide des preuves https://psyarxiv.com/whds4 A FINIR
.. Regarder aussi les papiers d'Ignacio

Les pratiques, pour quoi faire ?
--------------------------------

Comme le signale justement Biesta :cite:`biesta19`, dire qu'une pratique est efficace ne donne aucune information sur d'éventuelles valeurs morales, ou éthiques, liées à cette pratique. Comme il le signale directement (p. 3) "Il existe des tortures efficaces et des tortures non efficaces […], mais ça ne veut pas dire que les tortures efficaces […] sont bonnes".


Et si on demandait aux enseignants ?
------------------------------------

En lien avec le point 1 du début du document (“Sur quoi donc d'autre voudriez-vous  fonder l'éducation  ? des croyances ?”), on ne peut seulement considérer les preuves provenant d'études contrôlées et randomisées. Ce que l'on nomme, en médecine, "l'expérience clinique" ("expérience pédagogique" dans le champ éducatif), sont des éléments importants de l'expertise de chacun et le courant de la médecine fondée sur les preuves les prend en compte.

Le rapport précis et non ambigu d'observations et de leurs résultats contribue aussi à accroître la précision du diagnostic de problèmes, et par là la connaissance individuelle, mais aussi scientifique de phénomènes. En effet, comme le signale Eraut :cite:`eraut04`, de mauvais diagnostics entraînent de mauvaises  ou actions pédagogiques, qui biaisent à leur tour les prédictions des tests et recherches.

Les enseignants praticiens peuvent, eux aussi, amener des preuves pour instruire certaines situations et prendre les décisions qui s'imposent, on appelle ce type de données des “preuves fondées sur la pratique” (*practice-based evidence*) .

Une autre possibilité pour cerner la validité des preuves est tout simplement de demander aux enseignants ce qu'ils considèrent comme une preuve valide. Mockler et Stacey (2019) ont réalisé un questionnaire en ce sens, auprès de 500 enseignants australiens des premier et second degrés. Les preuves les plus fiables, selon eux, sont les suivantes, par ordre décroissant de fiabilité (seuil supérieur à 50 % de réponses "très fiable" ou "extrêmement fiable") :

* leur propre observation de classe systématique et réflexion ;
* les entretiens avec les étudiants (individuels ou collectifs) ;
* les activités d'évaluation ;
* l'observation de la classe par un collègue ou un pair ;
* l'observation de la classe par un collègue expérimenté.

Quels types de preuves chercher ?
---------------------------------

On peut distinguer les types de preuve *issus de la recherche*, utilisables par les enseignants pour mener des enseignements les plus efficaces possible, des types de preuves *issus du contexte d'enseignement*.

Pour les premiers, on peut se tourner :

* vers les prescriptions de type "apprentissage visible", notamment consignées dans :ref:`appr_visible` ;
* vers les synthèses de l'EEF (Education Endowment Foundation) ;
* vers les rapports, méta-analyses.

Pour les seconds, on peut recueillir :

* des observations d'enseignants en contexte scolaire ;
* des analyses de pratiques d'enseignants [augmenter ça].

Les enseignants, des chercheurs ?
---------------------------------

Si les enseignants recourent à l'EFP, cela veut-il dire qu'ils deviennent des chercheurs ? :cite:`stenhouse81` [à poursuivre].

Ce que l'on peut faire
======================

L'EFP fonctionne un peu comme un test de Rorschach : l'avis qu'ont les chercheurs et enseignants sur ce champ peut révéler leur propre opinion sur l'éducation, en plus de leur opinion sur l'EFP. Par exemple, on peut penser :cite:`alvarez13,bryk`, que l'EFP permet de rapprocher les enseignants de la recherche, ou, au contraire, que cela va les mener vers une technicisation, une scientificité artificielle :cite:`saussez09`.

Tout d'abord, peut-on  réfléchir à la manière d'implémenter une intervention inspirée par les preuves ? Ensuite, on peut explorer les nombreuses ressources, souvent gratuites, proposant des rapports très variés sur l'EFP. Par exemple, on peut s'inspirer d'EFP en concevant un cours (*e.g.*, pour écrire un document pour les élèves), mais aussi en s'inspirant d'une méthode.

EFP et preuves fondées sur la pratique
--------------------------------------

Cela amène à la notion, complémentaire de celle de l'EFP : la preuve fondée sur la pratique. L'un des problèmes de l'EFP est que les preuves ne proviennent pas directement du champ de la pratique, des enseignants eux-mêmes, mais d'articles de recherche souvent difficiles à lire, voire à se procurer. Pourtant, l'expérience des enseignants, ou bien ce qu'on peut appeler la “sagesse de l'expérience” peut être aussi un moyen efficace de collationner des preuves, quitte bien sûr à les traiter qualitativement et/ou quantitativement ensuite.


Le développement professionnel des enseignants
----------------------------------------------

Sous le terme vague de "développement professionnel des enseignants" se cache toute démarche de la part de ces derniers d'acquérir des connaissances réflexives sur leur métier :cite:`betts19` : formation continue (professionnelle ou universitaire), participation à des groupes de travail, groupes de développement professionnel en équipes, participation à des MOOC, etc. Ces démarches ont, comme la lecture de travaux scientifiques, un effet protecteur significatif sur la croyances en des mythes, comparées à des démarches sans développement professionnel.


Références
==========

Webographie
-----------

* Mockler, N., & Stacey, M. (2019, 18 mars). `What’s good ‘evidence-based’ practice for classrooms? We asked the teachers, here’s what they said <https://www.aare.edu.au/blog/?p=3844>`_. Blog EduResearch Matters.

Travaux
-------

.. bibliography::
    :cited:
    :style: apa
