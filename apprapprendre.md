(appr-apprendre)=

# Apprendre à apprendre

```{index} single: auteurs; Dessus, Philippe single: auteurs; Besse, Émilie
```

:::{admonition} Information
- *Auteur\** : [Philippe Dessus](http://pdessus.fr/), Inspé & LaRAC, Univ. Grenoble Alpes. Le quizz a été réalisé par Emilie Besse, projet [ReFlexPro](http://www.idefi-reflexpro.fr).
- **Date de création** : Janvier 2005.
- **Date de publication** : {sub-ref}`today`.
- **Statut du document** : Terminé.
- **Résumé** : Apprendre à apprendre est une activité souvent requise par l'enseignant chez les élèves. Cette activité regroupe en réalité de nombreuses activités différentes qui sont exposées ici, avec les principales difficultés théoriques et pratiques d'étudier une telle activité métacognitive.
- *Citation\** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.
- **Licence** : Document placé sous licence *Creative Commons* : [BY-NC-SA](http://creativecommons.org/licenses/by-nc-sa/3.0/fr/).
:::

(introduction)=

## Introduction

Le célèbre proverbe (chinois ?) : "Si je te donne un poisson, tu n'en mangeras qu'un. Si je t'apprends à pêcher, tu en mangeras longtemps" ({cite}`giordan87`, p. 3) est souvent cité pour montrer l'intérêt des démarches tentant d'enseigner aux élèves, non seulement des connaissances, mais aussi des connaissances sur les connaissances. Il existe un large débat (souvent philosophique et donc peu fondé sur des preuves empiriques {cite}`boillot93` ; mais également plus empirique, e.g., {cite}`winne97`) sur les possibilités de développer ces activités d'apprentissage à propos de l'apprentissage, que l'on peut rapidement nommer "recherches sur la métacognition". Ces recherches sont compatibles avec les récentes conceptions de l'apprentissage qui le considèrent comme une activité de la part de l'élève plutôt que comme une réception passive d'informations. Il est tout d'abord important de définir ce qu'est une stratégie : C'est "une séquence intégré, plus ou moins longue et complexe, de procédures sélectionnées en vue d'un but afin de rendre optimale la performance." {cite}`fayol94` p. 93)

Généralement {cite}`hadwin01`, un élève développant de telles stratégies est censé : 1°) évaluer de manière critique les tâches d'apprentissage qu'il se (ou qu'on lui) donne, et déterminer les conditions qui vont lui permettre de réussir ces tâches ; 2°) à partir de cette évaluation, définir des buts d'apprentissage adéquats ; 3°) connaître et savoir utiliser des stratégies d'apprentissage alternatives, si nécessaire ; 4°) juger la qualité de ces stratégies pour réaliser les buts qu'il s'est fixés en 2°). Ces différentes habiletés semblent être développées par tous les élèves, et à tous les âges, même si elles ne sont pas enseignées directement {cite}`winne97`. Le propos de ce document est de définir et de montrer l'intérêt, mais aussi les difficultés théoriques et pratiques, de ces différentes recherches (voir {cite}`depover99` pour une revue accessible en français) et voir aussi la section suivante.

## Ce que l'on sait

La formule "apprendre à apprendre" cache en fait de nombreuses activités qu'il est important de distinguer. La littérature en distingue généralement trois principales, qui correspondent aux phases avant, pendant et après l'activité (soit la planification, le contrôle/monitorage et l'évaluation). Soit un élève en train de réaliser une activité (*e.g.*, écrire). Il devra être à même de la planifier, de la contrôler/monitorer (le contrôle lui permet d'influer sur ses performances en écriture, le monitorage lui permet d'évaluer en direct la qualité de son écriture), puis de l'évaluer une fois réalisée. Le Tableau 1 ci-dessous regroupe quelques questions à propos de ces trois activités et la Figure 1 ci-dessous reprend les principaux paramètres d'une telle activité.

**Tableau I** - Quelques questions métacognitives {cite}`schraw98` p. 121).

1. Planification

   1. Quelle est la nature de la tâche ?
   2. Quel est mon but ?
   3. Quel type d'information et de quelles stratégies j'ai besoin ?
   4. De combien de temps et de quelles ressources je dispose ?

2. Monitorage

   1. Ai-je une bonne compréhension de ce que je suis en train de faire
      ?
   2. Est-ce que ma tâche a un sens pour moi ?
   3. Est-ce que je suis en train d'atteindre les buts que je me suis
      fixés ?
   4. Est-ce que je dois changer de stratégie ?

3. Evaluation

   1. Est-ce que j'ai atteint les buts que je me suis fixés ?
   2. Qu'est-ce qui a marché ? pas marché ?
   3. Qu'est-ce que je ferai différemment la prochaine fois ?

**Figure 1** - Un modèle de l'apprentissage autorégulé ({cite}`winne97` p. 399).

```{image} images/SRLschema.png
:align: center
```

### Connaître ses capacités en mémoire et en attention

Les élèves, dès le plus jeune âge (4 ans), parviennent à développer des stratégies relatives à leur mémoire (autorépétition, catégorisation d'objets avant mémorisation) ou leur attention (gérer leur attention), mais elles sont tout d'abord largement intuitives et embryonnaires et ne commencent à devenir conscientes qu'à partir du collège {cite}`fayol94`, mais de nombreux étudiants ou adultes parviennent à réussir des tâches de mémorisation complexes sans vraiment savoir comment ils s'y prennent, d'où la difficulté de questionner les élèves sur ce sujet.

### Décider quand on connaît ou comprend un matériel à apprendre

Cette activité de monitorage est d'une grande utilité à l'élève : elle lui permet d'arrêter de réviser, de travailler une leçon ou des exercices, mais aussi de prédire avec plus ou moins de précision ses notes futures, ou encore l'oubli des informations apprises {cite}`maki98`. C'est toutefois une activité difficilement enseignable, et la précision des notes prédites est faible, même pour des élèves de collège.

### Déterminer et évaluer le contexte de son apprentissage

Il est certain que le {index}`contexte d'apprentissage` dans et pour lequel on apprend un matériel a une incidence sur les stratégies employées. Hadwin et ses collègues {cite}`hadwin01` sont partis de trois contextes différents (lire pour apprendre, écrire une dissertation, étudier pour un examen), et demandé à des étudiants quelles stratégies d'apprentissage ils employaient. Les résultats montrent que leurs stratégies diffèrent selon ces tâches, beaucoup plus que ce qui est prédit par les théoriciens.

### Verbaliser son activité, ou expliquer ce que l'on a fait

Pour l'enseignant, il est également important de savoir si les questions qu'il pose à l'élève à propos de son activité seront suivies de réponses fiables, c'est-à-dire qu'il pourra considérer comme proches de la réelle activité de ce dernier. Comme indiqué plus haut (capacités en mémoire et attention), il convient, notamment pour les élèves les plus jeunes (avant le collège), de considérer avec précaution les verbalisations de leur activité. Il sera toutefois possible de construire des situations d'interrogation suffisamment neutres. Pour cela, les recommandations de Vermersch {cite}`vermersch94` seront utiles (voir aussi le Document SAPP {ref}`activite`). De plus, les consignes de verbalisation à haute voix, notamment pour la favoriser la compréhension de textes lus, peuvent être d'une efficacité certaines dès le cycle 3 {cite}`bianco04`. L'enseignant peut (voir Tableau II ci-dessous) verbaliser lui même à la lecture d'un texte, ce qui peut ensuite inciter les élèves à le faire, et aussi à débattre à propos d'autres stratégies de compréhension.

**Tableau II** - Pensée à voix haute comme aide à la compréhension de textes
lus ({cite}`bianco04`, p. 60).

« Je ne comprends pas qui est l'ennemi dans le texte. Je sais, d'après mes connaissances, que les ennemis font peur et qu'on se bat contre ses ennemis. Je vais donc relire, tout d'abord la phrase qui comporte le mot « ennemi » en cherchant des indices indiquant que le personnage du texte a peur ou qu'il se bat. Je peux lire que le personnage cherche de la lumière (il va d’une bougie à l’autre) et que l'ennemi avance. Ces indications ne sont pas suffisantes pour que je trouve qui est l'ennemi. Je vais donc relire le paragraphe en entier, peut-être m'apportera-t-il une explication. Je peux lire à la deuxième ligne que le personnage du texte court pour allumer des bougies, il se peut donc qu'il fasse nuit. Je peux lire, toujours dans cette deuxième ligne que le personnage parle des ombres qu'il redoutait et contre lesquelles il luttait. Je peux voir dans cette phrase que le personnage du texte a peur des ombres et qu'il lutte, c'est-à-dire qu'il se bat avec elles. Je peux donc dire que l'ennemi du personnage sont les ombres, c'est pour cela qu'il essaye d'allumer des bougies ; mais que sont ces ombres ?… »

### Les débats en cours

Il reste encore beaucoup de choses à comprendre à propos de la métacognition. Notamment, les chercheurs ne s'accordent pas encore sur l'idée que ces habiletés seraient ou ne seraient pas dépendantes du domaine à apprendre. Certains {cite}`schraw98` pensent que de telles habiletés sont propres à un domaine au début de son apprentissage, les élèves devenant, avec plus d'expérience, de connaissances et un âge plus élevé, capables de se constituer des habiletés métacognitives propres à plusieurs domaines (*e.g.*, comprendre ses propres limites en mémoire), en d'autres termes, de les transférer, ce que d'autres ({cite}`hadwin01`) contestent en montrant que les élèves sont capables d'avoir des stratégies très dépendantes de la tâche à accomplir. D'autres encore {cite}`cottrell02` ont récemment montré que les capacités métacognitives n'influent que très peu sur la compréhension de textes scientifiques, en tous cas bien moins que les connaissances initiales et les performances en lecture. Un autre problème souvent évoqué à propos de l'étude de la métacognition est le fait qu'elle est majoritairement étudiée en contexte de laboratoire et via questionnaires, ce qui restreint souvent la validité des résultats. Enfin, le fait de demander aux élèves de s'autoévaluer n'est pas sans problèmes sociaux : comment est prise en compte la parole de l'élève s'évaluant, centration sur les stratégies plutôt que sur les performances finales, désir de plaire à l'enseignant, risque d'accroître les inégalités, car les élèves étant les plus aptes à des stratégies métacognitives sont souvent les meilleurs {cite}`allal99` auquels l'enseignant doit aussi être attentif.

## Quizz

```{eval-rst}
.. eqt:: Apprapprendre-1

   **Question 1. Quelles sont les trois principales activités liées à la formule "apprendre à apprendre" ?**

   A) :eqt:`I` `Le contrôle ; L'alignement pédagogique ; La recherche`
   B) :eqt:`I` `La recherche ; La planification ; La synthèse`
   C) :eqt:`C` `La planification ; Le monitorage ; L'évaluation`
   D) :eqt:`I` `L'alignement pédagogique ; La recherche ; L'évaluation`
```

```{eval-rst}
.. eqt:: Apprapprendre-2

   **Question 2. Parmi les questions métacognitives suivantes, quelle est celle qui est liée à la planification ?**

   A) :eqt:`C` `Quelle est la nature de la tâche ?`
   B) :eqt:`I` `Est-ce que ma tâche a un sens pour moi ?`
   C) :eqt:`I` `Est-ce que je dois changer de stratégie ?`
   D) :eqt:`I` `Qu’est-ce qui a marché ?`
```

```{eval-rst}
.. eqt:: Apprapprendre-3

   **Question 3. Parmi les questions métacognitives suivantes, quelle est celle qui est liée au monitorage ?**

   A) :eqt:`I` `De quel type d’information et de quelles stratégies ai-je besoin ?`
   B) :eqt:`C` `Ai-je une bonne compréhension de ce que je suis en train de faire ?`
   C) :eqt:`I` `Qu’est-ce que je ferai différemment la prochaine fois ?`
   D) :eqt:`I` `Quel est mon but ?`

```

## Références

```{bibliography}
:filter: docname in docnames
```
