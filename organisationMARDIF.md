(organisation-cours-mardif)=

# Organisation du cours (MARDIF)

```{index} single: auteurs; Dessus, Philippe
```

:::{admonition} Information
- **Auteur** : [Philippe Dessus](http://pdessus.fr/), LaRAC & Inspé, Univ. Grenoble Alpes.
- **Date de création** : Mars 2016.
- **Date de publication** : {sub-ref}`today`.
- **Statut du document** : Terminé (ce document ne sera plus modifié).
- **Résumé** : Ce Document décrit l'organisation du cours de l'Atelier TICE du mois d'avril 2017, MARDIF, Univ. de Rouen/CNED. Cette page n'est plus maintenue.
- **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.
- **Licence** : Document placé sous licence *Creative Commons* : [BY-NC-SA](http://creativecommons.org/licenses/by-nc-sa/3.0/fr/)
:::

## Objectifs

Ce cours est conçu pour être suivi deux années successives. La première année, les étudiants qui l’auront suivi seront capables :

- de sélectionner l’activité d’apprentissage en lien avec leurs principes pédagogiques ;
- de construire des questions de QCM liées à un cours (également conçu pour l’occasion) qui soient en lien avec les activités d’apprentissage visées dans ce cours ;
- de composer un QCM, dans un contenu donné, qui ne contienne pas trop de biais ;
- de réaliser un bref cours et des QCM assortis avec le logiciel *eXe Learning* (ou un autre logiciel équivalent (demander confirmation à l'enseignant).

La deuxième année, les étudiants seront capables :

- d’organiser une passation, sur une vingtaines de personnes, du cours-QCM (niveau et âge approprié à son contenu) conçu la première année ;
- de recueillir et d’analyser les réponses, notamment en termes de difficulté et discrimination ;
- de tirer quelques enseignements à partir de l’analyse de ces réponses (items à garder, à rejeter) ;
- éventuellement (selon leur niveau de maîtrise de l’informatique), de convertir tout ou partie de leur cours sur *Oppia*, un nouveau système de conception de parcours d’apprentissage interactifs.

## Chronologie des tâches du séminaire TICE

L’organisation générale du séminaire peut se représenter comme suit dans le Tableau 1 ci-dessous. Le détail des tâches pour chaque promotion suit. Rappelons que ce séminaire est conçu pour être passé en 2 ans. La première année les étudiants suivent les activités de la 2{sup}`e` colonne et la deuxième année celles de la 3{sup}`e`.

**Tableau 1**– Chronologie des tâches du séminaire TICE, avril 2017.

| Dates(-Butoir)                                                        | Activités Promotion Gaston-Mialaret                                                                                                                                                      | Activités Promotion Louis-Legrand                                                                                                                                         |
| --------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1er avril 2017                                                        | Démarrage du séminaire                                                                                                                                                                   | Démarrage du séminaire                                                                                                                                                    |
| Questions le plus tôt possible et avant le 14 avril 2017 à 12:00 CEST | **Activité 1** : Lecture du cours selon le programme indiqué plus bas \[{ref}`1re_annee`\]. Installation du logiciel eXe Learning (voir {ref}`exe_learning`) et formulation de questions | **Activité 1** : Lecture du cours selon le programme indiqué plus bas \[{ref}`2e_annee`\]. Organisation de la passation et formulation de questions                       |
| À démarrer au plus tôt                                                | **Activité 2** : Construction et dépôt du cours-QCM créé avec le logiciel eXe Learning, dans l’espace de dépôt correspondant (Gaston-Mialaret)                                           | **Activité 2** : Réalisation de la passation et analyse des résultats. Rédaction d’un bref rapport. Dépôt du rapport dans l’espace de dépôt correspondant (Louis-Legrand) |
| 30 avril 2017                                                         | Clôture du séminaire                                                                                                                                                                     | Clôture du séminaire                                                                                                                                                      |

## Détail des activités par promotion

(re-annee)=

### A. 1{sup}`re` année (promotion Gaston-Mialaret)

Sont décrites ici les tâches des étudiants qui débutent ce séminaire TICE, et qui n’ont donc pas encore eu à réaliser de cours-QCM avec *eXe Learning* l’année précédente.

#### Activité 1 - Lecture du cours de l'atelier

(voir Tableau 1 ci-dessus pour la date-butoir)

- Lecture extensive et attentive du cours et travail sur les TP. Il est conseillé de respecter l'ordre de lecture suivant :

  > - Lire tout d'abord attentivement ce document décrivant l'organisation générale du cours.
  > - Lire la suite des documents, dans l'ordre indiqué dans l'index (ou dans le recueil), soit dans l'ordre les documents généraux sur les rétroactions : {ref}`tache`, {ref}`retro_defs`, {ref}`retro_formes`, puis sur les rétroactions informatisées : {ref}`retro_info`.
  > - Lire les documents introductifs sur les QCM : {ref}`QCM_def` et {ref}`QCM_formes`.
  > - Les documents suivants étant plus techniques, ne lire que ce qui vous intéresse ou vous concerne : le document {ref}`usages-peda` intéressera les enseignants. La lecture du document ({ref}`exe_learning`) est obligatoire à tous ceux utilisant *eXe Learning* comme outil de création de cours/QCM.

- Travail sur au moins 3 TP, au choix, qui sont des tâches énoncées à la fin de chaque Document. Noter que les réponses aux TP sont à garder pour soi, éventuellement à discuter dans le forum, mais pas à livrer à l’enseignant. Ce dernier pourra toutefois répondre aux questions à leur propos.

- Répondre aux questions suivantes de manière argumentée, en une dizaine de lignes par question. Ces questions sont conçues pour vérifier la compréhension des étudiants des tâches demandées et du contenu du cours :

  > - Quelles sont les trois principales notions exposées et discutées dans le cours, ainsi que les questions que je me pose à leur propos ? (p. ex., concernant leur possible application à mes propres recherches).
  > - Quels sont les trois principaux points du cours qu’il me reste à comprendre ou à approfondir ? Y a-t-il des points particuliers que je n’ai pas compris et sur lesquels j’aimerais avoir des précisions ?
  > - Questions techniques. Suis-je arrivé-e à installer le logiciel *eXe Learning* et ai-je compris son fonctionnement dans les grandes lignes ? Quelles sont les questions que je me pose encore, suite à la lecture de la documentation ?

- Dépôt des réponses au questionnaire du cours dans le forum de discussion du séminaire. Retour de l’enseignant sur chacune avant la réunion (lorsque possible, d’où l’intérêt de les poster assez tôt).

- Installation du logiciel *eXe Learning* et première manipulation à des fins d’exploration de ses fonctionnalités. L’essentiel des informations à propos du logiciel est mentionné dans le document {ref}`exe_learning` (notamment ses modes d’emploi). Il est important de signaler par courriel à l’enseignant, le plus rapidement possible, tout problème dans l’installation ou l’utilisation du logiciel. Les étudiants les plus avancés pourront envisager d’utiliser *Oppia*, mais uniquement après avoir réussi à répliquer sans problèmes l’Exploration décrite dans le document {ref}`exe_learning`.

- Pour information, le cours sous *eXe Learning* conçu par une étudiante de la promotion MARDIF 2010–11 peut être un bon exemple à suivre (<http://perso.numericable.com/annick.pradeau/ViolenceConjugale/index.html>). Le cours sous *Oppia* réalisé par une étudiante du Master MEEF-PIF 2016-17 en est un autre (<https://www.oppia.org/explore/9W1jUapxz_De>, nécessite de créer un compte). D’autres exemples peuvent encore figurer sur le site de l’atelier, s'il est encore disponible (Espace : Cours-QCM réalisés l’année précédente).

#### Activité 2 - Construction du cours/QCM

(voir Tableau 1 ci-dessus pour la date-butoir)

Note : dans la mesure du possible, il est conseillé de ne pas attendre la fin de l’Activité 1 pour démarrer l’Activité 2. Les deux activités peuvent se mener en partie en parallèle.

- Lire les documents orientés sur la conception. Le document {ref}`QCM_proc_conception` procure d'utiles éléments pour concevoir le document de leçon. Le document {ref}`QCM_principes` est à utiliser durant la phase de rédaction de QCM. Voici ci-dessous quelques précisions formelles sur les attendus à propos du cours/QCM à concevoir.
- Tout d'abord, par "document de cours" (ou de leçon), nous entendons un texte décrivant un contenu enseigné circonscrit, pouvant être présenté dans une leçon nécessitant environ 1 h de lecture, et destiné à un public clairement défini, à la fin de lui permettre une bonne compréhension des concepts ou compétences associées à ce contenu. Ce document sera bien sûr agrémenté de questions à choix multiple pour que l'apprenant puisse tester sa compréhension du contenu.
- Choisir un domaine dans lequel vous êtes expert-e et pour lequel vous voulez transmettre un contenu.
- Si vous êtes enseignant-e ou formateur(trice); choisir un niveau de classe (le vôtre, ou un niveau qui vous intéresse) et définissez quelques objectifs d’apprentissage (ce que les personnes qui suivront ce cours devront être capables de faire à l’issue du cours).
- Choisir un contenu à apprendre à propos du domaine choisi, pouvant être présenté au cours d'une leçon d'env. 1 h. Rédiger environ cinq pages A4 (soit environ 10 000 caractères, espaces compris) hiérarchisées à deux niveaux. Veiller à ce que le contenu soit également réparti dans les différentes sections du cours.
- Illustrer cette présentation par des images, des tableaux, des vidéos (selon les possibilités du logiciel utilisé), dont la référence sera mentionnée dans le corps du texte de la leçon.
- Concevoir au moins 5 questions interactives (QCM) qui utilisent les fonctionnalités d’*eXe Learning*. Parmi ces 5, l’une d’entre elles doit être une question Assertion-raison (ou à deux étapes). Ces questions doivent être correctement insérées dans la leçon et permettre une meilleure compréhension de cette dernière par le lecteur. Elles doivent, d’autre part, se conformer aux différentes prescriptions contenues dans ce cours (notamment en ce qui concerne le guidage). L'insertion des questions au sein du contenu est libre : soit en tant qu'évaluation diagnostique (début), formative (au long du cours), ou encore sommative (fin). Les réponses à ces questions seront analysées pendant l'atelier de l'année suivante.
- Pour être communiqué à l'enseignant à des fins d'évaluation, le fichier généré sous *eXe Learning* sera ensuite exporté en format HTML (si le logiciel le permet ; sous *eXe Learning*, via le menu **Fichier>Exporter>Site Web>Fichier compressé ZIP**). Le fichier correspondant peut être décompressé et placé sur un serveur web, ou utilisé en local en ouvrant les fichiers HTML avec un navigateur. Si un autre logiciel est utilisé il est nécessaire d'exporter la production en format HTML
- Sauf avis contraire de l’enseignant (ou de l'étudiant-e), les différents cours-QCM produits sur *eXe Learning* seront installés sur un site public du MARDIF et pourront être librement consultés, à la clôture du séminaire, soit courant juin 2017. À cette fin, chaque étudiant, une fois son cours entièrement conçu, devra réaliser la procédure suivante :
- Dépôt du cours et du formulaire sur l’espace de dépôt du site MARDIF (correspondant à la promotion de l’étudiant) :
  : 1. Déposer sur l’espace de dépôt approprié du séminaire (celui de votre promotion) les fichiers suivants :
    2. Le formulaire ci-dessous rempli et signé d’autorisation de publication du cours. Si vous n’acceptez pas que votre cours soit porté sur le site, veuillez prévenir l’enseignant du cours qui vous indiquera la procédure alternative ;
    3. Le fichier compressé (ZIP) obtenu par l'étape décrite ci-dessus ;
    4. Le fichier-source du cours (obtenu en faisant **File>Save As…**), de suffixe .elp. (là encore, uniquement si le cours est produit sous *eXe Learning*) ;
    5. Si le cours est produit sur Oppia, joignez un schéma organigramme décrivant le parcours de l'utilisateur, p. ex. à partir de l'"*exploration overview*" produite par Oppia.
    6. Envoyer un courriel à l’enseignant pour le prévenir du dépôt.
- Les critères d’évaluation suivants seront pris en compte dans l’évaluation du cours.
  : - qualité du contenu du cours (de fond et de forme) ;
    - qualité du lien entre cours et QCM ;
    - qualité de rédaction des QCM (conformes aux prescriptions de ce cours) ;
    - qualité des rétroactions (données en retour, en cas de bonne ou mauvaise réponse).

(e-annee)=

### B. 2{sup}`e` année (promotion Louis-Legrand)

Sont décrites ici les tâches des étudiants qui ont déjà, l’année précédente, conçu et réalisé les activités mentionnées ci-dessus. Elles se partagent également en deux activités.

#### Activité 1 – Lecture du cours et organisation de la passation

(voir Tableau 1  pour la date-butoir)

- Lire les deux documents donnant des informations resp. historiques ({ref}`qcm_historique`) et psychologiques ({ref}`QCM_cog`) à propos des QCM.

- Lire très attentivement le document ({ref}`analyse_items`) du cours pour comprendre de près de quelle manière on calcule différents indices liés au QCM. Parcourir à nouveau le document à la recherche des principales mises à jour (paragraphes avec une fine bordure noire à droite).

- Retrouvez dans vos archives le cours-QCM que vous avez conçu l’année précédente, et relisez-le attentivement, notamment pour vous le remettre en tête. Eventuellement, ajoutez ou supprimez quelques questions pour une meilleure cohérence des différents items. En effet (voir Document {ref}`analyse_items` du cours), l’utilisation d’indices statistiques suppose que les différents items mesurent la même dimension (connaissance, compétence).

- Vous assurer que le nombre de questions de type QCM (c.-a.-d., une question-amorce, un choix de réponses,) est au moins égal à 6. S’assurer de plus que chaque item comporte le même nombre de choix, pour rendre l’analyse plus aisée. Cela vous obligera peut-être à rédiger quelques nouveaux items ou affiner les réponses ;

- Trouver 20 participants volontaires à la passation du cours-QCM (en présence ou bien à distance, p. ex., autres étudiants du MARDIF, élèves) ;

  > - Dans le cas où vous démarchez auprès des étudiants du MARDIF, il sera donc nécessaire de placer la version éventuellement modifiée de votre QCM sur le site de l’atelier TICE. Veuillez svp contacter conjointement pour cela par courriel Samuel Labadie <mailto:samuel.labadie@univ-rouen.fr>, et ce le plus tôt possible (avant le 15 avril 2017) en lui envoyant votre site en .zip sans passer par l’enseignant, qui ne revalide donc pas le cours-QCM. Les sites seront placés dans un espace *ad hoc*, nommé « appel à sondage ». La manière dont les étudiants vont vous informer de leurs résultats (puisque *eXe Learning* n’a pas de procédure pour les collecter) est mentionnée dans l’item de niveau supérieur suivant.
  > - Pensez à désactiver les rétroactions de votre QCM, de manière à ne récupérer que les premières réactions des participants et éviter de possibles tricheries.
  > - Pour une passation avec d’autres participants (élèves, autres étudiants), ce sera à vous, soit de publier votre cours-QCM sur un site, soit de l’exécuter sur des ordinateurs, en local.

- Penser à un système de collecte des données centralisé (comme les questionnaires [FramaForms](https://framaforms.org)), ce qui est optionnel et dépend du niveau de connaissances de l’étudiant, même si c'est plutôt aisé. À défaut, vous demanderez aux participants (s’ils sont étudiants du MARDIF) de noter et déposer leurs résultats sur le thème du forum correspondant (ou, s’ils ne sont pas étudiants du MARDIF, par d’autres moyens de votre choix, p. ex. par courriel).

- Répondre aux questions suivantes de manière argumentée dans le forum du séminaire, en une dizaine de lignes par question. Ces questions sont conçues pour vérifier la compréhension des étudiants des tâches demandées et du contenu du cours.

  > - Quelles sont les trois principales notions exposées et discutées dans le Document {ref}`analyse_items` du cours, ainsi que les questions que je me pose à leur propos ? (p. ex., concernant leur possible application à mes propres recherches).
  > - Quels sont les trois principaux points du Document {ref}`analyse_items` du cours qu’il me reste à comprendre ou à approfondir ? Y a-t-il des points particuliers que je n’ai pas compris et sur lesquels j’aimerais avoir des précisions ?
  > - Questions techniques. Ai-je bien réussi à récupérer mon QCM avec *eXe Learning* et est-ce que j’ai pu inviter une vingtaine de participants à le passer ? Est-ce que je suis également au clair avec la manière dont les réponses au QCM vont me parvenir, et comment vais-je les analyser ?

- Le cas échéant, si votre niveau en informatique est suffisant, utilisez *Oppia* pour convertir une partie du cours.

#### Activité 2 – Passation, collecte et analyse des données

(voir Tableau 1 ci-dessus pour la date-butoir)

**Note** : dans la mesure du possible, il est conseillé de ne pas attendre la fin de l’Activité 1 pour démarrer l’Activité 2. Les deux Activités peuvent se mener en parallèle (notamment la passation du questionnaire, qui peut prendre du temps). L’Activité 2 est composée des sous-tâches suivantes :

- Faire passer le questionnaire aux 20 participants ;

- Récupérer les données de résultats et les placer dans un fichier tableur à des fins d’analyse (le fichier tableur est disponible sur le site de l’atelier et également à <http://webcom.upmf-grenoble.fr/sciedu/pdessus/cours/tabQCM.xls>). Le cas échéant, il faudra penser à augmenter les lignes et colonnes pour faire correspondre le tableau avec le nombre de vos participants, des items, et du nombre de réponses des items.

- Récupérer calculer les différents indices (*P* ou *B* et *ID*) et analyser les items comme indiqué dans le Document {ref}`analyse_items`.

- Considérer la suppression ou la reformulation de certains items, selon les résultats de l’analyse.

- Rédiger un bref document (4 p. maximum) qui sera ensuite mis en format PDF et déposé dans l’espace de dépôt de votre promotion, de structure comme suit :

  > - Titre (indiquant le thème du QCM) ; nom et prénom de l’étudiant ; promotion.
  > - Bref descriptif du nombre de participants de l’étude, du contexte de passation (présence, distance, à tour de rôle, en parallèle, etc.), de leur niveau scolaire si passation faite en contexte scolaire. Il sera également nécessaire de documenter *a priori* leur niveau (ont-ils un bon ou faible niveau de connaissances du domaine du QCM ?)
  > - Mention des indices (*P* ou *B* et *ID*), commentaires.
  > - Graphique (voir Figure 2 du Document {ref}`analyse_items`) indiquant la répartition du choix des réponses des élèves. Il faudra veiller à anonymer les participants du test.
  > - Brève analyse, item par item, avec justification des items éventuellement rejetés et proposition d’une nouvelle reformulation de ces derniers (items ou réponses).
  > - Copie du tableau entier de résultats.

- Signature de l'autorisation de diffusion de mon cours (voir Section suivante).

### Autorisation de diffusion de cours

Document à remplir (mentionner votre nom et prénom dans les \_\_\_\_\_\_, dater et signer, et renvoyer par courriel en format PDF à l’enseignant en même temps que le cours).

Autorisation entre \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_, étudiant-e au MARDIF, contractant-e
et le MARDIF, Univ. de Rouen-CNED, représenté par sa responsable, Mme Emmanuelle Annoot, éditeure,

Je soussigné, le/la contractant-e \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_, autorise gratuitement (1) / n’autorise pas (1), par la présente, la diffusion du cours *eXe Learning* que j’ai conçu et réalisé dans le cadre de l’atelier TICE du MARDIF, d’avril 2017, sous la forme d’un site internet placé sous la responsabilité éditoriale de l'univ. de Rouen-CNED.

L’enseignant du cours, M. Philippe Dessus, se réserve toutefois la possibilité de ne pas faire publier le cours.

Je suis informé-e que ce cours sera placé selon la licence Creative Commons BY-NC-SA, dont les conditions sont disponibles à <http://creativecommons.org/licenses/by-nc-sa/3.0/>. Je déclare avoir pris connaissance de cette licence et suis en plein accord avec ses conditions.

Je suis pleinement informé-e que le contenu de ce cours sera diffusé par le site intranet Blackboard géré par l'univ. de Rouen-CNED, MARDIF pendant une durée de 1 an, en libre accès pour tous les étudiants inscrits au MARDIF.

Je certifie également que je suis l’auteur de ce cours, et que aucun élément de ce dernier n’a un droit d’auteur possédé par une autre personne. Si j’ai inséré des images dans mon site, je certifie qu’elles sont libres de droits et peuvent être publiées sur des sites non commerciaux.

L'univ. de Rouen-CNED-MARDIF et l’éditeure ne pourront être tenus responsables dans toute réclamation d’un tiers pour une question de propriété intellectuelle de toute partie du contenu de mon cours.

(1) Barrer ou supprimer la mention inutile.

> Nom, prénom, date et signature
