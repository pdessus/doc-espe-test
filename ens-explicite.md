(ens-explicite)=

# L'enseignement explicite *vs*. par la découverte

```{index} single: auteurs; Nurra, Cécile single: auteurs; Pobel-Burtin, Céline single: auteurs; D'Ham, Cédric single: auteurs; Cosnefroy, Olivier single: auteurs; Dessus, Philippe
```

:::{admonition} Informations
- **Auteurs** : [Cécile Nurra], [Philippe Dessus], [Céline Pobel-Burtin], LaRAC & Inspé, Univ. Grenoble Alpes, [Cédric d'Ham](http://www.liglab.fr/util/annuaire?prenom=Cédric&nom=D'HAM), LIG, Univ. Grenoble Alpes, & Olivier Cosnefroy, DGESCO, Paris, et LaRAC, Univ. Grenoble Alpes.

- **Date de création** : Mars 2015.

- **Date de publication** : {sub-ref}`today`.

- **Statut du document** : En travaux.

- **Résumé** : Ce document compare deux attitudes de l'enseignant couramment rencontrées et débattues dans le champ de l'éducation : l'enseignant activateur et l'enseignant facilitateur, reliées respectivement à une approche explicite *vs*. constructiviste et en tire quelques conclusions.

- **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.

- **Note** : Une grande partie des informations ci-dessous provient des sources suivantes :

  > - Cours de C. Nurra & C. Pobel-Burtin, Espé, Univ. Grenoble Alpes.
  > - D'Ham, C., Cosnefroy, O., & Dessus, P. (2013, 12 février). Les élèves mènent l'enquête : L'enseignement des sciences aujourd'hui. *Conférence à MidiSciences*. Grenoble : Univ. Joseph-Fourier. [Vidéo de la conférence](http://podcast.grenet.fr/episode/les-eleves-menent-lenquete-lenseignement-des-sciences-aujourdhui/)

- **Licence** : Document placé sous licence *Creative Commons* : [BY-NC-SA](http://creativecommons.org/licenses/by-nc-sa/3.0/fr/).
:::

% VOIR ENS-explicitation chez Gagné, Briggs etc.

% http://www.cea-ace.ca/fr/education-canada/article/arrêtons-la-guerre-des-maths

% {Fiorella, 2015 #10275}

% https://www.democratisation-scolaire.fr/spip.php?article287

% https://twitter.com/BidaultBenjamin/status/1231471776884043777?s=20

% {Mourshed, 2017 #22142}

## Introduction

Pour débuter, demandons au lecteur de réaliser l'expérience de pensée suivante. Votre appareil informatique préféré affiche un message bizarre à chaque mise en marche. Vous avez l'idée d'appeler l'une de vos amies, connaissant très bien l'informatique, qui peut avoir l'un des deux comportements :

> - Elle manipule l'appareil jusqu'à trouver le problème et à démarrer l'appareil sans problèmes. Elle vous explique sa cause en détail, avec des mots simples, jusqu'à ce que vous ayez compris, et vous corrige si vous allez dans la mauvaise direction.
> - Elle vous laisse devant l'appareil en vous posant des questions du type : "à quoi vois-tu que cet appareil est en panne ?", "que pourrais-tu faire pour qu'il démarre ?", "essaie encore".

Laquelle de ces deux attitudes pensez-vous la plus propice à vous permettre la compréhension du problème de votre appareil ? Probablement la première : "des apprenants de tous âges savent comment construire des connaissances à partir d'informations adéquates et il n'est pas prouvé que les confronter à des informations partielles améliore leur capacité à construire une information d'une façon supérieure à si on la leur présentait entièrement" {cite}`kirschner06` p. 78. Toutefois, cet avis ne tiendrait-il pas compte du type de connaissance ? Si le problème était plus complexe (p. ex., si l'appareil avait une panne plus difficile à réparer), ne faudrait-il pas adopter plutôt la deuxième démarche ?

Tout le monde s'accorde sur l'idée qu'il est payé pour aider ou guider les élèves dans leur apprentissage. Mais on s'accorde moins sur la manière la plus adéquate de les aider, notamment lorsque les habiletés à faire acquérir sont complexes. Il existe un débat important sur deux attitudes très différentes, fondées sur des principes théoriques eux aussi différents, et qu'on oppose souvent : l'enseignant comme un *activateur* (en anglais "*sage on the stage*") ou comme un *facilitateur* (en anglais : "*guide on the side*") {cite}`hattie14`.

- l'enseignant comme *activateur* (ou encore, expositif, explicite) est vu comme un enseignant explicitant le plus clairement possible le contenu à enseigner, pouvant servir de modèle, donnant des tâches très définies en donnant des exemples sur la manière dont on les résout et des exercices d'application, pouvant poser des questions aux élèves, offrant un guidage et des rétroactions continues à l'apprenant.
- l'enseignant comme *facilitateur* (ou encore, constructiviste) est vu comme un enseignant offrant des tâches complexes aux apprenants, travaillant souvent en petits groupes, sans donner *a priori* d'exemples de résolution pour qu'ils découvrent par eux-mêmes la solution, ne posant pas ou peu de questions, leurs guidages et rétroactions sont limités.

Ces deux attitudes sont fondées sur des principes théoriques très différents, et des conceptions de la connaissance sont considérés comme opposés : l'enseignant activateur considère cette dernière comme exogène et connaître, c'est reconstruire une représentation du monde extérieur ; l'enseignant facilitateur considère que la connaissance est endogène et que connaître, c'est s'adapter au monde extérieur en construisant des structures mentales à partir des structures précédentes {cite}`ernest95,odonnell12`.  En bref, l'approche de l'enseignant activateur s'inspire de principes behavioristes ou cognitivistes ; celle de l'enseignant facilitateur s'inspire de principes (socio-)constructivistes.

L'objet de ce document n'est pas de reprendre en détail les argumentaires de chaque partie, mais plutôt d'analyser point par point comment fonctionnent ces deux approches et quels sont leurs avantages et inconvénients. Plus généralement, son but est d'apporter une réflexion sur des aspects essentiels du métier de l'enseignant et de l'apprentissage : Apprend-on mieux par soi-même ? En découvrant des choses ou bien en se les faisant énoncer ? D'où doivent venir les idées ? (de l'enseignant ou des élèves). En d'autres termes, il pourrait être intéressant de jouer à la fois les rôles de "facilitateur" et d' "activateur".

## Ce que l'on sait

### L'enseignement direct

Voici quelles sont les étapes standard d'une séance d'enseignement direct, elles peuvent s'étaler sur une ou plusieurs séances (voir {cite}`rosenshine10,kirschner06`).

1. L'enseignant explique la situation d'apprentissage et explicite ses objectifs.
2. Il rappelle les prérequis et les éléments révisés dans des séances antérieures.
3. Il fournit des modèles aux apprenants, leur montrant comment résoudre un problème, par exemple (exemples résolus) et verbalise la procédure. Dans cette phase les apprenants ne répètent pas aveuglément les paroles ou gestes de leur enseignant : ils tentent de comprendre les connaissances, ressources, stratégies qu'il utilise.
4. Il met en place une phase de *pratique guidée*, dans laquelle les élèves pratiquent sous le contrôle attentif de l'enseignant (qui leur pose des questions, vérifie leur compréhension, les aide en étayant leur apprentissage).
5. Il met en place une phase de *pratique autonome*, dans laquelle les élèves ont l'occasion de réaliser des exercices voisins de ceux rencontrés dans l'étape précédente.
6. Il n'oublie pas de faire réviser les élèves, et de leur donner les moyens de pratiquer sur d'autres exercices (*e.g.*, à la maison).

Voici les points-clés de cette stratégie d'enseignement : tout d'abord, les objectifs d'apprentissage sont clairement définis (les élèves savent le plus clairement possible ce qu'ils doivent apprendre, et ce sur quoi ils seront éventuellement évalués), les rétroactions aident les élèves à déterminer ce qu'ils doivent après leur tâche en cours, ou en cas d'incompréhension. Les exercices de pratique permettent d'intégrer progressivement des schémas d'action, rendant plus rapide et intégrée l'activité des élèves.

Les inconvénients liés à l'approche de l'enseignement direct sont bien connus et souvent discutés (REF). Elles peuvent laisser les apprenants dans une attitude passive, apprenant par cœur ce que l'enseignant dit et fait, acquérant de ce fait une comprehension de surface, hachée ou parcellaire du matériel à apprendre. Mais, comme l'a formulé Crahay {cite}`crahay99`, on a souvent jeté le bébé avec l'eau du bain, et les interrogations qu'il formule sont importantes : "N'est-il pas souhaitable que l'élève et l'étudiant puissent prendre connaissance des objectifs qui \[leur\] sont assignés \[…\] ? Ne faudrait-il pas exiger qu'à tous les niveaux scolaires l'évaluation terminale soit strictement ciblée sur les objectifs poursuivis ? N'est-il pas bon d'offrir régulièrement aux élèves la possibilité de mesurer l'état de leurs connaissances ? \[…\]"

### Les différents types de méthodes "facilitatrices"

Il n'est pas aisé de caractériser les multiples méthodes du courant constructiviste (fondées sur l'enquête, les problèmes, l'investigation, les tâches complexes,la pédagogie de projet, etc.). On peut les résumer ainsi {cite}`bachtold12,pedaste15` (voir aussi [Document SAPP "Construction de connaissances et apprentissage" \<http://webcom.upmf-grenoble.fr/sciedu/pdessus/sapea/constrconn.html][document sapp "construction de connaissances et apprentissage" <http://webcom.upmf-grenoble.fr/sciedu/pdessus/sapea/constrconn.html]) :

1. Une situation est mise en place et décrite (la situation est autant que possible authentique et répond à une question scientifique) ; cette phase est parfois appelée "orientation" ; les élèves explorent la situation pour la comprendre ;
2. Les élèves se posent des questions (les idées viennent d'eux, ce qui leur permet de s'approprier le problème, de formuler des hypothèses), élaborent des hypothèses, imaginent des solutions possibles. Cette phase est pafrois appelée "conceptualisation".
3. Les élèves formulent quelques éléments de réponse pouvant être des preuves pour répondre aux questions formulées dans l'étape précédente (ils sont actifs, apprennent de leurs erreurs, sont parfois guidés par l'enseignant). Ils peuvent également évaluer des explications alternatives. C'est le cœur de la procédure d'investigation.
4. Les élèves expriment leurs conclusions.
5. Les élèves communiquent leurs résultats, les discutent, ce qui les amène à argumenter, réfléchir

Si l'on considère qui mène l'activité dans les étapes précédentes (l'élève-E ou le professeur-P), on obtient le Tableau I suivant (d'après {cite}`lee11`) :

**Tableau I** - Qui dirige quoi dans les activités "facilitatrices" (P-rofesseur ; E-lève).

| Etapes                   | Trad. | Enquête guidée | Enquête/E | Recherche |
| ------------------------ | ----- | -------------- | --------- | --------- |
| 1. Situation/orientation | P     | P              | P         | P/E       |
| 2. Conceptualisation     | P     | P              | P/E       | E         |
| 3. Investigation         | P     | P/E            | E         | E         |
| 4. Conclusion            | P     | E              | E         | E         |
| 5. Discussion            | P     | E              | E         | E         |

Les avantages liés à ce type d'approche ne manquent pas {cite}`bachtold12,kirschner92,lee13,coquide09` :

- Faire acquérir des connaissances scientifiques d’une manière proche de celle des chercheurs ;
- Donner une image authentique du fonctionnement des sciences ;
- Préparer les futurs scientifiques/citoyens ;
- Faire exprimer plus de créativité dans la recherche d'idées, d'hypothèses ;
- Augmenter la motivation des élèves, leur donner le goût des sciences ;

Toutefois, il subsiste quelques problèmes importants à ce type d'approches : le manque de guidage et de rétroactions, laissant les élèves démunis à propos de ce qu'il faut faire, ou comprendre (l'approche facilitatrice ne faciliterait donc qu'assez peu l'apprentissage des élèves). Or, ces rétroactions sont indispensables pour que les élèves apprennent. D'autres problèmes sont listés ci-dessous.

### Problèmes avec l'approche "facilitatrice"

Tout d'abord, il n'est pas si aisé que cela de définir une "situation authentique", souvent point de départ de tout enseignement de ce type. Petraglia {cite}`petraglia98a,petraglia98b` la définit comme "digne d'être acceptée comme conforme aux faits et à la réalité", mais il convient de ne pas confondre "authenticité" avec :

- *Pertinence-utilité dans le monde réel* : On a pu enseigner la sténo-dactylographie aux lycéens fin XIXe s. pour cette raison {cite}`gardey08` ;
- *Transférabilité* : apprendre à faire des règles de trois n’est pas en soi une activité authentique du seul fait qu’elle peut servir à calculer des crédits ;
- Exactitude (correspondance avec le réel) : apprendre par cœur les composants de la navette spatiale n’est pas non plus une activité authentique

Mais une situation peut-elle être authentique en soi ? N’y a-t-il pas des niveaux d’authenticité ? L’enseignant s’occupe surtout de "pré-authentifier" les contenus, sans toujours penser que l’authenticité est fonction de l’âge et des connaissances des élèves. Les élèves doivent être persuadés que ce qu’ils apprennent est authentique et a une correspondance avec le monde (non pas le monde réel, mais ce qu’ils comprennent de ce monde réel) ; le leur dire ne suffit pas.

Ensuite, la question du guidage est importante. L’élève apprend et l’enseignant peut (au mieux) l’aider. Mais quelle aide, quel guidage lui apporter ?

- Lui dire ce qu’il doit savoir ?
- Le mettre en situation de découvrir par lui-même ?
- À quels moments aider ou ne pas aider ?

Présenter des exemples résolus aux élèves  (décrivant les étapes de résolution de problème pas à pas) les font mieux apprendre que de les faire chercher (voir DOC CHARGE COG).

parler des tâches complexes;

### Quels effets sur l'apprentissage ?

En matière d'éducation, comme le rappelle Hattie {cite}`hattie09`, presque toutes les méthodes "marchent" (très peu de méthodes ont réellement des effets délétères sur l'apprentissage). On devrait donc se poser la question de ce qui marche le mieux. La méta-analyse est une méthode de recherche agrégeant de très nombreux résultats d'études différentes mais partageant la même question de recherche (est-ce que la variable *x* a un effet sur une variable *y*, en comparant plusieurs groupes ?), et permettant de déterminer la taille de l'effet. Le Tableau 1 ci-dessous liste les tailles d'effet des pratiques d'enseignement associées l'activateur vs. le facilitateur. Le site <http://rpsychologist.com/d3/cohend/> permet de comprendre visuellement à quoi correspond cette valeur.

**Tableau 1** - Comparaison des tailles d'effets issues de méta-analyses des deux attitudes (activateur vs. facilitateur), {cite}`hattie14` p. 73. (Ens. : enseignement)

| Enseignant activateur        | effet | Enseignant facilitateur          | effet |
| ---------------------------- | ----- | -------------------------------- | ----- |
| Enseigner l'auto-explication | .76   | Enseignement inductif            | .33   |
| Clarté de l'enseignant       | .75   | Simulation et jeu                | .32   |
| Enseignement réciproque      | .74   | Enseignement fondé sur l'enquête | .31   |
| Rétroactions                 | .74   | Classes plus petites             | .21   |
| Stratégies métacognitives    | .67   | Enseignement individualisé       | .22   |
| Instruction directe          | .59   | Enseignement par le web          | .18   |
| Pédagogie de la maîtrise     | .57   | Ens. fondé sur les problèmes     | .15   |
| Donner des exemples résolus  | .57   | Ens. par la découverte           | .11   |

## Ce que l'on peut faire

### Synthèse des vues

"Il est paradoxal d'offrir de l'information que les apprenants pourraient trouver d'eux-mêmes" ({cite}`lazonder10`, p. 512), mais on peut aussi s'entendre qu'il importe - de vérifier que l'information trouvée n'est pas erronée (et dans ce cas aider l'apprenant) ; - de guider les élèves dans cette phase de recherche d'information.

Nous avons tenté de faire une description de chacune des approches, et ensuite vues de l'approche concurrente (tiré de {cite}`taber10`).

L'enseignant activateur (explicitant) :
: - à faire

L'enseignant facilitateur (constructiviste)
: - L’enseignement comprend des buts plus larges que des objectifs très spécifiques,tels que des compétences transférables ; certains des objectifs les plus importants ne peuvent se réduire à des éléments objectivement testables ;
  - Un enseignement efficace est un processus interactif et dialogique qui construit sur la pensée des élèves, une leçon doit donc comporter de la flexibilité ;
  - Apprendre quelque chose de complexe, spécifiquement dans des domaines non structurés, ne peut apparaître qu'avec du temps pour explorer "l'espace du problème" ; laisser les étudiants développer leur imagination et créativité est un but important de l’éducation ;
  - Enseigner c’est soutenir les élèves en utilisant de nombreuse stratégies et techniques, incluant des problèmes adéquats, l’organisation de groupes de travail, donner des ressources, aider les élèves a expliciter et réfléchir sur leur compréhension, etc., et être une source directe d’information.
  - Il a été montré que (a) les apprenants, partant inévitablement de connaissances initiales différentes, interpréteront l'enseignement différemment, et parviendront à des états de connaissances finaux uniques et (b) qu'un apprentissage significatif arrive après une longue période de temps (même si le changement final paraît soudain) : il est donc peu réaliste de se dire que l’élève acquerra une copie de ce que l’enseignant ou une cible sait, et le rôle de l'enseignant est d'aider le développement de la connaissance : une trajectoire d'apprentissage productive et une compréhension "suffisante" à la fin d'une séquence varieront d'un élève à l'autre.

L'enseignant facilitateur (constructiviste) vu de l'enseignant activateur (explicitant) (tiré de {cite}`taber10`) :

> - Les objectifs d'apprentissage qui sont donnés sont vagues.
> - Les leçons sont non structurées.
> - Les activités sont ouvertes.
> - L'enseignant ne donne que peu d'apport avant/pendant/après les séances.
> - Les élèves se cantonneront, au bout du compte, à leur propre point de vue (qui peut ne pas correspondre à la connaissance attendue).

L'enseignant activateur (explicitant) vu de l'enseignant facilitateur (constructiviste) :
: - Les objectifs découpent la connaissance par tranches, ce qui n'est jamais possible.
  - Les leçons sont des successions d'exposés de l'enseignant, sans participation active des élèves.
  - Les activités ne sont que des répétitions de ce que l'enseignant a fait.
  - Trop de place est donnée à l'enseignant.
  - Les élèves adopteront le point de vue de l'enseignant, sans avoir réfléchi, construit des connaissances.

### Se mettre d'accord : vers l'enseignement intelligent

La distinction entre l'enseignement "activateur" et "facilitateur" ne serait-elle pas une fausse opposition en pédagogie, domaine qui en contient de nombreuses ?

- Dire des choses ne suffit pas ;
- Faire travailler les élèves sans but précis ne suffit pas ;
- Ne pas guider les élèves ne suffit pas

### Quelques exemples de pratiques

Nous recensons ici quelques pratiques qui sont à la fois activatrices et facilitatrices (VOIR DOC SAPP SUR L'EXPLICITATION.

:::{topic} Exemple 1. Lecture silencieuse à partir de l'album *Lulu et la grande guerre* (Grégoire)
Lis les pages p. 7 à 11 et réponds aux questions :
Où se passe l’histoire ?
Quand se passe cette histoire ?
Quelle fête s’apprête-t-on à fêter ?
Comment s’appellent les 2 enfants ?
Pourquoi les gens se rassemblent-ils sur la place du village ?
:::

:::{topic} Exemple 2. Exemple d'une activité de lecture (Lectorino et Lectorinette, Lector, Letrix, Cèbe & Goigoux, REF?)
Les élèves ont un texte, lu en groupe.
Ils ont aussi un ques;onnaire déjà rempli sur ce texte par une élève fictive de CM1 (Nadia).
Les élèves doivent :

- lire le questionnaire et les bonnes réponses de Nadia ;
- pour chaque question, souligner dans le texte les mots ou groupes de mots qui, à leur avis, ont permis à Nadia de trouver la réponse ;
- ne rien surligner s'ils ne trouvent pas de mots qui s'y prêtent.

Q1 : pourquoi Demi-Lune prépare-t-il une lance ? R1 : Il prépare une lance pour essayer de tuer un aigle.
« Demi-Lune prépara ses affaires : un sac de provisions, une couverture et une lance. Aujourd'hui était le grand jour. Il devait tuer un aigle et ramener une plume pour faire preuve de son courage. Il monta sur son cheval et se mit en route. »

- informations dans le texte mais pas collées les unes aux autres : les relier
- Nadia n'a pas recopié le texte mais a écrit « pour essayer de tuer un aigle » (demander aux élèves d'expliquer le raisonnement)
:::

### Quelques conseils pour mettre en place un enseignement explicite

Les encadrés ci-dessous donnent des conseils utiles.

:::{topic} Conseils pour mettre en œuvre un enseignement explicite dans sa classe {cite}`rosenshine10` pp. 6-7.
01. Commencer une leçon par un bref rappel des apprentissages antérieurs.
02. Présenter les nouvelles notions par petites étapes avec une pratique des élèves après chaque étape.
03. Limiter la quantité de notions reçues en une fois par les élèves.
04. Donner des consignes et des explications claires et détaillées.
05. Poser un grand nombre de questions et vérifier la compréhension.
06. Mettre en place des occasions nombreuses de pratique active pour tous les élèves.
07. Guider les élèves au début de la phase de mise en pratique.
08. ‘Penser tout haut’ et proposer des modèles pour chaque étape d’un apprentissage.
09. Fournir des exemples de problèmes déjà résolus.
10. Demander aux élèves d’expliquer ce qu’ils ont appris.
11. Vérifier les réponses de tous les élèves.
12. Apporter systématiquement des commentaires et des corrections. Consacrer plus de temps à donner des explications.
13. Consacrer plus de temps à donner des explications.
14. Fournir un grand nombre d’exemples.
15. Enseigner à nouveau les notions si nécessaire.
16. Préparer suffisamment les élèves pour le travail en autonomie.
17. Accompagner les élèves au début de la phase de pratique autonome.
:::

## Références

```{bibliography}
:filter: docname in docnames
```

[cécile nurra]: https://larac.univ-grenoble-alpes.fr/membre/cecile-nurra
[céline pobel-burtin]: https://larac.univ-grenoble-alpes.fr/membre/celine-pobel-burtin
[philippe dessus]: http://pdessus.fr/
