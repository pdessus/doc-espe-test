(emotions-scolaire)=

# Les émotions en situation scolaire

```{index} single: auteurs; Dessus, Philippe
```

:::{admonition} Information
- **Auteur** : [Philippe Dessus](http://pdessus.fr), LaRAC & Inspé, Univ. Grenoble Alpes.
- **Date de création** : Septembre 2018.
- **Date de publication** : {sub-ref}`today`.
- **Statut** : En travaux.
- **Résumé** : Ce document donne des informations sur le fonctionnement des émotions en contexte scolaire.
- **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.
- **Licence** : Document placé sous licence *Creative Commons* : [BY-NC-SA](http://creativecommons.org/licenses/by-nc-sa/3.0/fr/).
:::

% Evoquer Haidt : l'éléphant et le cornac.

% {Rozek, 2019 #21373}

## Introduction

On croit souvent que les émotions sont des événements qui nous submergent, qu’on a parfois du mal à gérer avec notre intelligence. Dans notre société, un crime est par exemple jugé moins grave s’il a été commis sous l’influence d’une forte émotion (crime passionnel), si on le compare à un crime prémédité, qui impliquerait une planification rationnelle (ce qui suit est repris de {cite}`barrett17`).

Dans le même ordre d’idées, il est aussi couramment dit que notre cerveau aurait une architecture en plusieurs niveaux : un niveau “reptilien”, qui traiterait des informations de base nécessaires à notre survie, un niveau “émotionnel”, qui traiterait des informations émotionnelles, et enfin le cortex, qui traiterait des informations plus froidement, rationnellement. Et que les humains devraient, autant que possible, traiter les informations via le cortex, en essayant de court-circuiter les niveaux précédents.

Si l'on adopte une vue évolutionnaire (ce qui peut être contesté par certains), le  but principal de notre cerveau n’est pas de ressentir ou de penser, ou même de percevoir, mais de garder notre corps en vie, en bonne santé, de manière à ce qu’on puisse survivre et se développer (et finalement se reproduire). À cette fin, le travail du cerveau est de continuellement prédire ce que nous allons vivre. Ces prédictions deviennent des émotions ressenties, que l'on interprète chez les autres, mais aussi chez soi. Ce n’est bien sûr pas un travail facile.

Pour réaliser ces prédictions, le cerveau récupère des données du monde extérieur via nos cinq sens, mais aussi des données de notre corps (l’interoception). Il utilise l’expérience et les prédictions passées pour réaliser les prédictions les plus précises possible : précédemment, qu’est-ce qui a causé les sensations que vous êtes en train de ressentir, dans des contextes similaires ? Par exemple, notre estomac se tend. S’il est midi et que l'on n’a pas mangé depuis 4 ou 5 heures, il est probable que ce soit une sensation de faim. Mais ne serait-ce pas plutôt une gastro-entérite ? Pour répondre à cette question on a à réfléchir en repensant à des situations proches vécues par le passé : ce n’est pas donné directement, cela nécessite des interprétations.

L’émotion n’est pas une donnée qui nous est imposée, qui survient : l’émotion est une donnée construite par le cerveau, et cette construction dépend d’un grand nombre de paramètres, comme :

- le contexte : on ne peut comprendre une émotion si l’on n’a pas une idée précise du contexte dans laquelle on l’a vécue,
- les mots : on ne peut identifier une émotion si notre cerveau n’a pas les mots pour les nommer ; ainsi, une personne souriante pourrait tout aussi bien être “joyeuse”, “contente”, “réjouie”, ou “reconnaissante” ; une personne triste pourrait tout aussi bien être “en colère”, “indignée”, “amère”, ou “pleine de ressentiment”.
- mais aussi notre interoception, c’est-à-dire les ressentis que l’on perçoit de notre corps : est-on tendu, décontracté, etc. ? Ces informations influencent nos émotions, et aussi notre comportement, de manière pas toujours consciente : par exemple, la sensation de faim influence notre consommation dans les magasins (on dépense moins en nourriture quand on fait ses courses le ventre plein), mais aussi nos décisions (les juges prononcent des peines plus dures lorsqu’ils ont faim).

Ces prédictions ont un but principal, on l’a dit : elles permettent d’interpréter les situations que nous vivons afin de nous maintenir en bonne santé (c’est-à-dire avec un budget corporel, ou “bilan métabolique” positif), à des fins de survie et de reproduction.

## Les émotions de l'enseignant

Il découle de ce qui précède que l'évaluation que va pouvoir mener un enseignant de son contexte d'enseignement va jouer un rôle important dans ses émotions. Le travail de Frenzel {cite}`frenzel09` (p. 138) et ses collègues, décrit ci-dessous et dans la Figure 1. Ils ont élaboré un modèle où un certain niveau de compétences des étudiants en classe, qu'ils peuvent déduire de leurs résultats scolaires et comportement, est subjectivement perçu par les enseignants. Parallèlement à cela, chaque enseignant a des idéaux d'enseignement, des normes, qui lui permettent de définir, pour la classe donnée, un ensemble de buts en termes de comportements et connaissances.

À la suite de cela, l'enseignant fait en permanence des constats, des estimations, à partir des comportements et performances des étudiants, qu'il essaie de comparer avec les buts préalablement définis. Les questions qu'il se pose à propos de la cohérence résultats/buts, mais aussi la manière dont il peut essayer d'influer dessus, lui amènent des émotions. Ces émotions, en retour, influencent le comportement d'enseignement de l'enseignant et ce comportement, enfin, influence les conditions de classe.

```{image} /images/emotions-classe.jpg
:align: center
:scale: 70 %
```

**Figure 1**. Un modèle des relations réciproques entre émotions de l'enseignant, enseignement, et comportement des étudiants.

## Références

```{bibliography}
:filter: docname in docnames
```
