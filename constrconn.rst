.. _constr_conn:

======================================================
Construction sociale de connaissances et apprentissage
======================================================

.. index::
    single: auteurs; Dessus, Philippe
    single: auteurs; Besse, Émilie
    
.. admonition:: Information

   * **Auteurs** : `Philippe Dessus <http://pdessus.fr/>`_, Espé & LaRAC, Univ. Grenoble Alpes. Le quizz a été réalisé par Emilie Besse, projet `ReFlexPro <http://www.idefi-reflexpro.fr>`_.

   * **Date de création** : Avril 2005, mis à jour en avril 2015.

   * **Date de publication** : |today|.

   * **Statut du document** : Terminé. 

   * **Résumé** : Ce texte détaille des formes d'apprentissage, centrées sur la compréhension et la construction de connaissances, partiellement ou entièrement sociales (c'est-à-dire, prenant en compte les interactions enseignant-élèves ou entre pairs). Deux théories sont présentées : celle de la construction de connaissances (Bereiter et Scardamalia) et celle des communautés de pratique (Wenger).

   * **Note** : Une partie de ce document reprend des extraits de :cite:`pradeau13`.

   * **Voir aussi** : Document :ref:`org_travail_groupe`, document :ref:`demarche_sci`.
   
   * **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.

   * **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.


Introduction
============

"Il est incollable sur la vie de \*** [nom à remplacer par telle ou telle vedette en vue] !" est une réflexion perplexe que de nombreux enseignants peuvent faire, rêvant que leurs élèves pourraient passer autant de temps à travailler sur leur discipline qu'ils en passent à se renseigner à propos de la vie privée de stars, de vélomoteurs ou encore de vêtements. Effectivement, ces élèves apprennent, sans même s'en rendre compte, tant d'informations dans ces domaines non scolaires qu'ils en deviennent experts. Pourtant, ces mêmes élèves peuvent être en tel échec scolaire que leurs enseignants les déclarent "incapables d'apprendre". Que se passe-t-il ? Bien évidemment, stars, mécanique et vêtements ont des attraits plus importants, pour ces élèves, que les disciplines scolaires. Mais est-ce vraiment la seule différence ? Que se passerait-il si les enseignants, au lieu de passer beaucoup de temps à présenter des informations factuelles, essayent de faire en sorte que leurs élèves construisaient des connaissances ? Ce document fait le point sur cette question.


Ce que l'on sait
================

Construire des connaissances (la théorie de Bereiter et Scardamalia)
--------------------------------------------------------------------

L’esprit-conteneur *vs*. la construction de connaissances
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

La psychologie de sens commun, souvent présente dans le domaine de l'enseignement, considère qu'apprendre, c'est acquérir des contenus de savoirs par les sens, qui sont ainsi stockés en mémoire et peuvent être restituables au besoin (*e.g.*, lors d'évaluations). Dans cette acception, apprendre, c'est acquérir du savoir comme si on acquerrait de l'argent, ou des biens matériel. Ce savoir est considéré comme une copie assez fidèle de celle de l'enseignant.  Il existe au moins une autre théorie, dans laquelle on explorerait, fréquenterait ces contenus, comme s'ils étaient extérieurs à nous, qu'on en inventerait de nouveaux et que, ce faisant, on apprendrait, incidemment. Ce savoir, ici, n'est pas nécessairement une copie fidèle de celui de l'enseignant. Ce qu'il importe c'est que l'on passe du temps à sa fréquentation, comme l'élève qui fréquente la vie privée des stars et la mécanique des vélomoteurs. 

Dans les théories exposées plus bas, il ressort que posséder une information (en tant que fait), ce n'est pas nécessairement l'avoir comprise. La compréhension joue donc un rôle crucial dans la théorie de la construction de connaissances (voir :cite:`alamargot01` pour une revue en français de ces différents modèles de l'apprentissage). De plus, chez Bereiter comme chez Wenger, l'apprentissage est considéré comme *incident*

Comprendre le chien de Newton de la même manière que sa théorie
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Cette dernière théorie amène à considérer les savoirs comme des objets matériels qu'il est nécessaire de reconstruire, interpréter, recréer, améliorer, sans pour autant qu'on ait à les apprendre intentionnellement (en d'autres termes, naturaliser les objets abstraits). Ici, les savoirs sont accédés directement plutôt que reconstruits à partir de faits. Pour continuer la comparaison avec les objets qui passionnent les adolescents, aucun d'entre eux n'est appris intentionnellement : ces derniers les fréquentent journellement, lisent une presse spécialisée, les voient à la télévision ou en concerts, et le résultat est qu'ils connaissent vraiment beaucoup de choses sur ces objets, qui en viennent à être réels pour eux, et qu'ils y accèdent directement. Ils peuvent par exemple aisément deviner ce qu'un tel ou une telle penserait ou ferait dans telle ou telle circonstance, de même qu'un expert en chimie est capable, intuitivement, d'imaginer le résultats de réactions chimiques.  En d’autres termes, il faudrait arriver que, par exemple, la théorie de Newton soit aussi réelle, pour les élèves, que son chien : comprendre la théorie de la mécanique newtonienne pourrait passer par des états d’esprit voisins de ceux qu’on aurait si l’on se préoccupait de comprendre son chien, c’est-à-dire (:cite:`bereiter96`, p. 498 *et sq*.) :

-  se préoccuper d’agir intelligemment par rapport au chien,

-  se préoccuper d’expliquer les aspects du chien pouvant poser des problèmes,

-  être conscient des limitations de ses propres compétences par rapport à ces deux précédents points, ainsi qu’avoir une disposition à vouloir les améliorer.

Dans un travail plus récent, Bereiter :cite:`bereiter02` revient sur ce phénomène de compréhension, pour détailler onze caractéristiques principales qui permettent de la définir (voir Encadré 1 ci-dessous).

.. topic:: Encadré 1 - Principales caractéristiques de la compréhension :cite:`bereiter02`.

   - La compréhension est liée à la relation que nous entretenons avec le contenu.

   - La compréhension est intimement liée à – tout en étant différente de – notre capacité à utiliser et traiter le contenu intelligemment.
   
   - La compréhension est intimement liée avec l’intérêt.
   
   - Comprendre un contenu, c’est comprendre ses liens avec d’autres choses.
   
   - Comprendre un contenu n’est pas nécessairement lié à une capacité à l’expliquer. L’explication est une indication de compréhension, mais c’est surtout un moyen de la développer et partager.
   
   - Bien qu’il n’y ait pas de compréhension correcte, complète ou idéale d’un contenu, il y a des compréhensions fausses, qui sont potentiellement corrigeables.
   
   - Les discussions dans le but de comprendre un contenu ne sont pas centrées sur les états mentaux de leurs participants, mais sur l’utilisation de ce contenu, ainsi que de ses relations avec d’autres choses.
   
   - Une manière importante de comprendre un contenu se fait par la délibération, par le partage d’avis concernant les problèmes liés à ce dernier.
   
   - Avoir une compréhension profonde d’un contenu, c’est comprendre des choses profondes à son propos : pourquoi est-il conçu ainsi, ses fonctions, les principes physiques liés, etc.

   - Une compréhension profonde d’un contenu est clairement démontré par la résolution de problèmes le concernant.

   - Une compréhension profonde d’un contenu revient normalement à une implication profonde et complexe à propos de ce contenu.


La communauté de pratiques (Wenger)
-----------------------------------

Une autre théorie a été formulée par E. Wenger à la fin des années 1990 et considère l'apprentissage comme une activité informelle située dans des pratiques collectives de travail (ou de la vie de tous les jours), au cours d'interactions régulières visant à négocier le sens de ce travail :cite:`wenger98,pradeau13`. Nous appartenons ainsi tous à de nombreuses communautés différentes (sportives, culturelles, de loisir, etc.) et notre activité en leur sein n'est pas d'apprendre à proprement parler, mais y participer permet de générer une compréhension de plus en plus approfondie (voir ci-dessus) d'un domaine. Les membres d'une communauté de pratiques (CdP) sont davantage unis par leurs connaissances ou centres d’intérêt que par un objectif collectif spécifique à atteindre, comme ce peut être le cas dans une équipe de travail. 

Même si les CdP peuvent être créées intentionnellement, leur particularité est d’émerger de façon spontanée, leurs membres s’auto-organisant et collaborant en dehors des cadres définis institutionnellement (partageant informellement des astuces, des procédures de travail, des idées). Avec l'arrivée des réseaux sociaux via internet, ou même des sites d'enseignement à distance, l'étude des "communautés d'apprentissage virtuelles" s'est développé :cite:`gannon07`. Apprendre dans une CdP, c'est pratiquer, participer, à propos d'un domaine dans une communauté.

Qu'est-ce qu'une pratique ? Une pratique diffère d'une activité ou d'une culture en ayant les dimensions suivantes. Elle nécessite :

- *un engagement mutuel des participants*, les amenant à travailler et négocier ensemble le sens de leur action ;

- *une entreprise commune*, rendant ses participants mutuellement engagés dans des buts communs et coresponsables de leur poursuite ;

- *un répertoire partagé*, c'est-à-dire, un ensemble de manière de faire et d'objets ou ressources partagés, rendant les pratiques plus cohérentes et aisées.
  
La notion de *participation* est centrale dans les CdP : tout membre d'une CdP est défini par son niveau d'engagement, c'est-à-dire son implication dans l'aide qu'il apporte aux autres membres (être membre, c'est aider et se faire aider). Ainsi, certains agissent au cœur de la communauté (porter des projets, interagissent fréquemment avec les autres) alors que d'autres restent à la marge (participent moins fréquemment, observent les échanges).

Un *domaine* est simplement un intérêt partagé dans la CdP, qui la tient liée. Cela peut être un métier, un loisir, une discipline, etc.

L'intérêt de transposer la notion de CdP dans le domaine scolaire est visible, tant un enseignant et ses élèves peuvent partager des centres d'intérêt communs (les matières), et adopter des modes de fonctionnement proches de ceux rencontrés dans des CdP hors scolaire (créer un répertoire de trucs et astuces, discuter et approfondir des connaissances).

Ce que l'on peut faire
----------------------

À propos de la construction de connaissances
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Ces caractéristiques peuvent amener l'enseignant à faire en sorte que les situations d'apprentissages favorisent les éléments suivants :cite:`murphy97` :

-  *Perspectives multiples* : les problèmes discutés sont présentés selon des perspectives (points de vue) multiples, et discutés collaborativement.

- *Buts dirigés par les apprenants* : ce sont les élèves qui planifient les discussions, les recherches.

- *L'enseignant est un guide* : l'enseignant aide les élèves à reconnaître des connexions entre connaissances, faits, questions. Ils discutent des incohérences, suggèrent des sources d'informations. L'enseignant n'essaie pas d'être un expert dans la classe, mais plutôt d'être un modèle d'un apprenant expert. 

- *Activités authentiques* : les élèves reproduisent à leur niveau une enquête scientifique telle qu'elle serait conduite par un chercheur. 

- *Construction de connaissances* : Le but principal est de construire une base de connaissances. Les élèves se centrent sur la construction plutôt que la reproduction de connaissances. 

- *Connaissances en collaboration* : les élèves collaborent à cette construction, partagent leurs résultats, commentaires, suggestions. 

- *Considérer positivement les erreurs* : le travail des étudiants est commenté et questionné par les autres, afin de l'améliorer, le raffiner.

L'un des moyens, sans être le seul, est d'engager les élèves dans un processus de construction de connaissances circulaire, proche de celui ci-dessous :

.. image:: /images/conststahl.jpg
   :scale: 100 %
   :align: center

**Figure 1** - Construction collaborative de connaissances :cite:`stahl00`

 Processus qui peut se décrire ainsi :

#. Exprimer des opinions à propos de la question de départ en les justifiant au minimum "je pense que... car...".

#. Classement thématique des différentes opinions, établissement de liens entre elles.

#. Emergence de plusieurs perspectives, à partir du classement. Comparaison des différentes perspectives. Une perspective est une théorie qui explique l'ensemble des opinions.

#. Clarification du sens des idées exprimées dans chaque perspective.

#. Négociation. On établit la perspective du groupe.

#. Ecriture collective d'un résumé de cette perspective.

Voici maintenant un extrait de discussion à propos de biologie recueillie dans un logiciel de construction collaborative de connaissances, CSILE (:cite:`scardamalia96` p. 157), les lettres
au début de chaque ligne signifient respectivement P : Problème ; MT : Ma théorie ; JBC : j'ai besoin de comprendre ; NI nouvelle information ; les lettres en fin de ligne sont les initiales des élèves :

.. topic:: Encadré 2 - Le fonctionnement d'une cellule (:cite:`scardamalia96` p. 157).

   - P. Comment une cellule fonctionne ? (AK)
   - MT. Je pense qu'une cellule fonctionne en faisant entrer de l'oxygène et la cellule peut faire son travail en respirant (AR).
   - MT. Je suis d'accord avec ta théorie, mais quand la cellule fonctionne, je ne pense pas qu'elle respire. Je pense que respirer, c'est quand tu inspires de l'oxygène. (JD)
   - MT. Je pense que la cellule fonctionne grâce aux "choses" qu'elle contient (organelles) (AK).
   - JBC. Comment l'oxygène entre dans la cellule, si la cellule respire réellement de l'oxygène ? (AR).
   - MT. Je ne pense pas que la cellule respire de l'oxygène. Je pense juste que la cellule a besoin d'oxygène pour faire son travail. Mais si la cellule respire vraiment de l'oxygène, je pense qu'il y a quelque chose comme un tube dans la cellule qui lui permet de récupérer l'oxygène dont elle a besoin (AK).
   - NI. J'ai trouvé quelque part que la cellule prend de la nourriture et de l'oxygène par la membrane. Cela arrive régulièrement. La cellule transforme alors la nourriture et l'oxygène en énergie. Elle utilise cette énergie pour faire son travail. (AR).
   - JBC. Comment la nourriture et l'oxygène sont récupérées par la membrane ? (AR)
   - MT. Je pense qu'il y a des tubes très petits qui mènent à chaque cellule et que la nourriture et l'oxygène passent par ces tubes pour aller dans la cellule par sa membrane (AR).
   - MT. Je ne suis pas d'accord avec ta théorie, Andrea. Je pense que l'oxygène et la nourriture passent dans la cellule automatiquement, par un processus quotidien. (AK).
   - JBC. Qu'est-ce que l'oxygène fait lorsqu'il va dans la cellule ? (AK).
   - MT. C'est ce que je pense que l'oxygène fait lorsqu'il va dans la cellule. Je pense que l'oxygène va dans la cellule par la membrane et va alors dans le noyau où il est transformé en énergie. (AR). 

A propos des communautés de pratique
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Des auteurs :cite:`wenger02` cités par :cite:`bates15` ont identifié sept principes fondamentaux pour créer et entretenir des communautés de pratiques :

#. *Penser et prévoir l'évolution de la CdP* : s'assurer que la communauté va pouvoir évoluer et prendre en compte les possibles changements d'intérêt de ses membres, sans toutefois s'en éloigner trop ;

#. *Ouvrir un dialogue entre des perspectives internes et externes*, pour encourager la discussion de nouvelles perspectives amenées de l'extérieur de la CdP ;

#. *Encourager et accepter différents niveaux de participation*, du cœur (les participants les plus actifs) à la périphérie, ces derniers pouvant, le cas échéant, s'impliquer plus avant si les discussions ou le sujet les intéresse ;

#. *Développer des espaces de communauté publics et privés*, de manière à ce que la CdP favorise à la fois des formes de participation individuelles (p. ex., maintenir un blog de son activité) et également privées (p. ex., faire des vidéoconférences régulières) ;

#. Se centrer sur la valeur, la CdP doit identifier explicitement, par des discussions, les contributions qui sont les plus importantes pour la communauté, et les valoriser ;

#. *Combiner familiarité et défis*, la CdP doit se centrer à la fois sur des perspectives communes et "standard", mais aussi se donner des défis (à discuter ou pour agir) qui vont motiver ses membres ;

#. *Créer un rythme*, il est important de créer des activités régulières, permettant aux membres de la CdP de travailler collectivement tout en tenant compte de leurs contraintes temporelles et leur intérêt.

Quizz
=====

.. eqt:: Constrconn-1

   **Question 1. Quelles dimensions caractérisent une pratique ?**

   A) :eqt:`C` `Un engagement mutuel des participants; Une entreprise commune et Un répertoire engagé`
   B) :eqt:`I` `Un même domaine d'activité ; Un intérêt partagé et Un répertoire engagé`
   C) :eqt:`I` `Une même manière de travailler ; Un engagement mutuel des participants et Un même domaine d'activité`
   D) :eqt:`I` `Un nombre très restreint de participants ; Une même manière de travailler et Une entreprise commune"`

.. eqt:: Constrconn-2

   **Question 2. Selon Wenger et al. (2002) lequel des principes suivants est fondamental pour créer une communauté de pratiques ?**

   A) :eqt:`I` `S'assurer d'une participation homogène`
   B) :eqt:`C` `Combiner familiarité et défis`
   C) :eqt:`I` `Prévoir un petit nombre de participants`
   D) :eqt:`I` `Ne pas anticiper l'évolution de la communauté de pratiques`

.. eqt:: Constrconn-3

   **Question 3. Selon Wenger et al. (2002) lequel des principes suivants est fondamental pour créer une communauté de pratiques ?**

   A) :eqt:`I` `Prévoir un petit nombre de participants`
   B) :eqt:`I` `Ne pas anticiper l'évolution de la communauté de pratiques`
   C) :eqt:`I` `S'assurer d'une participation homogène`
   D) :eqt:`C` `Encourager et accepter différents niveaux de participation`


Analyse des pratiques
=====================

#. Décrivez une séance que vous avez menée dans laquelle les élèves ont pu mettre en place une activité de construction de connaissances. Quel est le rôle de l'enseignant dans une telle situation ?

#. Concevez une séance de construction collaborative de connaissances en utilisant le modèle de Stahl ci-dessus.

#. Si vous animez une Communauté de pratique, vérifiez que vous prenez bien en compte les sept principes pour l'entretenir (voir ci-dessus). 

Références
==========

.. bibliography::
   :cited:
   :style: apa

