% https://bokhove.net/2017/03/16/social-network-analysis-applications-for-education-research/

(sociometrie)=

# Réseaux d'affinités en classe, approche sociométrique

```{index} single: auteurs; Dessus, Philippe single: auteurs; Besse, Émilie
```

:::{admonition} Information
- **Auteurs** : [Philippe Dessus](http://pdessus.fr/), LaRAC & Inspé, Univ. Grenoble Alpes.  Le quizz a été réalisé par Émilie Besse, projet [ReFlexPro](http://www.idefi-reflexpro.fr).
- **Date de création** : Octobre 2000, révisé en octobre 2001.
- **Date de publication** : {sub-ref}`today`.
- **Statut du document** : Terminé.
- **Résumé** : S'initier à l'approche sociométrique pour mettre au jour les réseaux d'affinités dans une classe. Ce document traite de l'analyse des affinités des élèves au sein d'une classe. Son objet est de construire un sociogramme, qui rassemble les principaux liens interélèves, en termes de choix et de rejets.
- **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.
- **Licence** : Document placé sous licence *Creative Commons* : [BY-NC-SA](http://creativecommons.org/licenses/by-nc-sa/3.0/fr/).
:::

## Introduction

La sociométrie est une "méthode d'approche qui se veut métrique, du réseau des relations interpersonnelles au sein d'un groupe " ({cite}`visscher91` p. 113). L'outil principal, mis au point par Moreno, en est le sociogramme : "diagramme présentant symboliquement et schématiquement l'ensemble des choix et rejets obtenus lors de l'application d'un test sociométrique" (*ibid*.). Élaborer un sociogramme d'un groupe-classe, cela permet à l'enseignant de mettre au jour le réseau d'affinités qu'il a pu percevoir intuitivement. Un sociogramme se construit en faisant passer un questionnaire à chaque élève, où il indique les élèves avec qui il aime travailler et ceux avec qui il n'aime pas. Ensuite, une mise à plat de tous les choix permet de se rendre compte des choix et rejets réciproques, des oppositions (*A* choisit *B* qui le rejette). Un tel travail peut être utile à l'enseignant :

- pour constituer des groupes de travail respectant ce réseau (notamment en début d'année scolaire) ou, au contraire, essayant de l'étendre ;
- pour se rendre compte d'éventuels rejets, phénomènes à propos desquels l'enseignant doit être vigilant, même s'il n'est pas ici question de donner des pistes pour les résoudre.
- pour mieux comprendre la relation éducative qu'un enseignant tisse tout au long de son travail avec chacun de ses élèves. Être conscient de la place de chacun au sein du groupe permet assurément à l'enseignant de mieux travailler avec ses élèves.

Toutefois, il importe bien de comprendre qu'un sociogramme ne témoigne que *d'un état*, prélevé à un instant donné, de la situation de chacun dans le groupe. Nous encourageons l'enseignant d'être prudent quant à l'information qu'il peut tirer d'un tel outil.

Tout d'abord, le sociogramme peut avoir comme effet pervers de réifier, conforter chez l'enseignant la position d'un élève au sein d'un groupe : "oui, Untel est ici, je le savais, mais il l'a bien cherché..." L'enseignant gardera à l'esprit que cet outil n'est justement qu'un outil produisant des données chiffrées, souvent fascinantes et qu'il ne se substitue pas à son jugement. Ensuite, les résultats issus de cet outil ne doivent être qu'exceptionnellement diffusés aux élèves, car ils peuvent contenir des informations pouvant perturber psychologiquement certains (que peut penser un élève se voyant à l'écart d'un sociogramme ?). Il nous semble donc qu'un tel outil ne doit être qu'à l'usage exclusif de l'enseignant ; cela dit, il importe tout de même, par déontologie, de fournir un résultat général, p. ex. les groupes constitués à l'aide du sociogramme.

La mise en garde de Vasquez et Oury :cite:vasquez71\`est à ce sujet éloquente :

: "Avouons aussi certaine réticence \[à faire passer des sociogrammes\], d'ordre éthique cette fois, qui explique notre refus de savoir. Si pour vous les enfants sont des êtres humains, vous vous interdirez certaines questions. Leur vie privée, leurs choix affectifs, tout ce qui de près ou de loin touche au sexe appartient au sacré et n'a pas à être déballé en place publique. Les risques sont importants : le moindre serait de rendre inutile toute tentative d'expression personnelle de ces sentiments justement si difficiles à exprimer." (*id*., p. 517)

Cela posé, les auteurs ont longuement utilisé les sociogrammes dans diverses classes d'adolescents, en mileu institutionnel, sans divulguer les scores de chaque élève, ce que nous préconisons également.

Cela étant, il peut être intéressant, pour le bon fonctionnement du travail en petits groupes, de réaliser de tels sociogrammes, en se centrant exclusivement sur des critères de travail. La question posée serait : "Avec quels élèves préfères-tu travailler la matière *X* en petit groupe ?", et surtout pas : "Quels élèves préfères-tu ?", "À quels élèves voudrais-tu confier un secret ? ", "Avec quels élèves aimes-tu jouer ?", questions qui ont trait à la vie privée de l'élève et que l'enseignant doit s'interdire de poser.

Ces précautions étant énoncées, l'enseignant ne réalisera de tels sociogrammes que s'il le juge utile et surtout seulement s'il pense que cela ne perturbera pas le bon fonctionnement de son groupe-classe.

Ce document n'a pas pour objet d'exposer les aspects théoriques de la sociométrie (voir {cite}`ancelin72,vasquez71`, qui en font une utilisation dans le cadre de leurs classes coopératives, Vayer et Roncin {cite}`vayer87` réalisent le même type de travail en école primaire).

## Ce que l'on peut faire

### Expression des choix

1. Expliquer aux élèves que le questionnaire qui va suivre "va nous aider à former les équipes de travail en petits groupes".
2. Demander à chaque élève d'écrire son nom sur une feuille, puis de mentionner trois camarades avec lesquels il (elle) pourrait travailler en petit groupe, dans la matière concernée. Il (elle) doit penser aux compétences des camarades nommés, mais aussi aux relations qu'il (elle) a avec eux. Si un élève est absent, il peut bien évidemment être choisi.
3. S'il le désire, *et seulement s'il le désire*, l'élève peut mentionner trois camarades avec qui il ne pourrait pas travailler en petit groupe, toujours dans la matière concernée.
4. L'enseignant rappelle qu'il exposera les résultats de ce travail la séance suivante, en respectant bien sûr l'anonymat de chacun.
5. Ramassage des papiers.

### Traitement des données

1. Une sociomatrice est créée en dépouillant tous les résultats, selon l'exemple fictif suivant, où les choix sont mentionné par un "1" et les rejets par un "-1" ; le sexe (En effet, on estime que les élèves ne sont capables d'effectuer des choix réciproques entre garçons et filles qu'à partir de 14-15 ans {cite}`vayer87`).

**Tableau I -- Un exemple fictif de sociomatrice.** Ici, Adeline a choisi Béatrice et a rejeté Kevin, qui l'a choisie. L'indice de "sociabilité" de chaque personne est calculé en sommant tous ses scores.

|             | Sexe | Adeline | Béatrice | Johnny | Kevin | Mickaël |
| ----------- | ---- | ------- | -------- | ------ | ----- | ------- |
| Sexe        |      | F       | F        | M      | M     | M       |
| Adeline     | F    |         | 1        |        | -1    |         |
| Béatrice    | F    | 1       |          |        |       | -1      |
| Johnny      | M    |         | -1       |        |       | 1       |
| Kevin       | M    | 1       |          |        |       | -1      |
| Mickaël     | M    |         | 1        |        | -1    |         |
| Sociabilité |      | 2       | 1        | 0      | -2    | -1      |

2. On peut observer :

- les élèves beaucoup rejetés (ici Kevin),
- les élèves mentionnés par personne (ici Johnny) ;
- les groupes d'élèves qui se choisissent mutuellement (ici Adeline et Béatrice) ;
- les antagonismes, un élève rejeté par celui (celle) qu'il choisit (ici Kevin et Adeline) ;
- les *leaders*, ayant des scores élevés (ici Adeline).

3. On peut réaliser un sociogramme en forme de cible, groupant, par sexe, les élèves par score de sociabilité. On pourra regrouper les élèves ayant des scores voisin dans la même tranche de cible (*e.g.*, les scores entre 2 et 4, etc.), en distinguant bien sûr les scores positifs et les négatifs. Voici le sociogramme pour l'exemple ci-dessus, les sujets le plus au centre étant les plus populaires :

```{image} /images/sociometrie1.jpg
:align: center
```

**Figure 1 -- Sociogramme fictif tiré de l'exemple ci-dessus.**

4. On peut également représenter les différents réseaux au sein de la classe de la manière suivante. Il convient de placer les élèves les plus choisis au centre du graphique, les autres, selon les choix, en périphérie et de mentionner par des flèches les différents choix. Voici une représentation sous cette forme de la sociomatrice ci-dessus.

```{image} /images/sociometrie2.jpg
:align: center
```

**Figure 2 -- Autre représentation du sociogramme ci-dessus.**

5. Passons maintenant à la constitution de groupes de travail, but de ce sociogramme. On pourra les constituer en plaçant un *leader* par groupe ; en vérifiant que le nombre de rejets intragroupe est minimal ; en s'assurant de la mixité ; en veillant à ce que les élèves rejetés ne se retrouvent pas dans le même groupe ; les élèves non cités, eux peuvent être placés à volonté. On pourra réaliser une sociomatrice à chaque événement important de la vie de la classe (séjour, vacances).
6. Autres calculs : À titre de comparaison entre classes, on peut également calculer un indice de cohésion d'une classe ({cite}`ancelin72`) :

$$
\frac{\sum\nolimits_C}{n(n-1)/2}
$$

où *n* est le nombre total d'élèves et *C* le nombre de choix réciproques.

## Quizz

```{eval-rst}
.. eqt:: Sociometrie-1

        **Question 1. Qu'est-ce qu'un sociogramme ?**

        A) :eqt:`C` `Un diagramme présentant l’ensemble des choix et rejets obtenus lors de l’application d’un test sociométrique`
        B) :eqt:`I` `Un document recueillant les définitions des notions d'un test sociométrique`
        C) :eqt:`I` `Un schéma représentant les choix et rejets obtenus lors de l'application d'un test sociométrique`
        D) :eqt:`I` `Un outil de mesure utile lors d'un test sociométrique`
```

```{eval-rst}
.. eqt:: Sociometrie-2

        **Question 2. Quel est le principal risque de l'utilisation d'un sociogramme ?**

        A) :eqt:`I` `Que l'enseignant maîtrise mal l'outil`
        B) :eqt:`I` `Que les élèves ne répondent pas sincèrement`
        C) :eqt:`I` `De ne pas fonctionner`
        D) :eqt:`C` `De conforter chez l'enseignant la position d'un élève au sein d'un groupe`

```

## Références

```{bibliography}
:filter: docname in docnames
```
