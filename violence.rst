.. _violence_ecole:

=====================
La violence à l'école
=====================

.. index::
    single: auteurs; Dessus, Philippe

.. admonition:: Informations

  * **Auteur** : `Philippe Dessus <http://pdessus.fr/>`_, Inspé & LaRAC, Univ. Grenoble Alpes.
  
  * **Date de création** : Novembre 2001, mis à jour en Février 2009.
  
  * **Date de publication** : |today|.
  
  * **Statut du document** : Terminé.
  
  * **Résumé** :  La violence scolaire fait l'objet de nombreux débats, que ce soit dans les établissements ou dans les médias. Or, ces phénomènes, même s'ils ne sont pas proprement scolaires, mais hérités de la société, doivent être pris en charge par les enseignants. Nous verrons ici de quelle manière. 
  
  * **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.
  
  * **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.


Introduction
============

La question de la violence scolaire est très présente dans les médias et dans certains établissements. Ce phénomène est récent, ce qui amène la question de ses raisons, qui sont dues en partie à certains changements socioculturels (Dubet, 1997 ; Peralva, 1997) : 

* (a) la massification de l'école (70 % des élèves de 20 ans sont scolarisés) l'a fait devenir un enjeu social important, être exclu de l'école signifie être exclu de la société, ce qu'expriment clairement Garcia et Poupeau (2000) : "les élèves dont le comportement rend difficile l'appropriation collective des savoirs comprennent souvent qu'il s'agit moins pour eux d'acquérir des connaissances scolaires que de rester dans l'école. Et que, sauf à être délinquants et " traités " par les forces de l'ordre, on ne leur demande pas vraiment autre chose". ; 
* (b) le statut de l'adulte et donc les relations adulte-enfant ont changé, l'adulte n'est plus porteur de la loi, il peut être déclaré responsable d'erreurs et d'injustices ; 
* (c) la différence entre le statut social des enseignants - issus de classes moyennes - et celui de la majorité des élèves - milieu populaire - engendre des mécompréhensions pouvant dégénérer en violence.

Toutefois, comme l'indique Charlot (1997), il faut déjà définir ce qu'est ce phénomène. Cela amène les questions suivantes, souvent liées à des seuils ou des frontières (*id*.) :

* Quels événements sont "normaux", quels événements sont "pathologiques" ? : Le rapport enseignant-élève, sous certains aspects, implique que le premier est plus fort (plus savant) que le second, ce qui peut déjà être interprété comme une violence.  

* Quels événements doivent être considérés comme des prémices, quels événements sont des faits ? Par exemple, l'absentéisme est-il une violence scolaire ?  

* Quel seuil à partir duquel un phénomène se produisant à l'intérieur d'un établissement peut-il être considéré comme suffisamment fréquent pour caractériser l'établissement comme sujet à violence ?

Ces deux dernières questions sont également valides, non plus à l'intérieur de l'établissement, mais dans le territoire (ville, banlieue) dans lequel est l'établissement. On stigmatise la violence de certaines banlieues comme causes de la violence dans des établissements. Comment distinguera-t-on la violence interne à l'établissement et celle externe ? Par exemple, les bagarres ou le racket devant le portail, l'intrusion de parents ou d'anciens élèves dans l'établissement.

Toujours en suivant Charlot (1997), aux deux extrêmes, nous avons d'un côté une violence qui est "meurtre, coups et blessures avec ou sans arme, viol ou harcèlement sexuel, racket, vandalisme" (*id*., p. 5), c'est-à-dire punie par le Code pénal, des microviolences ou incivilités (Debarbieux, 2006, soit insultes, harcèlement entre élèves, vandalisme, paroles blessantes, grossièretés diverses, bousculades, interpellations, humiliations, racisme ouvert ou diffus). C'est la répétition des microviolences qui, à la longue, fait baisser le sentiment de sécurité des élèves et enseignants et augmente leur stress. Il est donc important de les prendre aussi en compte.  Enfin, le climat scolaire, lorsqu'il se dégrade, peut également être perçu par les protagonistes comme une situation de violence. Enfin, comme l'indique Bègue (2004), la violence scolaire n'est pas seulement la violence des scolaires (délinquance juvénile).

Ce que l'on sait
================

Quelques définitions
--------------------

Le terme de violence, comme nous venons de le voir, recouvre des événements très disparates. Cela incité les chercheurs à utiliser le terme d'"incivilités" pour caractériser certaines faits qui sont " des menaces contre l'ordre établi, transgressant les codes élémentaires de la vie en société, le code des bonnes manières" (Debarbieux, Dupuch & Montoya, 1997, p. 19) Ces incivilités ne doivent toutefois pas être sous-évaluées, car elles contribuent aussi à faire se développer un sentiment d'insécurité, une souffrance. Cela étant il ne doit pas en être déduit qu’ils sont en augmentation : les statistiques montrent qu’aux Etats-Unis, un jeune de 5-19 ans court 70 fois moins de risques de se faire assassiner à l’école qu’en d’autres circonstances (Debarbieux & Blaya, 2008). Roché (2004), après dépouillement de dossiers judiciaires impliquant des mineurs montre que seulement 7 % des faits de délinquance ont eu lieu à l'école. D’autre part, ils ne doivent pas non plus masquer les phénomènes de violence quotidienne, bien moins grave dans les faits, mais qui pèsent bien plus sur la vie des victimes.

Quelques chiffres
-----------------

Le sentiment que la violence dans les établissements scolaires croît est-il confirmé par les faits ? Tout d'abord, il semble bien qu'elle ait toujours existé (voir Lec & Lelièvre, 2007, pour un historique), mais qu'elle soit venue sur le devant de la scène au début des années 1990. De plus, Roché (2004) montre combien il faut être prudent avec les chiffres provenant d'études françaises, qui ne s'entourent pas toujours des précautions méthodologiques qu'il faudrait. Les chiffres dont nous disposons sont les suivants : (Bonafé-Schmitt, 2000), pour l'année 1993, de 771 infractions pour coups et blessures volontaires (CBV) ayant entraîné une incapacité de travail de plus de 8 jours et un homicide. Ces CBV représentent 38 % de l'ensemble des infractions. Sur ces 771, la moitié seulement a été commise au sein des établissements et les trois quarts ont été réalisés sans arme (dans 4 % des cas, usage d'une arme à feu, dans env. 10 %, usage d'une arme blanche). Le racket scolaire représente près de la moitié des infractions, les attentats aux mœurs représentant les 12 % des infractions restants (les chiffres de 1995 sont cités dans Coslin, 1997, ils sont du même ordre : 2 homicides, 982 CVB). Ces chiffres peuvent être considérés comme faibles si on les ramène aux nombre d'élèves (2000 faits pénalisés recensés pour 14 millions d'élèves, Debarbieux *et al.* (*op. cit.*, et Debarbieux, 2006)\ *,* soit 0,0014 % alors que, dans la population française le rapport des crimes et délits constatés est de 6,5 %). Ces statistiques ne prennent pas en compte les violences indirectes (visant par exemple des objets appartenant à la victime). Toutefois, il existe une violence endémique, subjective dont il faut tenir compte. Bonafé-Schmitt (2000) montre que la moitié des élèves estime que leurs camarades ont souvent ou très souvent un comportement agressif (questionnaire diffusé sur 900 élèves du primaire et du secondaire de banlieues de Lyon et de Paris). Des chiffres plus récents et fiables sont maintenant disponibles, grâce au logiciel Signa (voir *Le Monde* du 31 octobre 2001; et surtout DPD, 2001, Houllé, 2006 ; Laïb, 2008). Ils montrent que la moyenne d'incidents recensés dans les établissements scolaires (du primaire et du secondaire) sont de l'ordre de 4 à 5 pour 1 000 élèves, et que ce nombre décroît dans le cours de l'année scolaire 2001. Au cours de cette année 85 000 incidents ont été recensés (dont 95 % dans le secondaire), qui se répartissent en : 27 % de violences physiques sans armes, 23 % d'insultes ou menaces graves, 12 % de dommages aux biens et 11 % de dommages aux biens. Il est surtout à noter que 40 % des établissements qui se sont connectés au logiciel n'ont signalé aucun incident, contre 8 % qui en ont signalé au moins 10. 

Plus récemment encore, le ministère de l’Education Nationale a dévoilé les chiffres de la violence scolaire pour la période allant de décembre 2007 à février 2008 (Laïb, 2008). Ces données ont été recensées par le nouveau système Sivis (Système d’Information et de vigilance sur la sécurité scolaire, qui procède par sondage auprès de 1500 établissements, et préserve l'anonymat des déclarations). Les atteintes à la personne (insultes, violences verbales et physiques) représentent 80,1% des actes recensés, les atteintes aux biens (vols, dégradations des locaux et du matériel) 16,2% et les atteintes à la sécurité (trafic et consommation de stupéfiants, ports d’armes, etc.) 3,8 %. Les auteurs de ces violences sont à 85% des élèves et ont pour victimes 40% d’élèves et 35% de personnels.

Enfin, une étude plus circonscrite de Debarbieux (2006) montre qu'en collège, entre 1994 et 2003, il y a une augmentation limitée de la perception de la violence, même si le nombre de victimes déclarées de violences (racket, dans l'enquête) baisse un peu, mais il y a en parallèle une violence plus grande de chaque agression, commise de plus en plus souvent par un groupe; et cela sans que le rôle de l'enseignant en tant que transmetteur de savoirs ne soit plus remis en question (seulement 10 % des élèves pensent qu'on apprend mal ou très mal dans leur établissement).

Théories de la violence
-----------------------

Bègue (2004) montre qu'il y a trois grands types de théories expliquant la violence scolaire. 

* *La théorie du contrôle social*, qui pose que le respect de la loi par chacun dépend de leur respect des liens sociaux. Un élève qui enfreint la loi est à la fois quelqu'un peu attaché et impliqué dans l'école, et, par circularité, l'est d'autant moins qu'il l'aura enfreinte. Plusieurs types de contrôles sont possibles : le contrôle direct, où des agents (enseignants, éducateurs, parents) énoncent et font appliquer les lois. Le contrôle est en proportion inverse de la délinquance (ce qui explique pourquoi les filles, plus contrôlées par leurs parents, sont statistiquement moins délinquantes). Le contrôle interne, lui, est le jugement que l'individu lui-même porte sur la délinquance, des recherches montrant que les délinquants avaient un niveau de jugement moral moins élevé que les autres.

* *L'apprentissage social*, qui pose que la délinquance découle de l'association avec des modèles délinquants, permettant aux élèves d'acquérir des normes et des techniques de délinquance (voir le Document :ref:`socialisation_pairs`). Le fait que les comportements délinquants se réalisent souvent en groupe en est une trace. Selon cette théorie, les délinquants manquent de contact social et de contacts interpersonnels qui leur montreraient la norme. C'est dans ce cadre que la violence peut être en partie (on l'estime à 5 à 10 %) causée par l'exposition à la violence via médias  (télévision, jeux vidéos).

* *La violence comme effet d'une privation*, est une théorie qui pose que les conduites de délinquance ont pour but de réduire un état affectif désagréable (échec, privation, humiliation). Des recherches ont montré que les élèves insatisfaits de leur orientation scolaire ou en conflit avec le corps enseignant ont plus tendance à avoir des comportements violents que les autres, et ce d'autant plus que l'événement initial aura été perçu comme injuste. Pain (1997, p. 69) résume dans le tableau suivant les différents types de violence (d'après Pain).

**Tableau I** -- Les différents types de violence (Pain, 1997, p. 69).

+------------------------------------+----------+-----------+-----------------------------------------------------------------------------------+
| 1) Violences délinquantes, actives | Physique | Directe   | Atteintes physiques contre les personnes, suicide                                 |
+------------------------------------+----------+-----------+-----------------------------------------------------------------------------------+
|                                    | Physique | Indirecte | Atteintes contre les biens des personnes, de l'institution, vandalisme            |
+------------------------------------+----------+-----------+-----------------------------------------------------------------------------------+
|                                    | Verbale  | Directe   | Atteintes verbales contre les personnes : menaces, outrages, injures, harcèlement |
+------------------------------------+----------+-----------+-----------------------------------------------------------------------------------+
|                                    | Verbale  | Indirecte | Calomnies, médisances, taquineries                                                |
+------------------------------------+----------+-----------+-----------------------------------------------------------------------------------+
| 2) Violences d'attitude, passives  | Physique | Directe   | Exclusion, quarantaine, chahuts, absentéisme                                      |
+------------------------------------+----------+-----------+-----------------------------------------------------------------------------------+
|                                    | Physique | Indirecte | Evitement, présence non participante, sans matériel                               |
+------------------------------------+----------+-----------+-----------------------------------------------------------------------------------+
|                                    | Verbale  | Directe   | Refus de participer, de répondre, de politesse                                    |
+------------------------------------+----------+-----------+-----------------------------------------------------------------------------------+
|                                    | Verbale  | Indirecte | Ne pas prendre part au travail, ne pas communiquer, se taire                      |
+------------------------------------+----------+-----------+-----------------------------------------------------------------------------------+

Ce que l'on peut faire
======================

Tout d'abord, le Ministère de l'éducation nationale a édité récemment une série de brochures très bien faites sur les actions à mener concernant la violence scolaire (DGES, 2006a et b, `Site MEN <http://www.education.gouv.fr/cid2765/la-prevention-et-la-lutte-contre-la-violence.html>`_).

Les actions concrètes à mener pour lutter contre la violence sont difficile à résumer (voir Roché, 2004, pour une bonne synthèse), tant le climat général d'un établissement joue sur cette dernière. Bègue (2004) montre par exemple que les écoles de petite taille, avec des ressources, où il y a un équilibre entre élèves de haute et faible capacités intellecturelles, où les règles de discipline sont claires, équitables, et non extrêmes, qui fournissent des opportunités de réussite à tous,  qui ont des attentes élevées mais réalistes envers les élèves, tout en ayant une attitude positive, où les élèves ont des responsabilités en classe, mais aussi où les enseignants et l'administration travaillent en bonne entente... sont des écoles où la violence scolaire sera réduite.

En revanche, les thérapies de groupes d'élèves délinquants, les conférences sur le respect de la loi, les activités récréatives et une discipline plus rigoureuse ne sont en général pas suivis d'effets (Bègue, 2004).

Quelles sont les stratégies qui amènent moins de violence ? Les punitions et un climat de grande fermeté des enseignants marche-t-il ? On entend souvent dire que parfois "une bonne fessée", et un climat plus sévère dans les établissements et écoles arrangeraient bien des choses. Toutefois Debarbieux (2006) montre, en citant une recherche réalisée en Afrique, que ce sont les établissements pratiquant le moins des châtiments corporels par les enseignants que le sentiment de sécurité des élèves est le plus développé, et surtout que plus les enseignants sont violents, plus les élèves le sont également entre eux.

Debarbieux (2006, p. 239 ; voir aussi Roché, 2004, pour des conclusions proches) résume les stratégies de prévention de la violence juvénile efficaces *vs*. inefficaces, selon le niveau de violence des élèves. Les programmes qui fonctionnent sont en général fondés sur la prévention/encouragement et non la répression.

* **Niveau 1 : population générale**
	* *Efficaces* : Développement des compétences. Renforcement positif des comportements. Techniques comportementales pour la gestion de la 	classe. Construction de l'efficacité de l'établissement (planification, capacité à soutenir et implanter des changements positifs). Programmes scolaires progressifs. Apprentissage coopératif.
	* *Inefficaces* : Conseil et médiation par les pairs [mais Roché mentionne qu'ils fonctionnent parfois]. Non-promotion de la réussite. 	Conférences "en passant" de policiers ou psychologues.

* **Niveau 2 : Elèves à haut risque de violence**
	* *Efficaces* : Formation des parents. Prévention : visites à domicile (infirmières en post-natal). Entraînement au raisonnement moral, à la résolution de problèmes sociaux. Thérapie familiale. Services sociaux de proximité. Regroupement des élèves par petits groupes et implication dans des activités intéressantes.
	* *Inefficaces* : Programmes pour "ramener" les armes et pour les maîtriser. Changement de normes du groupe de pairs.  Tentatives pour "rediriger" le comportement des élèves.

* **Niveau 3 : Elèves violents et très délinquants**
	* *Efficaces* : Acquérir des perspectives sociales. Interventions comportementales (chercher une alternative à la colère). Entraînement des compétences. Thérapie familiales. Thérapies systémiques. 
	* *Inefficaces* : Camps de rééducation. Programmes avec hébergement. Conseil individuel.

Stratégies contre la violence : Se soumettre, fuir ou lutter
------------------------------------------------------------

Pain (1997) propose trois stratégies pour les enseignants (et les élèves) face à la violence. Si leur formulation indique clairement son parti, Pain montre que quand aucune de ces trois réponses sont possibles, la violence, l'agression, sont les seules issues :

* *Se soumettre*. "Tu n'as pas tes affaire, mais je ne dis rien". Evitement, crises éventuelles très ponctuelles.
* *Fuir*. Absentéisme scolaire, qui peut arranger tout le monde.
* *Lutter*. Prévenir le conflit, médiation scolaire : "tu as un problème, on va en parler".

La médiation scolaire
~~~~~~~~~~~~~~~~~~~~~

| Bonnafé-Schmitt (2000, chap. 4) a consacré un intéressant ouvrage à ce   sujet. Voici les différentes étapes de réflexion sur cette notion de   médiation, issue d'établissements anglais et américains, qu'il a pu mettre en place dans divers établissements.

* 1\ :sup:`re` phase de sensibilisation aux conflits

Cette première phase permet aux élèves, *via* des jeux de rôles, de réfléchir à différentes manières de résoudre des conflits.

	* partir des conflits dans le quartier, afin de relativiser les conflits scolaires, demander aux élèves de décrire de tels conflits, 	  puis de leur demander d'expliquer les rôles de la justice et de la police dans de tels événements.
	* Revenir dans l'établissement, pour leur faire raconter leurs 	propres "embrouilles", puis en venir aux personnes qui jouent le rôle 	  de la justice et de la police pour régler ce type d'affaires.

* 2\ :sup:`e` phase de sensibilisation à la médiation

	* commencer par questionner les élèves sur le sens du mot "médiation", "médiateur", puis exposer aux élèves différentes formes   de gestion de la médiation : négociation, conciliation, médiation,   arbitrage, jugement.
	* exposer ensuite la parabole de l'orange. Une mère revient du marché avec une seule orange pour ses deux enfants de 9 et 10 ans. Les   élèves doivent trouver une issue à cette histoire, sachant que les deux enfants veulent l'orange.

Nous ne décrirons pas ici les autres phases, qui concernent la désignation et la formation de médiateurs élèves, car cette procédure   est dépendante des choix faits dans les établissements.

Ce qui est négociable et ce qui ne l'est pas
--------------------------------------------

La `brochure du Comité national de la lutte contre la violence à l'école (voir page `Eduscol sur la violence <http://eduscol.education.fr/cid46846/agir-contre-la-violence.html>`_) indique clairement ce qui peut être négociable de ce qui ne peut l'être. Ces indications peuvent être utiles à l'enseignant car, souvent, l'incertitude de ce dernier sur l'aspect négociable de certains points est inductrice de problèmes.
 
+------------------------------------------------------+-----------------------------------------------------------------+
| Ce qui est négociable, on peut en délibérer          | Ce qui n'est pas négociable                                     |
+======================================================+=================================================================+
| **Dans l'enseignement**                                                                                                |
+------------------------------------------------------+-----------------------------------------------------------------+
| Les méthodes et les stratégies pédagogiques          | Les savoirs, les contenus de l'enseignement                     |
+------------------------------------------------------+-----------------------------------------------------------------+
| Les rythmes d'acquisition des connaissances          | La nécessité de travailler                                      |
+------------------------------------------------------+-----------------------------------------------------------------+
| Les modes de communication et de collaboration dans  | Les horaires et les volumes d'enseignement                      |
| la classe                                            |                                                                 |
+------------------------------------------------------+-----------------------------------------------------------------+
| Les moments de l'évaluation et la présentation en    | L'assiduité, la ponctualité, y compris aux enseignements        |
| amont des critères d'évaluation pour tel devoir ou   | optionnels                                                      |
| contrôle                                             |                                                                 |
+------------------------------------------------------+-----------------------------------------------------------------+
| Le rattrapage d'un cours, l'adaptation de l'emploi   | Le principe des contrôles, la définition des critères           |
| du temps                                             | d'évaluation, la notation avec les mêmes critères pour tous. La |
|                                                      | possession du matériel pédagogique                              |
+------------------------------------------------------+-----------------------------------------------------------------+
| **Dans l'ensemble des activités**                                                                                      |
+------------------------------------------------------+-----------------------------------------------------------------+
| La composition des classes, l'organisation           | La loi morale (respect de l'égale dignité des personnes,        |
| pédagogique de l'établissement (annuellement, en CA) | suspension de la violence)                                      |
+------------------------------------------------------+-----------------------------------------------------------------+
| L'élaboration et la révision du règlement intérieur  | Les règles du droit (public, pénal, civil) en particulier le    |
| (périodiquement sous le contrôle du CVL - Conseil    | respect des personnes, des biens et des obligations scolaires   |
| des délégués pour la vie lycéenne - et du CA)        | (assiduité, ponctualité, travail)                               |
+------------------------------------------------------+-----------------------------------------------------------------+
| L'élaboration et l'actualisation du projet           | L'application du règlement intérieur et notamment du régime des |
| d'établissement (idem)                               | sanctions et des punitions                                      |
+------------------------------------------------------+-----------------------------------------------------------------+
| L'orientation (adaptée à chacun)                     | La mise en œuvre du projet d'établissement et de l'organisation |
|                                                      | pédagogique arrêtés en CA                                       |
+------------------------------------------------------+-----------------------------------------------------------------+
| Les modalités de la formation des délégués           | Les procédures d'orientation (identiques pour tous)             |
+------------------------------------------------------+-----------------------------------------------------------------+
| Les modalités d'accès aux locaux communs (CDI,       | Les consignes de sécurité (ateliers…)                           |
| permanence, foyer)                                   |                                                                 |
+------------------------------------------------------+-----------------------------------------------------------------+
| Les activités associatives, culturelles et sportives | Le principe de la liberté d'expression                          |
+------------------------------------------------------+-----------------------------------------------------------------+
| Les modalités de la liberté d'expression             |                                                                 |
+------------------------------------------------------+-----------------------------------------------------------------+


Analyse de pratiques
====================

#. Reprenez le tableau des différents types de violence scolaire, citez  des cas auxquels vous avez été confrontés. Quelles ont été vos réactions ?
#. Reprenez le tableau des éléments négociables/non négociables et discutez-le. Éventuellement, augmentez-le.


Références
==========

Statistiques ministérielles
---------------------------

* DPD (2001). Recensement des actes de violence à l'école. Note d'information de la DPD, 2.
* Houllé, R. (2006). `Les actes de violence recensés dans SIGNA en 2005-2006 <http://media.education.gouv.fr/file/02/6/4026.pdf>`_. *Note d'information de la DPD*, 30. 
* Laïb, N.  (2008). `Les actes de violence recensés par SIVIS <http://media.education.gouv.fr/file/2008/73/0/NI0834_40730.pdf>`_. *Note   d'information de la DPD*, 34.


Articles et ouvrages
--------------------

* Bègue, L. (2004). La violence scolaire et ses déterminants. In M.-C. Toczek & D. Martinot (Eds.), *Le défi éducatif* (pp. 307-332). Paris: Colin.
* Bonafé-Schmitt, J.-P. (2000). *La médiation scolaire par les élèves*. Paris : E.S.F.
* Charlot, B., & Emin, J.-C. (Eds.)(1997). *Violences à l'école, état des savoirs*. Paris : Colin.
* Coslin, P. G. (1997), A propos des comportements violents observés au   sein des collèges. In Charlot, B., & Emin, J.-C. (Eds.)(1997). *Violences à l'école, état des savoirs*. Paris : Colin, 181-201. 
* Debarbieux, E. (2006). *Violence à l'école : un défi mondial ?* Paris: Colin.
* Debarbieux, E., & Blaya, C. (2008). Violence scolaire. In A. van Zanten (Ed.), *Dictionnaire de l'éducation* (pp. 680-684). Paris: P.U.F.
* Debarbieux, E., Dupuch, A., Montoya, Y. (1997). Pour en finir avec le "handicap socio-violent". In Charlot, B., & Emin, J.-C. (Eds.)(1997). *Violences à l'école, état des savoirs*. Paris : Colin, 17-40. 
* Garcia, S., Poupeau, F. (2000). `Violence à l'école, violence de l'élève <http://www.monde-diplomatique.fr/2000/10/GARCIA/14409.html>`_. *Le Monde Diplomatique*, octobre, 4–5.
* Lec, F., & Lelièvre, C. (2007). *Histoires vraies des violences à l'école*. Paris: Fayard.
* Pain, J. (1997). Violences et prévention de la violence à l'école.   *Les Sciences de l'Education*, 30-2, 57-87.
* Peralva, A. (1997), Des collégiens et de la violence. In Charlot, B., & Emin, J.-C. (Eds.)(1997). *Violences à l'école, état des savoirs* (pp. 101-115). Paris : Colin.
* Roché, S. (2004). L’efficacité des interventions pour lutter contre la violence et la délinquance en milieu scolaire : résultats des   évaluations d’impact. *Rapport pour la Commission nationale du débat sur l'avenir de l'école*. Paris : MEN.