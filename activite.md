(activite)=

# Observer le travail individuel des élèves

```{index} single: auteurs; Dessus, Philippe single: auteurs; Besse, Émilie
```

:::{admonition} Information
- **Auteurs** : [Philippe Dessus](http://pdessus.fr/), LaRAC & Inspé, Univ. Grenoble Alpes. Le quizz a été réalisé par Emilie Besse, projet [ReflexPro](http://www.idefi-reflexpro.fr).
- **Date de création** : Octobre 2000, révisé en octobre 2001 et en avril 2012.
- **Date de publication** : {sub-ref}`today`.
- **Durée de lecture** : {sub-ref}`wordcount-minutes`.
- **Statut du document** : Terminé.
- **Résumé** : Ce document a pour but de décrire des méthodes d'analyse du travail individuel des élèves. Ce document a trait à l'analyse du travail (l'activité des élèves), qu'ils soient au sein d'un groupe ou travaillant individuellement. Il décrit la méthode de l'entretien d'explicitation de Vermersch (travail individuel), ainsi que des méthodes d'observation de classe.
- **Voir aussi** : Doc. SAPP {ref}`taches_ens_el`.
- **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.
- **Licence** : Document placé sous licence *Creative Commons* : [BY-NC-SA](http://creativecommons.org/licenses/by-nc-sa/3.0/fr/).
:::


 {sub-ref}`wordcount-words` words | {sub-ref}`wordcount-minutes` min read

## Ce que l'on sait

### Observer l'activité individuelle

Vermersch {cite}`vermersch94` est un des auteurs qui a essayé de proposer une méthode d'analyse de l'activité qui puisse être appliquée dans l'enseignement. Baptisée "entretien d'explicitation", cette méthode permet à l'enseignant d'aider l'élève à verbaliser ce qu'il a mis en oeuvre dans l'activité. Il n'est guère possible, en si peu de place, de donner tous les éléments de sa méthode, aussi nous conseillons vivement la lecture de son ouvrage. En voici toutefois quelques éléments.

1. Commencer par demander au sujet une formulation libre (" pensez tout haut en vous remémorant les différentes opérations "), puis resserrer les questions au fur et à mesure de la progression de l'entretien. Laisser toujours le sujet aller au bout de ses phrases, pour le laisser revenir " dans son activité ". Mais ne pas hésiter à le recentrer sur l'activité réelle et non sur des généralités (" je fais toujours comme ça ", " le plus souvent, ça ne me pose pas de problème "). Types de questions à poser : " *comment* faites-vous pour... " " comment saviez-vous que... " pour demander une procédure (action d'exécution), ou les informations que le sujet prélève (action d'identification). On peut également interroger le sujet sur les début et fin des actions " comment saviez-vous que vous aviez terminé ? ", ou bien sur les éventuelles contraintes séquentielles : des actions ne peuvent-elles pas se faire que lorsque d'autres sont réalisées ? ou bien ne peuvent plus être réalisées après d'autres ? des actions doivent-elles se faire simultanément ?
2. Dans un deuxième temps, une fois que l'activité aura été décrite, on passera aux "pourquoi ?" : " Expliquez *pourquoi* vous faites cela " pour justifier la procédure et obtenir les buts poursuivis. Pour reformuler les paroles du sujet, afin de guider son expression : les répéter exactement ; focaliser, en sélectionnant un terme à expliquer, la déformation, en trahissant les propos de l'expert pour le provoquer à reformuler.

### Observer l'activité d'une classe

Nous présentons maintenant quelques grilles d'observation de classe qui pourront être utiles à tous ceux voulant analyser de plus près les aspects concernant la communication et les interactions élèves-enseignant. Toutes ces grilles peuvent être utilisées pour coder les interactions verbales dans une classe qui ont été relevées avec un simple magnétophone. Il va de soit que les comportements non verbaux devront être relevés à partir d'un enregistrement vidéo, ou bien codés directement par un observateur.

Il existe des grilles mesurant le type d'interactions enseignant-élèves, comme la grille de Flanders, en fonction des communications verbales de chacun, pour analyser de quelle manière le comportement de l'un influence celui de l'autre (perspective interactioniste). D'autres grilles, plus récentes, comme celle de Pianta et ses collègues {cite}`pianta08`, sont plus centrées sur la qualité des interactions enseignant-

#### La grille de Flanders

Cette grille (voir {cite}`postic81,ketele87,flanders76`) permet de relever les enchaînements d'interactions verbales enseignant-élèves. Il s'agit de relever, toutes les *n* secondes (p. ex. 3 ou 5 s) le type d'interactions verbales en cours dans la classe. L'observateur relèvera donc, toutes les 3 (ou 5) secondes, le numéro de l'interaction appropriée. On répétera donc le numéro de cette dernière si aucun changement de catégorie n'apparaît au bout de 3 secondes ; en revanche, si des interactions différentes surviennent dans une durée de 3 s, elles seront codées.

**Tableau I - Catégories de Flanders**

| **A. Influence indirecte**                                                                                                                                                                                               |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| 1. Accepte ou clarifie les sentiments des élèves : le professeur accepte les sentiments soit positifs, soit négatifs des élèves, sans aucune réprobation. Y sont inclus les prédictions et les souventirs de sentiments. |
| 2. Fait des éloges ou encourage : il félicite l'élève ou l'encourage dans son activité. Il plaisante pour détendre le climat de la classe, mais jamais aux dépens d'un élève.                                            |
| 3. Accepte ou utilise les idées des élèves : Il clarifie, développe les idées exprimées par les élèves. Si le professeur ne reprend l'idée d'un élève que pour l'amener vers la sienne, il s'agit de la catégorie 5.     |
| 4. Pose des questions : Il pose une question à propos du contenu ou de la méthode avec l'intention qu'un élève réponde.                                                                                                  |
| 5. Fait un exposé *ex cathedra* : Il donne des faits, des opinions sur le contenu et les méthodes, exprime ses propres idées.                                                                                            |

| **B. Discours de l'enseignant**                                                                                                                                                                                                                      |
| ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Influence directe                                                                                                                                                                                                                                    |
| 6. Donne des directives : Des ordres que l'élève doit suivre.                                                                                                                                                                                        |
| 7. Critique ou en appelle à son autorité : énoncés qui visent à changer un comportement de l'élève d'inacceptable qu'il est à acceptable, faire des remontrances, dire pourquoi l'enseignant fait ce qu'il fait, affirmer son autorité, sanctionner. |

| **C. Discours de l'élève**                                                               |
| ---------------------------------------------------------------------------------------- |
| 8. Répond aux questions : et aux sollicitations du professeur.                           |
| 9. Prend spontanément la parole : intervient sans avoir été sollicité par le professeur. |

| **D. Silence**                                                                                                                  |
| ------------------------------------------------------------------------------------------------------------------------------- |
| 10. Silence, confusion : Périodes de pause, de silence ou moment de confusion pendant lesquels on ne peut déterminer qui parle. |

Flanders {cite}`flanders76` part de l'idée que les comportements verbaux de l'enseignant de type "influence directe" (6 et 7) renforcent la domination que ce dernier peut avoir sur les élèves. En revanche, les 5 premiers items développeraient chez les élèves une attitude démocratique. Comme l'indique Flanders, il est toujours possible, selon les besoins de l'observation, de raffiner des catégories \[1. Ainsi, la catégorie 3, assez floue, peut être segmentée en :\]

- 3-1 : Simple répétition, pour montrer que les idées de l'élève ont été entendues.
- 3-2 : Réagit aux idées spécifiques de l'élève, mais seulement en fonction de la perception qu'il s'en fait.
- 3-3 : Réagit aux idées spécifiques de l'élève, mais cette réaction incorpore les perceptions d'un ou plusieurs élèves.
- 3-4 : Stimule la réaction aux idées d'un élève en posant des questions afin de faire réagir les autres élèves.

#### Analyse des interactions

Flanders pose qu'une interaction est une suite de de comportements verbaux observables. Par exemple, le professeur pose une question (item 4) et l'élève répond (item 8). Pour analyser les interactions, il faut relever toutes les paires d'items successivement observés. Le dernier item d'une paire étant également le premier de la paire suivante. Une séquence sera définie par des items précédés et suivis d'un silence (item 10). Par exemple, la séquence suivante :

P : En quelle année est mort Descartes ? (4)

E : En 1650. (8)

P : Très bien. (2)

sera codée :

```
                    10               ]1e paire
2e paire[             4              ]
        [             8              ]3e paire
4e paire[             2              ]
        [            10
```

Ces paires sont ensuite placées dans une matrice (voir plus bas), p. ex. (10,6) sera codé par une barre dans la case à l'intersection de la ligne 10 et la colonne 6 ; (6, 10) par une barre dans la case à l'intersection de la ligne 6 et la colonne 10 ; (10,7) par une barre dans la case à l'intersection de la ligne 10 et de la colonne 7, etc.

Par colonne, on peut calculer les pourcentages obtenus par chaque catégorie d'items (nb d'items d'une catégorie\*100/nb total de comportements observés). On peut ensuite calculer le pourcentage d'interventions du professeur (col. 1-7), des élèves (col. 8-9). L'examen de certaines aires (voir plus bas) permet d'identifier le mode d'intervention privilégié du professeur.

- La croix du contenu : De nombreuses observations dans cette croix montrent que l'enseignant privilégie la transmission de contenu (exposé). Cela varie bien sûr en fonction de la matière enseignée (50 % des items observés en maths, 30 % en sciences humaines et sociales).
- Les aires A, B, C, D montrent la proportion de temps pendant lequel, respectivement : -- le professeur a utilisé son influence indirecte, -- directe, -- les élèves ont parlé, -- il y a eu du silence ou de la confusion.
- " L'aire E : une haute proportion d'interventions figurant dans ces cases souligne que le professeur utilise surtout l'influence indirecte, en encourageant les idées des élèves, en les utilisant et en les amplifiant.
- " L'aire F : ces quatre cases sont une combinaison des interventions du professeur pour donner des directives, des critiques, ou pour faire acte d'autorité. La succession 6-7, ou 7-6, est particulièrement significative des difficultés de discipline rencontrées par le profsseur et des résistances opposées par les élèves. De hautes fréquences enregistrées dans la case 6-16 ou 7-7 donnent des indications sur l'attitude dominatrice du professeur.
- " Les aires G et H regroupent des interventions qui surviennent à l'instant où l'élève cesse de parler et où le professeur prend la parole, en exerçant une influence indirecte (aire G) ou une influence directe (aire H).
- " Une comparaison peut être faite entre G/H et A/B. Si les deux rapports sont tout à fait différents, cela montre que le professeur peut adapter sa réponse à la nature de l'intervention de l'élève et qu'il possède donc une certaine plasticité. D'intéressantes relations peuvent être trouvées entre l'aire E et l'aire G, entre l'aire F et l'aire H. Par exemple, l'aire G peut indiquer qu'un professeur intervient d'une manière indirecte au moment où les élèves ont terminé leur intervention, mais c'est l'aire E qui permettra à l'observateur de voir si l'intervention indirecte se prolonge toujours.
- " L'aire I offre une réponse à la question : quel type d'interventions de l'enseignant déclenche la participation des élèves. Généralement cette participation est entraînée par des interventions figurant dans les cases 4-8 et 4-9, c'est-à-dire par des questions posées par le professeur. Un score important dans la case 4-8, 8-4, montre habituellement que la classe réagit directement aux sollicitations du professeur. Le contraste des relevés figurant dans les colonnes 8 et 9 de cette aire donne une indication sommaire sur la fréquence des interventions spontanées des élèves par rapport aux réponses provoquées par des questions du professeur.
- " L'aire J indique soit de longues interventions d'élèves, soit des communications prolongées entre eux. On devrait normalement s'attendre à trouver des fréquences dans l'aire E en corrélation positive avec les fréquences de l'aire J. " {cite}`postic81`

#### Consignes de codage

La catégorie 7 est proche de la 6. Elle justifie plus les interventions autoritaires, invite à l'attention, alors que la 6 invite à la soumission {cite}`ketele87`:

```
- Remets ton stylo sur le banc et regarde ici. (7)

- J'ai besoin de toute votre attention car ceci est difficile. (7)

- N'utilisez pas de bic rouge. (6)

- Procédez de la même façon que dans le dernier exercice. (6)
```

## Ce qu'on peut faire

### Approfondissement de l'analyse avec la grille de Flanders

Concernant la grille de Flanders, pour analyser les enchaînements de comportements verbaux les plus probables, on pourra procéder ainsi :

- Relever la case avec le plus grand nombre d'occurrences, repérer sa ligne et sa colonne.
- Trouver ensuite l'événement précédent le plus probable, qui est la plus forte occurrence contenue dans la colonne de la catégorie repérée précédemment en tant que ligne. Trouver l'événement suivant    en repérant, à l'inverse, la plus forte occurrence dans la ligne    correspondant à la catégorie de la colonne repée précédemment (p.    ex., si la plus forte case est 4-8, on cherchera dans la colonne 4 le    plus grand nombre d'observations ; pour l'événement suivant, on    cherchera dans la ligne 8 le plus grand nombre d'observations).
- Repérer également les états stationnaires (p. ex. 5-5 ; 4-4, etc.), présents dans la diagonale de la grille.
- On pourra enfin repérer les enchaînements silence-parole (10-x), afin de voir qui relance l'activité et de quelle manière.

Soit la matrice d'observation suivante :

**Tableau II -- Matrice d'observation, exemple** {cite}`flanders76` p. 65

```
       1      2      3      4      5      6      7      8      9      10     Total
  1    --     --     --     --     --     --     --     --     --     --     --
  2    --     1      1      1      2      --     --     1      5      --     11
  3    --     --     5      1      4      --     --     --     --     --     10
  4    --     --     --     23     2      1      --     42     3      5      76
  5    --     2      1      22     80     1      2      3      3      3      117
  6    --     --     --     1      --     --     1      3      --     --     5
  7    --     --     --     --     2      1      1      --     --            4
  8    --     5      --     22     19     --     --     45     7      --     98
  9    --     3      3      3      7      --     --     3      32     --     51
 10    --     --     --     3      1      2      --     1      1      --     8
Total  --     11     10     76     117    5      4      98     51     8      380
  %    --     2,9    2,6    20     30,8   1,3    1,1    25,8   13,4   2,1    100
                 25,5                            33,2                  39,2          2,1
               Total enseignant           : 58,7                       Total         Sil.
                                                                      élèves
```

On peut repérer la séquence 5-5, état stationnaire, la plus observée. La séquence suivante la plus probable sera 5-4, trouvée en parcourant la *ligne* de la séquence précédente. On cherche ensuite dans la ligne 4 la suite la plus probable, c'est vraisemblablement 4-4, état stationnaire. La suivante est sans doute 4-8. Cherchons ensuite dans la ligne 8 la suite la plus probable, nous avons 8-4 et 8-5 comme chagements, et 8-8 comme état stationnaire. Notons que cet ensemble de séquences est trouvé en tournant, dans la matrice, dans le sens des aiguilles d'une montre. Les deux ensembles de séquences les plus courants sont donc (les états stationnaires sont entre parenthèses) :
(5-5) ; 5-4 ; (4-4) ; 4-8 ; (8-8) ; 8-5 et (5-5) ; 5-4 ; (4-4) ; 4-8 ;
(8-8) ; 8-4

### Observation de groupes de travail

La grille de Flanders, décrite ci-dessus, ne convient que pour observer des séances de classe animées par un enseignant, travaillant face à l'ensemble de sa classe. Si l'on veut se rendre compte des interactions dans un petit groupe de travail, il faut adopter d'autres indicateurs, comme ceux que nous allons lister maintenant. Là aussi, le dépouillement pourra se faire à partir d'une séquence enregistrée par audio, même si la vidéo offre une qualité de restitution plus fine.

Pour un groupe de travail centré sur un problème très précis (production de texte, résolution de problème), on pourra mesurer le temps total passé à -- planifier ; -- faire le brouillon ; -- corriger le brouillon.

O'Conaill et Whittaker {cite}`conaill93` livrent des indicateurs pour observer le déroulement des interactions verbales :

- nombre de tours de parole, durée moyenne d'un tour (un tour de parole    étant la période pendant laquelle un membre du groupe s'exprime seul, les autres l'écoutant) ;
- nombre d'encouragements à poursuivre un discours (produits par des    membres du groupe à l'intention de celui qui a la parole, p. ex. "hum " ; "oui ", etc.) ;
- nombre d'interruptions ;
- nombre de productions simultanées ;
- distribution des tours de parole (qui parle dans le groupe, après et avant qui) ;
- nombre de fois où celui qui parle cède explicitement la parole à quelqu'un d'autre.

Enfin, voici une grille plus complète, pour qualifier le contenu des interactions verbales {cite}`henri89`.

1. Interaction explicite : Toute déclaration contenant une référence explicite à un autre message, une autre personne ou à un groupe de personnes.

1.1. Réponse directe : Toute déclaration répondant à un questionnement dans une forme explicite, en se référenant directement à lui (p. ex., en réponse à Denis...).

1.2. Commentaire direct : Toute déclaration se référant ou poursuivant une idée qui a été produite, en se référant directement à elle (p. ex., Je suis d'accord avec Nicole...).

2. Interaction implicite : Toute déclaration contenant une référence implicite à un autre message, une autre personne ou à un groupe de personnes.

1.1. Réponse indirecte : Toute déclaration répondant visiblement à un questionnement, mais sans référence explicite (p. ex., Je pense que la solution au problème est...).

1.2. Commentaire indirect : Toute déclaration se référant ou poursuivant visiblement une idée, mais sans référence au message de départ directement à elle (p. ex., Le problème posé ici demande la collaboration de X).

3. Déclaration indépendante : Une déclaration traitant le sujet, mais sans répondre ou commenter, que ce soit explicite ou implicite une déclaration précédente.

## Quizz

```{eval-rst}
.. eqt:: Activite-1

    **Question 1. La grille de Flanders est-elle uniquement centrée sur l'enseignant ?**

    A) :eqt:`I` `Oui`
    B) :eqt:`C` `Non`
```

```{eval-rst}
.. eqt:: Activite-2

    **Question 2. Que qualifie-t-on de "réponse indirecte" selon F. Henry (1989) ?**

    A) :eqt:`C` `Toute déclaration répondant visiblement à un questionnement, mais sans référence explicite`
    B) :eqt:`I` `Toute déclaration contenant une référence implicite à un autre message`
    C) :eqt:`I` `Toute déclaration se référant ou poursuivant visiblement une idée, mais sans référence au message de départ`
    D) :eqt:`I` `Toute déclaration contenant une référence explicite à un autre message`
```

```{eval-rst}
.. eqt:: Activite-3

    **Question 3. Que qualifie-t-on de "commentaire direct" selon F. Henry (1989) ?**

    A) :eqt:`I` `Toute déclaration répondant à un questionnement dans une forme explicite`
    B) :eqt:`C` `Toute déclaration se référant ou poursuivant une idée qui a été produite, en se référant directement à elle`
    C) :eqt:`I` `Toute déclaration répondant visiblement à un questionnement, mais sans référence explicite`
    D) :eqt:`I` `Toute déclaration se référant ou poursuivant visiblement une idée, mais sans référence au message de départ`
```

## Références

```{bibliography}
    :filter: docname in docnames
```
