.. _erreur:

***********************
Erreur et apprentissage
***********************

.. index::
  single: auteurs; Dessus, Philippe
  single: auteurs; Besse, Émilie

.. admonition:: Information

 * **Auteurs** : `Philippe Dessus <http://pdessus.fr/>`_, Inspé & LaRAC, Univ. Grenoble Alpes. Le quizz a été réalisé par Émilie Besse, projet `ReFlexPro <http://www.idefi-reflexpro.fr>`_.

 * **Date de création** : Janvier 2006.

 * **Date de publication** : |today|.

 * **Statut du document** : Terminé.

 * **Résumé** : On a accordé récemment à l'élève un "droit à l'erreur", et les programmes officiels ont mentionné que cette dernière était "l'outil privilégié du maître". Cet intérêt pour l'erreur est-il si récent ? Est-il si avantageux pour l'apprentissage de se centrer ainsi sur l'erreur ? Ce document examine ces questions.

 * **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.

 * **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.


Introduction
============

Voici un florilège de citations à propos de l'erreur.

  *  "Tout le monde commence par faire des erreurs, et un peintre qui ne comprendrait pas les erreurs qu'il fait ne pourrait jamais les corriger". (Leonard de Vinci, 1452-1519).
  * "Les plus courtes erreurs sont toujours les meilleures." (Molière, *L'étourdi*, 1655)
  *  "S'il se trompe laissez-le faire, ne corrigez point ses erreurs, attendez en silence qu'il soit en état de les voir et de les corriger lui-même [...]" (Rousseau, *L'Emile*, 1762/1966, p. 219)
  * "Il est impossible qu'aucun élève ne se soit trompé dans les règles qu'on lui a données pour exemple. L'instituteur a dû le remarquer, et montrer comment et en quoi consistait l'erreur, et quelle en était la cause. Il doit ici rappeler ce fait, pour faire sentir aux élèves    l'utilité dont il est pour eux de savoir reconnaître eux-mêmes leurs erreurs" (Condorcet, 1794, cité par :cite:`trouve06`).
  * "L'expérience, c'est le nom que chacun donne à ses erreurs." (Oscar Wilde, 1892)
  * "ici [à l'école] l'on se trompe, l'on recommence ; les fausses additions n'y ruinent personne. Et ce n'est pas peu de chose si le sot rit d'une énorme erreur qu'il a faite. Par ce rire il se juge    lui-même. Remarquez que nous ne raisonnons jamais que sur une erreur reconnue." (Alain :cite:`alain32`, p. 77)
  * "Penser, c'est aller d'erreur en erreur." (Alain, :cite:`alain32`, p. 85)
  * "L'essence même de la réflexion c'est de comprendre qu'on n'avait pas compris" (Bachelard, cité par Astolfi :cite:`astolfi97`, p. 37) "Nous apprenons quelque chose de nos erreurs (ne serait-ce qu'à ne plus les commettre), mais [...] un comportement correct n'est pas tout simplement ce qui reste lorsque les comportements erronés ont été éliminés" (Skinner, 1968, cité par Crahay :cite:`crahay99`, p. 134)
  * "Tandis que les théoriciens aiment à recueillir, cultiver et classer les erreurs, les praticiens, quant à eux, sont plus intéressés par leur élimination et, quand ce n'est pas possible, tentent de contenir leurs effets néfastes en concevant des dispositifs tolérants à l'erreur" (Reason :cite:`reason93`, p. 11).

À la lecture de ces mots, il est flagrant, d'une part, que l'erreur (et leur compréhension) est une part importante du processus d'apprentissage. Il est inévitable que l'élève, au cours de son apprentissage, fasse un certain nombre d'erreurs (*i.e.*, produise des réponses qui diffèrent de la réponse correcte). On peut même estimer que c'est le rôle premier de l'école que de permettre de telles erreurs sans graves conséquences (pouvant en revanche être graves dans le monde professionnel). Si la présence d'erreurs dans l'apprentissage n'est contesté par personne, comment utiliser ces dernières et faire en sorte qu'elles ne surviennent pas ou plus, fait l'objet de nombreuses controverses.

L'enseignant et/ou l'élève pourraient déterminer les causes de ces erreurs, par divers moyens et, également de diverses manières, pourraient s'informer mutuellement de leurs progrès dans ce diagnostic. La question est donc bien de savoir comment.

Pour résumer un peu abruptement, pour certains courants récents et novateurs, l'erreur serait utile pour l'apprentissage, dans d'autres plus anciens (béhaviorisme, enseignement dit "traditionnel"), néfaste. Or, d'une part, la distinction n'est pas si nette, certaines méthodes privilégiant l'erreur ayant été élaborées il y a longtemps. Enfin, il est difficile de comparer terme à terme les deux courants, puisqu'ils préconisent des situations totalement différentes.

Les premiers consistent à mettre en place des situations d'apprentissage dans lesquelles, justement, il n'y aurait pas nécessairement une bonne réponse, et dans lesquels provoquer l'erreur permettrait, à terme, de l'éradiquer des productions :cite:`astolfi97`.

Les seconds se sont principalement intéressés à des situations dans lesquelles une bonne réponse était à trouver. Enfin, il a souvent été fait un parallèle entre la recherche scientifique, dans laquelle l'erreur joue un rôle important (voir les travaux de Popper, montrant qu'une théorie scientifique doit être réfutable, *i.e.*, pouvoir être invalidée), et l'apprentissage. Ce parallèle est parfois abusif : les élèves construisant un savoir ne pouvant pas (souvent) être considérés comme des chercheurs redécouvrant des principes. Fischer :cite:`fischer99`, en indiquant qu'on utilise erreur ce qui pourrait plutôt être appelé "essai", "impasse", "obstacle" ou "conjecture", signale bien que le travail de l'enseignant, soulignant régulièrement les erreurs de ses élèves au stylo rouge, ne fait pas nécessairement un travail vain...

N'importe quelle erreur n'est pas utile, n'est pas utile à tous (enseignant/élève), n'est pas utile dans n'importe quel apprentissage, et enfin n'importe quelle rétroaction à son propos. Ce sont ces différents paramètres que nous allons maintenant détailler.

Ce que l'on sait
================

L'erreur d'un point de vue philosophique
----------------------------------------

Il importe déjà de réfléchir à une position philosophique (voire pédagogique par rapport à l'erreur). Certains courants, en mettant en avant un "droit à l'erreur" de l'élève, mais, déjà, les béhavioristes (notamment Thorndike) ont été les premiers à montrer que l'on apprenait par essais/erreurs, et que les feedback positifs étaient plus fructueux que les négatifs. Les psychologues constructivistes, et Piaget le premier, ont insisté sur l'importance d'analyser les erreurs des élèves pour connaître leur niveau de pensée. Ils insistent aussi sur ses capacités d'auto-correction : le plus important étant, non pas qu'il sache qu'il ait fait une erreur, mais qu'il comprenne pourquoi il l'a faite.

Autre problème, invoquer ce "droit à l'erreur" en permanence la relativise, et peut empêcher de construire des connaissances exactes : si tout le monde se trompe, personne se trompe :cite:`scala95` : que penserait-on d'un élève invoquant son droit à l'erreur pour ne pas travailler ? Cela fait justement dire à Alain "Si on ne suppose pas que les hommes ont tous la même intelligence, et l'ont toute, il n'y a plus ni vérité ni erreur".

Toutefois, quel que soit le statut de cette erreur, il semble admis que l'un des buts de l'éducation est que l'élève lui-même puisse s'apercevoir de ses erreurs et les corriger seul :cite:`scala95`.

**Tableau I - Les différents statuts de l'erreur** (:cite:`astolfi97`, p. 23).

+-------------------+---------------------+--------------------+--------------------+
|                   | La faute            | La bogue (*bug*)   | L'obstacle         |
+===================+=====================+====================+====================+
| Statut de         | L'erreur déniée     |                    | L'erreur positivée |
| l'erreur          | (raté, perle)       |                    |                    |
+-------------------+---------------------+--------------------+--------------------+
| Origine de        | Responsabilité de   | Défaut repéré dans | Difficulté         |
| l'erreur          | l'élève qui aurait  | la planification   | objective pour     |
|                   | dû la parer         |                    | s'approprier le    |
|                   |                     |                    | contenu enseigné   |
+-------------------+---------------------+--------------------+--------------------+
| Mode de           | Evaluation *a       | Traitement *a      | Travail *in situ*  |
| traitement        | posteriori* pour la | priori* pour la    | pour la traité     |
|                   | sanctionner         | prévenir           |                    |
+-------------------+---------------------+--------------------+--------------------+
| Modèle            | Transmissif         | Béhavioriste       | Constructiviste    |
| pédagogique       |                     |                    |                    |
+-------------------+---------------------+--------------------+--------------------+


Différents types d'erreurs
--------------------------

De plus, il faut distinguer ce qui est de l'ordre d'un échec ou raté (mise en place une stratégie qui échoue malgré ma volonté, mais ni ma stratégie ni mes intentions ne peuvent être critiquées) ou du fait de me tromper, ou faire une faute (mise en place une stratégie qui échoue, mais dont on peut dire qu'elle (ou l'intention qui l'anime) est inappropriée en fonction de la situation. Si de nombreux psychologues se sont intéressés aux erreurs, il existe paradoxalement très peu de travaux essayant de prédire la manière dont survient une erreur : les travaux sont en général réalisés *a posteriori*. Reason :cite:`reason93`, un psychologue ergonome, a réalisé un tel classement, en distinguant trois grands types d'erreurs :

* *les erreurs fondées sur les automatismes* (ratés), où les actions s'écartent de l'intention poursuivie, suite à des défaillances dans l'exécution (*e.g.*, manque d'attention) ou le stockage (mémoire), ou encore suite à l'application d'un automatisme inadéquat (*e.g.*, appuyer sur la mauvaise touche). On ne peut s'apercevoir de ces dernières seulement dans l'action.
* *les erreurs fondées sur les règles* (lapsus), consistent en de mauvaises applications de règles dans la résolution d'un problème (*e.g*., employer le mauvais algorithme de résolution) ;
* *les erreurs fondées sur les connaissances* (fautes), consistent en un mauvais usage des connaissances dans la résolution d'un problème.

À partir de ce classement, Reason a proposé un catalogue d'erreurs très complet, dont voici quelques éléments. Ces exemples concernent la vie de tous les jours, mais pourraient assez aisément être transposés dans des situations scolaires (d'après Reason :cite:`reason93`, pp. 109 *et sq*.).

Activité fondée sur les automatismes (ratés)
````````````````````````````````````````````
**Inattention**

* *Double capture* : En commençant d'écrire une lettre, j'ai mis mon ancienne adresse au lieu de la nouvelle.
* *Omission suivant des interruptions* : Je prenais mon manteau quand le téléphone a sonné. J'y ai répondu et suis sorti ensuite sans mon manteau.
* *Intentionnalité réduite* : J'ouvre le réfrigérateur et suis incapable de me souvenir de ce que je veux.
* *Confusions perceptives* : Je voulais prendre la bouteille de lait mais je saisis à la place la bouteille de jus d'oranges.
* *Erreur d'interférence* : Je commençais à préparer du thé quand le chat a miaulé. Alors j'ai ouvert une boîte de pâtée que j'ai versée dans la théière.

**Attention excessive**

Ces ratés surviennent quand on accorde trop d'attention à un processus largement automatisé, où l'on opère des vérifications qui peuvent nuire au bon déroulement de l'action, pour peu qu'on analyse mal où l'on en est dans le processus.

* *Omissions* : Oublier de mettre du thé dans la théière.
* *Répétition* : Verser une deuxième bouilloire d'eau dans la théière déjà pétition : Verser une deuxième bouilloire d'eau dans la théière déjà pleine.
* *Retour en arrière* : J'ai pris de la monnaie pour payer le bus ; mais je l'ai remise dans le porte-monnaie avant que le chauffeur ait eu le temps de l'encaisser.

Activité fondée sur les règles (lapsus)
```````````````````````````````````````
**Mauvaise application de règles correctes**

* *Premières exceptions* : Appliquer une règle inappropriée à la première rencontre d'une exception (*e.g.*, un dauphin est un poisson).
* *Force de la règle* : Plus une règle a donné de bons résultats par le passé, plus elle a de chances d'être appliquée, même si elle n'est pas appropriée à la situation.
* *Redondance* : On accorde plus d'importance à des signes qui se présentent ensemble, et on rejette d'autres signes, que l'on estime à tort redondants (il y a plus de naissances, de crimes, etc., les soirs de pleine lune).

**Application de règles erronnées**

* *Déficience d'encodage* : On simplifie une tâche en ne considérant qu'une variable (les chauffeurs novices oublient de gérer la vitesse de la voiture et roulent trop lentement).
* *Déficience de l'action* :
 
  * *Règles erronées* : On applique des règles incorrectes à une situation (*e.g.*, soustraction de nombres).
  * *Règles inélégantes* : Pour économiser de l'essence, on roule en roue libre.
  * *Règles déconseillées* : On préfère risquer un accident plutôt que de rater un rendez-vous.

Activité fondée sur les connaissances (fautes)
``````````````````````````````````````````````
* *Limitation de l'espace de travail* : on se souvient d'événements dans l'ordre dans lesquels ils se sont produits.
* *Hors de la vue, hors de l'esprit* : on a du mal à prendre en compte des éléments non visibles.
* *Confiance excessive* : Une fois engagé dans la résolution d'un problème, on a tendance à justifier les actions choisies et à rejeter les signes qui contredisent le choix.
* *Vagabondage thématique* : on a tendance à voleter d'un aspect d'un problème à l'autre, afin de ne pas prendre le problème frontalement (fuite, procrastination).
* *Fixation* : en cas de situation critique, on ne se centre que sur des aspects marginaux du problème.


Attirer l'attention de l'élève sur ses erreurs
----------------------------------------------

Est-il toujours utile, non seulement de signaler une réponse erronée, mais de plus d'attirer l'attention de l'élève sur la nature de son erreur ? Deux auteurs ont exploré ces questions. George :cite:`george83` montre que, lorsque la mémorisation d'item corrects (*e.g.*, paires associées) influe sur la performance ultérieure, il est plus fructueux de mettre en évidence les réussites des élèves que leurs échecs. En revanche, lorsque les bonnes réponses sont définies par rapport à une règle ou une structure logique, il est plus fructueux de mettre l'accent sur les réponses incorrectes (qui, donc, falsifient la règle) que sur les réponses correctes. Enard :cite:`enard70`, de manière plus complète, montre que :

* faire pratiquer à des apprenant des erreurs volontairement et en le leur signalant explicitement dans des tâches sensori-motrices (*e.g.*, frappes sur un clavier) est plus profitable que lorsqu'on signale uniquement les réussites ;
* en revanche, pour des tâches faisant appel à la mémoire, les résultats rejoignent ceux de George : il vaut mieux mettre en évidence les réussites des élèves, en minimisant les risques d'erreur ;
* enfin, pour les tâches conceptuelles (résolution de problèmes ouverts, découverte de principes), la méthode du guidage moyen (laisser l'élève découvrir, mais en le guidant par des indices) semble donner de meilleurs résultats.

Ohlsson :cite:`ohlsson96` insiste sur le fait que dire à un élève : "Tu as fait *B*, mais c'est faux, tu aurais dû faire *A*" ne permet pas de lui donner de bonnes indications. Si c'était le cas, l'enseignement serait une tâche vraiment aisée... Ohlsson montre qu'il est nécessaire de repréciser le contexte (la situation dans laquelle l'erreur a été produite, puisque, vraisemblablement, l'élève a appliqué une règle ne convenant pas à la situation présente). Il est donc plus fructueux de lui dire "Dans cette situation [*i.e.*, la situation dans laquelle la décision de faire *B* a été prise], tu as décidé de faire *B* ; toutefois, tu n'aurais pas dû faire *B* car certaines caractéristiques de la situation, c\ :sub:`1`,..., c\ :sub:`n`, ont conduit à l'erreur indésirable *E*". Ainsi, l'élève est alerté sur les caractéristiques qui l'auraient empêché de mener à bien la tâche, et ainsi pourra dans l'avenir reconnaître de telles situations.

Pour résumer, la difficulté avec ce sujet vient que, d'une part, une erreur ne signifie pas que l'élève n'ait pas compris un contenu (*e.g.*, erreur d'inattention, voir ci-dessus).  Toutefois, il ne faut pas pour autant penser que l'étude des résultats corrects des élèves ne révèle pas, non plus, des surprises : il peut arriver qu'un raisonnement erroné donne une réponse exacte. Il convient donc que l'enseignant analyse au plus près la démarche de l'élève.

Il convient aussi de distinguer le moment dans l'apprentissage où se réalise l'erreur : pendant l'apprentissage, où l'erreur est effectivement normale et constitutive de ce dernier, ou bien dans le développement de compétences, où l'erreur, dans le cas d'habiletés peu complexes, doit sans doute être moins tolérée. Il existe même des domaines où l'on a pu prouver son rôle néfaste pour l'apprentissage.

Cas où l'erreur est néfaste : l'apprentissage de l'orthographe
--------------------------------------------------------------

L'enseignement de l'orthographe est un bon exemple pour analyser le rôle de l'erreur, puisque l'orthographe française présente un nombre d'irrégularités plus important que dans d'autres langues (espagnol ou anglais). Récemment, par exemple en orthographe, l'on s'est mis à parler plutôt d'erreur que de faute (l'erreur se corrige, la faute se pardonne). Si l'erreur peut jouer un rôle positif dans l'apprentissage, il est des cas où elle joue un rôle négatif : ceux où   l'apprentissage est implicite (*i.e.*, fondé sur la compréhension et la reproduction des régularités de l'environnement, de manière non intentionnelle). Par exemple, il a été montré que les élèves acquièrent largement l'orthographe de manière implicite, et les   exposer à des formes erronées, ou encore les amener à faire des erreurs va augmenter la probabilité qu'ils les produisent en retour :cite:`fischer99,perruchet04`.

L'exemple de l'apprentissage de l'orthographe nous permet aussi de   montrer que l'utilisation de l'erreur n'est pas l'apanage des méthodes   dites "modernes" : la cacographie était une méthode d'apprentissage de   l'orthographe en vogue au XIX\ :sup:`e` siècle. Elle consistait à   faire corriger à l'élève des mots écrits incorrectement, autant liés à   l'orthographe dite d'usage que grammaticale. Les tenants de la   rénovation pédagogique n'avaient pas manqué de critiquer  cette   méthode, qui habituaient les élèves à une orthographe défectueuse,   sans leur donner la possibilité de la distinguer de l'orthographe   adéquate :cite:`giolitto84`

Ce que l'on peut faire
======================

Insister auprès des élèves pour qu'ils travaillent mieux est une   stratégie que tout enseignant met en place, souvent sans beaucoup de   succès. Il est plus fructueux de s'interroger sur les raisons des erreurs, et les moyens de les signaler ou de les éviter. C'est ce que nous allons voir ici.

Une catégorisation des erreurs des élèves
-----------------------------------------

Tirer profit des erreurs des élèves, pour l'enseignant, c'est déjà en analyser la cause (Astolfi :cite:`astolfi97` p. 58, Colomb :cite:`colomb99`, pp. 16-17) ont listé les types d'erreurs pouvant être faites par les élèves. Ils   distinguent les erreurs :

* liées à la compréhension des consignes (tâche), ou d'un mauvais décodage des attentes de l'enseignant ;
* témoignant de conceptions alternatives, ou encore ayant leur origine dans une autre discipline ;
* liées aux processus cognitifs ou à une surcharge cognitive (tâche trop complexe à réaliser) ;
* portant sur les stratégies adoptées pour résoudre le problème  (raisonnements, stratégie différente de celle canonique) ;
* dues à la complexité propre du contenu ;
* dues au savoir enseigné (obstacle didactique, engendré par la manière même d'enseigner un contenu) ;

Il faut noter que la majorité de ces erreurs sont fondées sur la connaissance. Il conviendrait donc d'étendre ce classement à la lumière des travaux de Reason, ce qui ne paraît pas avoir été encore réalisé.

Les rétroactions de l'enseignant
--------------------------------

Ensuite, c'est délivrer des rétroactions (feedback). Crahay (:cite:`crahay99`, p. 144) donne   des conseils utiles concernant les types de réponses à donner à des   élèves pour qu'ils tiennent compte de leurs erreurs. "[Il] peut simplement signaler l'erreur [feed-back simple] (c'est faux, tu t'es   trompé, etc.). Il peut expliquer le pourquoi de l'erreur (c'est faux parce que...) ; on parle alors de feedback expliqué. Il peut encore   fournir un feed-back de contrôle ; celui-ci consiste à inviter l'élève à vérifier [par lui-même] l'exactitude de sa réponse. Il a montré, dans une étude auprès d'élèves de maternelle, que seul le troisième type de feed-back est utile pour faire progresser l'élève.

Aménager les supports d'apprentissage
-------------------------------------

Ensuite, cela peut être, parfois, anticiper les erreurs possibles soit pour qu'elles ne se produisent pas, soit pour qu'elles ne soient pas graves et n'entachent pas toute la performance de l'élève (voir plus haut Reason et les dispositifs tolérants à l'erreur). De ce point de   vue, la conception de documents limitant la charge cognitive des élèves peut être tout à fait utile (voir doc :ref:`charge_cognitive` pour plus de renseignements).

Discussion
==========

Pour clore ce document sur l'erreur, soulignons trois points principaux : l'erreur n'est pas faute, doit pouvoir être détectée et corrigée par l'élève lui-même, et, enfin et surtout : "L'erreur n'est pas l'ignorance, on ne se trompe pas sur ce qu'on ne connaît pas, on peut se tromper sur ce qu'on croit connaître. Un élève qui ne sait pas additionner ne fait pas d'erreurs d'addition et celui qui ne sait pas écrire ne commet pas de fautes d'orthographe. C'est une banalité. Toute erreur suppose et révèle un savoir." (Scala :cite:`scala95`, p. 23)

Quizz
=====

.. eqt:: Erreur-1

  **Question 1. Selon le psychologue J. Reason (1993), les erreurs se classent en trois catégories**

  A) :eqt:`I` `Les erreurs fondées sur l'inattention ; Les erreurs fondées sur l'oubli ; Les erreurs fondées sur les connaissances`
  B) :eqt:`I` `Les erreurs fondées sur la confusion ; Les erreurs fondées sur l'interférence ; Les erreurs fondées sur la peur`
  C) :eqt:`I` `Les erreurs fondées sur les consignes ; Les erreurs fondées sur les automatismes ; Les erreurs fondées sur la compréhension`
  D) :eqt:`C` `Les erreurs fondées sur les automatismes ; Les erreurs fondées sur les règles ; Les erreurs fondées sur les connaissances`

.. eqt:: Erreur-2

  **Question 2. Parmi ces erreurs, quelle est celle qui est fondée sur les automatismes ?**

  A) :eqt:`I` `Mauvaise application de règles correctes`
  B) :eqt:`I` `Confiance excessive`
  C) :eqt:`C` `Attention excessive`
  D) :eqt:`I` `Limitation de l’espace de travail`

.. eqt:: Erreur-3

  **Question 3. Parmi ces erreurs, quelle est celle qui est fondée sur les connaissances ?**

  A) :eqt:`C` `Confiance excessive`
  B) :eqt:`I` `Mauvaise application de règles correctes`
  C) :eqt:`I` `Limitation de l’espace de travail`
  D) :eqt:`I` `Attention excessive`



Analyse de pratiques
====================

#. À partir d'une série d'exercices d'élèves, classer leurs erreurs selon l'une des catégorisations proposées dans ce document.
#. Transposer le catalogue d'erreurs de Reason dans des situations scolaires.
#. Réfléchir à sa propre position philosophique par rapport à l'erreur. Utiliser pour cela les différentes citations de ce document.


Références
==========

.. bibliography::
 :cited:
 :style: apa
