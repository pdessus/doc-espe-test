.. _cartes_concepts:

===================================================
Les cartes de concepts pour évaluer l'apprentissage
===================================================

.. index::
    single: auteurs; Dessus, Philippe
    single: auteurs; Besse, Émilie

.. admonition:: Information

	* **Auteurs** : `Philippe Dessus <http://pdessus.fr/>`_, Inspé & LaRAC, Univ. Grenoble Alpes.  Le quizz a été réalisé par Emilie Besse, projet `ReflexPro <http://www.idefi-reflexpro.fr>`_.

	* **Date de création** : Octobre 2000 ; doc. révisé en Octobre 2001.

	* **Date de publication** : |today|.

	* **Statut du document** : Terminé.

	* **Résumé** : Les cartes de concepts sont devenues une forme répandue d'évaluation des connaissances des élèves. Ce document expose les différents types de cartes de concepts, ainsi que des utilisations possibles en classe.

	* **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.

	* **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.


Introduction
============

Une carte de concepts est le triplet suivant :

* une liste de concepts, représentés par des mots ou propositions sommets d'un graphe ;
* des relations entre ces concepts, représentés par les nœuds d'un graphe ;
* des étiquettes (optionnelles) précisant ces relations.

On a pu utiliser de telles cartes pour mettre au jour les connaissances des apprenants ou des enseignants à propos d'un sujet particulier ou bien comme outil d'aide à la recherche. Voici tout d'abord un exemple (extrait) de carte de concepts rédigée par un adolescent sur le sida :cite:`dallaglio96`. On remarque que le terme "sida" est placé au centre de la carte et que les différentes notions rayonnent à partir de lui, sans liaisons entre différents niveaux de hiérarchie (p. ex., "maladie" et "transmission", deux termes du même niveau, ne sont pas liés).

.. image:: /images/conceptmap1.jpg
   :alt: carte de concepts
   :align: center

**Figure 1 - Extrait de carte de concepts sur le sida** :cite:`dallaglio96`.

Ce que l'on sait
================

On a exposé de telles cartes à des élèves en préalable à un cours, ou bien comme aide à la révision de notions. Cela permet de mieux appréhender la hiérarchie des notions qui vont être exposées, ainsi que leur interrelations. Des chercheurs ont par exemple réalisé des cartes de concepts de cours d'enseignants. Ces derniers ont pu ainsi se rendre compte qu'ils utilisaient certains termes qu'ils ont jugé superflus, au détriment d'autres plus utiles :cite:`debueger94`. On a également demandé à des élèves de rappeler les éléments d'un cours sous cette forme.

Les cartes de concepts ont pu aussi être utilisées avec succès dans des travaux de résolution de problèmes en petits groupes. Enfin, faire concevoir deux cartes de concepts, une *avant* un ensemble d'enseignements, une autre *après*, permet d'évaluer les acquisitions des élèves. L'autre intérêt, que nous verrons plus bas, réside dans la possibilité de calculer, à partir des cartes, quelques indices résumant la structure de ces dernières (:cite:`lawson94,tochon90,febvre90` exposent quelques travaux montrant l'intérêt de l'utilisation de telles cartes, ainsi que le numéro 5 de la revue *Didaskalia*, 1994).

Ce que l'on peut faire
======================

Faire réaliser des cartes de concepts
-------------------------------------

Contrairement à ce que l'on pourrait penser, apprendre à réaliser des cartes de concepts n'est pas aisé. Il est donc nécessaire de faire réaliser plusieurs cartes, à titre d'exemple, avant de pouvoir les utiliser pour évaluer des apprentissages. Voici comment il est possible de présenter des cartes de concepts :

1. Réaliser, devant la classe, une carte de concepts sur un thème connu des élèves. La commencer, puis demander à quelques élèves de continuer telle ou telle "branche". Il importe de leur signaler
 * que la notion principale doit être placée au centre, de manière à pouvoir rattacher des termes de tous côtés ;
 * que l'on écrit uniquement le terme (nom, verbe, adjectif) principal, et non une phrase ; on n'utilisera un ensemble de termes que si un seul prête à confusion (p. ex. "système nerveux central") ;
 * que la liaison entre deux termes indique une relation de généralité, le terme le plus proche du centre étant plus général que celui ou ceux au(x)quel(s) il est relié. Attention, cette notion de généralité est loin d'être facile à faire comprendre ;
 * qu'il est possible, mais pas obligatoire, afin de mieux faire comprendre la relation entre deux termes liés, d'ajouter un mot sur le lien.

2. Utiliser quelques cartes de concepts pour présenter, ou faire rappeler, un contenu de séquence.

3. Passer à la réalisation de cartes de concepts par les élèves. Bien insister sur l'idée que chaque élève peut représenter la notion avec ses mots, qu'il n'existe pas de carte-type. On pourra commencer par donner à traduire des propositions, comme dans l'exemple ci-dessous, tiré d'un ouvrage de vulgarisation philosophique :

	« Suite à cette critique [de Hume, sur le transcendantal], Kant fait trois remarques capitales : - loin d'être chaotique, la nature est autonome, c'est-à-dire gouvernée par ses lois propres ; - loin d'être épars, le sujet est lui aussi autonome ; - loin d'être le produit d'habitudes socialement ordonnées, le rapport de l'homme au monde est celui de deux autonomies, celle du monde et celle du sujet. L'homme parvient à comprendre le fonctionnement autonome du monde, lorsqu'il devient lui-même autonome. » (:cite:`vergely96` p. 33).

Une carte résumant ce paragraphe pourrait être la suivante. Noter les étiquettes, en italiques, caractérisant les liaisons.

.. image:: /images/conceptmap2.jpg
   :alt: carte de concepts
   :align: center

**Figure 2 - Une carte de concepts (librement réalisée) sur les remarques de Kant à Hume** (d'après :cite:`vergely96`).

Analyser des cartes de concepts
-------------------------------

Un des avantages des cartes de concepts réside dans la facilité de leur analyse. Il est possible de calculer quelques indices permettant leur comparaison. L'exemple ci-dessous indique comment les calculer.

.. image:: /images/conceptmap3.jpg
   :alt: carte de concepts
   :align: center

**Figure 3 - Une carte de concepts** (d'après :cite:`lindsay80`, p. 405).

Il est possible de calculer les indices suivants à partir de la carte de concepts ci-dessus. Ces indices sont calculés quelle que soit la justesse de la citation. Il faudra donc rectifier ces indices en supprimant les termes mentionnés à tort :

* *nombre de concepts* (C) : nombre de concepts total (dans l'ex. : 20) ;

* *nombre de “nœuds“ (N) : nombre total de nœuds dans la carte, ou sommets du graphe (*i.e.*, une catégorie, soit au moins trois concepts reliés à des niveaux différents, dans l'ex. : 3 (autruche, canari, pingouin) ou, autrement dit, les termes qui sont à la fois des "parents" et des "enfants" ;

* *nombre moyen de concepts par nœud* (C/N), qui donne une idée de la densité des termes regroupés autour d'un même nœud.

Quizz
=====

.. eqt:: Conceptmap-1

	**Question 1. Quel est le triplet d'une carte de concepts ?**

	A) :eqt:`I` `Une trame de fond ; Une liste de concepts ; Un travail collaboratif`
	B) :eqt:`I` `Du travail collaboratif ; Une information complexe ; Des relations entre différents concepts`
	C) :eqt:`C` `"Une liste de concepts ; Des relations entre les concepts et Des étiquettes précisant ces relations`
	D) :eqt:`I` `Une information complexe ; Une liste de concepts ; et Une trame de fond`


.. eqt:: Conceptmap-2

	**Question 2. Contrairement à ce qu'on croit, apprendre à réaliser des cartes de concepts est :**

	A) :eqt:`C` `Plutôt difficile`
	B) :eqt:`I` `Plutôt facile`
	C) :eqt:`I` `Réservé aux enseignants`
	D) :eqt:`I` `Hors de la portée des élèves`


Références
==========

.. bibliography::
    :cited:
    :style: apa
