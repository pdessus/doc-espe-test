(discipline)=

# La discipline dans la classe

% https://www.edweek.org/tm/articles/2017/09/06/death-to-the-behavior-chart-3-reasons.html?cmp=SOC-EDIT-FB&utm_content=60270179&utm_medium=social&utm_source=twitter

```{index} single: auteurs; Dessus, Philippe single: auteurs; Besse, Émilie
```

:::{admonition} Information
- *Auteurs\** : [Philippe Dessus](http://pdessus.fr/), Inspé & LaRAC, Univ. Grenoble Alpes, (et surtout Archambault & Chouinard, 1996). Le quizz a été réalisé par Émilie Besse, projet [ReflexPro](http://www.idefi-reflexpro.fr).
- **Date de création** : Octobre 2000, mis à jour en octobre 2002.
- **Date de publication** : {sub-ref}`today`.
- **Statut du document** : Terminé.
- **Résumé** : Ce document permet de prendre connaissance de quelques principes permettant de gérer la discipline dans la classe et de quelques types de comportements d'élèves. Sont évoqués quelques aspects   de la gestion de la discipline en classe. Sont d'abord listés quelques principes favorisant le respect de la discipline, puis une manière de recourir le moins possible à la punition. Ensuite, sont détaillés les   types de problèmes de comportement des élèves, leurs raisons possibles ainsi que les possibilités d'intervention de l'enseignant.
- **Voir aussi** : Documents {ref}`sanction` et {ref}`questionnaire_discipline`.
- **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.
- **Licence** : Document placé sous licence *Creative Commons* : [BY-NC-SA](http://creativecommons.org/licenses/by-nc-sa/3.0/fr/)
:::

## Introduction

Il ne fait aucun doute que l'enseignant, dans sa classe, a un certain pouvoir (peut-être beaucoup plus qu'il ne le croit), dont il peut user pour faire respecter une ambiance de travail adéquate. Legault (1993, p. 40, citant French & Raven, 1968) distingue les types de pouvoir suivants. Il peut être utile, dans des situations de gestion de discipline, de déterminer quel type de pouvoir est en jeu :

- *le pouvoir de référence*, est lié au type de relation et de communication que l'enseignant arrive à mettre en place avec ses élèves.
- *le pouvoir d'expert*, est lié à la compétence de l'enseignant dans la matière enseignée et à ses compétences pour faire apprendre ce contenu.
- *le pouvoir légitime*, est lié à l'autorité légale de l'enseignant, qui est responsable de ses élèves et exerce des pouvoirs qui lui sont délégués de par son statut.
- *le pouvoir de récompenser ou de punir*, lié au pouvoir précédent, doit être utilisé à bon escient, car les effets de récompenses ou punitions peuvent être de courte durée.

## Ce que l'on sait

Quelques principes favorisent le respect de la discipline par les élèves : être sensible à ce qui se passe dans la classe, gérer un rythme de travail adéquat, intervenir discrètement, utiliser l'humour, faire preuve de tolérance, respecter les élèves et intervenir en fonction des causes du comportement perturbateur. Détaillons-les. Ce qui suit est tiré d'Archambault & Chouinard, 1996.

### La sensibilité à ce qui se passe dans la classe

L'enseignant doit essayer d'être conscient de tout ce qui se passe dans sa classe et le montrer aux élèves. Il supervise constamment le fonctionnement de sa classe, "il a des yeux tout autour de la tête ", il peut faire face à des événements inattendus. Trois caractéristiques de la classe sont à superviser :

- le groupe des élèves dans son ensemble, bien qu'il doive porter une attention particulière sur certains élèves ;
- les comportements des élèves, notamment des comportements hors des limites accordées ;
- le rythme de l'activité (*voir plus bas*).

Même si, réellement, on ne peut jamais être capable de tout superviser, l'enseignant doit montrer aux élèves que le moins possible de choses lui échappe, cela incitera les élèves à respecter les règles de discipline. Voici quelques comportements-types qui favoriseront ce comportement (Archambault & Chouinard, 1996, p. 36) :

- Balayer fréquemment et régulièrement la classe du regard.
- Éviter de tourner le dos aux autres élèves lorsqu'on aide un élève.
- Éviter de se concentrer sur une activité en particulier lorsque plusieurs activités sont en cours (ne pas donner de l'aide à un seul élève pendant trop de temps, ne pas s'attarder dans une équipe ou dans  un atelier au détriment des autres, etc.).
- Utiliser divers modes d'organisation du travail (collectif, individuel, coopératif).
- Repérer les signes d'ennui de la part des élèves (niveau de bruit, mouvements qui augmentent, expressions du visage, etc.)
- Réagir promptement pour faire cesser un comportement perturbateur.

### Le rythme de travail

L'enseignant doit faire en sorte que les différents événements de sa classe se succèdent "en douceur", de façon continue et selon une logique claire. Il doit éviter les hésitations et les délais entre les différentes activités. Une des caractéristiques les plus difficiles à appréhender, notamment pour les enseignants débutants, est le fait que différents événements se produisent en même temps (chevauchement) : par exemple, aider un élève qui tente de résoudre un problème, jeter un coup d'œil sur un groupe, intervenir pour qu'un élève cesse de déranger les autres, vérifier la durée d'une activité, etc. La seule manière de gérer efficacement ces chevauchements est de bien préparer ces différentes activités.

### Les interventions de l'enseignant

Comme l'enseignant doit réagir rapidement aux perturbations dans sa classe, il lui est souvent difficile d'en découvrir les causes véritables. C'est pourtant ce qu'il essaiera de faire, car une perturbation ne sont pas toujours intentionnelles, mais découlent parfois d'une mécompréhension de la situation d'apprentissage. Ainsi, l'enseignant devra tenter de détecter la cause du comportement, plutôt que de le faire cesser directement.

Les événements qui se produisent dans une classe ont un caractère public. Toutefois, les interventions d'un enseignants pour faire respecter les règles de discipline sont d'autant plus efficaces qu'elles se passent en privé : une remarque publique risque d'interrompre l'activité des autres élèves, de plus, un élève réprimandé devant ses pairs, comme il focalise leur attention, se sentira valorisé et son comportement inapproprié sera de fait renforcé. Enfin, une réprimande publique peut dévaloriser l'élève devant ses pairs, surtout si l'enseignant emploie le sarcasme afin de rappeler l'élève à l'ordre (*voir la section suivante*). Pour toutes ces raisons, il est préférable, dans la mesure du possible, de n'intervenir qu'auprès de l'élève qui pose un problème. Le Tableau I ci-dessous résume les éléments à prendre en compte en cas d'intervention (voir aussi des exemples de stratégies d'intevention dans la Section "Ce que l'on peut faire").

**Tableau I -- Guide d'intervention face à un comportement perturbateur (d'après Archambault & Chouinard, 1996, p. 41).**

| Questions à se poser                                                                                          | Intervention                                                                                                                                                                          |
| ------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1. L'élève est-il informé de mes attentes à son endroit ?                                                     | Préciser ou préciser de nouveau ses attentes en faisant référence à la règle ou à la procédure en cause                                                                               |
| 2. L'élève comprend-il bien ce que signifient mes attentes ?                                                  | Donner des exemples et des contre- exemples reliés à la règle ou à la procédure en cause. Présenter à l'élève un modèle de comportement adapté à la régle ou à la procédure en cause. |
| 3. L'élève possède-t-il les stratégies nécessaires pour se conformer à mes attentes ?                         | Montrer à l'élève des stratégies pertinentes quant à la règle ou à la procédure en cause.                                                                                             |
| 4. L'élève perçoit-il la pertinence des activités d'apprentissage et le défi réaliste qu'elles représentent ? | Expliquer à l'élève l'utilité pour lui de s'engager dans les activités d'apprentissage ou le rassurer quant à sa capacité à les réaliser.                                             |
| 5. L'élève fait-il preuve de mauvaise volonté ?                                                               | Faire en sorte que l'élève comprenne les comportements adaptés à la règle ou à la procédure en cause.                                                                                 |

### La tolérance

Dans une classe, la plupart des problèmes qui se posent quotidiennement sont en général mineurs et ne portent pas à conséquence (faire du bruit, chuchoter, se déplacer, bouger, rire, etc.). Il est difficile pour un élève de rester attentif une journée entière, sans bouger. Ces petits écarts ne sont des inconvénients que dans les classes où le moindre écart est sanctionné par une punition ou une réprimande. Cette attitude de l'enseignant incite les élèves à le défier, à le pousser vers ses limites. L'enseignant a avantage à faire preuve de tolérance, sans bien sûr laisser tout passer.

### Le respect

Le rôle de l'enseignant, outre de favoriser l'apprentissage des élèves, est de leur manifester son soutien, et ce avec respect. Respecter l'élève, c'est lui montrer qu'on l'accepte, quoi qu'il fasse. C'est lui faire sentir qu'on lui accorde de la valeur, comme personne, même si on lui demande de changer de comportement. C'est lui faire comprendre que l'on a confiance en lui et en son aptitude à apprendre.

### L'utilisation de l'humour

Les enseignants qui utilisent l'humour sont appréciés de leurs élèves. Il crée une atmosphère détendue, propice à l'apprentissage, et ne prendra pas au tragique les éventuels problèmes qui se poseront. Toutefois, il faut distinguer l'humour du sarcasme ou de la moquerie dirigée vers des élèves en particulier, qui sont eux à proscrire.

## Ce que l'on peut faire

### Des stratégies d'intervention

Voici quelques moyens permettant à l'enseignant de faire respecter les règles de fonctionnement de la classe. Ils sont classés du plus simple au plus complexe et repris dans le Tableau II ci-dessous.

- *Les indices non verbaux*. Un signe de la tête, du doigt ou de la main, une expression du visage, un contact visuel, permettent généralement de faire comprendre à l'élève que l'enseignant l'a vu et qu'il lui demande de mettre fin à son comportement perturbateur. L'avantage de ces signes est qu'ils permettent de ne pas briser le rythme de l'activité dans laquelle les autres élèves sont engagés.
- *Le rappel verbal.* Lorsque les indices non verbaux ne fonctionnent pas, l'enseignant peut rappeler verbalement à l'élève le comportement à adopter. Il le fait, dans la mesure du possible, discrètement, à l'élève fautif seul. L'enseignant devra également vérifier les causes du comportement perturbateur, en s'assurant que l'élève a compris ce qu'il a à faire, comment et pourquoi il doit le faire. Il doit aussi vérifier que l'élève accorde une valeur à l'activité demandée (voir partie sur la motivation). La remarque de l'enseignant doit être faite non pas à propos de la personne de l'élève, mais à propos de son comportement (dire : "Cesse de parler à ton voisin et continue à écrire. Si tu as besoin d'aide, fais-moi signe." plutôt que "C'est bien toi ça, jamais capable de te mettre au travail quand je te le demande."
- *La répétition du rappel (ou la technique du disque brisé)*. Il peut arriver que l'élève mette à l'épreuve la détermination de l'enseignant à faire cesser un comportement dérangeant. L'élève peut avoir compris la consigne, être capable de réaliser l'activité demandée, mais refuser de l'exécuter pour défier l'enseignant. Ce dernier peut simplement répéter sa demande plusieurs fois. Cela montre à l'élève que l'enseignant insiste pour qu'il se conforme à la demande. L'élève peut se mettre à discuter avec l'enseignant, mais ce dernier devra refuser de s'engager dans la discussion et répéter la demande. L'élève comprend dans ce cas que l'enseignant est sérieur et qu'il entend rétablir l'ordre.
- *L'intérêt pour les comportements adaptés et la distribution sélective de l'attention*. Un comportement d'élève a tendance a devenir d'autant plus fréquent qu'on lui accorde de l'attention. Ainsi, porter une attention excessive aux comportements inadaptés peut avoir pour effet de les renforcer. À l'inverse, il est faut accorder de l'importance aux comportements adaptés. Mais cet intérêt doit être authentique : il ne s'agit pas de manipuler les élèves en les louangeant excessivement, car ils se rendent compte que le compliment est faux. L'enseignant peut donc complimenter les élèves pour leurs comportements adéquats si le compliment est -- précis et non pas un vague commentaire, -- informatif, l'élève doit en tirer des renseignements pour apprécier eux-mêmes leur comportement. Cela correspond à décrire objectivement le comportement de l'élève, sans évaluer la personne (par exemple " Tu es bon ") ni faire de comparaisons entre élèves (" Tu es la meilleure de la classe ").
- *Le renforcement des comportements incompatibles*. Deux comportements sont incompatibles s'ils ne peuvent être produits en même temps par la même personne (par exemple, être assis et marcher). Il s'agit pour l'enseignant de choisir un comportement incompatible à un comportement perturbateur, et de lui accorder systématiquement de l'attention. Si l'enseignant veut que les élèves cessent de courir dans la classe, il accordera de l'attention au fait qu'ils sont assis à leur place, ou bien qu'ils marchent lorsqu'ils ont à se déplacer. Un élève qui fait du chahut lors d'une transition sera responsable d'une distribution de cahiers. Ainsi, plutôt que de sanctionner des comportements négatifs, on encourage la production de comportements positifs.
- *Le façonnement*. Comme il est improbable que les élèves réussissent à apprendre d'un seul coup tous les comportements positifs reliés au fonctionnement de la classe, car certains sont assez complexe, le travail de l'enseignant sera de réduire temporairement certaines de ses attentes, et de les augmenter graduellement par la suite. Cela "façonne" un comportement final en s'en approchant petit à petit. En d'autres termes, on segmentera un comportement complexe en plusieurs petites étapes. Toutefois, il faudra que l'élève garde à l'esprit la tâche dans sa globalité, car l'élève peut perdre de vue le comportement final à exécuter de nombreuses tâches simples.
- *Le retrait de la situation*. Si un comportement mineur persiste malgré les interventions répétées de l'enseignant, c'est vraisemblablement parce que l'élève cherche à garder l'attention de l'enseignant ou de ses pairs. Il peut être donc nécessaire de retirer à l'élève cette attention, en le retirant de la situation, si ce retrait est de courte durée (trois minutes environ).

**Tableau II -- Moyens d'intervention visant le respect des règles (Archambault & Chouinard, 1996, p. 43).**

| Moyens d'intervention                                                                | Description                                                                                                                                    |
| ------------------------------------------------------------------------------------ | ---------------------------------------------------------------------------------------------------------------------------------------------- |
| Les indices non verbaux                                                              | L'enseignant communique sa désaprobation non verbalement.                                                                                      |
| Le rappel verbal                                                                     | En privé, si possible, l'enseignant précise ses attentes à l'élève                                                                             |
| La répétition du rappel                                                              | L'enseignant précise ses attentes de manière répétée jusqu'à ce que l'élève s'y conforme.                                                      |
| L'intérêt pour les comportements adaptés et la distribution sélective de l'attention | L'enseignant accorde de l'attention aux comportements adaptés et ignore les comportements inadaptés.                                           |
| Le façonnement                                                                       | L'enseignant augmente graduellement sesattentes envers l'élève au fur et à mesure que celui-ci assimile les comportements qu'il attend de lui. |
| Le retrait de la situation                                                           | L'enseignant retire l'élève fautif de l'activité en cours et limite temporairement ses interactions sociales en classe.                        |

Nous allons maintenant recenser les types de problèmes de comportement puis les catégoriser selon leur gravité, enfin, nous essaierons d'en déterminer les raisons. Nous ne développons pas non plus ici le problème de l'application de sanctions, qui fera l'objet d'un document particulier (`` voir Document :ref:`sanction ``).

Même s'il est fastidieux, et très difficile, de décrire tous les types de problèmes qui peuvent survenir en classe, en voici quatre catégories, par gravité croissante, avec des exemples. Il importe toutefois de comprendre que ces catégories sont en partie dépendantes du milieu scolaire et social dans lequel évoluent les élèves : ainsi, dans certains milieux ou écoles, des comportements seront jugés gênants et non dans d'autres (par exemple, se lever quand on le désire, parler sans lever la main, etc.).

**Tableau III -- Catégories de problèmes de comportement (Archambault & Chouinard, 1996, p. 154).**

| Catégorie de problèmes                                    | Exemples                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| --------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1. Comportement fautif qui ne pose pas de problème        | Problème de courte durée qui ne dérange pas l'enseignement, comme : chuchoter pendant une transition, cesser de prêter attention durant quelques secondes, faire une courte pause durant une activité, rêvasser durant une courte période                                                                                                                                                                                                          |
| 2. Problème mineur                                        | Des manquements à des règles de la classe ou de l'école, peu fréquents, qui ne dérangent pas la classe et ne nuisent pas à l'apprentissage de l'élève : quitter sa place quand ce n'est pas permis, interpeller quelqu'un, manger des friandises ou mâcher un chewing-gum, lancer des objets (boulettes, gommes), parler durant un travail individuel, lire lorsque l'enseignant explique une activité ou faire autre chose que ce qui est demandé |
| 3. Problème majeur dont l'effet et l'étendue sont limités | Des comportements qui dérangent la classe et interfèrent avec l'apprentissage, limités à un ou quelques élèves : Ne pas terminer un travail, faire autre chose que ce qui est demandé, ne pas suivre les règles à propos du silence ou des déplacements dans la classe, Refuser de faire un travail, briser des objets, frapper un autre élève.                                                                                                    |
| 4. Escalade ou problème très étendu                       | Tout problème mineur qui devient régulier et qui menace l'ordre de la classe et le climat d'apprentissage, tout comportement qui constitue un danger pour l'élève ou pour les autres. Emettre continuellement des commentaires inadéquats ou désobligeants, continuer à parler lorsque l'enseignant demande de se calmer ou de baisser le ton, répondre constamment à l'enseignant ou le défier, refuser souvent de coopérer.                      |

Demandons-nous maintenant pourquoi certains comportements apparaissent en classe (Archambault & Chouinard, 1996).

- *L'élève ne sait pas quoi faire*, l'information donnée par l'enseignant n'est pas assez explicite, ou bien elle est changeante  d'un jour à l'autre.
- *L'élève ne sait pas comment faire*, l'élève sait ce qu'il faut  faire, mais pas comment. Il faut donc lui enseigner les stratégies  qui lui permettront de réaliser l'activité ou le comportement  attendu.
- *L'élève ne sait pas pourquoi il doit faire ce qu'on lui demande*, parfois, l'explication du but de l'activité n'est pas assez  explicite.
- *L'élève n'a rien à faire*, l'élève peut avoir terminé son activité avant les autres. Il faut donc lui indiquer ce qu'il doit faire  ensuite.
- *L'activité est trop difficile*, un élève qui sait qu'il ne pourra réaliser l'activité devient moins motivé.
- *L'activité est trop facile*, de même, une activité trop facile est démotivante et perd de la valeur aux yeux de l'élève.
- L'élève ne comprend pas et est dépassé dans la matière enseignée\*, l'élève qui doit entreprendre régulièrement des activités trop  difficiles pour lui perd de l'intérêt pour la matière enseignée.
- *L'activité d'apprentissage n'est pas intéressante ou ennuyeuse*.  L'élève ne sera pas motivé par une activité qu'il juge peu  intéressante ou utile.
- *L'élève reçoit de l'attention de la part de l'enseignant*. Un élève  aura tendance à maintenir des comportements perturbateurs s'il sait  que l'enseignant y réagit.
- *L'élève reçoit de l'attention de la part de ses pairs*. De même, un élève qui se sait observé par ses pairs aura tendance à maintenir des    comportements dérangeants.

Voici cinq principes généraux d'intervention de l'enseignant lors de problèmes de comportement (Archambault & Chouinard, 1996, p. 160 *et sq*.).

- Conserver son calme durant une intervention auprès de l'élève ; dans  une situation de crise, l'enseignant doit éviter que le ton monte,  tout en restant ferme, car un enseignant en colère a tendance à  rendre anxieux les élèves non concernés par l'intervention.  L'enseignant doit donc ne pas hausser le ton, continuer à avoir une  voix calme mais ferme, attendre quelques secondes avant de répondre à  l'élève, ne pas croire que les attaques de l'élève sont dirigées vers  soi, mais les considérer comme des signes d'impuissance et des  demandes d'aide, ne pas hésiter à reporter l'intervention lorsque  l'état émotif est intense.
- Choisir une intervention économique et efficace (voir ci-dessus).
- Choisir une intervention qui dérange le moins possible l'activité  d'apprentissage.
- Choisir une intervention qui favorise l'apprentissage de  comportements adaptés, en essayant de ne pas appliquer les punitions.
- Choisir une intervention qui favorise la prise en charge par l'élève de son comportement.

## Quizz

```{eval-rst}
.. eqt:: Discipline-1

        **Question 1.   Selon Legault, le pouvoir de référence est lié :**

        A) :eqt:`C` `Au type de relation et de communication que l’enseignant arrive à mettre en place avec ses élèves`
        B) :eqt:`I` `À la compétence de l’enseignant dans la matière enseignée`
        C) :eqt:`I` `À l’autorité légale de l’enseignant`
        D) :eqt:`I` `À ses compétences pour faire apprendre le contenu de son cours`
```

```{eval-rst}
.. eqt:: Discipline-2

        **Question 2.   Selon Legault, le pouvoir légitime est lié :**

        A) :eqt:`I` `À la compétence de l’enseignant dans la matière enseignée`
        B) :eqt:`C` `À l’autorité légale de l’enseignant`
        C) :eqt:`I` `À ses compétences pour faire apprendre le contenu de son cours`
        D) :eqt:`I` `Au type de relation et de communication que l’enseignant arrive à mettre en place avec ses élèves`
```

```{eval-rst}
.. eqt:: Discipline-3

        **Question 3.   Que signifie le moyen d'intervention "retrait de la situation" ?**

        A) :eqt:`I` `En privé, si possible, l’enseignant précise ses attentes à l’élève`
        B) :eqt:`I` `L’enseignant accorde de l’attention aux comportements adaptés et ignore les comportements inadaptés`
        C) :eqt:`C` `L’enseignant retire l’élève fautif de l’activité en cours et limite temporairement ses interactions sociales en classe`
        D) :eqt:`I` `L’enseignant communique sa désaprobation de manière non verbale`

```

## Références

- Archambault, J., & Chouinard, R. (1996). *Vers une gestion éducative de la classe*. Montréal : Morin.
- Charles, C. M. (1997). *La discipline en classe, modèles, doctrines et conduites*. Bruxelles : De Boeck.
- Davisse, A., & J.-Y. Rochex (1995)(Eds). *"Pourvu qu'ils   m'écoutent...", discipline et autorité dans la classe*. Créteil : CRDP de Créteil.
- Douet, B. (1987). *Discipline et punitions à l'école*. Paris : P.U.F.
- Legault, J.-P. (1993). *La gestion disciplinaire de la classe*. Montréal : Logiques.
