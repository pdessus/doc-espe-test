(metier-aphorismes)=

# Atelier -- Réfléchir à ses principes éducatifs

```{index} single: auteurs; Dessus, Philippe single: auteurs; Cyrs, Thomas E. single: auteurs; de Peretti, A.
```

:::{admonition} Informations
- **Auteurs** : Thomas E. Cyrs (traduit par [Philippe Dessus](http://pdessus.fr/), Inspé & LaRAC, Univ. Grenoble Alpes), et A. de Peretti.
- **Date de création** : 2008.
- **Date de publication** : {sub-ref}`today`.
- **Statut du document** : Terminé.
- **Résumé** : Enseigner est un métier complexe, aux multiples facettes. L'enseignant débutant doit faire face à de très nombreuses situations, parfois destabilisantes. Il se réfère, pour cela, à de nombreuses théories implicites qu'il a acquises de diverses manières (p. ex. en discutant avec ses collègues, son inspecteur, par son expérience d'élève, etc.). Ce document est consitué d'une cinquantaines d'aphorismes concernant le métier d'enseigner, aphorismes qui permettent de prendre position, d'affiner sa propre conception de son métier, qu'il utilise ou non des technologies numériques.
- **Voir aussi** : Le document {ref}`principes_educatifs` qui contient des éléments plus théoriques sur cette question.
- **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.
- **Licence** : Document placé sous licence *Creative Commons* : [BY-NC-SA](http://creativecommons.org/licenses/by-nc-sa/3.0/fr/).
:::

## Aphorismes sur l'éducation

### Tâche

Relevez une dizaine d'aphorismes qui vous semblent le mieux cadrer avec votre propre conception de l'enseignement. Dites pourquoi. À votre tour, écrivez, sous la même forme, les quelques aphorismes qui vous permettent d'exercer votre métier d'enseignant dans de bonnes conditions.

Ces aphorismes sont traduits de Cyrs, T. *EDA, 123 of the most Important things that I have learned about Teaching and Learning at a distance*. Texte qui n'est plus accessible hormis [ici](http://web.archive.org/web/20030212073907/http://www.zianet.com/edacyrs/things_I_learned.htm)

### Liste des aphorismes

01. L'enseignant ne dispose pas de toute la connaissance que les élèves doivent s'approprier.
02. Les élèves peuvent apprendre beaucoup à leur enseignant.
03. C’est très difficile d’admettre que vous ne savez pas quelque chose.
04. Parler n’est pas enseigner.
05. Les enseignants ne motivent pas leurs élèves, ils créent un environnement dans lequel les élèves choisissent de se motiver.
06. Enseigner avec passion.
07. L'enthousiasme entraîne l'enthousiasme.
08. Croyez en vous-même et les élèves vous feront confiance.
09. Apprenez avec vos élèves et dites-leur que vous apprenez avec eux.
10. Soyez disponible avant et après la classe.
11. Ne supposez jamais que vos élèves savent quelque chose, vérifiez-le.
12. Ne faites pas confiance à la technique. Elle n’est jamais réellement transparente, mais translucide.
13. Soyez conscient des possibilités et limites des technologies que vous utilisez.
14. Ayez des solutions de secours lorsque vous enseignez avec des technologies. Soyez prêt au pire.
15. Les élèves n’apprennent jamais grâce aux technologies. Ils apprennent grâce à la communication par la technologie.
16. Ne mettez jamais un élève dans l'embarras.
17. Ne faites pas apprendre par cœur des faits déconnectés les uns aux autres. En revanche, montrez aux élèves où ces faits sont disponibles.
18. Enseigner, ce n'est pas seulement délivrer des informations.
19. Amenez les élèves à être curieux du contenu que vous enseignez.
20. Si, pour quelque raison que ce soit, vous n'appréciez pas un élèves, c'est votre problème.
21. Chaque fois que vous donnez un exemple, donnez également un contre-exemple.
22. Ce qu'un élève sait n'est pas important. Ce qui est important, c'est ce que cet élève sait faire avec ce qu'il sait.
23. Arrêtez de parler autant et laissez les élèves apprendre.
24. Apprenez d’un élève, car personne n’est plus intelligent que tous les participants réunis.
25. Les élèves apprennent seulement lorsqu’ils veulent apprendre.
26. Vous pouvez placer l'élève devant une fontaine, mais vous ne le ferez boire que s'il a soif.
27. Vos élèves auront un plus grand impact sur l'avenir que vous-même.
28. Évaluez seulement ce que vous  avez enseigné.
29. La fontaine de la Connaissance et la source de la Vérité ne sont pas toujours placées sur l'estrade.
30. Les enseignants ne sont plus sources et transmetteurs de connaissances, mais facilitateurs et guides d'apprentissage.
31. Un chercheur crée des nouvelles connaissances. Un enseignant communique ces nouvelles connaissances de façon à ce qu’elle soient comprises.
32. Les trois caractéristiques d’un bon enseignant sont l’enthousiasme, la clarté et les interactions avant et après la classe.
33. Incitez les élèves à penser de manière critique plutôt que comme des perroquets.
34. Les seules questions idiotes sont les questions non posées.
35. Permettez à vos élèves de voir ce que vous dites en leur montrant des images.
36. Surprenez vos élèves de temps en temps en leur posant des questions provocantes.
37. Enseignez à des élèves, pas à des sujets.
38. Aménagez une classe sûre dans laquelle les élèves se sentent à l'aise pour exprimer leurs idées.
39. Soyez un voleur de bonnes idées, mais citez toujours leur auteur.
40. Les élèves peuvent être arrogants et impatients. Les enseignants également.
41. Un cours de qualité n’est pas seulement enseigné, il est aussi préparé.
42. Des enseignants nerveux rendent les élèves nerveux.
43. Il y a peu de bonnes réponses, seulement de bonnes questions.
44. Ce que l'on veut que les élèves apprennent est plus important que ce que l'on enseigne.
45. Si ce n’est pas écrit, cela n’existe pas.
46. Enseigner, ce n'est pas parler à, c'est parler avec.
47. Les élèves sont plus intéressés par les gens que par les choses. Laissez-leur savoir qui vous êtes.
48. Écoutez les élèves fréquemment. Ils peuvent avoir quelque chose d'important à dire.
49. Ce que vous connaissez de vos élèves dépend surtout de ce que vous cherchez en eux.
50. Il y a trois catégories d’enseignants : ceux que vous devez écouter ; ceux que vous ne pouvez vous empêcher d’écouter ; et ceux que vous voulez écouter.
51. Des cours traditionnels ne peuvent être portés sur Internet ou télévision sans modifications importantes.
52. Le mot coopération s’écrit NOUS.
53. Les bonnes questions n'arrivent pas du ciel, elles sont écrites et soigneusement préparées.
54. Les anecdotes renforcent les points importants d'un cours et établissent un rapport personnel avec l'enseignant.

## Q-Sort sur les conceptions de l'éducation

Le deuxième test est un *Q-sort* sur les conceptions de l'éducation. Ce test est tiré de Peretti, A. (Ed.). (1985). *Recueil d'instruments et de processus d'évaluation formative*, 2 T. Paris: I.N.R.P., pp. 539 *et sq*.

### Tâche

Vous avez à répartir les 32 propositions suivantes en 5 classes, en respectant le nombre d'items par classe.

Lister successivement (dans l'ordre qui vous convient)

- les *deux* conceptions mises en tête (que vous préférez) ;
- les *huit* conceptions   auxquelles vous avez parfois recours ;
- les *douze* conceptions considérées   comme neutres ;
- les *huit* conceptions auxquelles vous ne faites que rarement recours ;
- les *deux* conceptions que vous rejetez fondamentalement.

**Eduquer, c'est...**

01. savoir attendre.
02. inculquer le sens du devoir.
03. permettre aux possibilités d'une personne de se révéler.
04. laisser faire.
05. apporter les conditionnements qui faciliteront l'apprentissage des bonnes habitudes.
06. donner l'exemple.
07. communiquer en profondeur avec un jeune pour l'aider à communiquer avec lui-même.
08. savoir se taire.
09. instruire.
10. dresser.
11. révéler les valeurs essentielles.
12. entraîner les jeunes à obéir.
13. accompagner les démarches tâtonnantes des jeunes pour qu'ils prennent davantage de hardiesse et de sécurité.
14. présenter les modèles de comportements fondamentaux.
15. apporter les contraintes immédiates qui réfrènent les instincts et les pulsions anarchiques.
16. provoquer inlassablement.
17. aider progressivement un jeune à affronter son angoisse et à s'ouvrir aux autres.
18. savoir bousculer.
19. faire confiance
20. s'éduquer.
21. inculquer des règles de vie.
22. favoriser l'apprentissage d'une méthode personnelle.
23. pousser chaque jeune à réfléchir par lui-même.
24. conseiller sans contraindre
25. rendre conforme à la Société.
26. accepter en tant qu'adulte de discuter avec les jeunes.
27. protéger les jeunes contre leurs propres défaillances.
28. répondre à l'attente des jeunes.
29. s'ajuster avec souplesse à l'attente des jeunes.
30. aider les jeunes à s'insérer progressivement dans la société des adultes.
31. entraîner à des méthodes éprouvées.
32. démystifier les anxiétés des jeunes.

## Les valeurs de l'éducation

Ce dernier test liste quelques valeurs couramment exprimées dans le monde de l'éducation, au travers de dix positions   d'enseignants. Les voici résumées par Muller, F. [Diversifier sa pédagogie](http://francois.muller.free.fr/diversifier/). Voir sa [page sur les valeurs](http://francois.muller.free.fr/diversifier/mes6.htm)  (Muller citant Valentin, C. (2000). Formation des enseignants au travail en équipe. Communication à la 5{sup}`e` Biennale de l'éducation et de la formation. Paris : APRIEF. Voir aussi Valentin, C. (1997). *Enseignants : reconnaître ses valeurs pour agir*. Paris : ESF.

### Tâche

Lisez ces différents "mondes", et repérez votre ou vos modes de justification habituel(s). Repérez également ces mondes dans des situations scolaires précises, en montrant qu'elles peuvent être cause de blocage, d'immobilisme. Montrez enfin comment faire évoluer ces situations de manière constructive. L'exercice est également de Muller.

| Les positions                                                         | Ce qui empêche d'évoluer                                                                                             |
| --------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------- |
| Le paradis perdu : "C'était bien mieux autrefois !".                  | L'enseignant d'autrefois est le seul valable                                                                         |
| Le matérialisme : "On n'a pas les moyens !".                          | Revendiquer des moyens matériels avant d'avoir réfléchi à leur mise en œuvre et à la possibilité de faire autrement. |
| Le libéralisme : "Chacun fait comme il veut !"                        | Il suffit que l'enfant (un collègue) fasse ce qu'il veut pour qu'il apprenne (réussisse).                            |
| La défensive : "Méfions-nous, on cherche à nous nuire !"              | Chercher les inconvénients dans toute solution nouvelle, par nature gênante.                                         |
| La résignation : "A quoi bon !"                                       | Considérer que ce qui arrive est une fatalité.                                                                       |
| L'enfant-roi : "Les enfants ont toujours raison !".                   | Il suffit que l'enfant suive ses penchants pour qu'il apprenne.                                                      |
| Défaitisme : "J'ai tout essayé, on ne peut rien faire."               | Prédire l'échec de toute tentative, avant qu'elle ait démarré.                                                       |
| Légalisme : "Ce n'est pas dans les textes."                           | Se réfugier derrière les textes pour ne rien changer.                                                                |
| Changement à tout prix : "Tout cela ne vaut rien, il faut changer !"  | Vouloir changer sans en avoir évalué l'intérêt et les conséquences.                                                  |
| Réconciliation affective : "L'essentiel, c'est qu'on s'entende bien". | Il suffit que les enseignants s'entendent bien pour que les élèves apprennent et réussissent.                        |
