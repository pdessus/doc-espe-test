(corps-ecole)=

# Le corps à l'école

```{index} single: auteurs; Dessus, Philippe single: auteurs; Besse, Émilie
```

:::{admonition} Information
- **Auteurs** : [Philippe Dessus](http://pdessus.fr/), LaRAC & Inspé, Univ. Grenoble Alpes. Le quizz a été réalisé par Émilie Besse, projet [ReflexPro](http://www.idefi-reflexpro.fr).
- **Date de création** : Mars 2007.
- **Date de publication** : {sub-ref}`today`.
- **Statut du document**: Terminé.
- **Résumé** : Le corps des élèves (et celui de l'enseignant) ne sont pas toujours pris en compte dans l'école et les classes. En effet, l'école est censée être centrée sur l'acquisition et la construction de connaissances.  Dans ce document, nous verrons la place que joue le corps,  d'abord du point de vue de la connaissance, mais aussi des relations interpersonnelles.
:::

- **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.

> - **Licence** : Document placé sous licence *Creative Commons* : [BY-NC-SA](http://creativecommons.org/licenses/by-nc-sa/3.0/fr/).

## Introduction

Le statut du corps des enseignants et des élèves au sein de l'école est particulier, et mérite une analyse. Le corps des élèves (et de l'enseignant) ne sont pas toujours pris en compte dans l'école et les classes. Certains, comme Chobaux {cite}`chobaux93`, pensent même qu'il en est absent. D'autres, comme Faure et Garcia {cite}`faure03`, s'intéressant à la danse, signalent que les pédagogues en parlent beaucoup). Quoiqu'il en soit, les élèves ne sont pas que de purs esprits {cite}`maulini99`, même si l'école est censée être centrée sur l'acquisition et la construction de connaissances. Ensuite,  il est frappant que, pour les enseignants et les élèves, le corps des premiers n'est pas le même dans l'école qu'au dehors, et que l'école induit donc une représentation des corps spéciale. Beaucoup de jeunes élèves, par exemple, sont souvent surpris de rencontrer leur enseignant au dehors ; et les plus grands sont surpris d'avoir une autre relation avec leur enseignant lors de sorties scolaires.

## Ce que l'on sait

Foucault {cite}`foucault75` a bien montré comment, à partir du XVIII{sup}`e` siècle, s'est construit un pouvoir visant à contrôler individuellement, et à distance, les individus, et donc leurs corps, et comment certaines institutions (l'école, la prison) ont participé à cette entreprise. Le corps de l'individu, est passé d'un contrôle individuel (punitions corporelles fréquentes jusqu'au XVIII{sup}`e`, interdites en 1840) à un contrôle à distance, fondé sur la mesure (notes) et la compétition. Foucault note également que l'architecture même des bâtiment scolaires (longs couloirs sans recoins à parcourir avant d'entrer en classe, salles de classes rectangulaires voir Chobaux {cite}`chobaux93`) doit permettre le meilleur contrôle visuel possible (panoptisme).

L'inscription du corps dans la classe et l'école peut être envisagée de très nombreuses manières, et ce document en listera brièvement certaines.

### La communication non-verbale comme traces et aide à l'apprentissage

Goldin-Meadow {cite}`goldin04` montre combien la communication non-verbale enseignant-élève (principalement les gestes) est importante, et qu'une analyse peut révéler des informations importantes sur le niveau de compréhension des élèves. Par exemple, l'enseignant peut vérifier si, lors de l'explication de sa démarche, les gestes de l'élève sont compatibles avec ce qu'il dit. Dans le cas contraire, ils sont signes d'un problème de compréhension (e.g., une ambiguïté dans le langage peut être résolue par les gestes : lors de la résolution de l'équation suivante, $7 + 6 + 4 = 7 + ?$ , si l'élève dit, "j'ai additionné 7, 6, et 4 en montrant le 7 à droite du signe égal, il a manifestement pas compris la tâche).

Du côté de l'enseignant, Goldin-Meadow signale que, lors d'une séance de tutorat en mathématiques, 40 % des stratégies de résolution de problèmes sont exprimées par des gestes de l'enseignant, et une explication réalisée avec gestes est vraisemblablement plus efficace que sans.

### La discipline

Le fait que les élèves qui "savent se tenir" sont beaucoup moins sujets que les autres à des punitions et sanctions montre bien que les règlements en matière de discipline sont avant tout centrés sur les corps, autant du point de vue de la règlementation ou des interdictions (retards, déplacements dans l'établissement ou dans la classe, tenue), que du point de vue du contrôle (ne pas se lever, lever le doigt) ou du point de vue des sanctions (e.g., déplacements d'élèves de table en table, ou exclusion de la classe).

Il est intéressant de noter que cette "mise en ordre du corps", selon Raveaud {cite}`raveaud04`, peut différer d'un pays à l'autre. Dans une recherche sur ce sujet comparant des écoles primaires françaises et anglaises, Raveaud montre qu'en France, la gestion des corps des élèves est à la fois plus répressive et plus distante : l'élève doit se tenir tranquille pour des raisons fonctionnelles, pour ne pas gêner les autres et pour apprendre correctement (*e.g.*, se tenir droit pour écrire), mais il y a peu de contacts physiques enseignant/élèves. En Angleterre, les corps des élèves sont plus libres, mais la connotation est cette fois morale : un corps droit entraîne un esprit droit. De plus, les contacts physiques enseignants/élèves sont plus fréquents, comme pour rectifier, non seulement leur position, mais aussi leur attitude.

### Le corps de l'élève, ou les conséquences physiologiques du travail scolaire

Des travaux de médecins scolaires et d'ergonomes montrent bien le fait que l'école fait peser une charge importante (au sens propre) sur le corps de l'élève. Ce dernier doit porter et déplacer un cartable souvent très lourd, étudier dans un environnement pas toujours bien éclairé et chauffé, assis sur un mobilier pas toujours adapté. Par exemple, 10 % des élèves consultant un médecin scolaire le font pour des problèmes de dos {cite}`fortin88`, et 85 % des élèves de lycée agricole ont ou ont eu mal au dos {cite}`duchemin88`.

### Le corps de l'enseignant

Pujade-Renaud {cite}`pujade83b` a bien analysé combien l'enseignant était exposé au yeux de tous dans la classe, et que cela pouvait les gêner, d'une part pour s'approprier l'espace de classe (entrer avant/après les élèves ?), d'autre part en évoluant dans cet espace. À propos de ce dernier point, la position particulière de l'enseignant, surélevée par l'estrade, rend le regard particulièrement important : l'enseignant surplombe les élèves, et peut jouer de son regard pour influer sur leur comportement.

## Ce que l'on peut faire

Bien évidemment, la multiplicité des sujets sur le corps entraîne une multiplicité des interventions possibles, que nous ne pourrons ici qu'évoquer. Voici quelques pistes.

Marzin {cite}`marzin06` liste de manière complète les possibilités d'enseignements en sciences de la vie pouvant amener les élèves à réfléchir à leur rapport au corps. Ce sujet peut également être traité en  arts plastiques, en histoire (*e.g.*, la représentation du corps à différentes époques), en ECJS (les images du corps renvoyées dans les médias), et, bien sûr, en EPS.

Comme l'indiquent Garrett *et al*. {cite}`garrett94`, tout enseignant peut veiller, pendant son activité :

- au contact visuel qu'il entretient avec ses élèves : regarder les élèves directement dans les yeux, en variant les contacts, et en étant conscient du fait que cela peut en gêner certains ;
- à ses expressions du visage : sourires, expressions de surprise ou de déception donnent des feed-back aux élèves aisés à procurer et à interpréter. Il est toutefois nécessaire de contrôler ses expressions négatives ;
- à ses postures corporelles :  montrer du doigt un élève en particulier, ponctuer ses paroles de gestes, etc., cela ajoute du sens aux paroles, et il est important d'en être conscient ;
- à son espace physique :  la "distance sociale" que l'enseignant maintient avec ses élèves (*i.e.*, entre une "distance publique" et une "distance intime") est importante, et l'enseignant pourra la moduler selon les effets : se rapprocher d'un élève à qui il veut parler en particulier, se rapprocher d'un groupe d'élèves bavardant, etc.

## Quizz

```{eval-rst}
.. eqt:: Corpsalecole-1

        **Question 1. Dans quel pays les corps des élèves sont plus libres à l'école ?**

        A) :eqt:`I` `France`
        B) :eqt:`I` `Italie`
        C) :eqt:`C` `Grande-Bretagne`
        D) :eqt:`I` `Espagne`
```

```{eval-rst}
.. eqt:: Corpsalecole-2

        **Question 2. Dans quel pays la gestion des corps des élèves est plus distante et répressive ?**

        A) :eqt:`I` `Grande-Bretagne`
        B) :eqt:`C` `France`
        C) :eqt:`I` `Italie`
        D) :eqt:`I` `Espagne`


```

## Analyse des pratiques

1. Quelles stratégies corporelles vous êtes-vous forgées au cours de votre pratique (postures, déplacements, occupation de l'espace, gestion du tableau, regards, sourires, modulation de voix), et quelles règles de distance (ou, au contraire, de gestes que vous vous permettez) par rapport aux élèves avez-vous élaborées ? En fonction de telles situations (e.g., en cas de détresse d'un élève, bagarres, en cas de retenue à la fin d'un cours, etc.) allez-vous ou non avoir un contact physique avec vos élèves ?
2. Pouvez-vous porter une attention particulière à vos gestes lorsque vous enseignez, ou, à l'opposé, vous interdire, pendant un moment, de faire des gestes. Essayez d'analyser de quelle manière le contenu de votre matière peut influer sur ces gestes (évidemment, on communique par gestes des situations en EPS, mais aussi sans doute en langues (exprimer des phonèmes accentués), en physique (exprimer le mouvement, la pression, etc.).
3. Même question à propos de vos élèves. Essayez de porter une attention particulière à leurs gestes, notamment en situation de tutorat, et d'explication de leurs procédures de travail.

## Références

```{bibliography}
:filter: docname in docnames
```
