(motivation)=

# La motivation en milieu scolaire

```{index} single: auteurs; Dessus, Philippe single: auteurs; Besse, Émilie
```

:::{admonition} Information
- **Auteurs** : [Philippe Dessus](http://pdessus.fr/), Inspé & LaRAC, Univ. Grenoble Alpes. Le quizz a été réalisé par Émilie Besse, projet [ReFlexPro](http://www.idefi-reflexpro.fr).
- **Date de création** : Octobre 2000, mis à jour en Octobre 2001.
- **Date de Publication** : {sub-ref}`today`.
- **Statut du document** : Terminé.
- **Résumé** : Comprendre les principales causes de la motivation chez l'élève. Ce chapitre montre que la motivation des élèves à travailler est plus dépendante de leur manière de considérer leur environnement et leur propre capacité que de la matière enseignée ou de l'enseignant. Trois principaux paramètres influant la motivation sont considérés ici : l'attribution personnelle de la réussite ou de l'échec, la conception de l'intelligence, la conception des buts de l'école.
- **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.
- **Licence** : Document placé sous licence *Creative Commons* : [BY-NC-SA](http://creativecommons.org/licenses/by-nc-sa/3.0/fr/).
:::

## Introduction

Le "métier" de l'élève est de s'engager dans les activités d'apprentissage du mieux qu'il le peut et de les mener à leur terme, sans baisser les bras. Lorsqu'il est confronté à des problèmes, des erreurs, il doit en tirer parti pour progresser, et non abandonner. En bref, l'école attend des élèves qu'il soient motivés, or ce n'est vraisemblablement pas le cas de tous. Qu'est-ce qui fait que certains s'engagent pleinement dans les activités scolaires, alors que d'autres ont abandonné toute idée de suivre, et que d'autres encore perdent cette idée, d'année en année ?

En bref, la motivation d'un élève est principalement déterminée par l'image qu'il se fait de lui-même et de la situation dans laquelle il travaille. Il s'agit de bien comprendre que c'est la *représentation de la situation* que se fait l'élève qui compte et non la situation elle-même. Par exemple, une réussite peut être profitable pour un élève, le rendre motivé, seulement s'il s'en attribue la compétence. Dans le cas contraire, il aura tendance à baisser les bras.

## Ce que l'on sait

Les enseignants définissent intuitivement la motivation comme "*ce qui fait que leurs élèves écoutent attentivement et travaillent fort*" ({cite}`viau97` p. 6). Cette définition est toutefois trop vague pour que l'on puisse étudier ce phénomène et surtout infléchir sur lui -- qu'est-ce qu'une écoute attentive ? qu'est-ce qu'un travail fort ? -- De nombreux travaux ont permis d'élaborer la définition suivante : "*La motivation en contexte scolaire est un état dynamique qui a ses origines dans les perceptions qu'un élève a de lui-même et de son environnement et qui l'incite à choisir une activité, à s'y engager et à persévérer dans son accomplissement afin d'atteindre un but.*" ({cite}`viau97` p. 7).

On peut noter que la motivation est un phénomène *dynamique* (qui change constamment), qui est influencé par les perceptions de l'élève, son comportement, son environnement, qui implique l'atteinte d'un but. Ainsi, la motivation n'est pas seulement fonction de la discipline enseignée, comme parfois on peut le penser, mais aussi des conditions dans lesquelles l'élève apprend et des perceptions qu'il a de ces conditions. L'enseignant ne doit donc pas s'attendre que la matière enseignée soit, à elle seule, génératrice de motivation. La perception que l'élève a de l'intelligence et des buts de l'école vont intervenir : comment peut-il travailler en étant motivé s'il se sait faible, s'il pense que le but principal de l'école est de sélectionner et que l'intelligence est une caractéristique humaine stable ?

Nous détaillons ici les trois paramètres principaux qui influent sur la motivation :

- l'attribution des causes de réussite ou d'échec de la situation ;
- la conception de l'intelligence ;
- la perception des buts poursuivis par l'école ou par son enseignant. (Cette introduction d'après Crahay {cite}`crahay99` p. 281 *et sq.*).

### Trois paramètres influant la motivation des élèves

#### L'attribution des causes de réussite ou d'échec de la tâche

Cette théorie est issue de l'attribution causale ({cite}`crahay99` p. 283 *et sq.*). On a tendance à chercher une explication, une origine, à tous les événements qui nous arrivent, qu'ils soient heureux ou malheureux. Ainsi, les élèves auront tendance à trouver des causes à leurs réussites ou leurs échecs. Cette perception des causes de réussite ou d'échec d'une activité qu'on propose de faire à l'élève joue un rôle important dans sa motivation. Ces causes peuvent être analysées en trois catégories : le lieu de la cause, sa stabilité et sa contrôlabilité.

- La cause peut être *interne* lorsque l'élève attribue sa réussite ou son échec à un facteur qui lui est propre (talent, effort, aptitude intellectuelle, fatigue, méthode de travail, etc.), *externe* dans le cas contraire (qualité ou lourdeur du programme scolaire, compétence des enseignants, difficulté de l'épreuve, etc.).
- Une cause sera *stable* lorsqu'elle est permanente aux yeux de l'élève, *variable ou instable* lorsqu'elle lui paraîtra pouvoir varier avec la durée (*e.g.*, humeur de l'enseignant, divers aléas, etc.).
- Une cause sera *contrôlable* lorsque l'élève considère qu'il est l'acteur principal de ce qui s'est produit, ou qu'il pense qu'il pourra, dans l'avenir, infléchir sur ce type d'événement. Elle sera *incontrôlable* dans le cas contraire (il pense n'avoir aucun pouvoir sur ce qu'il lui advient). Ces trois paramètres se combinent pour donner huit causes différentes de réussite ou d'échec, ainsi que des émotions associées.

**Tableau I -- Combinaison des trois paramètres concernant l'attribution causale de la réussite ou de l'échec de la tâche, ainsi que les émotions associées en cas de réussite ou d'échec** (d'après {cite}`crahay99` p. 284 ; {cite}`archambault96` p. 110).

| Causes  | Stabilité | Contrôlabilité  | Variables en jeu                           | Emotions associées               |
| ------- | --------- | --------------- | ------------------------------------------ | -------------------------------- |
| Interne | Stable    | Contrôlable     | Stratégies d'apprentissage                 | Gratitude/Colère                 |
| Interne | Stable    | Non contrôlable | Aptitudes intellectuelles                  | Fierté/honte, désespoir          |
| Interne | Instable  | Contrôlable     | Effort                                     | Fierté culpabilité               |
| Interne | Instable  | Non contrôlable | Maladie                                    |                                  |
| Externe | Stable    | Contrôlable     | Cours de rattrapage                        |                                  |
| Externe | Stable    | Non contrôlable | Niveau de difficulté du travail, programme | Désintérêt, colère               |
| Externe | Instable  | Contrôlable     | Perceptions de l'enseignant                |                                  |
| Externe | Instable  | Non contrôlable | Chance, humeur de l'enseignant             | Gratitude, surprise, résignation |

Il est important de noter que c'est la *perception* de cette attribution causale par l'élève qui est importante et non pas une causalité *réelle*. Par exemple, un élève peut penser que, quoi qu'il fasse, son enseignant le considérera comme un incapable (perception de l'enseignant non contrôlable) ; alors qu'un autre élève peut penser l'inverse.

Cette perception de causalité est donc tout à fait personnelle, même pour des activités identiques, deux élèves pourront lui attribuer des propriétés différentes. D'autre part, ces différentes situations génèrent des émotions différentes.

Si un élève échoue pour la première fois à un examen, il ressentira du désespoir s'il considère qu'il n'a pas les capacités requises pour réussir ce genre d'épreuves (cause interne, stable et non contrôlable). Il sera déçu s'il attribue son échec à la difficulté de la tâche (cause externe, instable si la tâche peut être moins difficile la prochaine fois, non contrôlable).

Enfin, si l'on étudie de près le tableau ci-dessus, on s'apercevra que certains paramètres peuvent être classés dans d'autres catégories : les stratégies d'apprentissage, par exemple, peuvent ne pas être une cause stable chez tous les élèves. Un élève en situation d'échec répété développera peut-être un *sentiment d'incapacité acquis* (encore nommé résignation apprise), c'est-à-dire que l'élève, placé dans une situation pour laquelle il ne voit pas d'issue favorable, va s'abandonner passivement au destin. Il attribue ainsi ses échecs répétés à des causes internes, stables et non contrôlables. Il impute ses performances médiocres à une incapacité intellectuelle qu'il pense inchangeable.

Une étude citée dans Gagné *et al.* ({cite}`gagne93` p. 435) montre bien que l'attribution de l'échec à l'un ou l'autre des paramètres ci-dessus influe sur la performance des élèves. On demande à des élèves de réaliser une première tâche de remplacement de symboles, à l'issue de     laquelle on "crée de l'échec" en informant tous les élèves, avant qu'ils ont terminé leur travail que la durée est écoulée. On leur fait attribuer cet échec à l'un des paramètres suivants : manque d'habileté,   difficulté de la tâche, effort, manque de chance, habileté plus difficulté de la tâche. Ensuite, une deuxième tâche du même type leur est demandée. Normalement, une vitesse plus rapide doit être attendue,    simplement parce que les élèves ont déjà réalisé une tâche semblable. Mais seuls les élèves "non chanceux" et ceux qui n'ont pas fait assez d'efforts progressent significativement en vitesse d'exécution. Ainsi, l'attribution que l'élève se fait de son échec influe bien sur ses performances futures.

Ces différentes définitions nous permettent de classer les élèves en quatre catégories, selon leurs motivations à réussir et leur craintes d'échouer.

- Les élèves "*centrés sur la réussite*" sont activement engagés dans les tâches scolaires, peu anxieux et ennuyés par le travail.
- Les "*éviteurs d'échec*", sont des élèves très anxieux, peu motivés par le succès, essayant d'éviter les tâches demandées.
- Les élèves en "*surrégime*" sont des élèves à la fois motivés pour réussir et anxieux dans l'échec. Ils travaillent beaucoup, mais sont stressés, pessimistes.
- Les "*accepteurs d'échec*" sont des élèves peu concernés par les tâches scolaires et/ou par les modalités d'évaluation.

**Tableau II -- Quatre types d'élèves, selon leur motivation à réussir et leur craintes d'échouer** ({cite}`pintrich96`, p. 72).

```{eval-rst}
+------------------+-------------+--------------------+----------------------+
|                  |             | **Motivation à réussir**                  |
+==================+=============+====================+======================+
|                  |             | **Faible**         | **Forte**            |
+------------------+-------------+--------------------+----------------------+
| **Motivation à   | **Faible**  | Accepteurs d'échec | Centrés-réussite     |
| éviter l'échec** |             |                    |                      |
+                  +-------------+--------------------+----------------------+
|                  | **Forte**   | Eviteurs d'échec   | "En surrégime"       |
+------------------+-------------+--------------------+----------------------+
```

#### La conception de l'intelligence

Le deuxième facteur est *La conception de l'intelligence* ({cite}`crahay99` p. 285 *et sq.*). La psychologie populaire a tendance à représenter l'intelligence par une entité mesurable, (par les tests de QI), innée (on naît plus ou moins intelligent) et stable (il est difficile de devenir plus intelligent). Cette conception est présente chez certains élèves et elle influe sur leur manière de travailler ainsi que sur leur motivation (pourquoi travailler plus si l'on reste d'égale intelligence ?). D'autres élèves considèrent l'intelligence comme une caractéristique évolutive dans le temps et selon les matières.

Des études expérimentales ont confirmé cette idée que la conception de l'intelligence influe sur la motivation et le travail scolaire. On a montré que les élèves ayant une conception fixe de l'intelligence avaient tendance à choisir des activités qui leur assurent un jugement positif de la part de l'enseignant et tendent à éviter les risques d'évaluation négative, donc les activités plus complexes et pouvant en revanche les faire plus progresser. Les élèves ayant une conception fixe de l'intelligence se comportent en classe comme s'ils étaient face à un jeu de "quitte ou double" : en cas de réussite, ils croient en leur intelligence ; en cas d'échec, ils attribuent ce dernier à leur non-intelligence, définitive, puisqu'ils pensent qu'elle est stable dans le temps. En revanche, les élèves ayant une conception évolutive de l'intelligence considèrent les échecs non pas comme un signe d'incompétence définitive, intrinsèque à eux-mêmes, mais le signe d'une stratégie inappropriée.

#### La perception des buts poursuivis par l'école

Le troisième et dernier facteur est *La perception des buts poursuivis par l'école* ({cite}`crahay99` p. 287 *et sq*.) Au cours de sa scolarité, les élèves se construisent une représentation du système scolaire, de ses normes, ses buts, les manières de s'y comporter, etc. Cette représentation est construite par les multiples échanges qu'ils ont avec leurs parents, les enseignants, leur camarades. De plus, cette représentation évolue dans le temps. Elle passe d'une représentation de l'école comme un lieu d'apprentissage (école primaire) à une école comme lieu d'évaluation et de sélection (secondaire, supérieur). Ainsi, les élèves du primaire, lorsqu'on les questionne sur l'école, indiquent qu'ils y acquièrent des connaissances, y apprennent à lire, écrire, etc., alors que leurs aînés indiquent surtout qu'ils se soucient d'éviter l'échec en réussissant leurs examens.

La conception que l'élève se fait des buts de l'école influence donc les risques qu'il est prêt à prendre, le niveau de réussite qu'il se fixe, les émotions qu'il ressent ainsi que la nature de ses attributions causales (voir ci-dessus). Si l'élève pense que l'école est faite pour sélectionner et évaluer, il n'a aucun intérêt à dévoiler à l'enseignant ses lacunes. Il va de plus éviter toute situation où l'enseignant pourrait porter un jugement négatif sur lui. La stratégie générale de cet élève sera une stratégie de défense, d'évitement : ne rien tenter d'audacieux, fuir les tâches difficiles, esquiver les questions complexes. De même, toute l'énergie de l'élève va être consacrée à la validation de ses compétences existantes, plutôt qu'à l'acquisition de compétences nouvelles. Enfin, ce type d'élève va avoir tendance à attribuer ses réussites et échecs à des causes externes (c'est la faute au prof., le devoir était trop dur/facile). L'élève qui pense que l'école a pour but de favoriser les apprentissages va tenter tout ce qui est en son pouvoir pour progresser, réussir de nouvelles activités. Il attribuera sa réussite à des causes internes, ou externes, mais évolutives.

### Pour résumer

Voici maintenant un résumé des recherches sur la motivation, en lien avec la théorie de l'attribution ({cite}`viau97`, pp. 68-69).

- La plupart des élèves, quel que soit leur âge, a tendance à attribuer ses réussites à leurs capacités intellectuelles, alors qu'ils attribuent leurs échecs aux efforts qu'ils n'ont pas faits ou à des causes externes à eux-mêmes.
- Les élèves de maternelle et primaire confondent intelligence et effort, ce qu'ils ne font plus dès la fin de primaire. Ensuite, l'intelligence est perçue comme une cause interne, stable et incontrôlable.
- Certains élèves ont tendance à éviter d'entreprendre des tâches qui comportent un certain risque d'échec, afin de conserver une image auprès de leur camarades. Ils entreprennent donc des tâches très faciles ou très difficiles (des dernières ne menaçant pas leur image, puisqu'un échec sera considéré comme normal).
- Les élèves qui réussissent le mieux attribuent leurs succès aux efforts qu'ils fournissent et à leurs capacités intellectuelles, alors qu'ils attribuent leurs échecs à des causes internes, modifiables et contrôlables, comme l'effort.
- Les élèves développant une incapacité apprise ont tendance à attribuer leur succès à des causes externes, comme la chance, et leurs échecs à des causes internes, stables et incontrôlables, comme leurs capacités intellectuelles. Leurs échecs les conduisent ainsi à se diminuer, et ils ne s'accordent aucun crédit pour leurs succès.
- Lorsque les élèves faibles sentent qu'ils vont échouer, ils cherchent à préserver une image positive d'eux-mêmes en ne fournissant pas les efforts nécessaires pour réussir, afin de pouvoir se dire, en cas d'échec, "si j'avais voulu, j'aurais pu réussir".

## Ce que l'on peut faire

Les enseignants peuvent intervenir sur la motivation de leurs élèves. Tout d'abord en affirmant le caractère évolutif, non figé, de l'intelligence et la possibilité pour chacun d'essayer de maîtriser les compétences scolaires "*Tu es capable de faire cela comme les autres*" plutôt que "*Le calcul et toi, cela fait deux*". L'enseignant peut aussi sembler moins préoccupé par l'évaluation, afin de montrer aux élèves qu'il n'est pas là uniquement pour les évaluer. L'enseignant peut aussi tenter de montrer aux élèves que les élèves sont en grande partie responsables de leurs succès et échecs (attribution interne) ({cite}`crahay99` p. 289)

### Évaluer la motivation des élèves

Il peut être utile d'évaluer la motivation des élèves à une tâche. Trois questions principales seront posées, suivent quelques critères permettant d'y répondre {cite}`viau97` pp. 148 *et sq*.) :

Quelle valeur l'élève accorde-t-il à une activité et à la matière qui s'y rattache ?

- Il trouve intéressantes les activités proposées,
- Poser des questions qui dépassent le programme ou la matière,
- S'engager dans des activités d'apprentissage non obligatoires,
- Ne pas s'arrêter facilement quand on a commencé une activité.

Quelle opinion a-t-il de sa compétence à la réussir ?

- Persévérer dans l'accomplissement d'une tâche difficile,
- Travailler de façon autonome,
- Tenter de répondre volontairement aux questions posées en classe,
- Ne pas abandonner quand on rencontre des difficultés,
- Prendre plaisir à relever des défis dans le domaine d'apprentissage.

Quel degré de contrôle estime-t-il avoir sur le déroulement de cette
activité ?

- Prêter attention à ce que l'enseignant dit,
- Commencer immédiatement à travailler lorsqu'on demande d'accomplir une tâche,
- Demeurer attentif jusqu'à ce qu'on ait terminé un travail,
- Respecter les délais.

### Les stratégies de motivation

L'enseignant peut également préconiser quelques stratégies qui pourront aider l'élève à comprendre qu'il peut essayer de se motiver lui-même plutôt que d'attendre qu'on le motive ({cite}`viau97`, p. 170) :

- Se fixer des objectifs afin d'évaluer le travail accompli,
- Diviser son travail à faire en plusieurs parties,
- Se récompenser après la réalisation d'une activité longue et    difficile, par exemple en écoutant de la musique,
- Entrecouper les activités difficiles par des activités plus faciles (par exemple, recopier au propre les exercices réussis, afin d'en    commencer d'autres),
- Prendre le temps d'évaluer le chemin parcouru et les apprentissages réalisés,
- Dans les moments difficiles s'imaginer en train de faire le métier auquel on aspire,
- Se rappeler ses réussites antérieures et se dire qu'elles prouvent que l'on est capable de réussir.

## Quizz

```{eval-rst}
.. eqt:: Motivation-1

        **Question 1. Selon Crahay (1999), quels sont les trois paramètres qui influent sur la motivation ?**

        A) :eqt:`C` `1° L'attribution des causes de réussite ou d'échec ; 2° La conception de l'intelligence ; 3° La perception des buts poursuivis par l'école`
        B) :eqt:`I` `1° La dimension ludique de l'apprentissage ; 2° L'attribution des causes de réussite ou d'échec ; 3° L'attention de l'enseignant envers les élèves`
        C) :eqt:`I` `1° L'attention de l'enseignant envers les élèves ; 2° La conception de l'intelligence ; 3° La compréhension des informations`
        D) :eqt:`I` `1° La compréhension des informations ; 2° La perception des buts poursuivis par l'école ; 3° La curiosité`
```

```{eval-rst}
.. eqt:: Motivation-2

        **Question 2. Les élèves en surrégime sont**

        A) :eqt:`I` `des élèves activement engagés dans les tâches scolaires, peu anxieux et ennuyé par le travail`
        B) :eqt:`C` `des élèves motivés pour réussir et anxieux dans l'échec`
        C) :eqt:`I` `des élèves très anxieux, peu motivés par le succès, essayant d'éviter les tâches demandées`
        D) :eqt:`I` `des élèves non concernés par les tâches scolaires et par les modalités d'évaluation`
```

```{eval-rst}
.. eqt:: Motivation-3

        **Question 3. Les élèves ayant une conception évolutive de l'intelligence**

        A) :eqt:`I` `choisissent des activités qui leurs assurent un jugement positif de la part de l'enseignant`
        B) :eqt:`I` ` tendent à éviter les risques d'évaluations négatives`
        C) :eqt:`I` ` se comportent en classe comme s'ils étaient face à un jeu de "quitte ou double"`
        D) :eqt:`C` ` considèrent les échecs comme le signe d'une stratégie inappropriée`

```

## Références

```{bibliography}
:filter: docname in docnames
```
