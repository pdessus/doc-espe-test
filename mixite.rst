.. _mixite_ecole:

*******************
La mixité à l'école
*******************

.. index::
    single: auteurs; Dessus, Philippe
    single: auteurs; Besse, Émilie

.. admonition:: Information

 * **Auteurs** : `Philippe Dessus <http://pdessus.fr/>`_, LaRAC & Espé, Univ. Grenoble Alpes. Le quizz a été réalisé par Émilie Besse, projet `ReflexPro <http://www.idefi-reflexpro.fr>`_.

 * **Date de création** : Mars 2007.

 * **Date de publication** : |today|.

 * **Statut du document** : Terminé.

 * **Résumé** : Ce document s'intéresse à la notion de mixité à  l'école. Quels peuvent être ses avantages et inconvénients, et à propos de quels critères (mixité de genre, sociale) peut-on l'envisager ? Nous nous centrerons ici sur les différences sexuelles (biologiques) ou de genre (sociales).

 * **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.

 * **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.


.. https://www.thecut.com/2017/01/psychologys-racism-measuring-tool-isnt-up-to-the-job.html

Introduction
============

La question de l'hétérogénéité en éducation est complexe. D'une part, parce que les élèves sont naturellement hétérogènes (l'élève neutre n'existe pas), et de très nombreuses manières différentes. Ensuite, parce que l'enseignante ne peut pas prendre en compte toute cette hétérogénéité dans sa pratique. Enfin, parce que cette dernière ne peut contrôler elle-même la mixité de sa classe : certaines filières étant majoritairement composées de garçons ou de filles. Toutefois, malgré toutes ces limites, ce document donne quelques éléments pour juger de l'importance de la mixité à l'école, et de ses différentes implications sur la réussite scolaire ou même la société.

Pendant longtemps (ce qui suit est issu de Perrot :cite:`perrot04`), l'école a séparé garçons et filles (autant au niveau des élèves que des enseignants), pour des raisons de possible "fréquentation indécente". Sous la III\ :sup:`e` République (1880), les programmes sont mixtes, sinon les écoles. Les premiers lycées de filles sont ouverts au même moment, mais avec un programme spécifique (sans latin, avec travaux manuels), et sans permettre de passer le baccalauréat. Ce n'est qu'à la fin du XIX\ :sup:`e` siècle que de nombreux intellectuels militent pour la mixité totale, et elle sera tardive : 1957 dans l'enseignement primaire, 1963 dans les collèges, 1975 à tous les niveaux de l'enseignement primaire et secondaire (Loi Haby). Les écoles d'enseignement supérieur deviennent également mixtes à cette époque (Polytechnique en 1972, HEC, 1973, l'Ecole Normale Supérieure en 1981-86).


Ce que l'on sait
================

Gestion du parcours scolaire et conséquences sur l'apprentissage
----------------------------------------------------------------

Comme le rappelle l'ouvrage collectif de l'Unesco (:cite:`unesco03`, p. 30), chaque Etat-nation fait le choix de former ses jeunes citoyens de la manière qui lui convient, c'est-à-dire soit en se fondant sur des principes d'"égalité et d'inclusion sociale", pour "résorber les différences sociales, ethniques et culturelles/religieuses", soit en différenciant l'éducation par groupes d'enfants, pour prendre en compte les différences ou bien pour exclure certaines catégories.

Crahay et Delhaxhe :cite:`crahay04` étudient les effets sur l'apprentissage des élèves des deux stratégies ci-dessus. Soit on les sépare en fonction de leurs différences (contraindre l'homogénéité), soit  au contraire, on conserve leur diversité naturelle (de niveau scolaire ou socio-culturel, de genre, etc.) dans des groupes hétérogènes. On peut distinguer trois  groupes de pays, d'hétérogénéité croissante (voir aussi Doc. SAPP Aperçu des systèmes éducatifs européens) :

#. L'Allemagne, la Belgique, les Pays-Bas et l'Autriche, privilégiant l'homogénéité des classes par divers moyens (redoublement, différenciation dès le collège, filières spécialisées, classes non mixtes au secondaire) ;
#. les pays "centraux", ayant un tronc commun pour le collège, une grande mixité et orientant peu en enseignement spécial (Italie, Espagne, France, Portugal, Grèce, Grande-Bretagne, Irlande) ;
#. les pays d'Europe du Nord, où le redoublement est inexistant, où une structure unique est en place au collège et l'orientation en filières spécialisées très rare (Danemark, Suède, Islande, Finlande, Norvège).

Les résultats montrent que les pays promouvant l'hétérogénéité d'une manière ou d'une autre (groupes 1 et 2 ci-dessus) ont une moyenne générale en lecture (*IEA-Literacy*, PISA 2000) inférieure à ceux du 3e groupe (en moyenne, et aussi en pourcentage d'élèves faibles) et, paradoxalement, sans que les meilleurs élèves de ce dernier groupe ne se distinguent nettement des autres. Ainsi, globalement, l'hétérogénéité profite aux élèves les plus faibles, sans pour autant être préjudiciables aux plus fortes. Bien évidemment, il est difficile de montrer un lien de causalité net entre ces deux variables (gestion du parcours hétérogène implique unemeilleure réussite scolaire), car certains pays plus homogènes réussissent plutôt bien (comme la France).

Passons maintenant à un niveau individuel. Dans toute société, les enfants assimilent, au fur et à mesure qu'ils grandissent, les normes et modèles sociaux correspondant à leur genre (préféré ici au mot "sexe", référant au biologique), statut social, etc. Ces normes pourront, lorsqu'elles seront citoyennes, être reproduites à l'identique, ou encore atténuées, selon leur éducation. Il est donc important que l'école, par son rôle de formation des futures citoyennes, d'atténuer les effets néfastes de ces normes.

Les normes chez les enfants
---------------------------

Nous ne pouvons ici rendre compte des travaux en psychologie de l'enfant étudiant comment ces dernières acquièrent les normes liées à leur genre. Ce serait long, et surtout peu approprié, puisque dès 2 ans (donc avant d'être scolarisées), les enfants ont déjà établi de nombreuses connaissances relatives aux différences entre masculin et au féminin :cite:`lorenzi98`. En revanche, cet auteur souligne que ce ne sont que des constats de différences, et non pas d'une supériorité d'un genre (voire d'un sexe) sur l'autre. Ainsi, l'éducation familiale occidentale reproduit de nombreux stéréotypes liés au genre : on désaprouve plus un garçon s'engageant dans un jeu féminin que le contraire, et un enfant pleurant va être estimé en colère s'il est présenté comme un garçon, et estimé ayant peur en tant que fille. Enfin, jusqu'à 5-7 ans :cite:`lorenzi98`, les violations de stéréotypes de genre sont jugés au moins aussi incorrectes que des transgressions morales. Après cet âge, les enfants acceptent mieux un certain chevauchement dans les activités considérées comme acceptables pour les hommes *vs*. pour les femmes. Si ces différents stéréotypes existent en dehors de l'école, il faut souligner que l'école et les enseignants vont souvent perpétuer, voire parfois amplifier ces derniers (comme le pense Lorenzi-Cioldi).

Au collège et au lycée se mettent en place de nombreux comportements sexistes, soit au niveau du langage (insultes sexistes)  soit même du comportement. C'est à toute l'équipe éducative qu'il conviendra de mettre en place des solutions pour éviter la banalisation de ces comportemements (*voir ci-dessous*).

Les normes chez les enseignants, dans la classe et dans le système scolaire
---------------------------------------------------------------------------

Comme tout être humain, une enseignante a des préjugés concernant les normes sociales et tend à se comporter selon elles avec ses élèves. Son comportement est ainsi dépendant des caractéristiques sexuelles, sociales, culturelles de ces derniers. Dans la classe,  Aebischer :cite:`aebischer98` a montré, dans une étude expérimentale auprès d'élèves de 3e. Elle a demandé à ces derniers de réaliser une tâche dans un contexte organisé en deux groupes d'élèves. Variait la matière de la tâche (dessin *vs*. géométrie) et le contexte de cette tâche (deux groupes mixtes, *vs*. deux groupes non mixtes de genre opposé, *vs*. deux groupes non mixtes de même genre). Les performances des garçons était bien meilleure en mixité que lorsqu'ils étaient entre garçons seulement, alors que pour les filles, leurs performances sont identiques dans tous les cas (bien qu'un peu moins bonnes, au départ, lorsqu'elles travaillent en groupes mixtes). Ainsi, la mixité profite aux garçons. De plus, ces derniers ont tendance à surestimer leur résultats, et les filles à les sous-estimer.

Au niveau des interactions et du jugement scolaire, maintenant, de nombreuses études montrent que, quel que soit le niveau d'enseignement et le genre de l'enseignant(e) (ce qui suit est tiré de :cite:`mosconi94` ; voir aussi Duru-Bellat :cite:`duru95`) :

* elles interagissent plus avec les garçons que les filles (leur donnent plus la parole, évaluent plus leurs réponses) ;
* elles connaissent plus rapidement le prénom des garçons que celui des filles ;
* elles posent plus de questions complexes ou ouvertes aux garçons qu'aux filles, qui ont, elles, des questions fermées ou à choix multiple ;
* elles s'attendent à l'indicipline des garçons et la docilité des filles, en réprimant moins sévèrement l'indicipline des premiers ;
* globalement, leurs attentes à propos des garçons sont  qu'ils sont intelligents, souvent brouillons et peuvent mieux faire, s'ils font des efforts ; à propos des filles, qu'elles n'ont pas, en général, d'intelligence inemployée, mais font ce qu'elles peuvent, en restant soigneuses (le travail soigné d'une fille est moins félicité que celui venant d'un garçon, "les filles apprennent ce que les garçons comprennent", :cite:`perrot04`). Cela peut entraîner, chez les premiers, un meilleur sentiment de compétence scolaire, à niveau égal :cite:`bressoux03`.

| Au sein du système scolaire même, il y a des inégalités de traitement massives garçons/filles, et ce quel que soit le pays. Ainsi, dans le secondaire et le supérieur (alors même que les filles réussissent mieux que les garçons dans le secondaire et le début du supérieur), les filles sont majoritaires dans les filières littéraires ou sociales (80 %, et 95 % en SMS), les garçons majoritaires dans les filières scientifiques et d'ingénierie (66 % de garçons en sciences). Et ce, alors même, également, qu'on observe aucune différences biologiques garçons/filles en termes d'intelligence ou de capacités sociales.

Les normes à l'école et dans les manuels
----------------------------------------

Le fait qu'à l'école maternelle et élémentaire, l'élève ne rencontre qu'un pourcentage faible d'enseignants masculins (en 2000, les chiffres :cite:`unesco03`, p. 386, mentionnent 80 % d'enseignantes femmes en maternelle et en élémentaire, 60 % dans le secondaire et 46 % dans le supérieur) influe bien évidemment sur la perception de la difficulté et du prestige du métier d'enseignante, mais aussi sur la vision des compétences masculines *vs*. féminines.

Serait-il donc plus facile d'enseigner à des élèves de maternelle parce que les enseignantes (mais aussi le personnel d'encadrement, *e.g.*, seulement 66 % de directrices d'école primaire ou enseignantes spécialisées, et 21 % de proviseures, voir :cite:`cacouault04`) est presque exclusivement féminin ? Sans doute pas, mais faire que les hommes investissent également l'enseignement en école maternelle et élémentaire. De plus, de nombreuses analyses de manuels scolaires montrent que :cite:`lorenzi98`, d'une part, les femmes sont sous-représentées en valeur numérique et, d'autre part, 75 % des personnages centraux des histoires racontées sont masculins, et leurs sentiments sont stéréotypés (les hommes éprouvent majoritairement de la colère, le courage, la force, et sont dans des positions de commander ou d'exercer une profession hors du foyer ; les femmes, elles, éprouvent majoritairement de l'amour, de l'affection, la peur, la dépendance et sont plus souvent en position de s'occuper du foyer).

Une explication théorique
-------------------------

Si les phénomènes de discrimination liée au genre ont beaucoup été étudiés, on a peu essayé de les expliquer théoriquement. Jones et Dindia :cite:`jones04` montrent que trois types de théories ont été avancées sur ce point. Les théories :

* *fondées sur les processus cognitifs*, se centrent sur les attentes des protagonistes, communiquées par leur comportement, et entraînent donc un changement de ce dernier (e.g., l'effet Pygmalion : les prédictions des enseignantes quant aux performances de leurs élèves causent ces dernières) ;

* *fondées sur l'apprentissage social et le développement cognitif*, montrent comment, au cours de son développement, chaque individu apprend à s'identifier par rapport à son sexe biologique. Ainsi, les garçons et les filles comprennent et apprennent ce qu'ils doivent faire en tant que garçons/filles, au cours des situations qu'ils vivent ;

* *interactives*, montrent que l'enseignante (qui perçoit) et l"élève (qui reçoit) interagissent au sein d'une situation complexe où de très nombreux facteurs interagissent (biologiques, culturels, sociaux) pour entrer en relation en suivant des schémas de comportement reliés au genre/sexe.

Ce que l'on peut faire
======================

Très récemment, différents ministères, dont celui de l'éducation :cite:`men07`, ont réaffirmé l'importance de prendre en compte "la dimension sexuée dans l’ensemble de la démarche éducative, avec la mise en place de mesures spécifiques en direction des filles". Ainsi, une meilleure orientation scolaire et professionnelle des filles est envisagée, en essayant de sortir du déterminisme professionnel évoqué plus haut. De plus, une éducation à l'égalité entre les sexes sera mise en place (thématique de la mixité, prévention des violences sexistes). Enfin, des actions de formation des professionnels de l'éducation seront mises en place.

Se tester
---------

Puisqu'on a implicitement ces stéréotypes, la première chose à faire est sans doute de les tester. L'université Harvard a mis au point un test informatisé permettant de se tester sur la manière dont on associe, par exemple, les métiers scientifiques aux hommes et les métiers littéraires aux femmes (il en existe d'autres sur la perception des races). Nous en reproduisons ici un extrait, mais il faut noter que le site est plus précis, puisqu'il mesure la durée de réflexion. A partir de ce test (tiré de Gladwell :cite:`gladwell05` p. 79-80), réfléchir à un comportement d'enseignante égalitaire de ce point de vue. Voir aussi https://implicit.harvard.edu/implicit/selectatest.html pour d'autres tests selon ce principe.

**Tableau 1a – Test d'association implicite.** Cochez, pour chaque item du centre le plus vite possible la catégorie appropriée (case de gauche ou de droite).

+-----------------------+---------------------+-----------------------+
| Masculin ou familiale |                     | Féminin ou profession |
+=======================+=====================+=======================+
|                       | Enfant              |                       |
+-----------------------+---------------------+-----------------------+
|                       | Malika              |                       |
+-----------------------+---------------------+-----------------------+
|                       | Simon               |                       |
+-----------------------+---------------------+-----------------------+
|                       | Marchand            |                       |
+-----------------------+---------------------+-----------------------+
|                       | Emploi              |                       |
+-----------------------+---------------------+-----------------------+
|                       | Domestique          |                       |
+-----------------------+---------------------+-----------------------+
|                       | Entrepreneur        |                       |
+-----------------------+---------------------+-----------------------+
|                       | Bureau              |                       |
+-----------------------+---------------------+-----------------------+
|                       | Cousin              |                       |
+-----------------------+---------------------+-----------------------+
|                       | Maison              |                       |
+-----------------------+---------------------+-----------------------+
|                       | Eric                |                       |
+-----------------------+---------------------+-----------------------+
|                       | Fabrique            |                       |
+-----------------------+---------------------+-----------------------+

**Tableau 1b – Test d'association implicite.**


+--------------------------+---------------------+-----------------------+
| Masculin ou littérature  |                     | Féminin ou sciences   |
+==========================+=====================+=======================+
|                          | Arts                |                       |
+--------------------------+---------------------+-----------------------+
|                          | Tante               |                       |
+--------------------------+---------------------+-----------------------+
|                          | Neurosciences       |                       |
+--------------------------+---------------------+-----------------------+
|                          | Epouse              |                       |
+--------------------------+---------------------+-----------------------+
|                          | Chimie              |                       |
+--------------------------+---------------------+-----------------------+
|                          | Mari                |                       |
+--------------------------+---------------------+-----------------------+
|                          | Technologie         |                       |
+--------------------------+---------------------+-----------------------+
|                          | Musique             |                       |
+--------------------------+---------------------+-----------------------+
|                          | Homme               |                       |
+--------------------------+---------------------+-----------------------+
|                          | Biochimie           |                       |
+--------------------------+---------------------+-----------------------+
|                          | Grand-mère          |                       |
+--------------------------+---------------------+-----------------------+
|                          | Physique            |                       |
+--------------------------+---------------------+-----------------------+





Modifier les supports et veiller à la place et les rôles entre élèves
---------------------------------------------------------------------

Il est aussi possible de modifier les énoncés, les textes des supports utilisés en classe afin de faire varier le genre des protagonistes principaux, et de proposer des situations opposées aux stéréotypes (Maman lit le journal pendant que Papa fait la vaisselle). On peut également faire attention, dans les groupes de travail mixtes, que ce ne soit pas systématiquement les filles qui prennent des notes pendant que les garçons parlent :cite:`cabas04`, mais aussi au placement mixte de chacun dans la classe. En école maternelle ou en EPS, on proposera indifféremment aux garçons et filles des jeux orientés uniquement pour les uns ou les autres. De plus, selon la matière, on sera attentive à montrer les avancées concernant la situation des femmes (*e.g.*, les femmes scientifiques, l'obtention du suffrage pour les femmes dans les différents pays d'Europe, voir :cite:`zancarini04`). Eviter aussi de sous-entendre que les garçons et les filles auraient des capacités, ou une intelligence différentes. Enfin, intervenir lors de tout manifestation sexiste (langage, comportement).

Quizz
=====


.. eqt:: Mixite-1

	**Question 1. Quels sont les pays qui privilégient l'homogénéité des classes ?**

	A) :eqt:`I` `Grèce, Grande-Bretagne, Irlande`
	B) :eqt:`C` `Allemagne, Belgique, Pays-Bas`
	C) :eqt:`I` `Italie, Espagne, France`
	D) :eqt:`I` `Danemark, Suède, Finlande`

.. eqt:: Mixite-2

	**Question 2. Quels sont les pays qui privilégient la mixité des classes ?**

	A) :eqt:`C` `Italie, Espagne, France`
	B) :eqt:`I` `Danemark, Suède, Finlande`
	C) :eqt:`I` `Allemagne, Belgique, Pays Bas`
	D) :eqt:`I` `Grèce, Grande-Bretagne, Irlande`

.. eqt:: Mixite-3

	**Question 3. De quoi traite la théorie fondée sur les processus cognitifs de Jones et Dindia (2004) ?**

	A) :eqt:`I` `Elle montre comment chaque individu apprend à s’identifier par rapport à son sexe `
	B) :eqt:`I` `Elle montre que l’enseignant et l'élève interagissent au sein d’une situation complexe où de très nombreux facteurs interagissent`
	C) :eqt:`I` `Elle montre que les élèves s'identifient très rapidement à des groupes`
	D) :eqt:`C` `Elle se centre sur les attentes des protagonistes, communiquées par leur comportement`


Analyse de pratiques
====================

#. Comme nous l'avons vu dans le test d'association implicite, tout individu a des préconceptions, des préjugés à propos de ses élèves. Il s'agit ici d'essayer de s'en rendre compte et de les analyser. Ecrire, sans recours à une liste, les noms des élèves de votre classe. Notez l'ordre dans lequel vous les avez notés, et quels élèves vous avez du mal à vous souvenir. Ensuite, essayer d'analyser cet ordre : pourquoi arrivez-vous plus facilement à vous souvenir de certains élèves ? Qu'est-ce que cela reflète celles dont vous obtenez le meilleur, celles qui ont des problèmes, celles qui sont renfermées, celles que vous voudriez oublier. Est-ce qu'il y a une différence entre elles selon le genre, compétence, classe sociale, ou autre différence ?  (exercice de Pollard :cite:`pollard93`, p. 69).

#. En utilisant une caméra ou un magnétophone, enregistrez-vous en train de faire un cours. Ensuite, comptabilisez le nombre de fois où vous sollicitez ou répondez à des garçons ou des filles, en reportant ces données sur un plan de classe. Essayez aussi de mentionner la nature de l'interaction (félicitation/blâme, orienté forme/contenu). Que concluez-vous ? Est-ce que vous interagissez plus avec un groupe ou un autre ?


Références
==========

.. bibliography::
 :cited:
 :style: apa
