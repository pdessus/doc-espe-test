.. _metier_aphorismes:

**********************************************
Atelier -- Réfléchir à ses principes éducatifs
**********************************************

.. index::
	single: auteurs; Dessus, Philippe
	single: auteurs; Cyrs, Thomas E.
	single: auteurs; de Peretti, A.

.. admonition:: Informations

 * **Auteurs** : Thomas E. Cyrs (traduit par `Philippe Dessus <http://pdessus.fr/>`_, Inspé & LaRAC, Univ. Grenoble Alpes), et A. de Peretti.

 * **Date de création** : 2008.

 * **Date de publication** : |today|.

 * **Statut du document** : Terminé. 

 * **Résumé** : Enseigner est un métier complexe, aux multiples facettes. L'enseignant débutant doit faire face à de très nombreuses situations, parfois destabilisantes. Il se réfère, pour cela, à de nombreuses théories implicites qu'il a acquises de diverses manières (p. ex. en discutant avec ses collègues, son inspecteur, par son expérience d'élève, etc.). Ce document est consitué d'une cinquantaines d'aphorismes concernant le métier d'enseigner, aphorismes qui permettent de prendre position, d'affiner sa propre conception de son métier, qu'il utilise ou non des technologies numériques.

 * **Voir aussi** : Le document :ref:`principes_educatifs` qui contient des éléments plus théoriques sur cette question.
 
 * **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.

 * **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.


Aphorismes sur l'éducation
==========================

Tâche
-----

Relevez une dizaine d'aphorismes qui vous semblent le mieux cadrer avec votre propre conception de l'enseignement. Dites pourquoi. À votre tour, écrivez, sous la même forme, les quelques aphorismes qui vous permettent d'exercer votre métier d'enseignant dans de bonnes conditions.

Ces aphorismes sont traduits de Cyrs, T. *EDA, 123 of the most Important things that I have learned about Teaching and Learning at a distance*. Texte qui n'est plus accessible hormis `ici <http://web.archive.org/web/20030212073907/http://www.zianet.com/edacyrs/things_I_learned.htm>`_

Liste des aphorismes
--------------------

#. L'enseignant ne dispose pas de toute la connaissance que les élèves doivent s'approprier.
#. Les élèves peuvent apprendre beaucoup à leur enseignant.
#. C’est très difficile d’admettre que vous ne savez pas quelque chose.
#. Parler n’est pas enseigner.
#. Les enseignants ne motivent pas leurs élèves, ils créent un environnement dans lequel les élèves choisissent de se motiver.
#. Enseigner avec passion.
#. L'enthousiasme entraîne l'enthousiasme.
#. Croyez en vous-même et les élèves vous feront confiance.
#. Apprenez avec vos élèves et dites-leur que vous apprenez avec eux.
#. Soyez disponible avant et après la classe.
#. Ne supposez jamais que vos élèves savent quelque chose, vérifiez-le.
#. Ne faites pas confiance à la technique. Elle n’est jamais réellement transparente, mais translucide.
#. Soyez conscient des possibilités et limites des technologies que vous utilisez.
#. Ayez des solutions de secours lorsque vous enseignez avec des technologies. Soyez prêt au pire.
#. Les élèves n’apprennent jamais grâce aux technologies. Ils apprennent grâce à la communication par la technologie.
#. Ne mettez jamais un élève dans l'embarras.
#. Ne faites pas apprendre par cœur des faits déconnectés les uns aux autres. En revanche, montrez aux élèves où ces faits sont disponibles.
#. Enseigner, ce n'est pas seulement délivrer des informations.
#. Amenez les élèves à être curieux du contenu que vous enseignez.
#. Si, pour quelque raison que ce soit, vous n'appréciez pas un élèves, c'est votre problème.
#. Chaque fois que vous donnez un exemple, donnez également un contre-exemple.
#. Ce qu'un élève sait n'est pas important. Ce qui est important, c'est ce que cet élève sait faire avec ce qu'il sait.
#. Arrêtez de parler autant et laissez les élèves apprendre.
#. Apprenez d’un élève, car personne n’est plus intelligent que tous les participants réunis.
#. Les élèves apprennent seulement lorsqu’ils veulent apprendre.
#. Vous pouvez placer l'élève devant une fontaine, mais vous ne le ferez boire que s'il a soif.
#. Vos élèves auront un plus grand impact sur l'avenir que vous-même.
#. Évaluez seulement ce que vous  avez enseigné.
#. La fontaine de la Connaissance et la source de la Vérité ne sont pas toujours placées sur l'estrade.
#. Les enseignants ne sont plus sources et transmetteurs de connaissances, mais facilitateurs et guides d'apprentissage.
#. Un chercheur crée des nouvelles connaissances. Un enseignant communique ces nouvelles connaissances de façon à ce qu’elle soient comprises.
#. Les trois caractéristiques d’un bon enseignant sont l’enthousiasme, la clarté et les interactions avant et après la classe. 
#. Incitez les élèves à penser de manière critique plutôt que comme des perroquets.
#. Les seules questions idiotes sont les questions non posées.
#. Permettez à vos élèves de voir ce que vous dites en leur montrant des images.
#. Surprenez vos élèves de temps en temps en leur posant des questions provocantes.
#. Enseignez à des élèves, pas à des sujets.
#. Aménagez une classe sûre dans laquelle les élèves se sentent à l'aise pour exprimer leurs idées.
#. Soyez un voleur de bonnes idées, mais citez toujours leur auteur.
#. Les élèves peuvent être arrogants et impatients. Les enseignants également.
#. Un cours de qualité n’est pas seulement enseigné, il est aussi préparé.
#. Des enseignants nerveux rendent les élèves nerveux.
#. Il y a peu de bonnes réponses, seulement de bonnes questions.
#. Ce que l'on veut que les élèves apprennent est plus important que ce que l'on enseigne.
#. Si ce n’est pas écrit, cela n’existe pas.
#. Enseigner, ce n'est pas parler à, c'est parler avec.
#. Les élèves sont plus intéressés par les gens que par les choses. Laissez-leur savoir qui vous êtes.
#. Écoutez les élèves fréquemment. Ils peuvent avoir quelque chose d'important à dire.
#. Ce que vous connaissez de vos élèves dépend surtout de ce que vous cherchez en eux.
#. Il y a trois catégories d’enseignants : ceux que vous devez écouter ; ceux que vous ne pouvez vous empêcher d’écouter ; et ceux que vous voulez écouter.
#. Des cours traditionnels ne peuvent être portés sur Internet ou télévision sans modifications importantes.
#. Le mot coopération s’écrit NOUS.
#. Les bonnes questions n'arrivent pas du ciel, elles sont écrites et soigneusement préparées.
#. Les anecdotes renforcent les points importants d'un cours et établissent un rapport personnel avec l'enseignant.
   
Q-Sort sur les conceptions de l'éducation
=========================================

Le deuxième test est un *Q-sort* sur les conceptions de l'éducation. Ce test est tiré de Peretti, A. (Ed.). (1985). *Recueil d'instruments et de processus d'évaluation formative*, 2 T. Paris: I.N.R.P., pp. 539 *et sq*.

Tâche
-----
Vous avez à répartir les 32 propositions suivantes en 5 classes, en respectant le nombre d'items par classe. 

Lister successivement (dans l'ordre qui vous convient)

* les *deux* conceptions mises en tête (que vous préférez) ; 
* les *huit* conceptions   auxquelles vous avez parfois recours ; 
* les *douze* conceptions considérées   comme neutres ; 
* les *huit* conceptions auxquelles vous ne faites que rarement recours ;  
* les *deux* conceptions que vous rejetez fondamentalement.

**Eduquer, c'est...**
 
#. savoir attendre. 
#. inculquer le sens du devoir.                                                        
#. permettre aux possibilités d'une personne de se révéler.
#. laisser faire.   
#. apporter les conditionnements qui faciliteront l'apprentissage des bonnes habitudes.
#. donner l'exemple.   
#. communiquer en profondeur avec un jeune pour l'aider à communiquer avec lui-même. 
#. savoir se taire.
#. instruire.
#. dresser.    
#. révéler les valeurs essentielles.  
#. entraîner les jeunes à obéir.
#. accompagner les démarches tâtonnantes des jeunes pour qu'ils prennent davantage de hardiesse et de sécurité.
#. présenter les modèles de comportements fondamentaux.
#. apporter les contraintes immédiates qui réfrènent les instincts et les pulsions anarchiques.
#. provoquer inlassablement. 
#. aider progressivement un jeune à affronter son angoisse et à s'ouvrir aux autres.
#. savoir bousculer.    
#. faire confiance
#. s'éduquer.
#. inculquer des règles de vie.
#. favoriser l'apprentissage d'une méthode personnelle.
#. pousser chaque jeune à réfléchir par lui-même.
#. conseiller sans contraindre   
#. rendre conforme à la Société.  
#. accepter en tant qu'adulte de discuter avec les jeunes.
#. protéger les jeunes contre leurs propres défaillances.
#. répondre à l'attente des jeunes. 
#. s'ajuster avec souplesse à l'attente des jeunes.
#. aider les jeunes à s'insérer progressivement dans la société des adultes.
#. entraîner à des méthodes éprouvées.
#. démystifier les anxiétés des jeunes.

Les valeurs de l'éducation
==========================

Ce dernier test liste quelques valeurs couramment exprimées dans le monde de l'éducation, au travers de dix positions   d'enseignants. Les voici résumées par Muller, F. `Diversifier sa pédagogie <http://francois.muller.free.fr/diversifier/>`_. Voir sa `page sur les valeurs <http://francois.muller.free.fr/diversifier/mes6.htm>`_  (Muller citant Valentin, C. (2000). Formation des enseignants au travail en équipe. Communication à la 5\ :sup:`e` Biennale de l'éducation et de la formation. Paris : APRIEF. Voir aussi Valentin, C. (1997). *Enseignants : reconnaître ses valeurs pour agir*. Paris : ESF.

Tâche
-----
Lisez ces différents "mondes", et repérez votre ou vos modes de justification habituel(s). Repérez également ces mondes dans des situations scolaires précises, en montrant qu'elles peuvent être cause de blocage, d'immobilisme. Montrez enfin comment faire évoluer ces situations de manière constructive. L'exercice est également de Muller. 

+----------------------------------------------+-------------------------------------------------+
| Les positions                                | Ce qui empêche d'évoluer                        |
+==============================================+=================================================+
| Le paradis perdu : "C'était bien mieux       | L'enseignant d'autrefois est le seul valable    |
| autrefois !".                                |                                                 |
+----------------------------------------------+-------------------------------------------------+
| Le matérialisme : "On n'a pas les moyens !". | Revendiquer des moyens matériels avant d'avoir  |
|                                              | réfléchi à leur mise en œuvre et à la           |
|                                              | possibilité de faire autrement.                 |
+----------------------------------------------+-------------------------------------------------+
| Le libéralisme : "Chacun fait comme il veut  | Il suffit que l'enfant (un collègue) fasse ce   |
| !"                                           | qu'il veut pour qu'il apprenne (réussisse).     |
+----------------------------------------------+-------------------------------------------------+
| La défensive : "Méfions-nous, on cherche à   | Chercher les inconvénients dans toute solution  |
| nous nuire !"                                | nouvelle, par nature gênante.                   |
+----------------------------------------------+-------------------------------------------------+
| La résignation : "A quoi bon !"              | Considérer que ce qui arrive est une fatalité.  |
+----------------------------------------------+-------------------------------------------------+
| L'enfant-roi : "Les enfants ont toujours     | Il suffit que l'enfant suive ses penchants pour |
| raison !".                                   | qu'il apprenne.                                 |
+----------------------------------------------+-------------------------------------------------+
| Défaitisme : "J'ai tout essayé, on ne peut   | Prédire l'échec de toute tentative, avant       |
| rien faire."                                 | qu'elle ait démarré.                            |
+----------------------------------------------+-------------------------------------------------+
| Légalisme : "Ce n'est pas dans les textes."  | Se réfugier derrière les textes pour ne rien    |
|                                              | changer.                                        |
+----------------------------------------------+-------------------------------------------------+
| Changement à tout prix : "Tout cela ne vaut  | Vouloir changer sans en avoir évalué l'intérêt  |
| rien, il faut changer !"                     | et les conséquences.                            |
+----------------------------------------------+-------------------------------------------------+
| Réconciliation affective : "L'essentiel,     | Il suffit que les enseignants s'entendent bien  |
| c'est qu'on s'entende bien".                 | pour que les élèves apprennent et réussissent.  |
+----------------------------------------------+-------------------------------------------------+


