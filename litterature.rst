.. _litt_form_hum:

********************************
Littérature et formation humaine
********************************

.. index::
	single: auteurs; Dessus, Philippe
	single: auteurs; Besse, Émilie

.. admonition:: Informations

	* **Auteurs** : `Philippe Dessus <http://pdessus.fr/>`_, LaRAC & Inspé, Univ. Grenoble Alpes. Le quizz a été réalisé par Emilie Besse, projet `ReflexPro <http://www.idefi-reflexpro.fr>`_.

	* **Date de création** : Janvier 2008.

	* **Date de publication** : |today|.

	* **Statut du document** : Terminé.

	* **Résumé** : Ce document s'interroge sur "l'effet que ça fait" d'être lettré, c'est-à-dire de savoir lire et d'avoir accès à la littérature, à la fois d'un point de vue historique, car l'écrit a été inventé, mais aussi cognitif. Ce document se termine par une brève recension des activités possibles à partir de la littérature de jeunesse.

	* **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.

	* **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.


Ce que l'on sait
================

Perspective historique
----------------------

L'écriture est aujourd'hui massivement utilisée pour communiquer et diffuser des connaissances. Pourtant, on sait qu'elle n'a pas été conçue pour cela, mais à des fins économiques (recenser les dettes, des titres de propriété :cite:`olson98`. On a montré que ce sont les connaissances liées au temps et à l'espace qui ont, ensuite, été les premières à être diffusées par l'écrit, et ont permis à de plus en plus de personnes d'appréhender ces notions (quelle année est-on? quel âge j'ai, quand s'est passé tel ou tel événement?) qui restaient vagues dans toute culture orale :cite:`toth01`. D'un point de vue historique, il faut savoir qu'en France c'est au XIXe siècle que l'analphabétisme a reculé massivement (au tout début du XXe siècle, seulement 5 % des mariés étaient incapables de signer sur le registre, :cite:`toth01`).

Le rapport entre écrit et oral peut être considéré de deux manières :cite:`olson96` : soit l'écrit est la transcription de l'oral (on écrit ce que l'on dit, et uniquement cela), soit l'écrit est "une représentation non ambiguë d'une signification" (*id*., p. 86). De ce fait, l'écrit donnerait de nouveaux concepts et catégories pour penser la langue orale. Il est probable que la seconde hypothèse s'avère (d'autant plus que les premiers systèmes d'écriture ne sont pas syllabiques, mais graphiques). L'écrit, au fur et à mesure de la diffusion des compétences qui lui sont liées, a été utilisé à d'autres fins en devenant à la fois un moyen d'écrire la pensée, mais aussi un processus de pensée en soi. Savoir lire et écrire permet à la fois de pouvoir acquérir de nouvelles connaissances de manière autonome, mais aussi de pouvoir travailler sur elles, les améliorer, ce que Bereiter et Scardamalia :cite:`bereiter87` appellent le "*knowledge transforming*" (transformation de connaissances), par opposition au "*knowledge telling*" (tel pensé, tel écrit). Egan :cite:`egan97` (voir :cite:`dessus03` pour une revue en français) a montré que la lecture et l'écriture nous dotait de capacités spécifiques de compréhension qu'il nomme "compréhension romantique". La culture orale et écrite ont, en occident, cohabité pendant longtemps. Il est important de ne pas déprécier la première au profit de la seconde, tout d'abord parce que de nombreux éléments de la culture écrite proviennent d'une culture orale (contes, mythes), d'autre part parce que la culture orale permet dans de bonnes conditions, de diffuser une culture pouvant socialiser (*cf*. les rimes et rythmes). Egan montre que la centration sur les records, les hobbies et collections, et les héros sont des conséquences de l'acquisition de compétences liées à l'écrit.
Conséquences cognitives

Quel effet cela fait sur nos compétences cognitives d'être exposé à l'écrit ? Cette question simple a donné lieu à de très nombreux travaux, et a intéressé des psychologues prestigieux (dont Luria, Vygotski et Bruner, voir :cite:`dawda06`, pour une revue). On peut penser qu'il suffit de comparer les compétences ou connaissances de lettrés à celles d'illettrés (si tant est qu'on puisse en trouver). Mais la réponse est plus complexe, car, dans une société lettrée, les illettrés peuvent développer des habiletés cognitives spécifiques, du seul fait qu'ils sont eux aussi confrontés à l'écrit. Les premières études de ce que l’on appelle maintenant l’action (ou la cognition) située(s) ont été menées par Luria dans des tribus de tradition orale d’Asie centrale. On a posé à leurs membres la question suivante :

« *Dans le grand Nord, où il y a de la neige, tous les ours sont blancs. [La ville de] Novaya Zemlya est dans le grand Nord. De quelle couleur sont les ours, là-bas ?*» (*id.*, p. 74).

Ils ont eu la plus grande difficulté à répondre à ce type de question, qui fait appel à une généralisation, une induction. Ils répondaient à Luria que s’il voulait connaître la couleur des ours là bas, il n’avait qu’à y aller ou demander à quelqu’un qui y était allé, qu’eux ne connaissaient pas cette ville. De telles questions sont des syllogismes et, pour nous occidentaux alphabétisés, sont très faciles, car nous avons pris l’habitude, au cours de notre scolarité, de répondre à de telles questions, totalement décontextualisées. Nous nous concentrons sur la logique du syllogisme plutôt que sur ce à quoi il fait référence.
Toutefois, ces études ont été plus tard controversées, car elles induisaient un contexte trop prégnant pour les participants. Lorsque le contexte devient totalement irréaliste (p.ex., sur Mars, où il y a du Zlorg bleu, tous les Martiens sont bleus), les réponses deviennent similaires, que les participants soient ou non alphabétisés.

Le contact avec la littérature
------------------------------

Il nous faut déjà distinguer deux types de textes, les textes expositifs, dont le but est de transmettre des informations et les textes narratifs, dont le but est de raconter une histoire. Pourquoi nous racontons-nous des histoires ? Bruner :cite:`bruner95` répond à cette question en montrant que les histoires sont formées par (et exercent) notre pensée narrative, et que cela permet "de convertir l'expérience individuelle en une monnaie collective" (*id.*, p. 29), en mettant en tension des normes attendues et des transgressions. Pour Bruner, le récit est "le médium le mieux approprié pour décrire (quand ce n'est pas pour caricaturer) les grands problèmes de l'être humain : les histoires d'enfants perdus, d'amants jaloux [...]. Les problèmes les plus typiques finissent par constituer des métaphores de la condition humaine". (p. 75). Ainsi, nous l'avons signalé plus haut, beaucoup de textes littéraires proviennent de la culture orale. Ils ont des caractéristiques particulières qui les rendent aisément mémorisables (ce qui suit est repris de :cite:`bruner95,kreuz01`) : une formule de départ (il était une fois), de multiples répétitions (*e.g.*, événéments, nombres, objets), des personnages pouvant être considérés comme des "agents libres", auxquels quelque chose arrive (problème, rencontre), pour laquelle ils entreprennent une "quête" amenant la résolution du problème, un narrateur donnant son propre point de vue au récit.

Ce que l'on peut faire
======================

Les possibilités de travailler la littérature en classe sont multiples. En voici quelques éléments. Consulter pour plus de renseignements les documents d'accompagnement de la liste de littérature de jeunesse. Il donne une description détaillée des ouvrages et des pistes d'utilisation en classe celui du cycle 3 est disponible à http://www.crdp.ac-creteil.fr/telemaque/document/prog-notices2004.pdf

Apprendre
---------
Faire apprendre des textes (poésie, théâtre, romans) par cœur.

Lire et comprendre
------------------

1. **Travailler les tratégies de lecture de la compréhension** (ce qui suit est tiré de :cite:`lima06` pp. 29-30) : L'enseignant peut faire travailler les élèves dans quatre types de stratégies permettant d'améliorer la compréhension en lecture :

- *Stratégies d’enrichissement des connaissances*

	- *Clarifier le vocabulaire* : acquérir des procédures permettant d’identifier le sens de mots inconnus.

	- *Utiliser ses connaissances encyclopédiques* : il s’agit d’encourager les élèves à faire appel à leurs connaissances antérieures de diverses manières.

- *Stratégies d’organisation de l’information*

	- *Identifier et utiliser la structure des narrations* "consiste à conduire les élèves à déterminer le cadre spatio-temporel du récit, les personnages principaux et secondaires, leurs actions, leurs motivations et leurs buts, l’enchaînement des épisodes," etc.

 	- *L’entraînement au résumé* "permet d’apprendre les règles de la construction d’un résumé. La production de résumés peut être utilisée, soit pour réactiver les connaissances que l’on possède déjà sur une partie du texte, soit pour faire le point sur l’interprétation de l’extrait qui vient d’être lu."

 	- *Construire des organisateurs graphiques* "consiste à utiliser une schématisation afin de mettre en évidence les relations entre les idées énoncées. Il s’agit donc d’apprendre au lecteur à organiser les idées à l’aide de représentations graphiques".

- *Stratégies de traitement détaillé de l’information*

	- *Répondre à des questions* :  apprendre aux lecteurs à rechercher, dans le texte ou dans leurs connaissances, les informations nécessaires à l’élaboration d’une réponse, et à discriminer les différents types de questions selon qu’elles portent sur la surface du texte, sur des informations à inférer, ou sur les informations principales et la structure du texte.

 	- *Formuler des questions* : "les élèves apprennent à poser et à se poser des questions au cours de la lecture. Le professeur fournit aux élèves des indices procéduraux, sortes de guides destinés à repérer les endroits du texte et les manières de questionner."

2. **Travailler des stratégies de contrôle**

* *Évaluer sa compréhension* : "apprendre à prendre conscience de ses propres difficultés de compréhension, pendant la lecture ou en fin, et à repérer les sources de difficultés présentes dans les textes."

* *Réguler sa compréhension* : "acquérir un ensemble de procédures pouvant être utilisées pour lever une difficulté de compréhension. "

Lire et débattre
----------------

* Débats interprétatifs, à partir de textes, que ce soit avant, pendant ou après la lecture (voir :cite:`lima06`).
* Accès à une bibliothèque (de classe, école, municipalité).
* Lecture partagée : lecture à haute voix par l'enseignant d'un ouvrage (grand format), à un petit groupe d'élèves, et débat à propos du texte :cite:`lima06`.

Lire et produire
----------------
Concevoir des fiches de lecture.
Lien avec la production : faire lire pour faire écrire.
Faire lire des œuvres entières.

Quizz
=====

.. eqt:: Littérature-1

	**Question 1. Quels sont les deux principaux types de textes existants ?**

	A) :eqt:`I` `"Les textes descriptifs et les textes narratifs"`
	B) :eqt:`I` `"Les textes expositifs et les textes contemplatifs"`
	C) :eqt:`I` `"Les textes courts et les textes longs"`
	D) :eqt:`C` `"Les textes expositifs et les extes narratifs"`

.. eqt:: Littérature-2

	**Question 2. Quel est le but des textes expositifs ?**

	A) :eqt:`I` `De raconter une histoire`
	B) :eqt:`I` `D'exposer en détail une histoire`
	C) :eqt:`C` `De transmettre des informations`
	D) :eqt:`I` `De traduire un texte`

.. eqt:: Littérature-3

	**Question 3. Selon Lima et al. (2006), quelle est la stratégie de lecture permettant d'améliorer la compréhension ?**

	A) :eqt:`C` `Stratégie d'organisation de l'information`
	B) :eqt:`I` `Stratégie d'évaluation de sa compréhension`
	C) :eqt:`I` `Stratégie de régulation de sa compréhension`
	D) :eqt:`I` `Stratégie de tri de l'information`


Questions d'analyse de pratiques
================================

Repérez, dans les différents items du socle commun de connaissances et de compétences ceux qui font référence à la littérature. Comment comptez-vous (ou pensez-vous) les mettre en œuvre dans votre classe ?

Références
==========

.. bibliography::
	:cited:
    :style: apa
