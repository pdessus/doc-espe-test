.. _enseignant_reflexif:

***************************
Être un enseignant réflexif
***************************

.. index::
	single: auteurs; Brokfield, S. D.
	single: auteurs; Dessus, Philippe
	single: auteurs; Besse, Émilie

.. admonition:: Information

   * **Auteurs** : S. D. Brookfield, traduit par `Philippe Dessus <http://pdessus.fr/>`_, LaRAC & Espé, Univ. Grenoble Alpes. Le quizz a été réalisé par Emilie Besse, projet `ReFlexPro <http://www.idefi-reflexpro.fr>`_.

   * **Date de création** : Janvier 2008.

   * **Date de publication** : |today|.

   * **Statut du document** : Terminé.

   * **Résumé** : Ce document comporte une série d’exercices intéressant la  formation à la réflexion des enseignants en formation initiale. Il permet de questionner (ou faire questionner) ses pratiques d’enseignant de plusieurs manières :  se voir en tant qu’enseignant, en se   mettant à la place d’élèves ou collègues, en travaillant en groupes. D’après S. D. Brookfield (1995). *Becoming a critically reflective teacher*. San Francisco : Jossey-Bass. Traduit librement par Philippe Dessus.

   * **Note** : Pour des raisons de lisibilité, le masculin est utilisé dans ce document. Nous n'oublions pas que la majorité des enseignants du premier et second degré français sont des enseignantes.
   
   * **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.

   * **Licence** : Ce document est placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.


Introduction
============

Brookfield (1995) contient une série d’exercices intéressant la formation des enseignants en formation initiale à la réflexion. Même si les commentaires de Brookfield sont souvent liés au système éducatif américain (*voir l’Introduction ci-dessous*), les méthodes qu’il propose paraissent être utiles. Son ouvrage est constitué de quatre parties principales, qui permettent chacune une analyse différente de l’expérience de l’enseignant novice :

* les aspects autobiographiques (se voir en tant qu’enseignant) ; 
* l’expérience d’élève (se voir au travers d’élèves) ; 
* l’expérience de collègues ; 
* les aspects théoriques (cette dernière partie ne sera pas résumée ici).

Ce qu’il faut savoir
====================

Le but du livre de Brookfield (1995) est détaillé au Chap. 12, pp. 246 *et sq.* : “Créer une culture de la réflexion”. Il montre en effet qu’il existe quelques barrières à cette culture. Des barrières qui sont liées à la culture des enseignants : une culture du silence, de l’individualisme, du secret. Détaillons-les (Brookfield, 1995, pp. 247 *et sq*.). 

Pour Brookfield, *le silence* entoure les enseignants, un silence à propos du processus et du sens des manières d’enseigner. En effet, l’enseignant est confronté à la dynamique des processus de classe et la lutte quotidienne envers des dilemmes insolubles et requêtes contradictoires. De plus, les conditions de travail inhibent souvent la parole des enseignants. Brookfield cite Richert (1992, p. 193) : “Les enseignants ne sont pas écoutés parce qu’ils ne parlent pas. Et ils ne parlent pas parce qu’ils font partie d’une culture qui les rend silencieux par un ensemble de mécanismes d’oppression tels que le surmenage, un statut peu élevé, et un standard de performance externe”. Demander de l’aide est souvent considéré comme une preuve d’impuissance ou d’incompétence.

Ensuite, la culture de *l’individualisme*. L’action collective d’enseignants est rarement considérée comme une chose positive. L’excellence enseignante est souvent perçue comme une volonté et des efforts individuelles. Elle est souvent vue comme la manifestation de dinosaures face à l’innovation et les réformes. Il existe pourtant une rhétorique de la collaboration, mais elle est gênée par le prix à payer pour le travai collaboratif. Si cela peut rendre le travail plus facile, cela augmente le temps de réunions, le nombre d’élèves à gérer, etc. De plus, il y a peu de moyens en temps, espace et argent, consacrés à l’action collective d’enseignants. En bref, un travail collectif bien conduit est souvent plus coûteux en temps et ressources qu’un travail individuel. [Ces remarques concernent principalement le système éducatif américain. Il faudrait vérifier qu’elles concernent également le système français. NDT]

La troisième et dernière facette est la *culture du secret*. Une réflexion critique doit se faire dans des conditions dans lesquelles la personne qui la mène ne doit pas souffrir de censure, de désagréments, ou de conséquences négatives. Il est paradoxal qu’une profession qui a fait de la phrase “apprendre à partir de ses erreurs” un principe de travail pour ses élèves, ne la tient pas pour vraie pour elle-même. En effet, de telles erreurs sont immanquablement liées à une image d’incompétence. Pour Brookfield, cette image pourra changer seulement quand les enseignants chevronnés (ou les principaux) n’auront pas de réticences à déclarer publiquement à leurs collègues leurs propres erreurs.

Ce que l’on peut faire
======================

1. Aspects autobiographiques : se voir en tant qu’enseignant
------------------------------------------------------------

Les événements particuliers
```````````````````````````

Lorsque vous remarquez quelque chose qui vous semble important ou significatif — quelque chose qui vous  touche et vous rend ému, en colère, en joie, déprimé, ou dans un état notablement différence de votre état habituel, notez les détails suivants : 

* Décrivez brièvement l’événement — où et quand il est survenu, quels en sont les protagonistes, et ce qui le rend si important à vos yeux.

* Si c’est un événement plutôt positif, notez (1) ce qui fait que c’en est un à vos yeux (2) les choses que vous faites dans votre propre enseignement qui induiraient les mêmes événements auprès de vos élèves.

* Si c’est un événement plutôt négatif, notez (1) ce qui fait que c’en est un à vos yeux (2) les choses que vous faites dans votre propre enseignement qui induiraient les mêmes événements auprès de vos élèves.

* Indiquez aussi les actions que les protagonistes de l’événement auraient pu mener, et qui auraient rendu l’événement “banal”, et qui font que vous ne l’auriez pas remarqué.

* Enfin, compte tenu de vos réponses ci-dessus, formulez les leçons que vous pouvez en tirer pour votre propre expérience. (Brookfield, 1995, pp. 55-56).

Le journal de bord
``````````````````

Tenez un journal de bord en consignant vos réactions et vos interprétations à propos d’événements qui vous paraissent importants dans votre vie d’enseignant, en ce qu’ils vous permettent de comprendre des choses sur vous-même. Cela doit être fait de manière régulière (15 à 20 min. par semaine). Détaillez les événements les plus importants. Vous pouvez également répondre brièvement aux questions ci-dessous.

* Quel est le moment (ou les moments) de cette semaine dans lesquels j’étais le plus affirmé, présent, engagé en tant qu’enseignant — le moment où je me suis dit “C’est vraiment aussi facile que ça, être enseignant ?”

* Quel est le moment (ou les moments) de cette semaine dans lesquels j’étais le plus en retrait, ennuyé, etc. en tant qu’enseignant — le moment où je me suis dit “Je fais tout ça machinalement...”

* Quelle situation m’a rendu le plus anxieux ou bouleversé. Le genre de situation que je me repasse dans l’esprit avant de m’endormir, et qui me fait dire “Je ne veux plus endurer à nouveau ça”

* Quel événement m’a surpris, choqué, laissé sans défense, secoué, ou m’a réjoui ?

* De tout ce que j’ai fait cette semaine, qu’est-ce que je ferais différemment si je devais le refaire ?

* Qu’est-ce qui, de mes activités d’enseignement de cette semaine, me rend spécialement fier de moi ? Pourquoi ? (Brookfield, 1995, pp. 73-74).

L’audit d’apprentissage
```````````````````````

Ces questions permettent de faire le point sur ce que l’on a appris de sa propre expérience d’enseignement. Ce questionnaire est à réaliser à l’issue d’un trimestre ou d’une année scolaire. Il permet d’identifer les habiletés, connaissances et intuitions développées récemment. (Brookfield, 1995, pp. 75-76).

*Prenez en compte l’année/le trimestre écoulé(e) et répondez aux questions suivantes le plus honnêtement possible.*

* Par rapport à l’année/trimestre dernier(ère), je sais maintenant que...

* Par rapport à l’année/trimestre dernier(ère), je suis maintenant capable de...

* Par rapport à l’année/trimestre dernier(ère), je pourais maintenant montrer à un collègue comment...

* La chose la plus importante que j’ai apprise à propos de mes élèves, ce(tte) année/trimestre dernier(ère), est...

* La chose la plus importante que j’ai apprise à propos de ma manière d’enseigner, ce(tte) année/trimestre dernier(ère), est...

* La chose la plus importante que j’ai apprise sur moi, ce(tte) année/trimestre dernier(ère), est...

* Les choses que je savais à propos d’enseignement et d’apprentissage et qui ont le plus été confirmées ce(tte) trimestre/année sont que...

* Les choses que je savais à propos d’enseignement et d’apprentissage et qui ont le plus été questionnées ce(tte) trimestre/année sont que...

L’enseignant modèle
```````````````````

Une autre manière de se rendre compte de ses propres croyances et connaissances à propos d’enseignement est d’analyser de quelle manière on parle de collègues que l’on admire, et pourquoi on les admire. 

*Cet exercice vous demande de penser aux collègues avec lesquels vous travaillez ou avez travaillé, ou bien ceux quee vous connaissez et qui travaillent dans d’autres endroits. Répondez aux questions suivantes, à leur propos.*

* Lorsque vous pensez à votre propre carrière, quels collègues, à votre avis, représentent le mieux ce qu’un enseignant devrait être ?

* Qu’est-ce ce que ces personnes ont (ou font) qui les rend d’après vous si admirables ?

* Lorsque vous pensez à leur manière de travailler, citez les actions qu’elles mettent en œuvre qui représentent le mieux ce qui les rend admirables pour vous ?

* Si vous pensez aux actions de ces personnes, laquelle de leurs compétences emprunteriez-vous, serait-elle le mieux intégrée dans les vôtres propres ? (Brookfield, 1995, p. 77).

Consignes de survie
``````````````````` 

Imaginez que c’est votre dernier jour de travail dans votre établissement. Votre remplaçant arrive demain, mais vous aurez déjà quitté les lieux. Vous voulez, autant que possible, l’aider à éviter les problèmes et le stress que vous avez enduré au début de votre enseignement dans cet endroit. Vous décidez donc de lui écrire quelques “consignes de survie”. Ces consignes sont :

(1) ce qu’un enseignant a besoin de savoir pour survivre dans ce travail, 
(2) ce que votre remplaçant a besoin de connaître pour ne pas être dépassé dans ce travail, 
(3) ce que vous savez maintenant et que vous auriez aimé qu’on vous dise lorsque vous aviez commencé ce travail, 
(4) les choses que votre remplaçant devra éviter de penser ou faire. Ecrivez ces consignes le plus honnêtement possible.

Choisissez maintenant ce que vous pensez être la consigne la plus importante. Pourquoi pensez-vous cela ? Donnez une preuve convaincante à votre remplaçant que c’est bien le cas. Qu’est-il arrivé dans votre propre expérience qui vous fait penser que ce conseil est bien fondé, qu’il “fonctionne” bien dans l’action ? 

Après ces réflexions individuelles, formez un groupe avec trois autres personnes. Chaque personne prend 5 min. pour exprimer ses consignes. Ensuite, catégorisez ensemble les différentes sortes de consignes que vous avez écrites (*e.g.*, émotionnel, politique, instrumental, etc.). Passez ensuite 15 min. à analyser les preuves que vous avez citées comme fondement de votre consigne. Cette dernière était-elle un ouï-dire, un événement observé, une hypothèse testée, l’opinion d’une tierce personne, etc. ? (Brookfield, 1995, pp. 78-79).

2. Se voir avec des yeux d’élève
--------------------------------

Le journal d’apprenant
`````````````````````` 

La consigne est la même que pour le journal d’enseignant (voir ci-dessus). Elle se centre sur les pratiques d’apprentissage, plutôt que sur celles d’enseignant.

Consignes de survie
``````````````````` 

De la même manière, on peut rédiger des consignes d’étudiant. Le but de cet exercice est d’écrire une lettre à l’attention des prochains étudiants de ce cours, l’année prochaine. Vous écrirez ce que vous pensez utile qu’ils connaissent afin de survivre et de  profiter de ce cours. Vous pourrez notammen répondre aux questions suivantes : 

* “Qu’est-ce que je sais à propos de ce cours que j’aurais aimé connaître quand il a commencé”. 
* “Quelles sont les choses les plus importantes à connaître pour garder votre santé mentale dans ce cours”,
* “Quels sont les erreurs les plus fréquentes, et pourtant évitables, que nous avons faites pendant ce cours ?”
* “Quels sont les mots qu’on aurait pu écrire sur la table afin  de réussir ce cours”. (Brookfield, 1995, p. 107).

Le questionnaire d’incident critique
````````````````````````````````````
Ce questionnaire peut tout autant servir de moyen de régulation du groupe de la classe tout au long de l’année (à l’usage des professeurs-stagiaires et de leurs élèves), ainsi que pour réguler les cours que les formateurs mènent avec les professeurs-stagiaires.

*Prenez quelques minutes pour répondre à chaque question ci-dessous à propos des cours de cette semaine.*

* À quel moment des cours de cette semaine vous êtes vous senti le plus impliqué dans ce qui arrivait ? 
* À quel moment des cours de cette semaine vous êtes vous senti le plus distant avec ce qui se passait ?
* Quelle action (d’un élève, de l’enseignant) cette dernière semaine vous a-t-elle paru la plus positive ou profitable ?
* Quelle action (d’un élève, de l’enseignant) cette dernière semaine vous a-t-elle paru la plus bizarre ou gênante ?
* Qu’est-ce qui vous a surpris le plus dans les cours cette dernière semaine ? (Brookfield, 1995, p. 115)

3. L’expérience de collègues
----------------------------

Cette partie est plus précisément centrée sur les réflexions à mener en groupes d’enseignants. Certains questionnaires précédents (dont le questionnaire d’incident critique) peut aussi servir de support à des discussions en groupe lors de formations d'enseignants.

Inventaire réflexif 
```````````````````

Cet inventaire peut servir comme questionnaire de présentation à des sessions de formation d'enseignants.


* De quoi suis-je le plus fier dans mon travail d’enseignant ?
* Que voudrais-je que mes élèves disent de moi hors de ma présence ?
* Qu’est-ce que j’ai le plus besoin d’apprendre pour enseigner ?
* Qu’est-ce qui m’inquiète le plus dans mon travail d’enseignant ?
* Quand est-ce que je sais que j’ai fait du bon travail ?
* Quelle erreur ai-je faite qui m’a appris le plus de choses ? (Brookfield, 1995, p. 146)

Mettre la chair sur les os
``````````````````````````

*Pensez à votre propre conception d’un bon enseignant. Vous pouvez vous remémorer des bons enseignants, certaines de leurs actions, ainsi que des bonnes classes dans lesquelles vous avez été. Répondez aux questions suivantes, en vous centrant sur les actions d’un bon enseignant, plutôt que sur des idées générales à propos de ce dernier.*

* Si une personne autodidacte décidait de devenir un enseignant et venait vous voir pour savoir ce que c’est que bien enseigner, que lui diriez-vous d’observer quand il visitera une classe ?
* Si vous étiez juré au comité de “l’enseignant de l’année”, à quel genre d’actions d’enseignant décerneriez-vous un prix ?
* Pensez à la dernière fois que vous êtes dit, en observant un enseignant “c’est une idée géniale”. Quelle est cette idée ? (Brookfield, 1995, p. 147)

La réponse circulaire
`````````````````````

Choisir collectivement un thème de discussion. 

* Le groupe se met en cercle et un volontaire démarre la discussion. 
* Cette personne parle pendant 2 min. à propos du thème. 
* Ensuite, elle stoppe et son voisin de gauche prend la parole pendant 2 min. Le second discutant doit commencer par résumer les commentaires du premier discutant, et montrer dans sa contribution comment elle se sert, interprète, etc. (de) ses commentaires. 
* Ensuite, le second discutant s’arrête de parler et son voisin de gauche, etc. 

Les règles de base sont les suivantes. Personne ne peut être interrompu pendant son intervention, personne ne parle en dehors de sa propre intervention, chaque intervention dure 2 min., chaque personne doit commencer en résumant les commentaires de l’intervenant précédent et chacun doit montrer comment son intervention se nourrit de l’intervention précédente. Lorsque chaque intervenant a parlé, débute un débat général (réactions, etc.), et les règles précédentes n’y sont plus valables. (Brookfield, 1995, p. 150). 

Les rôles conversationnels
`````````````````````````` 

Chacun des rôles conversationnels ci-dessous peuvent être tenus par le formateur, ou bien des participants à un groupe de discussion.

* Formulateur de problème, de dilemme, de thème*. Cette personne a la responsabilité de cadrer le sujet de la conversation et d’utiliser sa propre expérience pour aider les participants dans leur discussion du thème.

* *Analyste réflexif*. Cette personne a la responsabilité de garder trace du développement de la discussion. Toutes les 20 min. environ, elle en donne un résumé en se focalisant sur les préoccupations communes et les thèmes de discussion qui en émergent.

* *Parasite*. Cette personne recueille les ressources, suggestions, et astuces que les participants mentionnent comme les ayant aidé dans leur pratique d’enseignant. Elle en dresse une liste qui est lue à tous avant que la session soit terminée.

* *Avocat du diable*. Cette personne, chaque fois qu’elle a l’impression qu’un concensus émerge, exprime une vue contradictoire avec ce dernier, afin d’éviter des “pensées consensuelles” et que les participants puissent explorer d’éventuelles alternatives.

* *Détective*. Cette personne guête les idées biaisées, non perçues, non vérifiées ni corrigées qui semblent émerger d’une conversation (*i.e.*, que le cours magistral tue l’apprentissage, que les petits groupes permettent toujous un meilleur apprentisasge, etc.). Elle a la tâche particulière de relever, dans les discussions, ce qui concerne les préjugés raciaux, sexistes, ou sociaux.

* *Traqueur de thème*. Cette personne identifie les thème qui apparaissent au détour d’une discussion, mais qui sont restés inexplorés et qui pouraient donc être l’objet d’une discussion ultérieure.

* *Arbitre*. Cette personne guête les éventuels commentaires jugeants qui transgressent les règles de base de communication en groupe. (Brookfield, 1995, p. 153)

L’audit des pratiques efficaces
```````````````````````````````

L’audit des pratiques efficaces est un processus en trois phases dans lequel les enseignants recherchent des solutions appropriées aux problèmes qu’ils rencontrent. Il comprend à la fois des phases de réflexion individuelle et collaborative. Les trois phases sont les suivantes.

1. *Formulation du problème*. Les enseignants identifient individuellement le problème le plus important qu’ils ont dans leur travail. Ensuite, le groupe détermine ensemble le problème (ou deux problème) le(s) plus crucial(ux) à résoudre, qui a(ont) un niveau de généralité qui permet à tous d’en tirer profit (i.e., motiver les non-motivables, etc.). 

2. *Analyse individuelle et collective de l’expérience.* En utilisant le Tableau I suivant, les membres du groupe résolvent le problème. Ils utilisent leur expérience personnelle en tant qu’élèves ou enseignants (*voir tableau*), en commençant par une analyse individuelle. 
  
Le Tableau I ci-dessous permet de voir le problème au travers de six “lentilles” différentes. Il est préférable de commencer par réfléchir aux pires expériences. Puis ils se réunissent en petits groupes afin d’étudier ce que chacun a écrit dans les différentes catégories du tableau. Les thèmes communs sont regroupés.

**Tableau I - Les meilleures et pires expériences en rapport avec le problème.**

+--------------------------------------+--------------------------------------+
| Pire expérience                      | Meilleure expérience                 |
+======================================+======================================+
| Comme élève                          | Comme élève                          |
+--------------------------------------+--------------------------------------+
| De collègues                         | De collègues                         |
+--------------------------------------+--------------------------------------+
| Comme enseignant                     | Comme enseignant                     |
+--------------------------------------+--------------------------------------+

3. *Compilation des suggestions pour l’action*. Les participants résument les idées, trucs, réponses et techniques qui ont été proposées dans la phase précédente. Éventuellement, faire des photocopies de chaque tableau. Il est préférable, dans cette phase, de commencer par examiner les expériences en tant qu’élèves, puis celles de collègues, pour finir par les leurs. Il est également important de noter systématiquement les points communs entre les expériences des participants. Enfin, la tâche finale du groupe est de prescrire des conseils *pour l’action*, et non des généralités. (Brookfield, 1995, pp. 161-172).

Quizz
=====

.. eqt:: Ensreflexif-1

	**Question 1. Selon Brookfield (1995), quels sont les trois barrières à la culture de la réflexion ?**

	A) :eqt:`I` `Le silence, le temps et la singularité des classes`
	B) :eqt:`I` `L'individualisme, la singularité des classes et le secret`
	C) :eqt:`C` `Le silence, l'individualisme et le secret`
	D) :eqt:`I` `La particularité des élèves, le secret et le temps`

.. eqt:: Ensreflexif-2

	**Question 2. Quelle est l'une des pratiques, proposée par Brookfield (1995), visant à se mettre dans la peau d'un enseignant ?**

	A) :eqt:`C` `L'audit d'apprentissage`
	B) :eqt:`I` `Le questionnaire d'incident critique`
	C) :eqt:`I` `L'inventaire réflexif`
	D) :eqt:`I` `La réponse circulaire`

.. eqt:: Ensreflexif-3

	**Question 3. Quel est le but de l'audit d'apprentissage ?**

	A) :eqt:`I` `De consigner ses réactions et ses interprétations à propos d’événements qui nous paraissent importants dans notre vie d’enseignant`
	B) :eqt:`C` `De faire le point sur ce que l’on a appris de sa propre expérience d’enseignement`
	C) :eqt:`I` `D'analyser de quelle manière on parle des collègues que l’on admire, et pourquoi on les admire`
	D) :eqt:`I` `De servir de moyen de régulation du groupe de la classe tout au long de l’année`



Références
==========

Richert, A. E. (1992). Voice and power in teaching and learning to teach. In L. Valli (Ed.), *Reflective Teacher Education: Cases and Critique*. Albany: State University of New York Press.
