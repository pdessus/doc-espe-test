:orphan:

.. _theories_apprentissage:

*****************************************
Une revue des théories de l'apprentissage
*****************************************

.. TODO : Reynolds, R. E., Sinatra, G. M., & Jetton, T. L. (1996). Views of knowledge acquisition and representation: A continuum from experience centered and mind centered. Educational Psychologist, 31(2), 93–104.

.. index::
    single: auteurs; Dessus, Philippe

.. admonition:: Informations

	* **Auteur** : `Philippe Dessus <http://pdessus.fr/>`_, LaRAC & Inspé, Univ. Grenoble Alpes.

	* **Date de création** : Janvier 2016.

	* **Date de publication** : |today|.

	* **Statut du document** : En travaux.

	* **Résumé** : Nous listons ici les principales théories de l'apprentissage.

	* **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.

	* **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_

Introduction
============

Ce document décrit quelques conceptions à propos de l'apprentissage, dans ses aspects individuels. Le lecteur pourra poursuivre sa lecture avec le document :doc:`constrconn.html`.



Le béhaviorisme
===============

Principes
---------

Cette conception (nous citons ici :cite:`vanmerrien14,spector12`) se centre sur l'étude des comportements observables (à l'exclusion de tout autre). Il part du principe que le comportement humain (et animal au sens large) est malléable et qu'un ensemble de lois basiques peut le guider. Deux de ces lois sont les suivantes (les exemples sont tirés de Pavlov, qui a découvert le phénomène) :

* *le conditionnement classique* : un stimulus neutre, lorsqu'il est associé un assez grand nombre de fois à un stimulus qui déclenche une réponse automatique (salivation en présence de nourriture). Dans ce cas, le comportement est réalisé de manière automatique ;

* *le conditionnement opérant* : survient lorsqu'un comportement donné est renforcé (suite à un événement positif, une récompense) ou au contraire puni (suite à un événement négatif). Dans ce cas, le comportement est volontairement exécuté.

L'enseignant peut avoir le rôle de modifier le stimulus ou les conditions dans lesquelles il est présenté ; il peut également renforcer les comportements positifs.

Application des conceptions béhavioristes
-----------------------------------------

Des principes de conception de l'enseignement/apprentissage ont été conçus à partir de cette école de pensée par des chercheurs comme Skinner, Thorndike, ou Watson. Bruillard :cite:`bruillard97`, p. 37 les liste ainsi :

* *Principe de structuration de la matière à enseigner* : le contenu à enseigner est présenté en unités élémentaires ;

* *Principe d'adaptation* : la progression dans le contenu à enseigner s'effectue par étapes, au rythme de l'élève ;

* *Principe de stimulation* : la participation active de l'élève est requise, il est important qu'il produise un comportement qui puisse ainsi être renforcé s'il est positif ;

* *Principe de contrôle et connaissance immédiate de la réponse* : le comportement à apprendre s'acquiert d'autant plus rapidement qu'il est renforcé (par des messages positifs, d'encouragement, etc.)


La conception cognitiviste du traitement de l'information
=========================================================

Le cadre précédent ne pouvant pas expliquer certains comportements humains (comme les comportements langagiers), les chercheurs se sont tournés, dans le cours des années 1960, vers des conceptions cognitivistes, c'est-à-dire tentant d'expliquer XX

Le cadre de conception et d’analyse *Ten steps to complex learning* (10 étapes jusqu’à l’apprentissage complexe) de van Merriënboer et Kirschner (2007) développe un modèle de conception et de présentation des informations liées à l’apprentissage qui pourra être repris dans le projet. La formulation des quatre ensembles d’informations ci-dessous permettra une conception de cours adaptées aux différents contextes universitaires prévus. Il est bien entendu possible de les spécifier, soit dans l'environnement, soit en dehors, en tant que moyens de travailler un ensemble de connaissances ou compétences donné.
1.	Le premier niveau de description s’intéresse à la conception de tâches d’apprentissage (symbolisées par les grands cercles), présentées du simple au complexe, intégrant des savoirs, savoir-faire, & savoir-être, elles sont autant que possible authentiques, et le guidage de l’enseignant (symbolisé par les niveaux dans les cercles) diminue au fur et à mesure de leur présentation dans la séance.
2.	Le deuxième niveau décrit les informations procédurales nécessaires à l’exécution de chaque tâche (symbolisées par les flèches ascendantes au niveau de chaque tâche). Elles concernent les pré-requis pour l’apprentissage et la mise en œuvre d’éléments récurrents des tâches (pratique). Elles précisent comment réaliser les aspects routiniers de la tâche. Cette information est présentée pendant le travail en tâches, au moment adéquat, et s’estompe quand les élèves acquièrent plus d’expertise.
3.	Le troisième niveau concerne les informations de guidage (représentées par les formes blanches en L), qui guident l’apprentissage et les aspects non routiniers de la performance. Elles permettent d’expliquer comment aborder le contenu et comment il est organisé, elles sont spécifiées par type de tâche et toujours disponibles pour l’élève (e.g., explications de l’enseignant, manuels, encyclopédie).
4.	La dernière étape correspond aux entraînements pratiques (représentés par les ensembles de petits cercles dans les rectangles), qui procurent de nombreux exercices pratiques supplémentaires pour atteindre un bon niveau d’automaticité dans les aspects routiniers de la tâche. Ils peuvent présenter des situations différentes et ne commencent que quand ces aspects ont été présentés au sein de la tâche (e.g., exercices d’entraînement, simulation, résolution de problèmes réels).



. image:: /images/10steps.jpg
	:scale: 80 %
	:align: center

 Figure 1 - Visualisation des différentes étapes d'une vision cognitiviste de l'apprentissage.


La conception cognitiviste
--------------------------

La conception connectiviste
{Dron, 2018 #21575}

Among the theories’ and models’ shared foundations are:
* that the problems to solve are not, as in mediaeval times, a paucity of resources but a surplus;

*􏰀 that learning (and, by extension, teaching) resides not just in individual people but in non-human entities, from simple web pages, to AI entities, to emergent behav- iours of networked systems;
􏰀* that learning in this context is primarily a process of connection more than construction or absorption, of knowing where to find knowledge and skills;
􏰀* that learning is a process of creation, best enabled when creations are shared;
􏰀* that learning should be both innately personal (controlled by the learner) and innately social (performed with, for and through others);
􏰀* that a sufficiently large network of learners can, in combination, be better, more personalized, and more flexible teachers than individual teachers themselves (recursively, of course, such networks typically include those who identify as teachers).

Références
==========

* Milwood, R. (2013). `Learning Theory <http://blog.richardmillwood.net/wp-content/uploads/2013/11/Learning-Theory.pdf>`_. Poster.

.. rubric:: Références

.. bibliography::
    :cited:
    :style: apa
    :filter: docname in docnames