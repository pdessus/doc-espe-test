
.. _socialisation_pairs:

====================================
Socialisation par le groupe de pairs
====================================

.. index::
    single: auteurs; Dessus, Philippe
    single: auteurs; Besse, Émilie

.. admonition:: Information

 * **Auteurs** : `Philippe Dessus <http://pdessus.fr/>`_, Inspé & LaRAC, Univ. Grenoble Alpes. Le quizz a été réalisé par Émilie Besse, projet `ReFlexPro <http://www.idefi-reflexpro.fr>`_.

 * **Date de création** : Octobre 2000, mis à jour en Octobre 2001.

 * **Date de publication** : |today|.

 * **Statut du document** : Terminé.

 * **Résumé** : Ce document détaille la théorie de Harris, théorie de la socialisation de l'élève par le groupe de pairs. L'enfant (l'élève) construit sa personnalité par l'éducation qu'il reçoit de ses parents, mais également et surtout par l'interaction avec le ou les groupe(s) de pairs. Cette théorie permet de comprendre et d'anticiper quelques phénomènes de groupe survenant dans les classes.

 * **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.

 * **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.


Introduction
============

Ce document reprend quelques éléments de la théorie de Harris :cite:`harris99` qui montre comment les enfants (mais aussi les élèves) se socialisent *via* leurs pairs. Nous ne reprenons pas ici la partie la plus polémique de la théorie, qui démontre le faible rôle que jouent les parents dans l'éducation de leurs enfants - hors de propos dans l'enseignement.

Ce que l'on sait : L'importance du groupe de pairs
==================================================

Faire des sous-groupes : nous et eux
------------------------------------

Le premier élément important de cette théorie est de signaler que les enfants d'une communauté ont un besoin de se distinguer, de se confronter à d'autres enfants d'une communauté différente. Le groupe qu'ils forment se distinguera des autres selon les paramètres les plus visibles (sexe, vêtements, origine sociale ou géographique, etc.) et les membres du groupe feront tout leur possible pour justifier une supériorité par rapport à l'autre (les autres) groupe(s) : "nous et eux". Soulignons que ce besoin de classer, catégoriser est un besoin constant de l'esprit humain, et pas seulement des enfants. On retrouve donc ce phénomène, à des degrés divers, dans les partis politiques, les groupes de supporters, les sectes, etc..

Les élèves feront également tout leur possible pour défendre, suporter les membres de leur groupe face aux concurrents. Les enseignants se trouvent confrontés à ce phénomène, et estiment qu'il ne peut survenir que lorsque le groupe se sera forgé une identité, un "esprit". Harris montre que cette identité peut se construire quelques minutes après la formation d'un groupe, même si les membres de ce groupe ne se connaissent pas. Toutefois, il est rassurant de voir que ces catégories peuvent évoluer dans le temps ; la composition ou les types d'association changer selon les événements.

Le conformisme de groupe
------------------------

Une fois le groupe constitué, ses membres vont tendre à se conformer à ce qui se dit ou fait dans le groupe. Ce conformisme n'est toutefois systématique, mais surtout très fort en cas d'agression de l'extérieur du groupe. En d'autres termes, chaque membre d'un groupe peut adopter un comportement, des attitudes sensiblement différentes des autres membres du groupe, du moins tant qu'il n'y a pas de menaces extérieures. Dans ce cas, les membres vont se "serrer les coudes " et adopter une ligne de conduite très unie, généralement calquée sur celle du meneur - " Ils sont comme moi, et je suis comme eux ". Ce comportement est souvent observé par les enseignants, qui sont souvent surpris de l'unité d'une classe lorsque survient une menace extérieure (c'est-à-dire, d'eux-mêmes). Les élèves, par exemple, pourront faire bloc pour éviter qu'un des leurs soit puni.

L'autocatégorisation
--------------------

Mais chaque enfant (élève) appartient en fait à de nombreux groupes différents, et il fait état de ses différentes appartenances selon les besoins. Ainsi, les enfants de deux ans ont déjà conscience d'être un garçon ou une fille, mais également d'être un enfant. Ils se détermineront, dans un groupe, en fonction de la catégorie la plus saillante du moment. Une catégorie étant saillante si, au même moment, une autre catégorie comparable et contrastée est présente. Les garçons d'un groupe se démarqueront en tant que tels seulement si un nombre de filles conséquent est présent. Dans le cas contraire, ils pourront s'autocatégoriser en tant qu'enfants, si des adultes sont présents. L'autocatégorisation pourra s'accompagner, en cas d'incident, d'une défense des membres du groupe ainsi défini. Des psychologues ont appelé ce groupe le " groupe de référence ". En voici une définition (Harris :cite:`harris99`, p. 185, citant Turner *et al.* :cite:`turner87` :

 "Un groupe psychologique se définit comme une catégorie psychologiquement signifiante pour ses membres, dont ils se réclament subjectivement lorsqu'ils se livrent à des comparaisons sociales et à l'acquisition de normes et de valeurs [...] dont ils adoptent les lois, les références et les croyances concernant les conduites à tenir [...] et qui influence leurs attitudes et leur comportement."

Qu'on soit enfant ou adulte, on peut donc se rattacher à de nombreuses catégories selon les circonstances, c'est-à-dire selon les catégories saillantes en présence : on est homme/ femme, citadin/ campagnard, supporter de Marseille/P.S.G., professeur-stagiaire/ formateur, professeur/élève, agrégé/certifié, jeune/âgé, célibataire/marié, avec/sans enfants, etc.

À l'école comme en dehors, la première tâche de l'enfant (élève) est de déterminer à quelles catégories il appartient. De ces catégories découleront son comportement, qui calquera sur les autres membres de la catégorie. Cette autocatégorisation a un effet puissant dans les classes : l'enseignant est le seul adulte, et il n'a pas toujours intérêt à rendre cette catégorie trop saillante, sous peine de voir se liguer contre elle les enfants ou adolescents de la classe. Quand l'adulte se montre un peu trop sévère, les élèves peuvent être plus agités, afin de manifester leur appartenance au groupe " enfants " (voir ci-dessus la section *Le conformisme de groupe*). Harris rappelle à ce sujet le comportement classique de l'adolescent, qui se place souvent à bonne distance de ses parents lors de promenades avec eux. S'ils croisent un camarade de leur classe, ils veulent que la situation soit claire : " Je ne suis pas avec ces gens-là, je ne suis pas comme eux. "

La première autocatégorisation : l'opposition filles/garçons
------------------------------------------------------------

Pour mieux comprendre ce processus d'autocatégorisation, il est utile de décrire la première catégorie dans laquelle s'inscrit le jeune enfant est celle du sexe.

 " Dès trois ans, on s'identifie comme fille ou garçon et on préfère jouer "entre filles" ou "entre garçons". À cinq ans, les enfants jouent en petits groupes où règne une ségrégation sexuelle presque totale. Cette division est possible parce que nos sociétés urbanisées offrent aux enfants de nombreux camarades du même âge, ce qui leur permet de faire les difficiles. À la maison ou dans le quartier, où il y a moins d'enfants, ils sont prêts à jouer avec n'importe qui. " (Harris, 1999, p. 219)

En fait, un garçon pourra jouer avec sa voisine s'il n'a personne d'autre avec qui jouer. Dans la réalité, comme le souligne Harris, la plupart des garçons ne détestent pas toutes les filles, et vice versa. Mais, à l'école, que ce soit en classe ou en récréation, la catégorie la plus saillante est "garçon/fille". Cette catégorisation sexuelle perdure en grandissant : les pré-adolescents ont des avis très rigoureux en ce qui concerne l'attitude à avoir vis-à-vis de l'autre sexe. Si, au sein d'une classe, un membre viole le tabou de son groupe (par exemple, jouer avec ou se placer à côté d'un membre du sexe opposé), il en est exclu. L'anecdote suivante décrit bien ce phénomène, où s'asseoir à côté d'une fille est, pour le garçon, aussi mal vu que faire pipi dans sa culotte :

 "En cours de sciences, M. Little demande aux élèves de constituer des groupes de trois pour faire une expérience. Aucun des groupes qui se forment n'est mixte. M. Little constate la présence d'un groupe de quatre garçons et dit à l'un d'eux, Juan (qui est noir) : "Mets-toi dans le groupe de Diane" (qui comporte deux filles noires). Secouant la tête, Juan dit "Non, je n'irai pas !" Calmement, mais d'un ton sans réplique, M. Little reprend : "Alors, retire ta blouse et quitte le labo. Retourne dans ta classe." Juan ne fait pas mine de bouger. Il ne dit rien. Après plusieurs minutes d'un lourd silence, M. Little dit : "Très bien, je vais le faire à ta place" Il enlève à Juan sa blouse et met le garçon à la porte. " (Harris, 1999, p. 220-221)

L'exemple suivant, vécu dans une école maternelle italienne, montre que cette catégorisation commence tôt...

 "La résistance des enfants aux règles imposées par les adultes peut être considérée comme habituelle, car elle se manifeste quotidiennement à la maternelle et sous des formes aisément reconnaissables pour leurs pairs. Ce sont des actes souvent très exagérés (faire des grimaces derrière le dos de la maîtresse ou courir dans la classe) ou précédés "d'appels à l'attention" des autres enfants ("regarde ce que j'ai" avant de montrer un objet interdit ou "regarde ce que je fais" pour attirer l'attention sur une activité défendue.") (Harris, 1999, p. 223)

Rien n'est donc plus amusant que de montrer à ses camarades que l'on n'est pas à la botte de l'enseignant. Et le jeu de nombreux élèves est de défier ce dernier sans aller jusqu'à la désobéissance.

"Pour tout élève, les personnes les plus importantes de la classe sont les autres élèves. Et ce qui compte le plus - ce qui rend la journée d'école supportable ou la transforme en un véritable enfer - est le statut dont jouit l'enfant. Le pouvoir de [l'enseignant] réside pour une bonne part dans sa capacité de braquer un projecteur sur tel ou tel enfant, de le désigner à l'attention de ses pairs. [Il] peut, à son gré, faire de lui un objet de raillerie ou d'envie pour les autres." (Harris, 1999, p. 301)

L'esprit de groupe en classe
----------------------------

Les élèves ont tendance à accentuer des différences rendues saillantes. Ce phénomène rend dangereux les groupes de niveaux que les enseignants créent parfois. Car les élèves de chaque groupe vont tout faire pour accentuer les différences, et tenter d'être fiers d'appartenir à leur groupe. Par exemple, tout en reconnaissant être des lecteurs moyens, les élèves d'un tel groupe vont parvenir à se considérer - au choix - comme plus gentils, forts. Ils pourront aussi développer des attitudes négatives par rapport à l'école : les bons lecteurs sont tous des lèche-bottes, des prétentieux, etc. Et cette attitude pourra avoir des effets cumulatifs au cours des ans, puisque, outre la différence garçon/fille, l'autre catégorie saillante est la réussite scolaire. Cette autocatégorisation est tellement forte que des études ont montré que si l'on rappelle à une jeune femme forte en maths qu'elle est du sexe féminin, elle réussira moins bien ses tests d'aptitude mathématique (Harris, 1999, p. 313).

Ce que l'on peut faire
======================

L'enseignant doit être vigilant envers ces phénomènes de catégorisation. Harris signale qu'il peut influencer le groupe d'élèves de trois manières :

* influer sur les normes du groupe. Il n'est pas nécessaire, pour cela, de faire adopter une comportement à tous les élèves de la classe, mais essayer d'influencer positivement les élèves qui focalisent l'attention des autres ;

* définir les limites du groupe : jusqu'où va le *nous* et où commence le *eux*. L'important étant, comme expliqué ci-dessus, que l'enseignant fasse partie du *nous*.

* définir l'image que le groupe se fait de lui-même.

Quizz
=====

.. eqt:: Socialisation-1

	**Question 1. Quelle est la première auto-catégorisation qui intervient chez les enfants ?**

	A) :eqt:`I` `La différence grand/petit`
	B) :eqt:`I` `La différence de classe sociale`
	C) :eqt:`C` `L'opposition fille/garçon`
	D) :eqt:`I` `La différence de culture`

.. eqt:: Socialisation-2

	**Question 2. Selon Harris (1999), quelles sont les trois manières dont l'enseignant peut influencer le groupe d'élèves ?**

	A) :eqt:`C` `Influer sur les normes du groupe ; Définir les limites du groupe ; Définir l'image que le groupe se fait de lui-même`
	B) :eqt:`I` `Influer sur les rôles de chacun ; Définir les places de chacun ; Définir les limites du groupe`
	C) :eqt:`I` `Définir l'image que le groupe se fait de lui-même ; Influer sur les rôles de chacun ; Définir les consignes`
	D) :eqt:`I` `Influer sur les normes du groupe ; Définir les consignes ; Définir les places de chacun`


Références
==========

.. bibliography::
 :cited:
 :style: apa
