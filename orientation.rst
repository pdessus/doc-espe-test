.. _orientation:

========================
L'orientation des élèves
========================

.. index::
    single: auteurs; Dessus, Philippe
    single: auteurs; Besse, Émilie

.. admonition:: Informations

  * **Auteurs** : `Philippe Dessus <http://pdessus.fr/>`_, Inspé & LaRAC, Univ. Grenoble Alpes. Le quizz a été réalisé par Emilie Besse, projet `ReflexPro <http://www.idefi-reflexpro.fr>`_.
  
  * **Date de création** : Février 2002, mis à jour en décembre 2005.
  
  * **Date de publication** : |today|.
  
  * **Statut du document** : Terminé
  
  * **Résumé** :  Le processus de l'orientation des élèves dans le secondaire est à la fois contraint par l'offre de places par filières, le prestige de certaines filières et professions et enfin le projet individuel de l'élève. Et ce dernier, malgré les différentes directives et les intentions, n'est pas toujours pris en compte. Ce  document donne des pistes pour mieux faire formuler le projet d'orientation des élèves, sans transformer l'enseignant en conseiller d'orientation.

  * **Voir aussi** : Le Document :ref:`tache` à propos du portfolio comme instrument d'évaluation.
  
  * **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.
  
  * **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.


Ce que l'on sait
================

Les contradictions de l'orientation
-----------------------------------

Comme le signale Agulhon (1998), de nombreux critères contraignent l'orientation des élèves, et leur projet personnel n'est pas prioritairement pris en compte. La hiérarchisation des filières, mais aussi l'âge, les résultats scolaires, l'origine sociale et le sexe des élèves définissent en grande partie leur orientation. Cela semble paradoxal, surtout depuis que la `Loi d'orientation du 10 juillet 1989 <http://www.education.gouv.fr/cid101274/loi-d-orientation-sur-l-education-n-89-486-du-10-juillet-1989.html>`_ (et surtout son `rapport annexé <http://www.lille.snes.edu/spip.php?article290>`_) est censée mettre l'élève au centre du dispositif éducatif, et donc d'orientation. En voici les grande lignes concernant l'orientation des élèves :

  "Le jeune construit son orientation au lieu de la subir. Nul ne peut, en effet, décider à sa place. Pour effectuer son choix, il reçoit information, aide et conseil. Sa famille et l'école (enseignants, chef d'établissement, personnels d'éducation et d'orientation) y participent. Cependant, la mise en pratique du principe fondamental de la maîtrise de son orientation par le jeune peut rencontrer deux limites. Il s'agit tout d'abord de la nécessité d'avoir acquis certaines connaissances et certaines aptitudes pour tirer profit d'un enseignement ultérieur. Il s'agit ensuite des limites de l'offre de formation, en particulier dans le cas des formations professionnelles dont le développement est en partie lié à l'importance des débouchés.

  Les conflits qui peuvent surgir sont traités par des efforts d'information et de dialogue, notamment dans le cadre du contrat de formation. La diminution des cas de désaccord est un objectif à réaliser à tous les niveaux d'enseignement et dans le projet d'établissement. Aucune décision de refus du *projet de l'élève* ne peut être prise sans être explicitement motivée.

  L'évaluation des connaissances et des compétences de l'élève est nécessaire pour qu'il construise son orientation ; elle fait partie de la formation. Cette évaluation doit être aussi continue que possible. Les modalités d'attribution des diplômes combinent l'évaluation en cours de formation et des examens terminaux" [C'est nous qui soulignons]

Le processus d'orientation des élèves est peu transparent, car segmenté et dépendant de personnes et de textes très différents. Une des raisons de cette opacité, comme le reconnaît Agulhon (*op. cit.*) est l'existence d'un double discours : un discours sur la sélection des élèves en fonction de leur niveau et des offres d'emploi, qui hiérarchise nécessairement les filières ; un autre discours sur le projet professionnel de l'élève, qui au contraire les met au même niveau.

Agulhon (1998) rappelle aussi que, localement, l'offre de formation (nombre de places d'élèves par filière) influence fortement la demande (des élèves). Il y a par exemple une très forte corrélation entre le nombre de places offertes affichées et le nombre d'élèves entrant dans cette section. Cette offre de formation (mais aussi, par exemple, les taux de passage en seconde) est également très variable selon les districts.

La demande des élèves : leur projet personnel
---------------------------------------------

Si l'on se penche maintenant sur la demande des élèves, on s'aperçoit qu'elle n'est pas rationnelle, souvent faute d'information. Les filières connues sont donc choisies en priorité (cinq à six demandes par place offerte dans les professions de la santé pour les filles, emplois du tertiaire pour les deux sexes), au détriment de formations du bâtiment ou de l'industrie (moins d'une demande par place). Nous l'avons vu, la Loi d'orientation insiste sur le projet de l'élève, or, d'une part, le projet d'orientation d'un élève mûrit beaucoup plus lentement que ce que voudrait l'institution ; d'autre part, ceux qui doivent s'orienter le plus tôt sont souvent des élèves en échec scolaire, et leur "projet" est dans ce cas quasiment imposé (Agulhon, 1998 ; Dumora, 1998). En d'autres termes, les élèves de collège découvrent, plus ou moins rapidement, les implications sociales futures de leurs performances scolaires (alors que ces mêmes performances sont plus immédiates et familiales en primaire).

Ce projet est aussi façonné par la perception qu'a l'élève des différents groupes d'appartenance (groupes de pairs, voir Document :ref:`socialisation_pairs`). L'élève est intégré à une communauté de pairs qui a ses propres valeurs et fonctionnements, qui peuvent différer des valeurs sociales ou scolaires. La construction personnelle des élèves, au sein de ces groupes, joue donc un rôle important, et les projets d'orientation peuvent, de ce fait, être très décontextualisés (acteur de cinéma, pilote, astronaute, etc.).

Ensuite, ce projet personnel est déterminé par les valeurs diffusées dans l'école. Les personnels de l'éducation, qui sont eux-mêmes passés par les filières les plus prestigieuses, ne connaissent pas, ni ne valorisent les filières professionnelles. Les élèves intègrent donc tôt le leitmotiv "Si tu ne réussis pas, tu iras dans le technique". | L'écart entre leur projet professionnel et la réalité de leurs performances scolaires et/ou les analyses des professionnels de l'éducation amènent souvent les élèves à rationnaliser (se résigner à propos de) leur devenir. Dumora (1998) montre par exemple que, si les lycéens professionnels s'évaluent comme moins motivés et moins performants que les lycéens de filière générale, ils pensent que leur avenir professionnel est plus prometteur que celui de ces derniers (meilleur salaire et possibilités d'emploi). 

Ce que l'on peut faire
======================

Comme on l'a vu précédemment, le processus d'orientation, contrairement à ce qui peut être préconisé dans les textes, ne prend pas suffisamment en compte le projet de l'élève et/ou de sa famille. Dumora (2001) signale à ce propos que, lors de la réunion parent/conseiller d'orientation en fin de 3\ :sup:`e`, 20 % des désaccords se transforment en accords par modification des voeux de l'élève. En revanche, seulement 3 % des propositions du conseil de classe sont modifiées.

Il ne s'agit pas, bien sûr, que l'enseignant se substitue au conseiller d'orientation ; mais l'on peut décrire la manière dont se passe de tels entretiens, ce qui peut permettre à l'enseignant de les faire préparer à ses élèves. Par exemple, Zarka (2000) montre que l'on peut distinguer les éléments suivants dans la demande des élèves pendant un conseil en orientation :

#. *le motif* : qui sont les raisons  pour lesquelles on consulte le conseiller. Ces raisons peuvent être explicites, mais aussi plus ou    moins cachées.
#. *la demande* : concerne les modalités de la démarche, la manière dont l'élève formule sa ou ses questions en fonction du motif. Elle    concerne l'implication, la participation du sujet.
#. *l'appel* : est l'expression implicite, non verbalisée de l'élève.

De manière plus précise, de nombreux documents permettent aux élèves de faire le point sur leurs compétences, et donc de mieux définir leur projet. Parmi eux, le site canadien "`La boussole <http://www.cforp.on.ca/boussole/>`_" est particulièrement complet. Voici le plan de ce site à propos de la connaissance de soi : 

*  1\ :sup:`e` étape - Quelles choses sont les plus importantes pour vous ? Vos valeurs.
*  2\ :sup:`e` étape - Aptitudes - Quels sont vos points forts ?
*  3\ :sup:`e` étape - Quelles sont vos formes d'intelligence ? Information & test.
*  4\ :sup:`e` étape - Quels secteurs professionnels vous intéressent le plus ?
*  Votre profil général - Décrire votre profil en vos termes !

Un autre moyen d'élaborer son projet professionnel est de se constituer un :index:`portfolio`, qui est une manière de se présenter de plus en plus utilisée dans le monde du travail. Il peut être réalisé via un projet impliquant le conseiller d'orientation de l'établissement, noté par l'enseignant, et transmis aux parents, faire l'objet d'une exposition dans la classe ou l'école. Voici brièvement ce qu'est un portfolio et comment mettre en oeuvre sa conception avec des élèves. Un portfolio est :

  "une collection significative et intégrée des travaux de l'élève   illustrant ses efforts, ses progrès et ses réalisations dans un ou   plusieurs domaines. Cette collection repose sur des normes de   performance et témoigne de la réflexion de l'élève et de sa   participation dans la mise au point de celle-ci, le choix des   contenus et les jugements portés. Le portfolio indique ce qui est   appris et en quoi c'est important." (Goupil & Lusignan, 1993, p. 304, citant Paulson & Paulson, 1990).

Quatre types de données peuvent être consignées dans un portfolio (Goupil & Lusignan, 1993, p. 306 *et sq.*) :

* les remarques de l'enseignant sur le processus d'apprentissage (pourquoi ai-je choisi ce travail ? qu'ai-je appris ? quels sont mes prochains objectifs ?)
* les remarques de l'élève sur ses réalisations,
* les informations recueillies pendant les activités d'évaluation menées par l'enseignant,
* les informations recueillies pendant les activités d'évaluation menées à l'extérieur (examens, etc.).

Quizz
=====

.. eqt:: Orientation-1

  **Question 1.   Quelle principale limite que peut rencontrer un élèves dans la maîtrise de son orientation ?**

  A) :eqt:`I` `Un désaccord avec l'équipe pédagogique`
  B) :eqt:`I` `Un désaccord avec ses parents`
  C) :eqt:`C` `L'acquisition d'un certain nombre de connaissances`
  D) :eqt:`I` `Le manque d'information`

.. eqt:: Orientation-2

  **Question 2.   D'après Dumora (1998) :**

  A) :eqt:`I` `Les lycéens professionnels s'évaluent comme moins bons que ceux des filières générales`
  B) :eqt:`I` `Les lycéens de filières générales s'évaluent comme moins motivés que ceux de filières professionnelles`
  C) :eqt:`I` `Les lycéens de filières générales pensent que leur avenir professionnel est plus prometteur`
  D) :eqt:`C` `Les lycéens professionnels pensent que leur avenir professionnel est plus prometteur`

.. eqt:: Orientation-3

  **Question 3.   Quel est l'outil de plus en plus utilisé pour se présenter dans le monde du travail ?**

  A) :eqt:`I` `Le CV`
  B) :eqt:`C` `Le portfolio`
  C) :eqt:`I` `La lettre de motivation`
  D) :eqt:`I` `La lettre de recommandation`



Analyse de pratiques
====================

#. Menez la réalisation d'un portfolio dans votre classe.

Sites internet
==============

* `La Boussole <http://www.cforp.on.ca/boussole/>`_. *Site d'information sur l'orientation*.

Références
==========

* Agulhon, C. (1998). L'orientation scolaire, prescription normative et   processus paradoxal. *L'Orientation Scolaire et Professionnelle*, *27*\ (3), 353-371.
* Dumora, B. (1998). Expérience scolaire et orientation. *L'Orientation Scolaire et Professionnelle*, *27*\ (2), 211-234.
* Dumora, B. (2001). Les intentions d'orientation et leur argumentation : aspects développementaux et psycho-sociaux. *L'Orientation Scolaire   et Professionnelle*, *30*, 148-165.
* Goupil, G., & Lusignan, G. (1993). *Apprentissage et enseignement en   milieu scolaire*. Montréal: Gaëtan Morin.
* Guichard, J., & Huteau, M. (2001). *Psychologie de l'orientation*. Paris : Dunod.
* Zarka, B. (2000). Conseils et limites. *L'Orientation Scolaire et Professionnelle*, 29, 141-169.