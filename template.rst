.. La ligne qui suit est une "ancre", placée toujours avant un titre, utilisable si on veut créer un lien dans un autre document menant à celui-ci par la directive :ref:

.. _fake_doc:

.. Attention, le nombre d'étoiles ou de traits doit être strictement égal au nombre de caractères. Sinon Sphinx indique une erreur.

*************************
Titre de niveau principal
*************************

**Auteur** : `Albert Bébert <http://webcom.upmf-grenoble.fr/>`_ (admirez le lien hypertexte !), LSE & Espé, Univ. Grenoble Alpes.

**Date de création** : Avril 2015.

**Date de modification** : |today|.

**Statut du document** : En travaux.

**Résumé** : Ce document décrit XX

**Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_

Introduction
============

Taper le texte.
Utiliser *ces étoiles* pour mettre des mots en italiques, ou bien ces **doubles étoiles** pour les mettre en gras. Voici un :index:`mot` à indexer.
Ceci est une liste d'items :

* Premier item
* Deuxième item
* Troisième item

Voici une liste numérotée :

#. Premier
#. Deuxième
#. Troisième

Insérer une image se fait ainsi (attention aux tabulations et aux lignes avant/après) :

.. image:: /images/item-distract.jpg
	:scale: 80 %
	:align: center


**Tableau 1** — Organisation des données pour le calcul de l’indice *B* de Brennan (:cite:`laveault02`, p. 222).

 +------------------+------------------+---------------------+----------------------+
 |                  |                  | Non-maîtrise a + c  | Maîtrise b + d       |
 +==================+==================+=====================+======================+
 |                  | **Réussi a + b** | a = 5               | b = 16               |
 +------------------+------------------+---------------------+----------------------+
 | Item             | **Echoué c + d** | c = 9               | d = 4                |
 +------------------+------------------+---------------------+----------------------+

Ensuite, il est aisé de calculer les indices de difficulté de l’item pour ces deux groupes, resp. *Pm* et *Pnm*.

.. math::

	Pm = b/(b+d) = 0,8

	Pnm = a/(a+c) = 0,36

	B = Pm – Pnm = 0,8 – 0,36 = 0,44

Cela signifie que si on compare la réussite des élèves du groupe « maîtrise » à celle du groupe « non-maîtrise », il y en a 44 % de plus qui réussissent l’item dans le premier groupe. On peut considérer que cet item est à conserver. Un onglet du tableur *Excel* dont l’URL figure dans la Section :ref:`calcul_indices` permet de calculer *B*.

Le but d’un questionnaire peut être de mesurer une compétence/connaissance donnée, et donc de pouvoir discriminer les élèves en fonction de cette dernière. Bien évidemment, il est possible de considérer exactement le contraire : que le questionnaire n’est pas fait pour discriminer ou sélectionner, mais pour simplement certifier des compétences (test critérié). Dans ce dernier cas, calculer un tel indice de discrimination peut tout de même être utile pour détecter les items mal formulés ou ayant trait à une dimension de connaissance/compétence différente de celle mesurée par le test.

Le rôle des réponses distractrices
==================================

La troisième et dernière procédure va s’intéresser, item par item, aux :index:`réponses distractrices`. Tout item de QCM contient une bonne réponse et des réponses distractrices. Il convient aussi de vérifier si les différents distracteurs ont bien joué leur rôle (pour un distracteur donné, si personne ne l’a sélectionné il ne l’est pas, si tout le monde l’a sélectionné, il l’est sans doute trop et les étudiants ont été massivement induits en erreur). Johnstone :cite:`johnstone03` considère qu’un distracteur remplit son rôle si environ 5 % au moins des personnes le choisissent. La valeur de choix maximale dépend bien sûr du nombre de réponses, mais il est possible, en affichant une représentation graphique de tous les choix question par question, de déterminer les réponses distractrices qui ont été massivement évitées : il faudra sans doute les reformuler.

Le moyen de déterminer si les réponses distractrices remplissent bien leur rôle est simple : il suffit de représenter graphiquement une distribution par item et par réponse, et de vérifier visuellement la distribution des choix. La Figure 2 ci-dessous en donne un exemple.



**Figure 2** – Pourcentage des réponses des élèves par item (1–6) et par réponse (*a–c*). Les flèches indiquent les bonnes réponses.

Une analyse de ce type de graphique peut nous permettre de tirer les conclusions suivantes. Tout d’abord, il est possible de déterminer si la place de la bonne réponse se distribue uniformément entre les différentes réponses (ici il y a uniformité). Ensuite, il est possible de détecter les distracteurs jamais choisis (*e.g.*, *c* pour l’item 2) et de le reformuler de manière à le rendre plus proche du bon résultat, tout en étant faux. Enfin, comme le suggèrent :cite:`crisp07`, lorsque les items sont de difficulté croissante, il est ainsi possible de vérifier qu’au début, les élèves choisissent plus aisément la bonne réponse qu’à la fin du questionnaire (ici, ce sont les questions 4 et 5 qui ont les distracteurs les plus attractifs).

Une stratégie d’analyse des items
=================================

L’examen attentif et parallèle des deux derniers indices (difficulté et discrimination) permet d’analyser finement la qualité des items. Il est facile de régler les cas où les indices *P* et *ID* d’un item sont conjointement bas (item très difficile et peu discriminant, donc à rejeter) ou conjointement élevés (item très facile et très discriminant, donc à conserver, mais en gardant également des items plus difficiles). Evoquons les autres cas.

Lorsqu’un item a un *ID* élevé et un *P* bas, cela indique que seuls les meilleurs élèves ont réussi l’item. Cet item très sélectif pourra être conservé, là aussi en le mixant avec des items plus faciles. Lorsqu’un item a un *ID* faible et un *P* élevé, il faut en analyser la raison élève par élève : il est très probable qu’un ou plusieurs bons élèves (*i.e.*, ayant bien répondu à des items plus difficiles que celui-ci) n’aient pas compris la consigne de l’item. Il est possible que cet item doive être reformulé ; mais aussi qu’il mesure des connaissances ou compétences non reliées aux autres.

.. _calcul_indices:

Calculer les différents indices
===============================




Références
==========

.. bibliography:: refs_qcm.bib
..	:cited:
..	:filter: docname in docnames
..	:style: alpha

