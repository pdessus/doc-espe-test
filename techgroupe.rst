.. _org_travail_groupe:

==============================
Organiser le travail en groupe
==============================

.. index::
    single: auteurs; Dessus, Philippe
    single: auteurs; Besse, Émilie

.. admonition:: Information

  * **Auteurs** : `Philippe Dessus <http://pdessus.fr/>`_, Inspé & LaRAC, Univ. Grenoble Alpes. Le quizz a été réalisé par Emilie Besse, projet `ReflexPro <http://www.idefi-reflexpro.fr>`_.

  * **Date de création** : Octobre 2000, révision en décembre 2007.

  * **Date de publication** : |today|.

  * **Statut du document** : Terminé.

  * **Résumé** : Ce document permet de connaître les principales raisons de l'intérêt de l'apprentissage coopératif à la fois dans le cadre de débats ou de travail en groupes. Il est également fait référence à une trentaine de fiches présentant des techniques de travail en groupe issues de la formation des adultes. Cela permettra à l'enseignant de proposer des situations de travail variées et motivantes. Télécharger les :download:`fiches </images/techgr.pdf>`.

  * **Voir aussi** : Document :ref:`constr_conn`.

  * **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.

  * **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.



Ce que l'on sait
================

Ce document liste quelques informations à propos de l'intérêt d'organiser le travail des élèves en groupes, soit pour qu'ils débattent, soit pour qu'ils produisent un document. Envisageons ces deux activités.

Le débat
--------

La grande majorité des connaissances et compétences du socle commun met en œuvre la notion de débat (maîtrise de la langue, culture sceintifique et technologique, citoyenneté, autonomie/initiative). Pour autant, il est erroné de penser qu'il suffise de mettre les élèves en petits groupes pour qu'un débat ait lieu. En effet, on a montré qu'il n'était guère possible d'envisager que des élèves débattent, en échangeant des arguments, avant 8-9 ans.

Le rôle du débat dans la construction de connaissances  a souvent été montré. On en trouvera un détail dans le Document :ref:`constr_conn`.

Des recherches montrent des dérives dans la mise en application des débats réglés. Si les objectifs du débat réglé sont donc multiples, ils ne sont pas nécessairement mis en oeuvre pour la même raison selon les types d'école. Par exemple, Fontani :cite:`fontani07` montre que, dans les ZEP, le débat réglé est mis en place pour développer les interactions langagières et la civilité : "le débat permet de parler pour ne pas frapper" (p. 6). Hors ZEP, en revanche, il a plutôt pour but, selon les enseignants, d'initier l'élève à la vie démocratique dans le fonctionnement de l'école", ou de "réfléchir par soi-même" (p. 6). Dans les ZEP, seraient donc mis en avant une citoyenneté libérale et de conformité à la civilité, hors d'elles une citoyenneté républicaine et de concertation (p. 7).

Le travail coopératif
---------------------

Il n'est pas question ici de résumer l'ensemble des travaux faits à propos de la gestion des groupes d'élèves au sein d'une classe, mais de livrer quelques données et pistes de lecture pour travailler au mieux avec une classe. Nous nous intéressons ici au travail coopératif mis en oeuvre dans des groupes d'élèves, c'est-à-dire à un type de travail où les élèves sont complémentaires les uns des autres, et travaillent dans des petits groupes (env. 4-5 élèves).  Il ne suffit donc pas de regrouper les élèves en petit nombre pour que l'apprentissage soit de fait facilité. Pour Goupil et Lusignan (:cite:`goupil93`, voir aussi :cite:`buchs04`), il y a apprentissage coopératif dans un groupe si :

* *Il y a interdépendance positive des membres du groupe*, les élèves ont  conscience des multiples interdépendances les uns envers les autres  qui se tissent dans le groupe. Il existe ainsi une interdépendance (il  importe bien de souligner que la distribution des rôles de chacun  doit se faire au sein du groupe lui-même et ne doit pas être imposée  par l'enseignant) :

  * envers les buts : tous les élèves doivent atteindre les mêmes buts ;
  * envers la tâche : elle peut être subdivisée ;
  * envers les ressources : les élèves partagent le même matériel, les mêmes informations ;
  * envers les récompenses : elles sont données à l'ensemble du groupe ;
  * envers les rôles, chaque élève jouant un rôle complémentaire.

* *Il y a interaction verbale entre les élèves*, il est important que les  informations, les idées, hypothèses, solutions, puissent aisément  circuler dans le groupe. L'enseignant doit donc proposer un contexte  propice à ces échanges (groupes suffisamment éloignés les uns des  autres, techniques de communication, etc.).

* *La responsabilité individuelle existe*, découlant du premier point. Il  importe que chaque élève puisse remplir la tâche qui lui échoit et  cette responsabilité augmentera en fonction du sentiment de chacun  d'appartenir au groupe, en fonction de la motivation par rapport à la  tâche.

Goupil et Lusignan :cite:`goupil93` proposent à cet usage quelques règles de fonctionnement d'un groupe d'apprentissage qu'il peut être utile de faire adopter en classe :

* Tous doivent s'entendre sur la définition de la tâche à accomplir  pour que le but à atteindre à chaque séance soit clair.
* Tous doivent participer ; une absence de participation (silence)  prolongée de la part d'un membre est inacceptable.
* Chacun doit se centrer sur la tâche à accomplir ; tous les  commentaires doivent avoir rapport au sujet.
* Chacun doit contribuer de façon valable ; tous les membres doivent  écouter attentivement les autres et tenter de se comprendre  mutuellement.
* Chacun doit répondre à un commentaire fait par un autre membre de  l'équipe.

Le rôle de l'activité dans le groupe
------------------------------------

Comme nous l'avons signalé ci-dessus, l'activité est organisatrice dans le groupe. Si cette dernière est bien spécifiée, alors les membres du groupe seront essentiellement préoccupés par la tâche définie, et les difficultés seront mineures. Des études ont montré que (:cite:`vayer87`, p. 51-52) :

* "les individus tendent à abdiquer leur jugement et à dépendre des  autres quand ils se trouvent dans des situations ambiguës ou confuses  ;
* le consensus, ou conscience chez les membres d'un groupe de  sentiments partagés, joue un rôle déterminant et puissant dans la  dynamique de tous les groupes ;
* le conflit comme l'agressivité sont inévitables et doivent être  considérés comme des phénomènes dynamiques du fait de la  réorganisation des informations qu'ils impliques de la part des  protagonistes pour que le groupe conserve sa cohérence.
* s'il y a des objectifs communs, il y a plus de chances pour que la  collaboration puisse s'établir au sein du groupe ;
* si les buts ont été fixés par le groupe, les membres du groupe se  sentent plus impliqués que s'ils ont été définis de l'extérieur ;
* une fois la décision arrêtée d'être et de faire ensemble, plus les  dimensions du groupe sont restreintes mieux il fonctionne."

Pourquoi est-ce que cela marche ?
---------------------------------

Buchs *et al*. (:cite:`buchs04` pp. 177-179) signalent qu'il y a plusieurs raisons pour lesquelles les interactions entre élèves favoriseraient l'apprentissage de ces derniers :

* *Mise en place d'un climat positif d'entraide*. Le fait que les élèves  sont interdépendants les incite à s'entraider et à s'encourager.
* *Echanges et discussion*. Les élèves s'échangeant verbalement des  informations ont à les résumer, à faire l'effort d'être  compréhensible, mais aussi à construire ensemble un point de vue  commun. Ces trois caractéristiques étant favorables à  l'apprentissage.
* *Confrontations et déséquilibres*. Les confrontations de points de vue  entraînent des déséquilibres cognitifs propices à des réorganisations  autorisant la formulation de points de vue plus généraux, complexes,  et résultat d'une meilleure compréhension du contenu.
* *Influence mutuelle entre pairs*. Le travail coopératif peut permettre  le développement de conflits sociocognitifs, propices à  l'apprentissage.

Mais il y a des interactions ou des conflits qui ne favorisent pas l'apprentissage :cite:`buchs06` : c'est le cas lorsque le but des protagonistes n'est pas de résoudre la tâche ou de comprendre le point de vue de l'autre, mais plutôt de comparer leurs compétences. Cette comparaison amène des stratégies de protection de sa propre compétence, et de dénigrement de celle de l'autre, qui sont incompatibles avec l'avancement du débat ou du travail.

Ce que l'on peut faire
======================

Voici, tout d'abord, quelques recommandations générales pour favoriser le travail en groupe (issues de Buchs *et al*. (:cite:`buchs04`, p. 180).

* "Proposer une tâche commune réalisable en groupe. Si la tâche peut  être réalisée individuellement, il n’y a pas de raison pour que les  élèves interagissent.
* Faire travailler un nombre restreint d’élèves ensemble (entre 2 et 5). Plus le nombre est important, plus les habiletés sociales  requises sont importantes.
* Proposer des activités qui permettent de développer un esprit  d’équipe et la confiance. Il est nécessaire de créer un climat  positif avant de commencer le travail proprement dit grâce à des  activités favorisant la connaissance des autres, la communication, le  respect et le soutien entre les participants.
* Créer une interdépendance positive entre les élèves. Le but commun  doit être structuré de manière à ce que chaque membre du groupe  perçoive qu’il ne peut atteindre son but que si tous les membres de  son groupe atteignent également le leur.
* Maintenir la responsabilité personnelle de chaque membre du groupe.  Il s’agit d’assurer que chaque participant fasse de son mieux pour  faire sa part du travail. Enseigner les habiletés sociales et  interpersonnelles nécessaires pour le travail en groupe. Il ne suffit  pas de vouloir coopérer pour savoir le faire.
* Faire réfléchir les élèves sur leurs habiletés scolaires et sociales.  Cette réflexion porte sur ce qui a été utile pour faire avancer le  groupe et ce qu’il faut améliorer.
* Renforcer les comportements coopératifs et les interactions  constructives. L’enseignant observe et donne des retours positifs sur  les comportements valorisés.Quels types d’interactions renforcer ?  Les activités d’enseignement aux autres. L’échange d’informations  comprenant l’apport d’explications et des activités de  questionnement. Les confrontations de points de vue dans un contexte  coopératif en faisant porter les critiques sur les idées et non les  personnes."

Voici pour finir un tableau récapitulant, par but, les différentes techniques de travail en groupe que l'on trouvera ci-après, par ordre alphabétique de nom de technique. Issues pour la plupart des techniques d'animation de groupes d'adultes, ces techniques nous paraissent utilisables sans trop d'adaptations à des participants plus jeunes. Elles sont classées dans le tableau suivant par buts : analyser (un problème, un événement, une situation) ; apprendre (former à une habileté) ; convaincre (débattre et argumenter à propos d'un sujet de fond) ; décider (face à un problème ayant un grand nombre d'alternatives) ; échanger (communiquer) ; informer ; produire.

**Tableau I** -- Les buts, et caractéristiques des techniques de travail en groupe présentées en :download:`fiches <downloads/techgr.pdf>`. Note : Les références des ouvrages utilisés pour collationner les techniques de travail en groupe se trouvent sur chaque fiche.

+---------------+------------------------------+---------------+--------------------+
| But           | Technique                    | Durée         | Nb de participants |
+===============+==============================+===============+====================+
| Analyser      | Expression graphique         | 30 min        | < 30               |
+---------------+------------------------------+---------------+--------------------+
|               | La parole est d'or           | 1 h           | < 20               |
+---------------+------------------------------+---------------+--------------------+
|               | Petite annonce               | 1 h           | < 30               |
+---------------+------------------------------+---------------+--------------------+
|               | Technique du risque          | > 1 h         | Indiff.            |
+---------------+------------------------------+---------------+--------------------+
| Apprendre     | Etude de cas                 | 1 h           | Indiff.            |
+---------------+------------------------------+---------------+--------------------+
| Convaincre    | Argumentation                | 30 min        | Indiff.            |
+---------------+------------------------------+---------------+--------------------+
|               | Exposé                       | < 1h          | Indiff.            |
+---------------+------------------------------+---------------+--------------------+
|               | Panel                        | > 1 h         | Indiff.            |
+---------------+------------------------------+---------------+--------------------+
|               | Pour et contre               | 1 h           | > 15               |
+---------------+------------------------------+---------------+--------------------+
|               | Symposium                    | >= 1 h        | Indiff.            |
+---------------+------------------------------+---------------+--------------------+
| Décider       | Aquarium                     | 1 h           | Indiff.            |
+---------------+------------------------------+---------------+--------------------+
|               | Brainstorming                | >= 1 h        | > 6                |
+---------------+------------------------------+---------------+--------------------+
|               | Composition de menu          | 1 h           | < 15               |
+---------------+------------------------------+---------------+--------------------+
|               | Délégués                     | 1 h           | > 15               |
+---------------+------------------------------+---------------+--------------------+
|               | Etude de cas                 | 1 h           | Indiff.            |
+---------------+------------------------------+---------------+--------------------+
|               | Méthode des cartes           | 30 min        | < 30               |
+---------------+------------------------------+---------------+--------------------+
|               | Phillips 6.6                 | 1 h           | Indiff.            |
+---------------+------------------------------+---------------+--------------------+
|               | Scintillement                | > 30 min      | > 15               |
+---------------+------------------------------+---------------+--------------------+
|               | Technique du risque          | > 1 h         | Indiff.            |
+---------------+------------------------------+---------------+--------------------+
| Echanger      | Aquarium                     | 1 h           | Indiff.            |
+---------------+------------------------------+---------------+--------------------+
|               | Argumentation                | 30 min        | Indiff.            |
+---------------+------------------------------+---------------+--------------------+
|               | BaFa BaFa                    | 1 h           | < 30               |
+---------------+------------------------------+---------------+--------------------+
|               | Composition de menu          | 1 h           | < 15               |
+---------------+------------------------------+---------------+--------------------+
|               | Délégués                     | 1 h           | > 15               |
+---------------+------------------------------+---------------+--------------------+
|               | Expression graphique         | 30 min        | < 30               |
+---------------+------------------------------+---------------+--------------------+
|               | Jeu de rôle                  | 1 h           | Indiff.            |
+---------------+------------------------------+---------------+--------------------+
|               | La discussion interrompue    | 1 h           | < 10               |
+---------------+------------------------------+---------------+--------------------+
|               | La parole est d'or           | 1 h           | < 20               |
+---------------+------------------------------+---------------+--------------------+
|               | Panneaux d'exposition        | 1 h           | < 30               |
+---------------+------------------------------+---------------+--------------------+
|               | Petite annonce               | 1 h           | < 30               |
+---------------+------------------------------+---------------+--------------------+
|               | Phillips 6.6                 | 1 h           | Indiff.            |
+---------------+------------------------------+---------------+--------------------+
|               | Photo-langage                | 1 h           | < 15               |
+---------------+------------------------------+---------------+--------------------+
|               | Pour et contre               | 1 h           | > 15               |
+---------------+------------------------------+---------------+--------------------+
|               | Questionnaire-opinions       | 2 h           | > 20               |
+---------------+------------------------------+---------------+--------------------+
|               | Réacteurs subjectifs         | 1 h           | Indiff.            |
+---------------+------------------------------+---------------+--------------------+
|               | Réunion discussion           | 30 min        | < 15               |
+---------------+------------------------------+---------------+--------------------+
|               | Scintillement                | > 30 min      | > 15               |
+---------------+------------------------------+---------------+--------------------+
|               | Technique du voisinage       | 1 h           | < 30               |
+---------------+------------------------------+---------------+--------------------+
|               | Tournante                    | > 1 h         | > 15               |
+---------------+------------------------------+---------------+--------------------+
|               | Triade                       | > 2 h         | Indiff.            |
+---------------+------------------------------+---------------+--------------------+
| Informer      | Ecoute d'émission            | 4 h           | > 12               |
+---------------+------------------------------+---------------+--------------------+
|               | Exposé                       | < 1h          | Indiff.            |
+---------------+------------------------------+---------------+--------------------+
|               | Jeu de rôle                  | 1 h           | Indiff.            |
+---------------+------------------------------+---------------+--------------------+
|               | Mixage                       | > 2 h         | < 40               |
+---------------+------------------------------+---------------+--------------------+
|               | Panel                        | > 1 h         | Indiff.            |
+---------------+------------------------------+---------------+--------------------+
|               | Panneaux d'exposition        | 1 h           | < 30               |
+---------------+------------------------------+---------------+--------------------+
|               | Réacteurs subjectifs         | 1 h           | Indiff.            |
+---------------+------------------------------+---------------+--------------------+
|               | Symposium                    | >= 1 h        | Indiff.            |
+---------------+------------------------------+---------------+--------------------+
| Produire      | Brainstorming                | >= 1 h        | > 6                |
+---------------+------------------------------+---------------+--------------------+
|               | Ecoute d'émission            | 4 h           | > 12               |
+---------------+------------------------------+---------------+--------------------+
|               | Méthode des cartes           | 30 min        | < 30               |
+---------------+------------------------------+---------------+--------------------+
|               | Mixage                       | > 2 h         | < 40               |
+---------------+------------------------------+---------------+--------------------+
|               | Photo-langage                | 1 h           | < 15               |
+---------------+------------------------------+---------------+--------------------+
|               | Tournante                    | > 1 h         | > 15               |
+---------------+------------------------------+---------------+--------------------+

Analyse des pratiques
=====================

* À quels types de conditions pensez-vous qu'il peut y avoir apprentissage dans un groupe ? Listez-les et réfléchissez à la manière de les favoriser en classe.

* Relire les différentes compétences et connaissances du socle commun, et notez pour chacune la place du débat.

* Décrire une modalité de travail en groupes mise en oeuvre en classe, selon les points suivants (trame d'après Françoise Campanale, IUFM Grenoble) :

  * Dans quelle classe, de combien d’élèves ? 
  * Quel est l’objectif de la séquence d’apprentissage dans laquelle  prend place ce travail en groupes ? Le travail pouvait-il se réaliser  individuellement ?
  * Combien d’élèves par groupe ? Comment les groupes ont-ils été  constitués ?
  * Quelle tâche avait à réaliser le groupe ? Quelles ont été les  consignes ? Comment ont-elles été données (écrit/oral) ?
  * Quelles contraintes éventuelles avez-vous ajouté à la tâche ?
  * Travaillaient-ils dans une situation de simulation (reproduisant une  expérience de la vie courante) ou abstraite ?
  * Les élèves avaient-ils des rôles différents dans le groupe ? Pourquoi  ?
  * Combien de temps a duré le travail en groupe ? De quoi a-t-il été  suivi ?
  * Le travail du groupe a-t-il été évalué ? Si oui, comment ?
  * Quelle appréciation portez-vous sur ce travail en groupes ?  Pensez-vous que les élèves ont pu travailler dans une interdépendance  positive ?
  * Qu’envisagez-vous de modifier et pourquoi ?

Quizz
=====

.. eqt:: Techgroupe-1

  **Question 1. Selon Goupil et Lusignan (1993), il y a apprentissage coopératif si**

  A) :eqt:`I` `Il y a au moins trois personnes au sein du groupe`
  B) :eqt:`I` `Les membres du groupe sont du même niveau scolaire`
  C) :eqt:`I` `L'enseignant est présent pour cadrer le travail en groupe`
  D) :eqt:`C` `La responsabilité individuelle existe`

.. eqt:: Techgroupe-2

  **Question 2.   Selon Bush et al. (2004) quel est l'une des raisons pour lesquelles les  interactions entre élèves favorisent l'apprentissage ?**

  A) :eqt:`I` `L'intervention régulière de l'enseignant`
  B) :eqt:`I` `Le cadre bien explicite du travail à réaliser`
  C) :eqt:`C` `L'influence mutuelle entre les pairs`
  D) :eqt:`I` `La mise en valeur des "leaders" de la classe`

.. eqt:: Techgroupe-3

  **Question 3. Quelle technique de travail en groupe va-t-on utiliser si notre but est de convaincre ?**

  A) :eqt:`I` `Le brainstorming`
  B) :eqt:`I` `Le jeu de rôle`
  C) :eqt:`I` `L'exposé`
  D) :eqt:`C` `L'argumentation `


Références
==========

.. bibliography::
  :cited:
  :style: apa
