(ateliers-profs-chercheurs)=

# Présentation des Ateliers profs-chercheurs

```{index} single; auteurs: Lejeune, Pleen     single; auteurs: Atal, Ignacio single; auteurs: Dessus, Philippe
```

:::{admonition} Information
- **Auteur** : Pleen Lejeune, CRI, Univ. de Paris, [Ignacio Atal](https://www.cri-paris.org/en/profile?id=c7946a22-d065-4663-a7ed-71b5b5ef53f2), CRI, Univ. de Paris, & [Philippe Dessus](http://pdessus.fr/), LaRAC & Inspé, Univ. Grenoble Alpes.
- **Date de création** : Janvier 2021.
- **Date de publication** : {sub-ref}`today`.
- **Statut** : En cours.
- **Résumé** : Ce Document présente la démarche des [Ateliers profs-chercheurs](https://profschercheurs.cri-paris.org/en), une initiative de développement professionnel collaboratif mise en place au [Centre de recherches interdisciplinaires](https://cri-paris.org/en) de l'Université de Paris.
- *Citation\** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.
- **Licence** : Document placé sous licence *Creative Commons* : [BY-NC-SA](http://creativecommons.org/licenses/by-nc-sa/3.0/fr/).
:::

## Introduction

Les acteurs de l’éducation font face à un grand nombre de défis pour assurer une éducation de qualité pour tous. Le collectif Profs-Chercheurs propose une démarche  de recherche collaborative pour faire face à ces défis grâce à l'identification :

- des **défis** auxquels nous faisons face \[[Vidéo de présentation](https://www.youtube.com/watch?v=9VZgpCEOjqA&feature=youtu.be)\] ;
- des **actions** que nous mettons en place pour répondre à ces défis, en situation scolaire \[[Vidéo de présentation](https://www.youtube.com/watch?v=9UabXJjvusM&feature=youtu.be)\] ;
- des **retours d’expérience** sur la capacité de chaque action à relever chaque défi, faisant une présentation du résultat des actions \[[Vidéo de présentation](https://www.youtube.com/watch?v=O0BYBWrbTZg&feature=youtu.be)\] ;
- des **synthèses** d'un ensemble de retours d'expérience provenant de contextes différents \[[Vidéo de présentation](https://www.youtube.com/watch?v=0emNrbzpKsY&feature=youtu.be)\].

## Vidéos de présentation plus générale

- [Pourquoi participer aux ateliers ?](https://www.youtube.com/watch?v=lYItWnRKQNQ&feature=youtu.be)
- La [démarche des ateliers](https://www.youtube.com/watch?v=cDrddmj670Y&feature=youtu.be)
- Les [ateliers](https://www.youtube.com/watch?v=RgLPDK6yZDQ)
- Le [collectif profs-chercheurs](https://www.youtube.com/watch?v=qPXFfepgxr8)
