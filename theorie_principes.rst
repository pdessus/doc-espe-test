.. _principes:

**********************************
Théories et principes en éducation
**********************************

.. index::
    single: auteurs; Dessus, Philippe

.. admonition:: Informations

	* **Auteur** : `Philippe Dessus <http://pdessus.fr>`_, LaRAC & Inspé, Univ. Grenoble Alpes

	* **Date de création** : Janvier 2015.

	* **Date de publication** : |today|.
	
	* **Statut du document** : En travaux.

	* **Résumé** : Cette section répertorie différents types de données visant à synthétiser ce qu'on connaît à propos de l'apprentissage et de l'enseignements XXX

	* **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.

	* **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/2.0/fr/>`_



Utilisation des principes en recherche
======================================

Les sous-sections ci-dessous présentent de nombreux principes issus de la recherche en éducation. Il est possible de générer des sujets de recherche à partir d'eux en sélectionnant un principe et (par ordre croissant de difficulté) :

* essayer de le valider dans une matière et un contexte scolaire donnés ;

* essayer de trouver des moyens d'assister ce principe par l'utilisation de matériel innovant (informatique, etc.) ;

* essayer de trouver une manière d'expliquer le principe.


Principes sur l'apprentissage (Graesser)
========================================

Le bref document de Graesser :cite:`graesser09` est une excellente synthèse de ce que l'on sait à propos d'apprentissage. Voici ses "25 principes sur l'apprentissage" (*id*, p. 260) :

.. topic:: **Les 25 principes sur l'apprentissage de Graesser.**

	#. *Effets de contiguïté*. Les idées qui nécessitent d'être associées entre elles doivent être présentées de manière contiguë dans l'espace et le temps.

	#. *Fondation perceptivo-motrices*. Les concepts bénéficient d'être fondés dans des expériences perceptivo-motrices, particulièrement dans les premières étapes.

	#. *Double codage et effets multimédia*. Les matériels présentés dans des formes verbale, visuelle et multimédia forment des représentations plus riches que lorsqu'ils sont présentés dans des média uniques.

	#. *Effet du test*. Le fait de tester les apprenants augmente l'apprentissage, particulièrement lorsque les tests sont à propos d'un contenu important. 

	#. Effet de l'espacement*. La répartition du travail et des tests dans le temps entraîne une meilleure mémorisation qu'une unique session de travail ou de test.
	 
	#. *Attentes liées à l'examen*. Les apprenants bénéficient plus de tests répétés quand ils sont dans l'attente d'un examen final.

	#. *Effet de production*. L'apprentissage est amélioré lorsque les apprenants formulent des réponses (production) plutôt qu'ils les sélectionnent dans une liste (reconnaissance).      

	#. *Effet d'organisation*. Transformer l'information en plans, l'intégrer à d'autres informations ou la synthétiser génère un meilleur apprentissage que relire l'information ou employer des stratégies plus passives.

	#. *Effet de cohérence*.  Des matériels présentés sous la forme de multimédia devrait explicitement relier les idées présentées et minimiser les informations distractrices et non pertinentes.

	#. *Histoires et cas exemplaires*. Des histoires ou cas exemplaires sont mieux rappelés que des faits décontextualisés et des principes abstraits. 

	#. *Exemples multiples*. La compréhension d'un concept abstrait est améliorée par la présentation d'exemples multiples et variés. 

	#. *Effet des rétroactions (feedback)*. Les rétroactions dans une tâche d'apprentissage améliorent ce dernier, mais le délai optimal des rétroactions dépend de la nature de la tâche.

	#. *Effet des suggestions négatives*. Lorsque la rétroaction est immédiate, on a moins tendance à apprendre des informations erronées.
	
	#. *Difficultés attractives*. Les défis rendent l'apprentissage et le rappel plus ardus, ce qui améliore la mémorisation à long terme.
	
	#. *Prise en compte de la charge cognitive*. L'information présentée à l'apprenant ne devrait pas surcharger sa mémoire de travail.
	
	#. *Le principe de segmentation*. Une leçon complexe devrait être divisée en sous-parties plus accessibles. 
	
	#. *Effet de l'explication*. Il est plus bénéfique d'apprendre en construisant des explications cohérentes et profondes du matériel plutôt que de mémoriser des faits isolés et de surface.
	
	#. *Questions profondes*. Il est plus bénéfique de formuler et de répondre à des questions profondes qui avancent des explications (p. ex., pourquoi, pourquoi pas, si-alors) que des questions de surface (p. ex., qui, quoi, quand, où).
	
	#. *Déséquilibre cognitif*. Les raisonnements et apprentissages profonds sont stimulés par des problèmes qui créent un déséquilibre cognitif, comme des obstacles à des buts, des contradictions, des conflits, des anomalies.
	
	#. *Flexibilité cognitive*. Lorsqu'on est confronté à de multiples points de vue qui relient faits, habiletés, procédures, et concepts profonds, notre flexibilité cognitive est entraînée et s'améliore.
	
	#. *Le principe de Boucle d'Or*. Les tâches ne devraient pas être trop faciles ou trop difficiles, mais à un niveau de difficulté adapté au niveau de connaissances et compétences de l'apprenant.  
	
	#. *La métacognition imparfaite*. Les apprenants ont rarement une connaissance exacte de leur propre cognition. Donc on ne peut avoir une confiance totale en leur capacité à gérer leur compréhension, apprentissage, et mémoire.
	
	#. *L'apprentissage par la découverte*. La plupart des apprenants ont des difficultés à découvrir d'eux-mêmes des principes importants, sans une aide attentive, un étayage, ou des matériels avec des instructions bien pensées.
	
	#. *L'apprentissage auto-régulé*. La plupart des apprenants ont besoin qu'on les aide à entraîner leur manière d'auto-réguler leur apprentissage et leurs autres processus cognitifs.
	
	#. *L'apprentissage ancré*. L'apprentissage est plus profond et les apprenants plus motivés lorsque les matériels et les compétences sont ancrés dans des problèmes du monde réel qui concernent l'apprenant. 

L'apprentissage visible (Hattie)
================================

Une autre intéressante source d'informations est la compilation sur l' "apprentissage visible" effectuée par Hattie :cite:`hattie09`. C'est une recension de très nombreuses méta-analyses en éducation.

Hattie Ranking: http://visible-learning.org/hattie-ranking-influences-effect-sizes-learning-achievement/

AJOUTER: Dunlosky, J., Rawson, K. A., Marsh, Elizabeth J., Nathan, M. J., & Willingham, D. T. (2013). Improving students' learning with effective learning techniques: Promising directions from cognitive and educational technology. Psychological Science in the Public Interest, 14(1), 4–58. 

Voir aussi Marzano
