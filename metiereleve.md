(metier-eleve)=

# Métier d'élève et travail scolaire

```{index} single: auteurs; Dessus, Philippe single: auteurs; Besse, Émilie
```

:::{admonition} Information
- **Auteurs** : [Philippe Dessus](http://pdessus.fr/), Inspé & LaRAC, Univ. Grenoble Alpes. Le quizz a été réalisé par Émilie Besse, projet [ReflexPro](http://www.idefi-reflexpro.fr).
- **Date de création** : Octobre 2004.
- **Date de publication** : {sub-ref}`today`.
- **Statut du document** : Terminé.
- **Résumé** : Ce document présente quelques réflexions sur les notions de métier et de travail d'élève. Si l'école est censée préparer les citoyens de demain, il est difficile de comprendre comment elle le fait sans essayer de savoir ce que recouvrent ces notions.
- **Voir aussi** : Documents {ref}`culture_familiale`, et {ref}`taches_ens_el`.
- **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.
- **Licence** : Document placé sous licence *Creative Commons* : [BY-NC-SA](http://creativecommons.org/licenses/by-nc-sa/3.0/fr/).
:::

## Introduction

La question rituelle d'un parent : "qu'est-ce que tu as fait aujourd'hui" s'adresse tout aussi bien, à la fin d'une journée de travail, à son conjoint qu'à son enfant élève.  De nombreux ouvrages et articles francophones {cite}`perrenoud94,laborderie91,sirota93` ont essayé de définir le métier des élèves. Si les élèves ont un métier, ils ont aussi un travail, qu'il est souvent difficile de décrire (voir Doc. SAPP {ref}`activite`). Pourtant, être élève, c'est le premier métier du monde (La Borderie), à au moins trois titres : premier contact pour l'élève avec le monde du travail et ses contraintes ; premier pour notre société, qui sera composée de citoyens et de personnes issues de l'école ; premier par les moyens financiers engagés dans la plupart des états modernes. D'autre part, on ne peut prédire un apprentissage satisfaisant seulement à partir des tâches qu'un élève aura remplies. Il est donc utile d'y aller voir de plus près.

## Ce que l'on sait

### Le métier de l'élève : suivre les  curricula ?

Perrenoud {cite}`perrenoud94` a bien mis en valeur les différentes contraintes mises en place dans toute institution scolaire, qui font que son public est assujetti à un métier ayant des règles et des normes très précises : horaire stable, souvent rythmé par une sonnerie, absences et retards relevés et sanctionnés, travail à faire en classe et à la maison,  performances (connaissances aussi bien que comportement) notées individuellement,  et communiquées aux parents, écarts de conduite relevés et sanctionnés,  conseil de classe décidant de l'orientation de l'élève, organisation de classe et d'établissement codifiée par l'enseignant et le règlement.

Pour Sembel {cite}`sembel03`, le travail scolaire ne doit pas seulement recouvrir ce qui permet à l'élève de réussir dans son apprentissage, car est évacué ainsi le travail jugé "inefficace", qui existe pourtant. Les différentes variantes du curriculum (*i.e.*, programme, formel, réel, caché) permettent justement de mieux comprendre l'inscription sociale du métier d'élève.

- le *curriculum formel* est celui des programmes officiels appliqués par les enseignants ; le curriculum réel est le travail réellement réalisé par les élèves, une fois qu'il a été organisé par l'enseignant qui tient compte de diverses contraintes
- le curriculum caché\* étant enfin ce que les élèves doivent faire pour réussir, sans que cela soit explicitement annoncé par l'institution ou les enseignants. Les élèves d'origine populaire essaient justement de décoder, de s'approprier ce curriculum caché qui provient souvent d'une culture autre que la leur (voir plus bas).

Pour analyser le travail de l'élève, on peut comparer deux à deux ces curricula (formel vs. réel, formel *vs*. caché, et réel *vs*. caché) pour mieux comprendre ce que l'élève doit vraiment faire pour être efficace. Comme le soulignent Barrère et Sembel {cite}`barrere98`, le métier de l'élève est nécessairement plus complexe que de seulement suivre les curricula ; cela montre aussi que son activité est beaucoup plus autonome par rapport à l'institution scolaire qu'on le pense.

### L'élève pour les sociologues

Le fait de considérer l'activité de l'élève comme un travail a d'importantes répercussions sur la manière de l'étudier. Plusieurs courants de recherche, très divers, sont partis de ce présupposé pour mener des études à la fois originales et très différentes les unes des autres.

L'expression "métier d'élève", selon Sirota {cite}`sirota93`, est couramment utilisée dans la sociologie de l'éducation française (bien qu'elle soit inexistante dans la sociologie anglo-saxonne).  Sirota (voir aussi Barrère & Sembel {cite}`barrere98`) dégage trois images de l'élève, tel qu'il est vu par les sociologues :

- *l'héritier* : les différences culturelles interélèves ont pour origine leur milieu social, et chaque élève est porteur de compétences socialement acquises plus ou moins proches des compétences requises par l'institution scolaire. Il hérite du capital culturel de ses parents, presque sans travail. La récente massification de l'enseignement secondaire a justement confronté des élèves issus des classes moyennes à une culture scolaire des classes supérieures.

* *le stratège* :  l'élève est ici vu comme un stratège qui, dans un marché de biens intellectuels, choisit la meilleure trajectoire d'orientation possible, pèse les coûts et bénéfices de chaque alternative qui se présente pour choisir la meilleure solution.
* *le consommateur* :  l'élève (et ses parents) peut aussi être vu comme un fin connaisseur de la réputation des établissements scolaires, choisissant celui qui lui convient le mieux en contournant la carte scolaire.

D'autres sociologues, comme Perrenoud {cite}`perrenoud94`, ont montré qu'être élève, ce n'est pas seulement acquérir des connaissances et des savoirs-faire, mais aussi de nombreuses règles du jeu, parfois implicites (montrer de l'intérêt, éviter les punitions, faire le travail demandé, etc.), souvent conformistes, qui lui permettront sans trop de mal de s'intégrer dans son établissement et dans les groupes de ses pairs.

### L'élève pour les ergonomes

Ensuite, le récent courant de recherche de la psychologie ergonomique, en considérant les aspects prescrits et effectifs du travail de l'élève (voir par exemple {cite}`dessus03`, et le Document SAPP {ref}`taches_ens_el`), considère le travail de l'élève de manière moins prescriptive qu'à l'habitude. Pour les tenants de ce courant, il est normal que l'élève reconsidère et adapte les prescriptions de l'enseignant dans son propre travail : la notion de "grève du zèle" n'a-t-elle pas été forgée pour montrer que les travailleurs qui appliquent le plus strictement possible les prescriptions font leur travail inefficacement ? Il peut être donc utile, une fois qu'on a analysé les tâches prescrites et les tâches effectivement mises en œuvre par les élèves, de les comparer afin de mieux comprendre ce qui a amené l'élève à s'écarter de la prescription :  des contraintes ? ses propres compétences ?

### Elève : un vrai métier ?

Mais, pour autant, le travail scolaire n'est pas un travail comme les autres {cite}`perrenoud94`. Tout d'abord, il n'a pas d'utilité immédiate visible.  Ensuite, la plupart des tâches ne sont pas décidées par les élèves, mais imposées par l'enseignant. La réussite à ces tâches ne garantit aucun salaire immédiat, mais l'approbation des adultes. De plus, ces tâches sont souvent réalisées de manière fragmentée et surtout hachée. En bref, toujours selon Perrenoud (*id*., p. 62) : "Faire du bon travail, à l'école, c'est faire un travail non rétribué, largement imposé, fragmenté, répétitif et constamment surveillé." Face à cela, les élèves peuvent, toujours selon Perrenoud, adopter les stratégies suivantes :

- *Boire le calice jusqu'à la lie* : l'élève accepte la logique du système en s'y soumettant, sans poser de questions ni discuter. Il peut ainsi gagner la confiance de l'enseignant, qui peut lui laisser une certaine autonomie.
- *Vite ! vite ! vite !* L'élève réalise le plus rapidement possible les tâches demandées pour faire autre chose, pour avoir un moment de répit avant le prochain travail.
- *Hâte-toi lentement*.  Plutôt que de refuser frontalement le travail, l'élève trouve des moyens de le différer, de l'interrompre. Il prend l'air occupé sans faire d'effort, feint de s'intéresser au travail.
- *Je n'y comprends rien*. L'élève s'avoue incompétent, incapable de comprendre la tâche. Cela lui fait gagner du temps lorsque l'enseignant est occupé avec d'autres élèves, et lui permet de soutirer à ce dernier quelques informations lorsqu'il viendra à sa table.
- *Contestation ouverte*. L'élève nie l'intérêt du travail demandé, refuse explicitement de le faire en invoquant diverses raisons (fatigue, envie, humeur). En raison de son risque, c'est souvent une stratégie occasionnelle. Toutefois, elle peut être le départ de véritables négociations avec l'enseignant, qui est souvent soucieux de ne pas donner du travail que ses élèves trouvent ennuyeux.

## Ce que l'on peut faire

Le métier de l'élève (ce qui suit est fortement inspiré de Barrère et Sembel {cite}`barrere03`, c'est se forger différentes stratégies pour  arriver à mieux comprendre et utiliser à son profit les différents curricula proposés à l'école, et notamment le curriculum caché.  C'est aussi comprendre que tout travail n'est pas également récompensé par de bons résultats et qu'au bout du compte, il est plus important de réussir en travaillant "juste ce qu'il faut".

Cela amène les enseignants à réfléchir au travail réellement mis en oeuvre par l'élève. Reconnaître, par exemple, qu'au secondaire chaque enseignant donne des devoirs sans vraiment considérer la charge totale qu'endure chaque élève, notamment aux environs de la fin d'un trimestre. Reconnaître aussi que la remarque classique "manque de travail" cache souvent  un manque d'analyse des problèmes d'un élève.

La non-adéquation entre travail des élèves et résultats doit ainsi faire à la fois l'objet d'une analyse plus fine et, de plus, il est utile que les enseignants explicitent le plus possible leurs exigences. Cela permet notamment d'éviter que les élèves fassent appel à leur connaissance (plus ou moins bonne et avancée) du curriculum caché. Cela permet d'éviter aussi que des élèves travaillent sans réussir.

On peut aussi questionner plus précisément sa propre activité : par exemple, réaliser qu'on écrit sans doute beaucoup moins que ses élèves (et parfois de manière aussi peu lisible) , qu'on est parfois aussi comédien qu'eux, en invoquant des problèmes familiaux ou sociaux {cite}`barrere03`) ; et aussi qu'on qu'on fait parfois bien plus de bruit qu'eux.

## Quizz

```{eval-rst}
.. eqt:: Metiereleve-1

        **Question 1. Quelles sont les deux variantes du curriculum selon Sembel (2003) ?**

        A) :eqt:`C` `Curriculum formel et Curriculum caché`
        B) :eqt:`I` `Curriculum formel et Curriculum informel`
        C) :eqt:`I` `Curriculum réel et Curriculum irréel`
        D) :eqt:`I` `Curriculum visible et Curriculum caché`
```

```{eval-rst}
.. eqt:: Metiereleve-2

        **Question 2. Quelle est, selon les sociologues, la caractéristique d'un élève "stratège" ?**

        A) :eqt:`I` `Il hérite du capital culturel de ses parents`
        B) :eqt:`C` `Il choisit la meilleure trajectoire d’orientation possible`
        C) :eqt:`I` `Il choisit l'établissement scolaire qui lui convient le mieux`
        D) :eqt:`I` `Il laisse les choses arriver à lui`
```

```{eval-rst}
.. eqt:: Metiereleve-3

        **Question 3. Quelle est, selon les sociologues, la caractéristique d'un élève "consommateur" ?**

        A) :eqt:`I` `Il choisit la meilleure trajectoire d’orientation possible`
        B) :eqt:`I` `Il hérite du capital culturel de ses parents`
        C) :eqt:`I` `Il laisse les choses arriver à lui`
        D) :eqt:`C` `Il choisit l'établissement scolaire qui lui convient le mieux`


```

## Analyse des pratiques

1. Quel est le curriculum caché correspondant à votre propre métier d'étudiant ou de professeur stagiaire à l'Espé ?
2. Même question à propos des élèves de votre établissement.
3. Même question à propos de leurs activités réelles (Barrère, 2003 parle des trois activités d'un élève du secondaire : écouter, participer, prendre des notes )
4. Mettre au jour différentes applications du fameux proverbe : "faite ce que je dis, pas ce que je fais" dans sa propre pratique d'enseignant.

## Références

```{bibliography}
:filter: docname in docnames
```
