.. TODO : voir le Diigo.

.. _res_apprendre:

**********************************************************
Ressources -- Textes sur l'apprentissage et l'enseignement
**********************************************************

.. index::
    single: auteurs; Dessus, Philippe

.. admonition:: Informations

  * **Auteur** : `Philippe Dessus <http://pdessus.fr/>`_, LaRAC & Inspé, Univ. Grenoble Alpes.

  * **Date de création** : Décembre 2017.

  * **Date de publication** : |today|.

  * **Statut du document** : En travaux.

  * **Résumé** : Ce Document liste un choix raisonné de ressources (textes, vidéos, sites sur l'apprentissage et l'enseignement), en français et anglais.
  
  * **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.

  **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.

.. contents:: Table des matières du document
  :local:
  :depth: 2


Dictionnaires et lexiques
=========================

* `Lexique sur le transfert de connaissances en éducation (CTREQ) <http://www.ctreq.qc.ca/realisation/lexique-transfert-de-connaissances-education/>`_
* `Vocabulaire des TIC (Min. de la culture) <http://www.culturecommunication.gouv.fr/Thematiques/Langue-francaise-et-langues-de-France/Politiques-de-la-langue/Enrichissement-de-la-langue-francaise/FranceTerme/Vocabulaire-des-TIC-2017>`_

Les classiques
==============

Philosophie
-----------

* Popper, K. (1998). *La connaissance objective*. Paris: Flammarion.


Psychologie
-----------

* Bruner, J. S. (1991). *... car la culture donne forme à l'esprit*. Paris : Eshel.

* Vygotski, L. (1985). *Pensée et langage*. Paris : Messidor.

* 📺 Bloom, P. (2007). `Psych 101. Introduction to psychology <https://debunkingdenialism.com/2017/07/31/introduction-to-psychology-video-lectures-from-yale/>`_. Yale Univ. 

Aspects historiques
-------------------

* `Bibliothèque patrimoniale numérique de l'Espé d'Artois <http://bibnum-bu.univ-artois.fr/about>`_

* Gardey, D. (2008). *Écrire, calculer, classer. Comment une révolution de papier a transformé les sociétés contemporaines (1800-1940)*. Paris: La Découverte.

* Gauthier, C., & Tardif, M. (Eds.). (1996). *La pédagogie, théories et pratiques de l'Antiquité à nos jours*. Montréal: Gaëtan Morin. 

* Querrien, A. (2005). *L'école mutuelle. Une pédagogie trop efficace ?* Paris: Les empêcheurs de penser en rond.

* `Ressources numériques en histoire de l'éducation (LARHA, ENS-Lyon) <http://rhe.ish-lyon.cnrs.fr>`_

Méthodes de recherche
---------------------

* van der Maren, J.-M. (1996). `Méthodes de recherche pour l'éducation <https://www.pum.umontreal.ca/catalogue/methodes-de-recherche-pour-leducation>`_. Bruxelles : De Boeck.


Éducation et sciences cognitives
================================

Généralités
-----------

* Altman, W.S., Stein, L., & Westfall, J. E. (2018). *Essays from E-xcellence in Teaching* (Vol. XVII). Society for the Teaching of psychology. [PDF <http://teachpsych.org/resources/Documents/ebooks/eit2017.pdf>`_

* Bereiter, C. (2002). *Education and mind in the knowledge age*. Mahwah: Erlbaum. [`Extraits <http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.693.3964&rep=rep1&type=pdf>`_]

* 📺 Bjork, R. A. (2018). Why don't the trials and errors of everyday living and learning teach us how to learn?  [ `Vidéo <https://youtu.be/ouocL4ssJLY>`_]

* Brown, P. C., Roediger, H. L., McDaniel, M. A. (2016). *Mets-toi ça dans la tête !: Les stratégies d'apprentissage à la lumière des sciences cognitives*. Genève : Markus Haller.

* `Introduction à des thèmes en lien avec l'éducation (CIRCL, univ. Chicago) <https://circlcenter.org/primers/>`_ 

* 📺 Conférences du 1\ :sup:`er` Colloque du Conseil scientifique de l'éducation nationale (CSEN) [`Vidéos <https://www.dailymotion.com/playlist/x57uhl>`_] 

* 📺 Conférence Int. du CSEN sur la métacognition et la confiance en soi (2018) [`Vidéos <https://www.reseau-canope.fr/conference-internationale-sur-la-metacognition-et-la-confiance-en-soi/captations1491.html>`_]

* Crahay, M. (1999). *Psychologie de l'éducation*. Paris: P.U.F.

* Dessus, P., & Gentaz, É. (Eds.). (2006). *Apprentissages et enseignement : sciences cognitives et éducation*. Paris : Dunod.

* Donald, M. (1999). *Les origines de l'esprit moderne*. Bruxelles : De Boeck. 

* Egan, K. (1997). *The educated mind. How cognitive tools shape our understanding*. Chicago : Chicago UP. 

* Gentaz, É., & Dessus, P. (Eds.). (2004). *Comprendre les apprentissages : sciences cognitives et éducation*. Paris : Dunod.

* Kail, M., & Fayol, M. (Eds.). (2003). *Les sciences cognitives et l'école*. Paris: P.U.F.

* National Academies of Sciences, Engineering, and Medicine (2018). *How People Learn II: Learners, Contexts, and Cultures*. Washington, DC: The National Academies Press. https://doi.org/10.17226/24783. [`Livre en PDF <https://www.nap.edu/catalog/24783/how-people-learn-ii-learners-contexts-and-cultures>`_

* Olson, D. R. (2005). *L'école entre institution et pédagogie*. Paris: Retz. 

* 📺 Oudeyer, P.-Y. (2017). Intelligence artificielle et sciences cognitives: modéliser l'apprentissage du langage [`Vidéo <https://youtu.be/7HAZiA3CxXA>`_]

* 📺 `Entretiens sur différents objets de sciences de l'éducation <https://tecfa.unige.ch/perso/peraya/index.php?page=ressources>`_ 

* 📺 Ramus, F. (2018). Tout ce que vous avez toujours su sur l'éducation et qui est faux. [`Vidéo <https://youtu.be/UMQEzHX8_98>`_]

* Richard, M. & Bissonnette, S. (2012). Les sciences cognitives et l'enseignement. In C. Gauthier & M.Tardif. *La pédagogie. Théories et pratiques de l’Antiquité à nos jours* (3\ :sup:`e` éd., pp. 237-255). Montréal : Gaëtan Morin. [`PDF <http://r-libre.teluq.ca/1375/1/Sc%20cognitives%20et%20enseignement.pdf>`_]

* van Merriënboer, J., & Kirschner, P. (2007). *Ten steps to complex learning*. Mahwah: Erlbaum.

* Willingham, D. T. (2010). *Pourquoi les enfants n'aiment pas l'école !* Paris : La librairie des écoles.
  

Charge cognitive et attention
-----------------------------

Voir Doc. :ref:`charge_cognitive`.

* Chanquoy, L., Tricot, A., & Sweller, J. (2007). *La charge cognitive, théorie et applications*. Paris : Colin.

* Citton, Y. (2014). *Pour une écologie de l'attention*. Paris: Seuil.

* Tricot, A. (1998). Charge cognitive et apprentissage. Une présentation des travaux de John Sweller. *Revue de Psychologie de l'Éducation*, *3*. [`PDF <http://andre.tricot.pagesperso-orange.fr/tricotRPE.pdf>`_]

Théories sur l'apprentissage
----------------------------

* 📺 `Le constructivisme (M. Crahay) <http://tecfa.unige.ch/tecfa/teaching/bachelor_74111/podcasts_concepts/constructivisme_crahay.mov>`_.
podcasts_concepts/communautes_pratique_amaury_daele.mov>`_.

* 📺 `le modèle ICAP sur l'engagement des élèves dans l'apprentissage (Pleen le Jeune) <https://youtu.be/v-D1p2kwKEo>`_

* Milwood, R. (2013). `Learning Theory <http://blog.richardmillwood.net/wp-content/uploads/2013/11/Learning-Theory.pdf>`_. Poster.


Stratégies d'apprentissage
--------------------------

* Hattie, J. A. C., & Donoghue, G. M. (2016). Learning strategies: a synthesis and conceptual model. *NPJ Science of Learning, 1*, 16013. [`PDF <http://www.singlestepslearning.co.uk/uploads/1/6/6/4/16649496/npjscilearn201613.pdf>`_]
 
* McCrea, P. (2018). Learning, what is it, and how might we catalyse it? London: The Institute for teaching. [`PDF <https://ift.education/learning-paper/>`_

* Tricot, A. (2017). La connaissance et la solution. *Éducation et Didactique*, *11*(2), 57–61. [`Article <https://www.cairn.info/revue-education-et-didactique-2017-2-page-57.htm>`_]

* 📺 Weinstein, D. (2017). Quelles stratégies pour un apprentissage efficace ? [`Vidéo <http://www.canal-u.tv/video/universite_de_toulouse/conference_quelles_strategies_pour_un_apprentissage_efficace.35961>`_]

* 📺 Weinstein, D. (2018). Le modèle labo -> classe [`Vidéo <https://webtv.u-picardie.fr/watch_video.php?v=53DBNX9D3YB8>`_]


Systèmes scolaires
------------------

* OCDE (2018). `How to build a 21st Century school system <https://www.oecd-ilibrary.org/education/world-class_9789264300002-en>`_. OCDE.


Évaluation
----------

* ENhance student learning and develop your teaching practice (Univ. Edimbourg) [`Site <https://staff.napier.ac.uk/services/dlte/ENhance/Pages/ENhanceQuickGuides.aspx>`_

Différenciation pédagogique
---------------------------

Voir Doc. :ref:`peda_differenciee`

* CNESCO (2017). `Différenciation pédagogique : comment adapter l'enseignement pour la réussite de tous les élèves ? <http://www.cnesco.fr/fr/differenciation-pedagogique/>`_ Conférence de consensus.

Éducation fondée sur les preuves
--------------------------------

* `Best evidence encyclopedia <http://bestevidence.org>`_.

Apprentissage et utilisation du numérique
-----------------------------------------

* Amadieu, F., & Tricot, A. (2014). *Apprendre avec le numérique : mythes et réalités*. Paris : Retz

* Bruillard, É. (1997). *Les machines à enseigner*. Paris: Hermès. [`Chap. 2 <http://stef.ens-paris-saclay.fr/version-francaise/membres/les-machines-a-enseigner-268671.kjsp?RH=1215529015990>`_]

* Charlier, B., & Henri, F. (2010). *Apprendre avec les technologies*. Paris : PUF. 

* 📺 Dillenbourg, P. (2018). *Pensée computationnelle: pour un néo-papertisme durable car sceptique*. Conférence à HEP Vaud. [`Lien <https://vimeo.com/254811757>`_]
 
* `EduTech Wiki <http://edutechwiki.unige.ch/fr/Accueil>`_

* Erhel, S. (2021). `Jeux vidéos et apprentissage <https://video.toutatice.fr/cardie/journee-academique-de-linnovation-2021/video/22194-conference-de-madame-erhel-a-la-9-jai/>`_. Rennes, journée académique de l'innovation.  

* 📺 Loiseau, M. (2014). Notions d'algorithmique pour comprendre les réseaux sociaux [Vidéo]. Conférence au colloque éducation aux médias. Grenoble [`Lien <https://youtu.be/_dNHQ-ucLtM>`_]

* Morozov, E. (2014). *Pour tout résoudre cliquez ici*. Limoges: Fyp. [`Une revue <http://dms.revues.org/963>`_]

* OECD (2014). Measuring innovation in education: A new perspective. CERI, OECD Publishing [`PDF en ligne <http://www.keepeek.com/Digital-Asset-Management/oecd/education/measuring-innovation-in-education_9789264215696-fr>`_

* 📺 Romero, M. (2018). `Le numérique à l’école: consommation ou outil de co-création? <https://youtu.be/i6ZOQ70lbAM>`_. Conférence TEDx Talk Dunkerque.

* Selwyn, N. (2016). *Is technology good for education?* Cambridge: Polity. [`Une revue <https://www.alternet.org/books/technology-good-education>`_]

* 📺 Tricot, A. (2016). `L'école du futur <https://youtu.be/q5e3ZyqWE4Q>`_. Conférence TEDx Talk Toulouse.
 
* Tricot, A. (2017). *L’innovation pédagogique*. Paris : Retz.

* West, R. E. (2017). *Foundations of learning and instructional design technology*. Pressbooks. [`Livre en ligne <https://lidtfoundations.pressbooks.com>`_`] 
  
Ouvrages plus techniques sur le raisonnement
--------------------------------------------

* Mercier, H., & Sperber, D. (2017). *The enigma of reason. A new theory of human understanding*. London: Allen Lane.

* Minsky, M. (2006). *The emotion machine*. New York: Simon & Schuster. 


Mythes et neuro-mythes
----------------------

Voir Doc. :ref:`mythes_education`.

* Baillargeon, N. (2006). *Petit cours d'autodéfense intellectuelle*. Montréal: Lux.

* Baillargeon, N. (2013). *Légendes pédagogiques. L'autodéfense intellectuelle en éducation*. Montréal: Poètes de brousse.

* Pasquinelli, E. (2015). *Mon cerveau, ce héros - Mythes et réalité*. Paris : Le Pommier.

* 📺 Pasquinelli, E. (2012). `Les neuromythes <http://savoirs.ens.fr/expose.php?id=661>`_.

Aspects sociaux
---------------

* CNESCO (2016). Inégalités sociales <http://www.cnesco.fr/fr/inegalites-sociales/>`_. Conférence de consensus.

* CNESCO (2017). `Prévention et intervention : comment agir efficacement face au décrochage scolaire ? <http://www.cnesco.fr/fr/decrochage-scolaire/>`_ Conférence de consensus. 

* CNESCO (2017). `Qualité de vie à l'école <https://www.cnesco.fr/fr/qualite-vie-ecole/>`. Rapport.

* 📺 Croizet, J.-C. `Le mystère des sports d'hiver <https://youtu.be/Fsoxr4tNmWE>`_.

* 📺 Daele, A. `Les communautés de pratique <http://tecfa.unige.ch/tecfa/teaching/bachelor_74111/>_ 

* 📺 Dubet, F. `Les épreuves de la massification scolaire <https://youtu.be/7nU-w25lQjs>`_.

* Merle, P. (2005). *L'élève humilié : l'école, un espace de non-droit ?* Paris: P.U.F.

* 📺 Vouillot, F. `Egalité filles-garçons <https://youtu.be/N8YME2rldVw>`_.
 
* Wenger, E. (2005). *La théorie des communautés de pratiques*. Laval : PU Laval. 

Émotions
--------

* 📺 Barrett, L. F. (2018). `You aren't at the mercy of your emotions. Your brain creates them <https://www.ted.com/talks/lisa_feldman_barrett_you_aren_t_at_the_mercy_of_your_emotions_your_brain_creates_them>`_. TED Talk. 

Intégration
-----------

* `Intégration - Penser l'éducation: concepts-clefs <https://mediaserver.unige.ch/play/VN4-147a-2012-2013>`_

Petite enfance
--------------

* Bigras, N., & Lemay, L. (2012). *Petite enfance, services de garde éducatifs et développement des enfants. Etat des connaissances*. Québec: Presses de l’Université du Québec.

Domaines
========

Lecture et compréhension
------------------------

* Bianco, M. (2015). *Du langage oral à la compréhension de l'écrit*. Grenoble: P.U.G.

* CNESCO (2016). `Lire, comprendre, apprendre : comment soutenir le développement de compétences en lecture ? <http://www.cnesco.fr/fr/lecture/>`_. Conférence de consensus.
* 📺 `The power of words and the secrets of language (D. McNamara) <https://youtu.be/bSdJYhID_AQ>`_ 
  
Écrire et rédiger
-----------------

* CNESCO (2018). `Écrire et rédiger <https://www.cnesco.fr/fr/ecrire-et-rediger/>`_. Conférence de consensus.


Nombre et calcul
----------------

* CNESCO (2015). `Nombres et opérations : premiers apprentissages à l'école primaire <http://www.cnesco.fr/fr/numeration/>`_. Conférence de consensus.

Sciences
--------

* BNF (2018). `Exposition Sciences pour tous (1850-1900) <http://expositions.bnf.fr/sciencespourtous/>`_

Enseignement 
============

Enseignement et pédagogie
-------------------------

* `Une collection d'articles sur l'enseignement-apprentissage <https://teacherhead.com/2017/06/03/teaching-and-learning-research-summaries-a-collection-for-easy-access/>`_

* `Rapports OCDE <http://www.oecd-ilibrary.org/education/l-enseignement-a-la-loupe_6bdc2298-fr>`_

* OCDE (2018). `Teaching for the future. Effective classroom practices to transform education <http://www.oecd.org/education/school/teaching-for-the-future-9789264293243-en.htm>`_. Paris : OCDE, http://dx.doi.org/10.1787/9789264293243-en

* `Recherche pour enseignants <http://www.cem.org/blog/what-is-worth-reading-for-teachers-interested-in-research/>`_

* Crahay, M., & Lafontaine, D. (Eds.). (1986). *L'art et la science de l'enseignement*. Bruxelles: Labor.
* Murillo, A. (2021). *Prendre un bon départ avec ses classes*. Paris : ESF.
  
Formation des enseignants
-------------------------

* 📺 Condette, J.-F. `L'histoire de la formation des enseignants <https://youtu.be/oYGn0SFRovk>`_

Enseignement et sciences cognitives
-----------------------------------

* Bressoux, P. (Ed.). (2002). Les stratégies de l'enseignant en situation d'interaction. Rapport pour l'ACI Cognitique. [`PDF <https://halshs.archives-ouvertes.fr/edutice-00000286/document>`_].

Le métier de l'enseignant
-------------------------

* CNESCO (2016). L'attractivité du métier d'enseignant <http://www.cnesco.fr/fr/attractivite-du-metier-denseignant/politiques-de-recrutement/>`_. Rapport.

Conception de l'enseignement
----------------------------

Voir Doc. :ref:`concevoir_enseignement`.

* Dessus, P. (2006). Quelles idées sur l’enseignement nous révèlent les modèles d’instructional design ? *Revue Suisse des Sciences de l’Education*, *28*(1), 137–157. [`PDF <http://www.pedocs.de/volltexte/2011/4143/pdf/SZBW_2006_H1_S137_Dessus_D_A.pdf>`_]

* Musial, M., Pradère, F., & Tricot, A. (2012) *Comment concevoir un enseignement ?* De Boeck : Bruxelles. 

Les rétroactions
----------------

* Hattie, J., & Timperley, H. (2007). The power of feedback. *Review of Educational Research*, *77*(1), 81-112.

* 📺 `Dessin animé sur les rétroactions efficaces <https://www.youtube.com/watch?time_continue=92&v=LjCzbSLyIwI>`_ 
   

Observation de l'enseignement
-----------------------------

* Cantin, G. & Marcel, J.-F. (Eds.) (2017). CLASS(e) en observation. *Dossiers des Sciences de l'Éducation, 37*.

* Dessus, P. (2007). Systèmes d'observation de classes et prise en compte de la complexité des événements scolaires. *Carrefours de l'Éducation, 23*, 103–117. [`PDF <http://hal.univ-grenoble-alpes.fr/hal-00321422>`_]

Connaissances et jugements des enseignants
------------------------------------------

* Bressoux, P., & Pansu, P. (2003). *Quand les enseignants jugent leurs élèves*. Paris: P.U.F.

* Gauthier, C. (Ed.). (1997). *Pour une théorie de la pédagogie*. Bruxelles/Laval: De Boeck/P.U.L.


Méta-analyses
-------------

* Bergeron, P.-J. (2016). Comment faire de la pseudoscience avec des données réelles : une critique des arguments statistiques de John Hattie dans Visible Learning par un statisticien. *McGill Journal of Education/Revue des sciences de l'éducation de McGill*, *51*(2). [`PDF <http://mje.mcgill.ca/article/view/9394>`_]

* Hattie, J. (2017). *L'apprentissage visible pour les enseignants : Connaître son impact pour maximiser le rendement des élèves*. Québec : Presses de l'université du Québec.

L'enseignement professionnel
----------------------------

* CNESCO (2016). `Orientation, formations, insertion : quel avenir pour l’enseignement professionnel <http://www.cnesco.fr/fr/dossier-enseignement-professionnel/>`_. Conférence de consensus.

Sites
=====

* `Mon cerveau à l'école <https://moncerveaualecole.com>`_
* `Théories de l'apprentissage <https://www.learning-theories.com>`_
* `Bibliographie de Cortecs sur la pédagogie <https://cortecs.org/videotex/petite-bibliographie-audiographie-sur-leducation-la-pedagogie-et-toutes-ces-sortes-de-choses/>`_
* `Le Tableau : Portail de soutien à la pédagogie universitaire <http://pedagogie.uquebec.ca/portail/letableau>`_
* `Fraschini, J., Gestes professionnels à l'école primaire <http://www.gestesprofessionnels.com>`_