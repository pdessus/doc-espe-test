.. _concevoir_enseignement:

=========================
Concevoir un enseignement
=========================

.. index::
    single: auteurs; Dessus, Philippe
    single: auteurs; Besse, Émilie

.. admonition:: Information

	* **Auteurs** : `Philippe Dessus <http://pdessus.fr/>`_, Inspé & LaRAC, Univ. Grenoble Alpes. Le quizz a été réalisé par Emilie Besse, projet `ReFlexPro <http://www.idefi-reflexpro.fr>`_.

	* **Date de création** : Avril 2015.

	* **Date de publication** : |today|.

	* **Statut du document** : Terminé.

	* **Résumé** : Ce document expose quelques méthodes de conception de l'enseignement, selon trois catégories : comportementales (béhavioristes), cognitivistes et constructivistes.

	* **Voir aussi** : :ref:`concevoir_enseignement` pour plus d'informations sur l'activité de conception de cours. Le doc. :ref:`sbd` donne une grille de conception narrative des usages du numérique, le doc. :ref:`tuto_scenariser_numerique` donne une grille de conception plus simple, centrée sur les activités.

	* **Note** : Des éléments de ce document sont repris de :cite:`dessus06a`. 
	
	* **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document. 

	* **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.


Introduction
============

Au sens large, la planification est donc une sorte de réflexion préparatoire, chargée d’intentions, qui survient dans un environnement différent de celui de la tâche elle-même. L'ouvrage de Musial et ses collègues :cite:`musial12` donnera d'utiles compléments à ce document.

Van Merriënboer et Kirschner :cite:`merrienboer01` ont réalisé une excellente présentation des différents mondes auxquels sont reliés les différentes méthodes de conception de l'enseignement : 

* Tout d’abord, le mal nommé *monde de la connaissance* (que nous renommons plus justement « monde des habiletés », traduction de *skills*, souvent employées par les tenants du courant behavioriste) a été défriché par Gagné et ses successeurs, proposant des hiérarchies d’objets de connaissance de plus en plus fines. 
* Ensuite, le *monde de l’apprentissage*, représenté par les différentes méthodes cognitivistes, centrées sur l’analyse des processus d’apprentissage et les moyens de les favoriser. 
* Enfin, le *monde du travail*, qui concerne plus précisément les méthodes constructivistes. La suite de ce document détaille une méthode reliée à chacun de ces mondes.

Ce que l'on sait
================

Les méthodes de conception de l'enseignement
--------------------------------------------

La méthode de conception de l'enseignement la plus ancienne et qui insipiré le plus d'autres méthodes est celle de Tyler :cite:`tyler50`, qui prescrit les étapes suivantes :

#. Déterminer les objectifs d'apprentissages poursuivis ;
#. Déterminer les actions d'enseignement pour y parvenir ;
#. Organiser ces actions (choix du matériel, de la durée, du type de groupement, etc.) ;
#. Spécifier les méthodes d'évaluation qui permettront de vérifier que les objectifs d'apprentissage ont bien été atteints.

On s'en rend compte, la plupart des enseignants réalise intuitivement ces quatre phases, même si elles sont parfois pensées dans un ordre différent. Ainsi, on a pu montrer que les enseignants expérimentés ne déterminaient les objectifs d'apprentissage qu'après avoir prévu les phases 2 et 3 (choix et organisation des événements d'apprentissage).


On a pu montrer que les modèles et méthodes de conception de l'enseignement sont utilisés pour quatre fonctions :cite:`braden96` :

* améliorer l’apprentissage et l’enseignement ;
* améliorer la gestion de la phase de conception de l’enseignement ;
* améliorer les processus d’évaluation ;
* tester et construire de nouveaux modèles et méthodes.

Comme l’ont formulé très simplement certains auteurs :cite:`merrienboer01`, les deux questions principales, et interreliées, auxquelles les méthodes de conception de l'enseignement essaient de répondre sont « comment aider à mieux apprendre ? » (soit « comment enseigner ? », du point de vue de l’enseignant) et, ensuite, « quoi enseigner ? ».

Les méthodes linéaires inspirées du béhaviorisme, ou le monde des habiletés
---------------------------------------------------------------------------
Les modèles béhavioristes partagent les caractéristiques suivantes (:cite:`villiers02` p. 95) :

* une analyse des pré-requis, afin de démarrer l’enseignement à un niveau adapté à l’élève ;
* le fait de procurer des encouragements ou un feedback directs lorsque de bonnes réponses sont données, encouragements pouvant s’estomper au cours de la leçon ;
* des domaines complexes peuvent être enseignés par leur décomposition en sous-habiletés, sous-domaines ;
* la visée d’automatiser ces sous-habiletés, afin de parvenir à la maîtrise progressive de domaines complexes ;
* la visée d’atteindre un niveau de maîtrise donné, et de continuer l’enseignement tant que ce niveau n’est pas atteint (l'un des principes de la pédagogie de la maîtrise).

Évidemment, cette analyse hiérarchique des habiletés proposée par les méthodes linéaires est nécessairement limitée, puisque prenant assez peu en compte les processus cognitifs des élèves.

**Tableau I** – La méthode de conception de l'enseignement selon Gagné (:cite:`merrienboer01`, p. 430). Les 6 premiers items concernent l’analyse de ce qui est à apprendre, les autres concernent la mise en place et l’évaluation des événements d’enseignement.

+----------------------------------------------------------------------+
| A. Analyser les conditions pour l’apprentissage en partant du but    |
| final                                                                |
+======================================================================+
| 1. Identifier les performances d’apprentissage voulues.              |
+----------------------------------------------------------------------+
| 2. Hiérarchiser ces performances de manière à obtenir des            |
| performances simples, s’enchaînant.                                  |
+----------------------------------------------------------------------+
| 3. Identifier les conditions internes ou les processus de            |
| l’apprenant devant survenir afin qu’il accomplisse ces performances. |
+----------------------------------------------------------------------+
| 4. Spécifier quelles sont les conditions de l’environnement, ou de   |
| l’enseignement, afin que ces conditions internes soient réunies.     |
+----------------------------------------------------------------------+
| **B. Sélectionner le média**                                         |
+----------------------------------------------------------------------+
| 5. Prendre en compte le contexte d’apprentissage.                    |
+----------------------------------------------------------------------+
| 6. Prendre en compte les caractéristiques des apprenants.            |
+----------------------------------------------------------------------+
| 7. Sélectionner le média pour l’enseignement.                        |
+----------------------------------------------------------------------+
| **C. Concevoir l’enseignement afin de favoriser les activités        |
| d’apprentissage**                                                    |
+----------------------------------------------------------------------+
| 8. Planifier des tâches motivantes.                                  |
+----------------------------------------------------------------------+
| 9. Concevoir des événements d’enseignement pertinents pour le type   |
| de performances d’apprentissage requis, dans la hiérarchie           |
| préalablement réalisée, en fonction des pré-requis des élèves et     |
| avec les médias et tuteurs adéquats.                                 |
+----------------------------------------------------------------------+
| 11. Tester l’enseignement (évaluation formative).                    |
+----------------------------------------------------------------------+
| 12. Juger l’efficacité de l’enseignement *a posteriori* (évaluation  |
| sommative)                                                           |
+----------------------------------------------------------------------+

Une méthode cognitiviste, ou le monde de l'apprentissage
--------------------------------------------------------

La méthode de Merrill :cite:`merrill13` s'intéresse à décrire les événements d'apprentissage d'un point de vue cognitif, et donne donc une idée des phases par lesquelles un apprenant peut passer, et le type d'expériences qu'un enseignant peut organiser ou donner (ce qui suit est partiellement repris de :cite:`dessus14`) :

* *activation de l’expérience antérieure*, l’apprentissage est favorisé quand les apprenants activent des connaissances et habiletés existantes comme fondations de nouvelles habiletés ;

* *démonstration d’habiletés*, l’apprentissage est favorisé lorsque les apprenants observent une démonstration des habiletés à apprendre ;

* *application d’habiletés* (l’apprentissage est favorisé lorsque les apprenants appliquent leurs compétences nouvellement acquises pour résoudre des problèmes) ;

* *intégration de ces habiletés* dans des activités du monde réel, l’apprentissage est favorisé lorsque les apprenants réfléchissent, discutent, et défendent leurs habiletés nouvellement acquises.

Ensuite, cinq modes d’enseignement très simples sont présentés : dire (*tell*), montrer (*show*), demander (*ask*), appliquer (*do*), ces modes pouvant être assortis de guidage ou feedback afin de permettre une focalisation de l’attention de l’apprenant sur tel ou tel aspect du contenu. Les deux premiers procurant de l’information à l’apprenant, les deux derniers laissant ce dernier répondre. Ces modes, combinés et imbriqués, génèrent quatre niveaux de complexité et d’efficacité croissantes pour l’apprentissage :

* *Niveau 0* – Information seule : présentation ou présentation-rappel (*Tell ou Tell-and-Ask*) ;
* *Niveau 1* – Démonstration : information + démonstration (*Tell-and-Show*) ;
* *Niveau 2* – Application : niveau 1 + application (*Do*) (avec rétroactions et guidage) ;
* *Niveau 3* – Centré problème : niveau 2 + problème (avec collaboration et critique entre pairs).

Ces modes sont ensuite combinés avec les éléments de contenu pour prescrire des événements d’enseignement (*instructional events*). Il est hors de propos de ce document de les décrire, mais les informations ci-dessus permettront de réfléchir à une manière de concevoir l'enseignement cognitiviste.

.. _crossley:

Une méthode constructiviste, ou le monde du travail
---------------------------------------------------

La méthode de Crossley et Green :cite:`crossley90` est l'un des exemples les plus achevés d'un type de méthode permettant de construire un environnement d'apprentissage dans lequel l'élève est peu contraint. Cette méthode, utilisable pour la conception d’environnements d’apprentissage informatiques, utilise la métaphore du marché : l’élève se promène dans les divers étals d’un marché virtuel (environnement d'apprentissage), et réalise librement les activités qu’il désire mettre en œuvre, acquérant par elles des connaissances. La liste ci-dessous décrit les principales étapes de cette méthode :

#. Sélectionner un sujet.
#. Définir l’expérience d’apprentissage, le point de vue (espace, temps et rôle de l’apprenant), et la table des responsabilités, spécifiant les rôles respectifs de l’apprenant, de l’ordinateur et de l’enseignant
#. Conception du diagramme de circulation
#. Conception de l’écran-clé, dans lequel l’apprenant passe le plus de temps
#. Conception des principales étapes de l’écran-clé
#. Conception des commandes de l’utilisateur
#. Conception de la table des conditions, (in)validant les actions de l’apprenant
#. Conception de la table des réactions de l’ordinateur
#. Conception des écrans secondaires
#. Récupérer l’ensemble des données nécessaires pour le logiciel
#. Rédiger les règles de comportement du logiciel pour le programmeur
#. Écriture du manuel de l’enseignant et de l’élève


Ce que l'on peut faire
======================

Une présentation des activités
------------------------------

Il est bien sûr possible, pour préparer son enseignement, de suivre l'une des méthodes décrites plus haut. On peut également considérer l'enseignement comme la mise en œuvre de routines (ou automatismes), c'est-à-dire de comportements partiellement automatisés, dont l'adaptation à la situation (les élèves, le matériel, etc.) se fait en "remplissant" (ou instanciant) certaines variables. Ainsi, l'enseignant se construit un certain nombre de routines qui vont l'aider à enseigner plus efficacement, car elles nécessiteront moins d'attention de la part de l'enseignant, que ce soit pendant leur préparation que pendant leur mise en œuvre. La liste suivante en présente :cite:`leinhardt86` est bien sûr à augmenter ou modifier selon le contenu à enseigner.

* *Présentation* : Explication ininterrompue de l'enseignant à propos d'un matériel nouveau ou récemment appris, pendant que les élèves écoutent.

* *Présentation partagée* : L'enseignant présente le matériel (texte, exercice, objet, etc.), la plupart du temps par le biais de questions ou avec l'aide d'un ou plusieurs élèves, oralement ou au tableau.

* *Présentation guidée* : Travail sur table dans lequel les élèves travaillent sur des problèmes présentés au tableau ou sur feuille, l'enseignant les guidant.

* *Pratique monitorée* : L'enseignant se déplace de bureau à bureau pour contrôler et aider les élèves qui travaillent.

* *Correction de devoirs* : Vérification et collecte des devoirs à la maison (ou en classe), la plupart du temps en début ou fin de journée.

* *Exercice* : Rappel de données par les élèves, mené par l'enseignant, oralement, par écrit ou au tableau.

* *Exercice-jeu* : Exercice entre groupes ou élèves, avec un climat de compétition.

* *Tutoriel* : Présentation prolongée à quelques élèves pendant que les autres travaillent à leur place.

* *Transition* : Passage d'une activité à l'autre. L'enseignant donne plusieurs consignes que les élèves exécutent.

La grille de Merrill
--------------------

Merrill :cite:`merrill17` propose une méthode intéressante, que nous résumons ici. Selon lui, il existe un nombre restreint d'activités de base dans l'enseignement/apprentissage :

* Dire (*tell*) : l'enseignant énonce une information ;
* Questionner (*ask*) : l'enseignant pose une question (notamment pour vérifier ou susciter la compréhension des apprenants) ;
* Montrer (*show*) : l'enseignant montre une habileté, une compétence, des exemples, à ses élèves, pour étayer les 2 précédentes activités ;
* Faire (*do*) : l'enseignant demande aux apprenants de réaliser une activité, de manière à mettre en pratique ce qui a été dit, questionné, montré. Il existe deux sous-éléments : Faire-Identifier et Faire-Exécuter.

Merrille signale que beaucoup de temps, dans l'enseignement, se passe par des boucles “dire-questionner” (*Tell-Ask*). On peut améliorer cela en réalisant les tâches de préparation et d'analyse du cours suivantes :

* identifier les sujets (notions) du cours en question ;
* créer un tableau et lister ces sujets dans la première colonne du tableau ;
* créer 4 nouvelles colonnes et les nommer par les activités : dire, questionner, montrer, faire ;
* identifier, pour chaque sujet, l'information “dite” et la mentionner dans la cellule correspondante ; s'assurer que chaque information est suffisamment détaillée pour l'enseignement de la notion ;
* identifier, toujours par sujet, un événement “montrer” ; s'il n'en existe pas, ou s'il n'est pas suffisamment adéquat, le créer ou le modifier ;
* identifier, pour chaque sujet, un événement “Faire” ; s'il n'en existe pas, ou s'il n'est pas suffisamment adéquat, le créer ou le modifier ;
* réaliser le cours.


Quizz
=====

.. eqt:: Conception-cours-1

	**Question 1. La méthode de conception de l'enseignement la plus ancienne est celle de :**

	A) :eqt:`C` `R. W. Tyler`
	B) :eqt:`I` `R. A. Braden`
	C) :eqt:`I` `M. R. de Villiers`
	D) :eqt:`I` `M. D. Merrill`

.. eqt:: Conception-cours-2

	**Question 2. Dans quel ordre interviennent les actions dans la méthode de conception de Gagné ?**

	A) :eqt:`I` `1° Sélectionner le média, 2° Analyser les conditions pour l'apprentissage en partant du but final, 3° Concevoir l'enseignement afin de favoriser les activités d'apprentissage`
	B) :eqt:`C` `1° Analyser les conditions pour l'apprentissage en partant du but final, 2° Sélectionner le média, 3° Concevoir l'enseignement afin de favoriser les activités d'apprentissage`
	C) :eqt:`I` `1° Analyser les conditions pour l'apprentissage en partant du but final, 2° Concevoir l'enseignement afin de favoriser les activités d'apprentissage, 3° Sélectionner le média`
	D) :eqt:`I` `1° Concevoir l'enseignement afin de favoriser les activités d'apprentissage, 2° Sélectionner le média, 3° Analyser les conditions pour l'apprentissage en partant du but final`

.. eqt:: Conception-cours-3

	**Question 3.  Quelle méthode de conception de l'enseignement utilise la métaphore du marché ?**

	A) :eqt:`I` `Le behaviorisme`
	B) :eqt:`I` `Le cognitivisme`
	C) :eqt:`C` `Le constructivisme`
	D) :eqt:`I` `Le comportementalisme`


Références
==========

.. bibliography::
   :cited:
   :style: apa
