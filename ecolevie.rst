.. _ecole_vie:

=========================
L'école, un lieu de vie ?
=========================

.. index::
	single: auteurs; Dessus, Philippe
	single: auteurs; Besse, Émilie

.. admonition:: Informations

  * *Auteurs** : `Philippe Dessus <http://pdessus.fr/>`_, Inspé & LaRAC, Univ. Grenoble Alpes. Le quizz a été réalisé par Emilie Besse, projet `ReflexPro <http://www.idefi-reflexpro.fr>`_.

  * **Date de création** : Octobre 2004.

  * **Date de publication** : |today|.

  * **Statut du document** : Terminé.

  * **Résumé** : En quoi l'école peut-elle être considérée comme un lieu de vie ? Que mettre en place pour favoriser cela ? Ces questions peuvent tout aussi bien être considérées comme des évidences ou des   paradoxes. Il convient donc d'analyser les différents avis des philosophes de l'éducation sur ces questions.

  * **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.
  
  * **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_


Introduction
============

Comme le remarque Perrenoud (1994), on s'interroge souvent du rapport entre la "vraie vie" et l'école, soit pour dénoncer "l'école contre la vie", soit pour "faire entrer la vie dans l'école". Pour ce dernier, et bien d'autres (voir plus bas), l'école serait tout simplement "la vie, une partie de la vie".  Est-il si facile de distinguer l'école en tant que lieu d'apprentissage et les autres situations professionnelles ? Pas sûr, tant ces dernières sont souvent aussi fermées sur elles-mêmes, coupées des autres travailleurs et de la vie sociale.

Quoi qu'il en soit, il peut être fructueux de considérer l'école comme un lieu de vie sociale, à la fois pour mieux le comprendre, mais aussi pour faciliter cette dernière.

Ce que l'on sait
================

Tout le monde n'est pas d'accord avec l'idée que l'école soit vraiment un lieu de vie.  L'héritage de cette pensée est lointain (Houssaye, 1987) :  elle prend racine en plein Moyen Âge, lorsqu'il s'est agi de former des enfants à la vie monastique. Les courants éducatifs ultérieurs reprirent cet héritage même lorsqu'ils sont devenus plus laïques : l'enfant pouvait être considéré soit comme un être ne distinguant pas le bien du mal, soit  comme un être en devenir à protéger des aléas de la vie de tous les jours. 

On peut distinguer trois grandes périodes à propos du fait de considérer l'école comme préparation ou non à la vie : la pédagogie traditionnelle, qui conçoit l'école comme à l'écart de la vie, la pédagogie nouvelle, qui conçoit l'école comme une expérience de la vie et une préparation à cette dernière et, plus récemment, un courant dénonçant les aspects trop utilitaristes de l'éducation.

De "l'école contre la vie" à "l'école est une vie"
--------------------------------------------------

Claparède (1950, dans la lignée de Dewey et Decroly, et même Rousseau), affirmant que l'éducation est une vie, et non une préparation à la vie. Pour argumenter cela, Claparède indique que la pédagogie "traditionnelle" pose que l'enfant ressemble structurellement à l'adulte (mêmes processus psychologiques), tout en en différant  (l'enfant étant plus malléable que l'adulte, et réalisant ce qu'on lui demande de faire sans avoir besoin de but explicite). Or, les travaux de Piaget l'ont montré, il est possible de renverser ces affirmations : l'enfant serait structurellement différent de l'adulte, et fonctionnellement semblable. Le premier point amène que l'éducation ne devrait avoir pour but de préparer à la vie d'adulte, et le second que l'éducation devrait tout simplement être une vie (i.e., que l'enfant considère son expérience scolaire comme une partie de sa vie). Pour Claparède, l'important n'est pas que les élèves fassent tout ce qu'ils veulent (comme les détracteurs de l'éducation nouvelle l'ont dénoncé), mais qu'ils veuillent tout ce qu'ils font.

L'article de Gilliard (s.d.),  dont le titre est repris ici, a une vue plus pessimiste encore sur la pédagogie traditionnelle que les auteurs vus ci-dessus :  pour Gilliard, l'école baîllonne les élèves, les obligeant à réciter des leçons tout en les empêchant de baîller ou de dire qu'ils s'y ennuient. Certaines matières (comme le latin)  font que les élèves en viennent à se conformer au discours scolaire, plutôt qu'à enrichir leurs connaissances et leur langage.

Les dangers de l'utilitarisme
-----------------------------

Ce que nous venons de montrer plus haut peut laisser pencher la balance du côté de l'école pour la vie. Pour autant, certains auteurs ont dénoncé les inconvénients de vouloir, trop tôt, préparer les élèves à la vie professionnelle. Ces auteurs (*e.g.*, de Sélys & Hirtt, 1998 ; Hirtt, 2001 ; Laval, 2003), en dénonçant entre autres les récentes incursions de l'entreprise dans l'école, montrent  que des conceptions utilitaristes et libérales de l'éducation peuvent rendre l'école directement au service de l'économie (voir Document :ref:`droit_info_soc`). 

La forte croissance économique de l'après-guerre des pays occidentaux a eu pour conséquence d'essayer de fournir, à la fois des travailleurs qualifiés, mais aussi des consommateurs avertis pouvant utiliser les produits de plus en plus complexes fabriqués par l'industrie. Ainsi, en France, certains Plans de l'Etat ont directement mis en perspective besoins des entreprises et taille optimale du système éducatif. Le rendement, la performance et la rentabilité deviennent des critères plus importants parfois que la transmission ou construction de connaissances. Plus récemment, la distinction de plus en plus nette entre diplôme et emploi, au profit de l'évaluation de compétences spécifiques, pouvant et même devant être acquises tout au long de la vie, participent aussi de ce courant sans doute trop utilitariste.

A quoi sert l'école ?
---------------------

Avant tout, il peut être utile de revenir à cette question, dont certaines réponses figurent dans les différents livrets des programmes officiels. Par exemple, l'école élémentaire doit offrir à tous les enfants des chances égales et une intégration réussie dans la société française. Elle est aussi "le socle sur lequel se construit une formation complexe et de longue durée menant chacun à une qualification, pour la plupart d'un niveau élevé et, pour tous, devant être mise à jour tout au long de la vie" (Programmes de l'école élémentaire, 2002). L'essentiel est dit ici, et il sera facile à  chacun de réinterpréter chacun de ces buts à la lumière de ce qui a été exposé plus haut. 

Ce que l'on peut faire
======================

L'anecdote suivante résume bien ce vers quoi il est possible d'aller : 

.. epigraph::

  "M. Dewey s'était rendu un jour dans un magasin de mobilier scolaire pour y acquérir des tables pouvant convenir aux diverses occupations qui se pratiquaient dans son école. Comme il ne parvenait pas à trouver ce qu'il cherchait, le marchand finit par lui dire :

  --  Je crains que nous n'ayons pas ce qu'il vous faut. Vous désirez des meubles permettant aux enfants d'exécuter du travail ; mais tous ceux que nous avons ne sont faits que pour écouter". (In Claparède, E., préface de Dewey, L'école et l'enfant. Delachaux & Niestlé).

L'école, et la classe, sont des organisations dont le principal but est l'apprentissage de leurs participants, mais aussi des lieux où se tissent des liens relationnels importants, entre élèves et entre enseignants et élèves. Il peut donc être utile d'organiser les conditions matérielles et environnementales afin que les élèves puissent vivre, communiquer, avoir une vie sociale riche. A cet effet, l'ouvrage de Vayer *et al*. (1991) donne des pistes de travail intéressantes. Ils montrent à la fois comment une structuration de l'environnement matériel (position des tables et chaises, études ergonomiques), et une organisation de l'espace de la classe par les élèves eux-mêmes favorisent cette vie sociale. Les auteurs montrent que les enfants n'ont pas défini l'espace en fonction des activités qu'ils peuvent y mener, comme les adultes le font généralement, mais en termes de relations sociales : travaux individuels, de groupes, collectifs. Dans une étude, la classe a été subdivisée en plusieurs espaces (activité personnelle, espace conseil, espace de transition et espace d'activités de groupe).

Quizz
=====

.. eqt:: Ecole-vie-1

	**Question 1. Durant quelle époque l'Etat Français a mis en perspective les besoins des entreprises et le système éducatif ?**	

	A) :eqt:`I` `Pendant l'entre deux guerres`
	B) :eqt:`I` `Avant la première guerre mondiale`
	C) :eqt:`C` `Après la seconde guerre mondiale`
	D) :eqt:`I` `A la fin du XIXe siècle`


.. eqt:: Ecole-vie-2			

	**Question 2. Pour Claparède (1950), qu'est-ce que l'éducation se devrait d'être ?**	

	A) :eqt:`C` `Une vie`
	B) :eqt:`I` `Une préparation à la vie adulte`
	C) :eqt:`I` `Une école contre la vie`
	D) :eqt:`I` `Une école à côté de la vie`

.. eqt:: Ecole-vie-3			

	**Question 3. La pédagogie nouvelle part du principe que :**	

	A) :eqt:`I` `L'enfant est structurellement différent de l'adulte`
	B) :eqt:`C` `L'enfant ressemble structurellement à l'adulte`
	C) :eqt:`I` `L'enfant est fonctionnellement différent de l'adulte`
	D) :eqt:`I` `L'enfant est psychologiquement différent de l'adulte`


Analyse des pratiques
=====================

#. A propos de votre classe, analysez les aspects de son organisation facilitant ou au contraire gênant  la vie sociale de vos élèves.


Références
==========

* Claparède, E. (1950). *L'éducation fonctionnelle* (3\ :sup:`e` éd.). Neuchâtel : Delachaux & Niestlé.
* De Sélys, G., & Hirtt, N. (1998). *Tableau noir. Résister à la privatisation de l'enseignement*. Bruxelles : EPO.
* Gilliard, E. (1984). L'école contre la vie. In *Trois pamphlets pédagogiques* (pp. 75-115). s.l. :  L'âge d'Homme (éd. orig. 1940-1945).
* Hirtt, N. (2001).  *L'école prostituée*. Bruxelles : Labor.
* Houssaye, J. (1987). *École et vie active*. Neuchâtel : Delachaux & Niestlé.
* Perrenoud, P. (1994). *Métier d'élève et sens du travail scolaire*. Paris : E.S.F.
* Vayer, P., Duval, A., & Roncin, C. (1991). *Une écologie de l'école*. Paris : PUF. 
