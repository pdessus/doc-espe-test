.. _travail_equipe:

***************************************
Les enseignants et le travail en équipe
***************************************

.. index::
	single: auteurs; Dessus, Philippe
	single: auteurs; Besse, Émilie

.. admonition:: Informations

  * **Auteurs** : `Philippe Dessus <http://pdessus.fr/>`_, Inspé & LaRAC, Univ. Grenoble Alpes. Le quizz a été réalisé par Emilie Besse, projet `ReFlexPro <http://www.idefi-reflexpro.fr>`_.

  * **Date de création** : Février 2002 ; mis à jour en mars 2009.

  * **Date de publication** : |today|.

  * **Statut du document** : Terminé.

  * **Résumé** : Le travail en équipe des enseignants est une pratique courante, mais la diversité, d'une part des intervenants dans le système éducatif, d'autre part des dispositifs autorisant ce dernier, le rend souvent lourd et parfois difficile. Ce document fait le point sur les principales formes de travail en équipe des enseignants.

  * **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.

  * **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.


Ce que l'on sait
================

Des études ont montré que les établissements dans lesquels les élèves réussissent  étaient ceux dans lesquels les enseignants coopèrent, reconnaissent que l'amélioration des conditions de travail est une tâche collective et non individuelle :cite:`gather00`. Comme l'indique une partie de l'ouvrage de Tardif et Lessard :cite:`tardif99`, et le préconise la `Loi d'orientation du 10 juillet 1989 (art. 14) <http://www.ac-amiens.fr/iaoise/textes_officiels/role_directeur/loi_orientation.htm>`_, l'enseignement peut être vu comme un travail collectif. L'enseignant a non seulement des rapports avec ses collègues enseignants, les autres employés non enseignants de l'établissement, mais aussi hors établissement (psychologues, etc.), sans oublier les `parents d'élèves <parents.html>`_. De plus, la difficulté de l'activité réalisée en équipe va influer sur sa fréquence et sa qualité : qu'il s'agisse de seulement échanger des points de vue, coordonner différentes pratiques, ou encore assumer collectivement une responsabilité d'élèves.

Quel type de travail un enseignant peut-il mettre en place avec ces multiples partenaires ? Faisons un point catégorie par catégorie de partenaire, en nous inspirant principalement de Tardif et Lessard :cite:`tardif99`.

Rapports entre les enseignants
------------------------------

La plupart des études sur le travail des enseignants concluent que ces derniers se sentent assez isolés (:cite:`tardif99`, chap. 1). Toutefois, lorsqu'il est fait état de collaboration entre collègues, elle porte principalement avec ceux de la même matière, voire du même niveau de classe. Lorsque des collaborations interniveaux et intermatières se réalisent, c'est surtout à propos d'organisation de l'établissement (horaires, discipline générale, règlement intérieur, etc.). De plus, la manière dont est compartimentée la salle des professeurs joue pour beaucoup dans les relations et les éventuelles collaborations.

Quelles sont les formes de collaboration entre enseignants ? Nous verrons que cette collaboration n'implique en général pas la présence de collègue(s) dans la classe, et qu'elle est d'autant plus importante que les enseignants sont novices.

* La plus répandue semble être à propos de *préparation de cours*. En règle générale, les enseignants de même matière et niveau s'échangent des documents, des fiches de préparation, des fiches de TP, etc., tout en gardant une marge de manœuvre et leur autonomie.
* Il existe aussi une forme de *tutorat informel*, sous forme de conseils d'enseignant expérimenté à collègue débutant. Ce tutorat est considéré par les enseignants débutant comme un véritable soutien moral, un encouragement.
* Enfin, une collaboration s'établit souvent pour partager des tâches d'enseignement (plus fréquente au primaire). Ainsi, lorsqu'un enseignant est plus compétent dans telle ou telle partie du programme, il peut intervenir dans les classes de ses collègues, *et vice versa* :cite:`tardif99`.

Rapports avec les autres professionnels
---------------------------------------

De nombreux professionnels non enseignants travaillent dans les établissements (agents administratifs, de direction, de santé, etc.). Toutes ces personnes réalisent un travail utile, dont l'influence sur la réussite scolaire des élèves est indiscutable, bien qu'indirect et difficilement évaluable (:cite:`tardif99` chap. 13). Les établissements se sont dotés d'un certain nombre de services qui n'interviennent pas *directement* sur leurs missions éducatives ; tout se passe comme s'il y avait deux cercles : les enseignants, et, en périphérie, les autres professionnels, qui peuvent travailler *avec* eux, mais pas forcément *pour* eux. D'où certaines frictions. Ces professionnels (psychologues, orthophonistes, travailleurs sociaux, etc.) sont à la fois en contact avec les enseignants et les élèves, mais aussi adoptent des modes de travail qui les distinguent des enseignants (travail de bureau, classement administratif des problèmes, dossiers, etc.), ce qui crée parfois des frictions ou des conflits.

Rapports avec les personnes extérieures à l'école (parents)
-----------------------------------------------------------

Les relations parents-enseignants, notamment lors des réunions spécifiques ont déjà été abordées dans un autre `document <parents.html>`_. Comme Tardif et Lessard (:cite:`tardif99`, chap. 14) le signalent, leur place au sein de l'établissement est plus importante qu'auparavant. Il faut noter (*id*.), que les attentes des parents à propos des enseignants sont variables. Ils ont des exigences scolaires : que leur enfant "apprenne", des exigences quant au choix de l'enseignant ils peuvent préférer les enseignants jeunes aux plus âgés ; et des exigences quant à la discipline : que les enseignants fassent preuve de fermeté.

Travailler en équipe, les limites
---------------------------------

Gather Thurler (:cite:`gather00`, p. 94 *et sq.*) montre que, dans certaines situations, il est fructueux de *ne pas* travailler en équipe (voir aussi ce qu'en dit `Perrenoud, 1993 <http://www.unige.ch/fapse/SSE/teachers/perrenoud/php_main/php_1993/1993_16.html>`_). En voici brièvement les raisons :

#. Les enseignants manquent en général de temps, et les réunions sont organisées sur des temps de pause, ou de fin de journée. Et ce temps occupé  n'est pas toujours reconnu comme faisant partie du service réel des enseignants, ce qui n'est pas incitatif.
#. L'équipe, centrée sur la résolution de problèmes de l'établissement, peut perdre de vue qu'elle a aussi besoin d'instances de régulation de son propre fonctionnement, qui sont aussi nécessaires. Sinon, en effet, le conservatisme, l'autoritarisme, l'acharnement peuvent subsister. Mais ces instances prennent également du temps... (retour au point 1).


Ce que l'on peut faire
======================

Le cycle du travail en équipe d'enseignants
-------------------------------------------

Gather Thurler (:cite:`gather00`, p. 91 *et sq.*) présente un "cycle de résolution du problème", permettant à une équipe d'enseignants d'initier un projet en commun.

 "Le cycle commence par quelques *inputs conceptuels*, suivis d'un partage d'expériences qui permettra aux uns et aux autres de mieux se connaître et de mieux faire connaître les pratiques des uns des autres. [...] Les *orientations didactiques* permettront à l'équipe de passer à l'action, de se donner un plan de travail et de déterminer sa démarche de mise en oeuvre. Suivront ensuite une *série d'observations* pour identifier les pratiques et ressources existantes et susceptibles d'être développées [...] Pendant la phase d'*expérimentation*, chacun des membres de l'équipe explore les pistes décidées. Les données ainsi obtenues seront ensuite discutées et analysées en vue d'une nouvelle phase d'expérimentation. A la fin du cycle, l'équipe adoptera - ou écartera - définitivement quelques-unes des approches qui auront été élaborées."

Travailler en équipe, pour quoi faire ?
---------------------------------------

Il existe, dans l'enseignement secondaire, quelques dispositifs favorisant explicitement le travail en équipe des enseignants, les voici (Grandguillot, 2001) :

* Les parcours diversifiés (5\ :sup:`e`), itinéraires de découverte et travaux croisés (4e) au collège. Permettent, à partir de thèmes interdisciplinaires (comme la nature et le corps humain,  les arts et les humanités,  les langues et les civilisations,  la création et les techniques), de mener à bien un projet liant au moins deux disciplines (`site d'information sur les parcours diversifiés <http://parcours-diversifies.scola.ac-paris.fr/>`_).
* Les modules en classe de seconde, permettant de regrouper les élèves    en groupes de besoin.
* Les TPE (Travaux personnels encadrés en 1ere et Terminale (`site   d'information TPE du CNDP <http://www.cndp.fr/lycee/tpe/accueil.htm>`_).
- Les dispositifs d'aide et de soutien, notamment dans le cadre des études dirigées.

Meirieu :cite:`meirieu` propose également quelques "exercices pour le travail en équipe" qui peuvent être transposables à de nombreux niveaux d'enseignement et de matières. Une équipe d'enseignants :

* élabore une séquence d'apprentissage mettant en œuvre différentes situations et différents outils pour atteindre un objectif donné (chacun d'eux mènera cette séquence dans sa classe).
* élabore un programme de travail pour un mois, construit un programme d'évaluation diagnostique permettant de proposer à chaque élève un plan de travail différencié.
* élabore trois "itinéraires" différents pour atteindre le même objectif.
* élabore un test final (sur un objectif précis) comportant certains critères nécessaires pour engager des remédiations.
* prépare un projet de contrat à négocier avec quelques élèves en grande difficulté sur l'ensemble des disciplines.

Quizz
=====

.. eqt:: Equipe-1

	**Question 1. 	Quelle est la forme de collaboration entre les enseignants la plus répandue ?**

	A) :eqt:`I` `L'aide et le soutien`
	B) :eqt:`C` `La préparation de cours`
	C) :eqt:`I` `Le tutorat informel`
	D) :eqt:`I` `Le partage des tâches d'enseignement`

.. eqt:: Equipe-2

	**Question 2. 	Quel dispositif mis en place favorise le travail en équipe des enseignants ?**

	A) :eqt:`I` `Le tutorat informel`
	B) :eqt:`I` `Le soutien scolaire`
	C) :eqt:`C` `Les TPE`
	D) :eqt:`I` `La préparation au baccalauréat`

.. eqt:: Equipe-3

	**Question 3. 	Quelle est la limite du travail en équipe entre enseignants la plus répandue ?**

	A) :eqt:`C` `Le manque de temps`
	B) :eqt:`I` `La singularité des matières`
	C) :eqt:`I` `La perte de l'autonomie`
	D) :eqt:`I` `Le manque de motivation`


Analyse de pratiques
====================

#. Reprendre les différentes catégories de "collaborateurs" (collègues d'une même matière, d'une autre matière, autres professionnels, parents). Décrire pour chacune de ces catégories des exemples de collaboration.
#. Reprendre `les fiches de techniques de groupes <../techgroupe.html>`_, en utiliser une pour animer une session de travail en équipe.
#. Pour analyser votre situation au sein de l'équipe des enseignants de votre établissement, détaillez et expliquez (d’après Wenger :cite:`wenger98`)

*  Comment avez-vous été impliqué-e dans ce projet ? (au début, en cours..., quelle sollicitation ?, pourquoi avez-vous accepté ?)
* Comment les ressources ou la conception de ressources (cours, documents, etc.) ont-elles été partagées entre les enseignants ? Quel rôle précis avez-vous joué à ce sujet ?
* Quel(s) rôle(s) précis avez-vous joué(s) dans ce projet ? Comment les avez-vous négociés avec vos collègues ? Vous sentez-vous plutôt à la place de la personne qui imagine de nouvelles choses, ou celle qui s’adapte à ce qui est mis en place ? Pourquoi ?
* comment vous sentez-vous impliqué-e dans ce projet (au centre, en périphérie, quel type de négociations menez-vous avec vos collègues ?) ?
* Comment les tâches d’enseignement devant les élèves se répartissent-elles ?
* Que pensez-vous retirer de ce projet pour votre expérience d’enseignement future ? Notamment, mais pas seulement, pour la mise en place de futurs projets ? Pour collaborer avec de futurs enseignants débutants ?

Webographie
===========

* `Parcours diversifiés <http://parcours-diversifies.scola.ac-paris.fr/>`_, site d'information de l'Académie de Paris.
* `Loi d'orientation sur l'éducation du 10 juillet 1989 <http://www.ac-amiens.fr/iaoise/textes_officiels/role_directeur/loi_orientation.htm>`_


Références
==========

.. bibliography::
    :cited:
    :style: apa
