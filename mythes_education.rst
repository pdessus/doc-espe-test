.. _mythes_education:

**********************************************
Quelques mythes dans la recherche en éducation
**********************************************

.. Pratique de 10 000 h: {Macnamara, 2019 #21815}
.. https://www.amazon.fr/Research-Guide-Education-Myths-Evidence-Informed/dp/1912906392/ref=pd_sim_14_2/259-9017944-8867507?_encoding=UTF8&pd_rd_i=1912906392&pd_rd_r=a7d0d08e-bf63-4fed-9ed2-9715fe6fb516&pd_rd_w=ooBNE&pd_rd_wg=MADq5&pf_rd_p=13666dbd-eacc-4961-ae51-8b11476bba29&pf_rd_r=P2HDMAKFX7CE9VZ8983Z&psc=1&refRID=P2HDMAKFX7CE9VZ8983Z (notamment le chap. historique sur les mythes)

.. Les raisons des neuromythes https://olc-wordpress-assets.s3.amazonaws.com/uploads/2019/10/Neuromyths-Betts-et-al.-September-2019.pdf (p. 14)

.. index::
    single: auteurs; Dessus, Philippe
    single: auteurs; Besse, Émilie
    single: auteurs; Charroud, Christophe


.. admonition:: Information

   * **Auteurs** : `Philippe Dessus <http://pdessus.fr/>`_, LaRAC & Inspé, Univ. Grenoble Alpes & Christophe Charroud, Inspé, univ. Grenoble Alpes. Le quizz a été réalisé par Emilie Besse, projet `ReflexPro <http://www.idefi-reflexpro.fr>`_.

   * **Date de création** : Février 2017.

   * **Date de publication** : |today|.

   * **Statut du document** : Terminé.

   * **Résumé** : Ce document présente et discute quelques mythes présents dans la littérature de recherche en éducation.

   * **Note** : Ce document a fait l'objet d'une recension par C. Beyer (2020, 20 fév.). `Éducation, ces fausses théories qui ont la vie dure <https://www.lefigaro.fr/actualite-france/education-ces-fausses-theories-qui-ont-la-vie-dure-20200220>`_. *Le Figaro*.

   * **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.

   * **Licence** : Ce document est placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.


Introduction
============

Ce document s'intéresse à certains mythes, c'est-à-dire, des propositions très répandues expliquant des phénomènes liés à l'éducation (sur l'enseignement, l'apprentissage), mais qui ne reposent pas (ou plus, ou pas encore) sur des résultats de la recherche suffisamment valides.

Des mythes peuvent rester très diffusés malgré des preuves certaines de leur fausseté. Très certainement parce qu'ils expliquent des éléments auxquels il est important de croire, et qu'on peut donc continuer à diffuser sous couvert de recherche. Pour autant, il nous paraît important d'essayer de les débusquer : pas pour avoir une position surplombante, du type "moi, je ne tombe pas dans ces travers, je suis quelqu'un d'avisé", mais vraiment pour garder un esprit critique sur les théories et faits véhiculés dans la recherche (dans notre cas, en éducation). Certains vont même jusqu'à dire que ces mythes sont une menace pour les systèmes éducatifs.

Ce qui fait que ces mythes subsistent souvent, même des années après avoir été débusqués, est qu'ils contiennent souvent des éléments de vérité. Le reste de ce document présente certains des mythes les plus couramment rencontrés dans le domaine de l'éducation. Nous renvoyons le lecteur intéressé par ce sujet aux nombreux ouvrages expliquant et débusquant des mythes en éducation (voir par exemple :cite:`berliner14,baillargeon13,bruyckere15,pasquinelli15,holmes16`). Dans la suite de ce document, nous présentons cinq mythes à propos d'éducation :

* *Les styles d'apprentissage*, ou la manière dont on prête attention au monde qui nous entoure.
* *Les intelligences multiples*, ou les types de développement de l'intelligence humaine.
* *Le cône de l'expérience de Dale*, ou la hiérarchie d'expériences auxquelles on est confronté et leur effet sur la mémoire ou l'apprentissage.
* *La Zone proximale de développement de Vygotski*, ou les effets de l'aide de plus experts sur le développement intellectuel et la réalisation d'une tâche d'apprentissage.
* *Les digital natives*, ou les natifs numériques, grands utilisateurs des objets numériques.

Nous en viendrons ensuite à des mythes de moins grande portée.


Les styles d'apprentissage
==========================

Nous différons tous les uns des autres. Nous aimons donc certaines couleurs, aliments, certaines musiques, certains livres, certains contenus à apprendre, et pas d'autres :cite:`kirschner17`. De cette évidence, certains ont pu inférer que, *pour apprendre*, d'une part, on préférerait apprendre par des voies sensorielles qui nous seraient privilégiées (audition, vision, kinesthésie) et que, d'autre part, les enseignants pourraient nous faire découvrir lesquelles, et se servir de ces préférences pour nous faire mieux apprendre.

De ces prémisses, la notion de "style d'apprentissage" a été mise au jour, et a fait l'objet, dans le cours des années 1960-90, de beaucoup de travaux. Des auteurs comme La Garanderie :cite:`garanderie80` ont étudié les "profils pédagogiques" des élèves, qui seraient des manières spécifiques de prêter attention aux objets perçus du monde qui nous entoure. Deux types d'évocation seraient possibles, visuelles ou auditives. Selon La Garanderie, l'échec scolaire viendrait du fait que l'enseignant inciterait les "élèves visuels" à évoquer des objets auditifs et *vice versa*.

C'est une théorie très diffusée dans le monde de l'éducation (une enquête :cite:`dekker12` signale que 93 % des enseignants du Royaume-Uni interrogés et 96 % de ceux des Pays-Bas, du premier et second degré, la soutiennent en 2012 ; pour 91 % des enseignants d'Espagne, :cite:`ferrero16`). La Section :ref:`mythes_test` ci-dessous reprend les éléments de cette enquête.

Des chercheurs comme Lieury :cite:`lieury96,vergnaud94` en ont détaillé les problèmes. D'une part, parler de style (ou de profil) d'apprentissage fige les démarches d'apprentissage, les réifie, en faisant comme si elles n'étaient pas changeables -- on peut préférer écouter du rock, mais aimer jouer de la musique classique. D'autre part, il procède d'une confusion entre perception et rappel : les mémoires sensorielles (donc, auditives, visuelles, etc.) existent bien, mais sont de durée très limitées (quelques secondes), ce qui fait que les informations qu'elles récupèrent, pour être pérennes, doivent être traitées sémantiquement, et de manière amodale. Enfin, rien ne dit que suivre les préférences d'un élève donné (si tant est qu'on en ait et qu'elles soient reliées positivement à sa performance) doit être la bonne manière d'enseigner. On pourrait tout aussi bien dire qu'il est nécessaire que les prétendus "élèves visuels" soient confrontés à des "informations auditives", justement pour travailler des informations auxquelles ils seraient moins réceptifs. Kirschner :cite:`kirschner17` décrit bien les problèmes liés à cette question.

* on peut classer les élèves en des catégories extrêmement fines et détaillées (certains tests discriminent jusqu'à une cinquantaine de sous-catégories), mais ne serait-ce pas une précision qui engendre de la confusion ? ;
* de plus, la fidélité test-retest des tests de styles d'apprentissage n'est pas élevée. En d'autre termes, faire passer le même test à des élèves à deux moments différents ne donne pas les mêmes résultats. L'une des raisons, toujours selon Kirschner, est que ces tests se fondent principalement sur l'opinion des apprenants eux-mêmes sur leurs préférences. Rien ne dit qu'ils soient vraiment capables de les analyser correctement ;
* enfin, des études expérimentales ont mis à l'épreuve cette théorie en classant les élèves par "style", leur ont présenté du matériel "compatible" *vs*. "pas compatible" à leur style (en les distribuant aléatoirement dans l'un ou l'autre des présentations), et ont ensuite mesuré si les élèves ayant été confrontés à du matériel compatible à leur style apprenaient mieux que les autres. Les résultats ne concluent pas à une différence significative au bénéfice du respect des styles :cite:`pashler08`.
* de plus, une étude de Chabris et ses collègues (2006) ne montre aucun lien entre les préférences cognitives (spatiales ou reliées aux objets) et les performances réelles des participants.

On le voit, ce classement par styles, d'une part ne paraît fondé sur aucune théorie psychologique valide (Lieury la fait remonter à Charcot, mi-XIX\ :sup:`e` s., rapidement invalidé) et, d'autre part, a le risque d'enfermer les élèves dans un format d'informations donné. Ce n'est pas très utile, pour un enseignant, de perdre du temps à identifier le "style" de chacun de ses élèves puisque *tous* peuvent bénéficier d'un apprentissage où l'information est représentée sous différentes modalités. Comme l'indique Kirschner :cite:`kirschner17` en citant l'entrée de blog de Wheeler (voir :ref:`mythes_web`), la seule raison pour laquelle les enseignants s'intéressent à la question des styles d'apprentissage est qu'il est pratique de tester les styles des élèves et que cela n'amène pas les enseignants à travailler plus dur avec eux. Toutefois, l'effet de bord de cette théorie, qui consiste à varier les supports d'apprentissage, reste une idée intéressante à suivre.


Les intelligences multiples
===========================

La théorie des intelligences multiples (IM) a été élaborée il y a plus de trente ans ; elle est bien installée dans le monde éducatif. De nombreux établissements s'en réclament dans le monde (notamment au Canada et en Grande-Bretagne). Son auteur, Howard Gardner, qui a travaillé avec Jerome Bruner et Nelson Goodman, pense que l'intelligence n'est pas unique, mais peut se développer selon plusieurs formes, ou types (logico-mathématique, verbo-linguistique, spatiale, intra- et inter-personnelle, musicale, corporelle-kinesthésique, naturaliste-écologique), qui mettent en avant des capacités particulières dans chaque domaine. La liste de ces types a été amendée plusieurs fois, sans que les ajouts paraissent toujours pertinents (comme l' "intelligence laser" qui permet de générer les progrès et les catastrophes de la société en explorant à fond un sujet sans aller voir ses relations avec d'autres, et l'"intelligence torche", qui travaille moins en profondeur mais repère les rapports avec les autres, voir :cite:`gardner09`).

Par exemple, une personne avec un type d'intelligence musicale aura, non seulement, des facilités dans la perception et la production de la musique, mais y développera une véritable intelligence : "Une capacité biopsychologique de traiter de l'information qui peut être activée en contexte culturel pour résoudre des problèmes ou produire de biens qui ont de la valeur dans une culture" (:cite:`baillargeon13`, p. 85).

Cette théorie, d'un premier abord, paraît à la fois plausible (on a tous des capacités de niveau varié dans chacun des domaines ci-dessus), et aussi rassurante (comme l'exprime Baillargeon :cite:`baillargeon13`, si on n'est pas tous compétents dans les domaines mathématiques ou littéraires, plutôt valorisés dans le système scolaire, on peut en développer d'autres). Pour autant, ce n'est pas parce qu'on peut réaliser des performances dans un domaine particulier qu'on peut nécessairement inférer qu'il existe des compétences *spécifiques et isolées* pour les réaliser. Les principaux problèmes de la théorie des IM sont les suivants (d'après :cite:`baillargeon13`) :

* ils n'ont pas été validés par des questionnaires psychométriquement valides. Il n'est pas possible de diagnostiquer, *via* un questionnaire ou un entretien, le type d'intelligence d'une personne donnée, sans que ce type ne corrèle également avec les autres types. Les différents types d'intelligence ne sont donc pas aussi "étanches" que Gardner le dit.
* en réalité, et cela est une conséquence du premier point, Gardner ne se fie pas sur des questionnaires pour déterminer le type d'intelligence d'une personne, mais sur des critères assez vagues, dont le nombre n'est pas fermement déterminé à l'avance.
* de plus, on retombe dans le problème vu à propos des styles d'apprentissage : quand bien même on arriverait à déterminer le type d'intelligence d'un élève, il est difficile de savoir s'il faut privilégier le type ou au contraire l'ouvrir à d'autres, dans une perspective d'éducation. Sans compter la difficulté de représenter des connaissances d'un format à l'autre : comme l'indique Baillargeon, faudra-t-il "à celle qui possède une intelligence musicale [lui faire] chanter les règles de la ponctuation" ? (:cite:`baillargeon13`, p. 96).

Pour résumer cette question des styles d'apprentissage, on s'aperçoit qu'en pratique, les enseignants ne classent pas les élèves en 7, 9, ou 20 catégories, mais souvent seulement trois, résumées par le sigle VAK (visuel, auditif, et kinesthésique). Ne pourrait-on pas simplement dire qu'il y aurait autant de types d'intelligence que de types de buts humains (:cite:`white05`), et qu'essayer de compartimenter les élèves en des catégories ne les aide pas.

Le cône de Dale
===============

La proposition nommée "le cône de Dale", qui paraît scientifique au premier abord, a le pattern suivant (les valeurs peuvent varier d'une formulation à l'autre, avec des valeurs croissantes) :

**On se souvient de A % de ce qu'on entend, B % de ce qu'on lit, C % de qu'on écrit, D % de ce qu'on fait, etc. (d'autres verbes impliquant plus ou moins d'attention ou de charge cognitive peuvent remplacer ceux présents ici)**

`Edgar Dale <https://en.wikipedia.org/wiki/Edgar_Dale>`_ (1900-1985) est un chercheur en éducation américain intéressé par les principes d'enseignement audiovisuels. Il publie, en 1946, le célèbre graphique en forme de cône (intitulée : "Une aide visuelle pour expliquer les inter-relations entre les différents types de matériels audio-visuels, ainsi que leurs rangs individuels dans le processus d'apprentissage" :cite:`subramony03`, p. 25), qui relie la performance de mémorisation humaine à la manière dont cette dernière arrive par la perception (activités plus ou moins concrètes), mais sans indiquer de valeurs d'efficacité, ni citer de recherches, plutôt de manière intuitive. De très nombreuses versions de ce cône sont ensuite publiées avec des valeurs et des activités variables. Initialement, donc, le base du cône mentionne des expériences "de première main" et le haut des expériences plus abstraites (symboliques). Dans l'esprit de son concepteur, le cône mentionne des degrés différents d'abstraction, mais pas de difficultés de mémorisation ou d'apprentissage, ce que ce cône en est venu à représenter, selon les personnes qui l'ont repris.

Il existe de très nombreuses versions de ce cône de l'expérience (ou de l'apprentissage) (voir la :ref:`mythes_web` pour un répertoire de versions, voir aussi :cite:`bruyckere15`, chap. 2), et il a été (est encore) très enseigné dans les cours d'éducation aux médias. Pourtant, il semble qu'aucune étude scientifique sérieuse ne puisse être citée pour corroborer une telle hiérarchie. La seule leçon intéressante que l'on peut retirer du travail de Dale est celle-ci : varier "[…] les types d'expériences sensorielles que l'on peut proposer en classe" (:cite:`subramony03`, p. 30), qui a été, elle, prouvée.

Toutefois, diverses théories, comme celle du multimédia – qui montre que l'on apprend mieux lorsqu'on est confronté à du texte et de l'image plutôt qu'à un texte seul (voir doc. :ref:`charge_cognitive`) –, ou celles de la cognition incarnée, montrant que réaliser (ou être témoin)  d'actions nous permet de mieux les rappeler :cite:`hainselin17`, ont mis en lumière le rôle du médium d'apprentissage, mais de manière bien plus subtile que le cône de Dale. Quand on s'intéresse à vérifier les effets réels des différentes expériences du cône :cite:`lalley07`, on tire les conclusions suivantes : chacune d'entre elles peut être plus efficace que les autres, en fonction du contexte, d'autre part, les effets de la lecture et de l'instruction directe, fondamentaux, sont largement minimisés.

La Zone proximale de développement
==================================

Le concept de Zone proximale de développement (ZPD), parfois aussi nommée "zone de proche développement", mis au jour par le psychologue russe `Lev Vygotski <https://fr.wikipedia.org/wiki/Lev_Vygotski>`_ (1896-1934) :cite:`vygotski85`, est un mythe un peu à part des autres. Nous l'avons intégré à ce document, non pas pour le concept original, mais pour les interprétations qui ont pu en être faites. À ce titre, il est similaire au mythe du cône de Dale.

Rappelons ce que la ZPD signifie : la "disparité entre l'âge mental, ou niveau de développement présent, qui est déterminé à l'aide des problèmes résolus de manière autonome, et le niveau qu'atteint l'enfant lorsqu'il résout des problèmes non plus tout seul mais en collaboration détermine précisément la zone de proche développement" (:cite:`vygotski85`, p. 270).

Or, comme le signale Didau (voir :ref:`mythes_web`), cette définition, initialement en lien avec le développement intellectuel, est devenue liée à *toute* activité d'apprentissage, un peu comme un "effet Boucle d'or" (du célèbre conte *Boucle d'or et les trois ours*) qu'il formule ainsi : un travail doit être juste à point, ni trop facile (l'élève n'apprendra rien), ni trop difficile (l'élève va le trouver inaccessible et se frustrer) ; de plus, l'élève pourra mieux réussir s'il se fait aider d'un expert. Cela décrit une sorte de vision parfaite de l'enseignement, dans laquelle l'enseignant est capable de proposer à chaque élève une activité parfaitement adaptée à son niveau, qu'il réalise sans effort. Intuitivement, cela tombe sous le sens, mais cela n'est pas ce que Vygotski a formulé avec sa ZPD.

Didau montre, en reprenant Chaiklin :cite:`chaiklin03`, que trois présupposés sont souvent pris à tort (et par de nombreux chercheurs) à propos de la ZPD. Les présupposés :

* *de généralité*. La ZPD serait censée s'appliquer à toutes les situations d'apprentissage, mais plutôt au développement cognitif de l'enfant, au sens large. Ainsi, comme Piaget, Vygotski pensait que l'enfant progressait de niveau en niveau (stade, chez Piaget), chaque progression lui permettant d'utiliser des outils cognitifs plus sophistiqués, et donc de résoudre des problèmes plus complexes. Au mieux, comme le dit Chaiklin, le nombre ou le type de problèmes résolus peuvent donner des indices à propos du niveau de développement.

* *d'assistance*. L'apprentissage dépendrait des interventions de personnes plus compétentes. Or, l'aide d'un plus expert n'est pas un pré-requis pour que l'apprentissage se fasse, ni ne doive empêcher de se questionner *pourquoi* l'aide d'un plus expert (ou d'un pair) peut favoriser l'apprentissage. Ce n'est pas la compétence du plus expert qui est importante en soi, c'est la manière dont l'expert peut aider l'enfant à progresser et se développer.

* *de potentialité de l'apprenant*. La ZPD serait censée améliorer grandement l'apprentissage de l'enfant, s'il est prêt à cela et si l'enseignant identifie correctement l'activité à donner. Il y aurait donc une zone propice dans laquelle l'enfant s'engouffrerait et apprendrait correctement. Parfois, aussi, il est dit que l'apprentissage de l'enfant dans la zone se fait sans effort.

Pour résumer, la ZPD considère le développement de l'enfant *dans sa globalité*, et non les seules tâches d'apprentissage, comme le mythe à son propos l'a trop souvent transformée.

Les *Digital Natives*
=====================

Depuis quelques années, rares sont les enfants qui ne sont pas en contact avec le numérique dès le plus jeune âge. C’est probablement en observant ce fait qu’en 2001 Marc Prensky :cite:`prensky01` a popularisé l’idée que le monde était fait de *digital natives* et de *digital immigrants*.

Pour Prensky, les *digital natives* auraient des caractéristiques communes qui les différencieraient des générations précédentes dans leurs utilisations pratiques des objets et services numériques, mais aussi dans leur façon d’apprendre en avançant, par exemple, le fait que ces individus seraient capables de faire plusieurs choses à la fois (écouter leur MP3 tout en faisant recherche sur l’internet et conversant sur un réseau social ), ou encore qu’ils seraient plus habiles pour les travaux collaboratifs. Une partie de ses arguments tendraient à montrer que les systèmes éducatifs doivent évoluer car ils ne sont plus adaptés à cette nouvelle génération d’apprenants. Les idées de Prensky se sont propagées et continuent de l'être grâce aux médias qui s’appuient régulièrement sur ces idées.

Cependant, de nombreux auteurs se sont prononcés contre ces idées. En effet, apprendre à l’école repose sur des taches spécifiques, qui ne sont pas ou peu influencés par la maîtrise des objets et services numériques (:cite:`bennett08`), d’autres arguments tendent à confirmer cette non-influence.

On peut par exemple citer cette expérimentation destinée à mesurer le bénéfice de l’usage d’un lecteur MP3 pour l’apprentissage d’une langue vivante. Elle a montré que malgré un usage très familier de cette technologie hors cadre scolaire, les élèves en difficultés n'en tirent qu’un bénéfice très limité. D’un point vu cognitif, l’usage du lecteur MP3 nécessite de prendre des décisions (s’arrêter, reprendre, réécouter un passage) qui se révèlent très coûteuses pour ces élèves :cite:`roussel08`.

De même, lorsque l’on demande à des apprenants de réaliser des tâches collectives à distance en leur proposant un panel d’outils numériques destinés à les aider à organiser, planifier et réguler les tâches collectives et individuelles, on constate qu’ils ont tendance à surestimer leur disponibilité pour le projet et à sous-estimer le travail d’autrui. Les *digital natives* sont comme les apprenants d’avant, ils font leur travail au dernier moment (:cite:`romero10`).

On peut constater que la maîtrise des objets et services numériques est utile pour la vie courante, et que cette maîtrise peut apparaître comme une « évolution mentale » mais il ne faut pas oublier que le système cognitif des *digital natives*, et par extension le système cognitif de tous les humains contemporains est vraisemblablement le même, il ne nous permet pas de faire plusieurs choses à la fois comme regarder une vidéo en ligne et comprendre en même temps un énoncé d’exercice :cite:`lieury12`.

Parfois même, les compétences et habiletés d’usages du numérique sont contre-productives en contexte scolaire, lorsque l’on doit faire comprendre aux apprenants que ce qu’on leur demande à l’école avec des objets numériques est différent de leurs pratiques personnelles, et que cela nécessite d’autres compétences (:cite:`ng13`).

Pour résumer, les *digital natives* ont développé des compétences et habiletés très utiles pour la pratique domestique et probablement pour leur vie sociale, et leur future vie professionnelle, mais des études scientifiquement étayées montrent que ces compétences et habiletés ont un impact très limité et parfois même négatif sur les performances d’apprentissages. Cependant il ne faut pas ignorer ces compétences et essayer de les prendre en compte dans la conception de situations d’enseignement, soit comme un point de départ pour aller plus loin, soit comme obstacle à dépasser (:cite:`amadieu14`).

Des mythes plus locaux
======================

Les mythes précédemment décrits sont des objets de taille et de grande portée ; il existe, dans la littérature en éducation des mythes plus "locaux", c'est-à-dire concernant des aspects plus méthodologiques, liés à des savoir-faire pédagogiques. Nous en présentons rapidement quelques-uns :

- *Les rétroactions-sandwiches* (*feedback-sandwich*) : il est souvent recommandé (notamment dans les formations au management, mais parfois aussi en éducation) de formuler des rétroactions-sandwiches, c'est-à-dire un retour qui "enrobe" un élément critique ou négatif entre deux éléments positifs, qui rendrait le récepteur de la rétroaction plus confiant, motivé, et engagé à poursuivre son travail. Mais, bien sûr, si ces éléments positifs ne sont pas guidants, ne donnent pas des pistes pour s'améliorer, il y a peu de chances que les personnes le recevant puissent s'en servir pour s'améliorer. En d'autres termes, il est maintenant connu que complimenter "dans le vide", sans éléments précis, ne sont pas des rétroactions efficaces. Une étude :cite:`parkes13` montre que, si de telles rétroactions sont perçues comme efficaces par les des étudiants (dans un cours d'entraînement à l'écriture), elles n'ont en revanche aucun résultat sur la performance réelle des étudiants.

- *Un cours ne devrait durer que 10-15 minutes* : Il est souvent dit, et le succès des conférences `TED <https://www.ted.com/>`_ n'y sont pas pour rien, que les élèves ne peuvent diriger leur attention pendant la durée d'un cours standard (soit 50 min), et qu'on ne devrait proposer que des activités durant environ 10-15 minutes. Bradbury :cite:`bradbury16` montre, d'une part,  qu'aucune recherche sérieuse n'a pu mettre ce point en évidence et que les résultats les plus convaincants montrent en réalité un déclin régulier de l'attention tout au long du cours (et ce quelle que soit la méthode pédagogique utilisée), et que l'attention des élèves depend plus de l'enseignant, de la motivation des élèves, que du contenu présenté ou de la méthode pédagogique en eux-mêmes.


Les mythes, un objet à étudier
==============================

Ce que disent les mythes
------------------------

Les mythes étudiés ici ont été d'abord un objet de recherche, puis ont été invalidés (entièrement pour certains, partiellement pour d'autres). Malgré cette invalidation, ils ont continué à être utilisés, que ce soit dans ou hors le monde académique, parce qu'ils paraissent plausibles. Les mythes nous racontent des choses particulières auxquelles on a parfois besoin de croire,tellement, d'ailleurs, que parfois des concepts sont "tordus" pour tendre vers ces croyances (voir la ZPD, le cône de Dale) :

* que nous sommes tous différents ;
* que nos capacités mentales peuvent être augmentées pour peu qu'on trouve l'outil (physique, humain, ou méthodologique) approprié ;
* que chacun peut arriver à réussir s'il fait les efforts, met en œuvre les stratégies appropriées.

Les mythes falsifiés
--------------------

Les objets présentés dans ce document on pu être qualifiés de mythes parce qu'on a pu les formuler de manière suffisamment précise pour pouvoir les falsifier (prouver leur fausseté, selon Popper). À leur lecture, nous pouvons constater que cela n'est pas toujours le cas de tous. Par exemple, le cône de Dale, initialement, ne prédit aucune performance que l'on puisse tester empiriquement (il indiquait seulement que des expériences sont plus abstraites que d'autres) ; les versions suivantes du cône sont, elles, devenues testables empiriquement, sans qu'aucune validation ne puisse être faite.

Comme indiqué en introduction, il est possible que certains des mythes exposés ci-dessus n'en soient en réalité pas, si des recherches valides le montrent. En science, on ne démontre pas la vérité d'un phénomène, mais on écarte sa possible fausseté.

Les mythes et l'imprécision
---------------------------

C'est une caractéristique reliée à la précédente : un mythe peut d'autant plus se perpétuer et s'enrichir qu'il contient des propositions vagues : les styles d'apprentissages sont à la fois vagues et très nombreux ; le cône de Dale est censé tout aussi bien être celui de la mémorisation ou de l'apprentissage ; la ZPD est à propos du développement ou de l'apprentissage. De plus, le mythe contient des éléments qui peuvent être partiellement vrais, au moment de son élaboration.

Les mythes et l'école
---------------------

Une autre caractéristique commune à beaucoup de mythes est qu'ils ne permettent pas souvent de dériver, à partir de leur formulation, des prescriptions utiles pour l'enseignant. Les élèves sont différents, c'est sûr, mais faut-il tenir compte de ces différences au risque de les accentuer, de rendre les élèves hyperspécialisés, ou au contraire faut-il essayer de proposer des activités communes dans lesquelles le plus grand nombre pourrait se retrouver.


Conclusion
==========

Quand on est enseignant, il est important de connaître ces mythes, pour plusieurs raisons. D'une part, cela évite de perdre du temps (et parfois même de l'argent) à des idées dont l'efficacité n'a pas été démontrée. Ce temps gagné sera mieux utilisé à des démarches pédagogiques qui auront fait leurs preuves. D'autre part, analyser ces mythes permet de mieux comprendre pourquoi ils ne sont pas valides, en acquérant ainsi des connaissances scientifiques plus fiables. L'esprit critique exercé, l'enseignant pourra peut-être même utiliser ce qu'il a appris pour mieux former ses élèves.

.. _mythes_test:

Test
====

Etes-vous sensible aux (neuro-)mythes ? Le petit test ci-dessous, issu de :cite:`dekker12` vous permettra de le savoir.

* On apprend mieux lorsqu'on reçoit l'information dans son style d'apprentissage préféré (e.g., auditif, visuel, kinesthésique).
* Les différences entre apprenants s'expliquent par des différences hémisphériques (cerveau gauche/droit).
* De courts exercices de coordination peuvent améliorer les fonctions des hémisphères droit/gauche du cerveau.
* Des exercices de coordination perceptivo-motrice peuvent améliorer les habiletés en lecture.
* Des environnements riches en stimuli améliorent les cerveaux des jeunes enfants.
* Les enfants sont moins attentifs après avoir consommé des sodas et/ou des casse-croûte.
* Il a été scientifiquement prouvé que des acides gras polyinsaturés (oméga 3 et oméga 6) ont 69 % d'effets positifs sur la réussite scolaire.
* Il y a des périodes critiques dans l'enfance après lesquelles certaines choses ne peuvent plus être apprises.
* Nous utilisons seulement 10 % de notre cerveau.
* Boire régulièrement des boissons caféinées réduit la vigilance.
* Les enfants doivent acquérir leur langue maternelle avant d'apprendre une deuxième langue...
* ... Sinon ils n'apprendront aucune langue parfaitement.
* Les difficultés d'apprentissage associées à des différences développementales dans le fonctionnement du cerveau ne peuvent être remédiées par l'éducation.
* Si les élèves ne boivent pas suffisamment d'eau (6-8 verres par jour), leur cerveau se rapetisse.
* L'entraînement répété de certains processus mentaux peut changer la forme et la structure de parties du cerveau.
* Les élèves ont des préférences envers certaines manières de recevoir l'information (*e.g.*, visuelle, auditive, kinesthésique).

**Note** : toutes les propositions ci-dessus sont erronées.

Quizz
=====

.. eqt:: Mythes_education-1

	**Question 1. 	Le mythe sur les "styles d'apprentissage" porte sur**

	A) :eqt:`I` `le développement intellectuel et la réalisation d’une tâche d’apprentissage`
	B) :eqt:`I` `la hiérarchie d’expériences auxquelles on est confronté et leurs effets sur la mémoire ou l’apprentissage`
	C) :eqt:`I` `les types de développement de l’intelligence humaine`
	D) :eqt:`C` `la manière dont on prête attention au monde qui nous entoure`

.. eqt:: Mythes_education-2

	**Question 2. 	Le mythe sur la "zone proximale de développement de Vygotski" porte sur**

	A) :eqt:`C` `le développement intellectuel et la réalisation d’une tâche d’apprentissage`
	B) :eqt:`I` `les types de développement de l’intelligence humaine`
	C) :eqt:`I` `la manière dont on prête attention au monde qui nous entoure`
	D) :eqt:`I` `la hiérarchie d’expériences auxquelles on est confronté et leur effet sur la mémoire ou l’apprentissage`

.. eqt:: Mythes_education-3

	**Question 3. 	Qu'est-ce qui fait que souvent les mythes persistent même lorsqu'ils ont été débusqués ?**

	A) :eqt:`I` `Ils ont été diffusés sous couvert de recherche`
	B) :eqt:`C` `Ils contiennent souvent des éléments de vérité`
	C) :eqt:`I` `Le public n'est pas convaincu`
	D) :eqt:`I` `Ils ne sont pas assez pointés du doigt`


Analyse de pratiques
====================

Après avoir parcouru ce document, choisir l'un des mythes présentés et répondre aux questions ci-dessous. L'ensemble des réponses pourra faire l'objet d'un exposé (écrit/oral).

* décrire l'argument principal du mythe,
* expliquer quel(s) processus psychologique(s) -- supposé(s) -- est (sont) sous-tendu(s), le(s) comparer avec le(s) réel(s),
* expliquer, si possible sous la forme d'un schéma, pourquoi ce qui fait le mythe, en mettant en évidence les éléments erronés du mythe, comme si l'explication était adressée à un collègue,
* chercher sur internet des recherches en lien avec le mythe, utiliser pour cela la référence de Cook & Lewandowsky (voir :ref:`mythes_web`),
* réfléchir au design d'une expérimentation, la mieux contrôlée possible, qui pourrait valider les hypothèses du mythe,
* trouver un autre mythe en lien avec l'éducation *via* une recherche sur internet (mots-clés : <mythe éducation>) et le décrire brièvement.
* le site `Teaching & learning toolkit <https://educationendowmentfoundation.org.uk/resources/teaching-learning-toolkit/>`_ peut aider à déterminer les pratiques les plus efficaces.

.. _mythes_web:

Webographie
===========

Débusquer les mythes
--------------------

* Cook, J., Lewandowsky, S. (2011), `Précis de réfutation <https://skepticalscience.com/docs/Debunking_Handbook_French.pdf>`_. St. Lucia, Australia: University of Queensland.
* `Cortecs, Univ. Grenoble Alpes - Pédagogie et didactique <https://cortecs.org/thematix/pedagogie-didactique/>`_.
* Goudeseune, D. (2018). `L'erreur fondamentale d'enseigner avec les intelligences multiples, les styles ou profils d'apprentissage des élèves <https://par-temps-clair.blogspot.com/2018/04/pourquoi-la-theorie-sur-les-styles.html>`_. Blog Par temps clair.
* OCDE (2018). `World Class: Debunking some myths <https://www.oecd-ilibrary.org/docserver/9789264300002-2-en.pdf>`_. Paris : OCDE.
* `The debunker club <http://www.debunker.club>`_.
* `The learning scientists <http://www.learningscientists.org>`_.
* `Teaching & learning toolkit <https://educationendowmentfoundation.org.uk/resources/teaching-learning-toolkit/>`_.
* Vellut, D. (2018). `Les neuromythes sont parmi nous <https://www.davidvellut.com/les-neuromythes-sont-parmi-nous/>`_, billet de blog.

Les styles d'apprentissage
--------------------------

* Baillargeon, N. (2010). `Les styles d'apprentissage en éducation <https://www.ababord.org/Les-styles-d-apprentissage-en>`_. *Revue à babord, 33*.
* Chabris, C. *et al*. (2006). `Spatial and object visualization cognitive styles: Validation studies in 3800 individuals <http://chabris.com/Chabris2006d.pdf>`_. Rapport technique.
* Le Diagon, S. (2021). `Neuromythe n° 1 : Les styles d'apprentissage <https://www.cortex-mag.net/neuromythe-n1-les-styles-dapprentissage/>`_. Blog Cortex-Mag.


Les intelligences multiples
---------------------------

* Juhel, J. (2014). `La théorie des intelligences multiples : entre science et rhétorique <https://perso.univ-rennes2.fr/system/files/users/juhel_j/Présentation%20TIM%20Juhel%20avril%202014.pdf>`_. Conférence à Carhaix. Rennes : Univ. Rennes-2.
* Vargas-Gonzales, E. & Gautier-Martins, M. (2021). `Neuromythe n° 2 : les intelligences multiples <https://www.cortex-mag.net/neuromythe-n2-les-intelligences-multiples/>`_. Blog Cortex-Mag.

Le cône de Dale
---------------

* Thalheimer, W. (2006). `People remember 10%, 20%... oh really? <http://www.willatworklearning.com/2006/05/people_remember.html>`_. Billet de blog.
* Wheeler (2011). `A convenient untruth <http://www.steve-wheeler.co.uk/2011/11/convenient-untruth.html>`_ (blog).

La Zone proximale de développement
----------------------------------

* Didau (2017). `Problems with the 'ZPD' <http://www.learningspy.co.uk/featured/problems-zone-proximal-development/>`_.
* Revue Skholè (2009). `Extrait sur la zone prochaine de développement <http://skhole.fr/lev-vygotski-extrait-la-zone-prochaine-de-développement>`_.

Références
==========

.. bibliography::
    :cited:
    :style: apa
