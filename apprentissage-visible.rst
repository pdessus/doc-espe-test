.. _appr_visible:

*************************************************************
L'apprentissage visible : qu'est-ce qui marche en éducation ?
*************************************************************

.. {Gorard, 2017 #22352}

.. index::
    single: auteurs; Dessus, Philippe
    single: auteurs; Besse, Émilie

.. admonition:: Information

	* **Auteurs** : `Philippe Dessus <http://pdessus.fr/>`_, LaRAC & Inspé, Univ. Grenoble Alpes. Le quizz a été réalisé par Emilie Besse, projet `ReflexPro <http://www.idefi-reflexpro.fr>`_.

	* **Date de création** : Avril 2015.

	* **Date de publication** : |today|.

	* **Statut du document** : Terminé.

	* **Résumé** : L'apprentissage visible est une manière d'envisager l'étude des phénomènes éducatifs en se fiant à des preuves empiriques. Ce document explore différentes approches de ce type, des méta-analyses à l'énoncés de principes issus d'études expérimentales.

	* **Voir aussi** : Le Document :ref:`ens_preuves` donne quelques éléments théoriques sur le courant de l'éducation fondée sur les preuves et le Document :ref:`res_ens_preuves` liste des ressources sur ce courant.

	* **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.

	* **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.


Introduction
============

De nombreuses approches éducatives sont fondées sur des principes philosophiques ou pédagogiques qui n'ont pas été testés empiriquement (c'est-à-dire, dont la preuve que ces approches amènent, comparativement à une autre approche concurrente, des résultats significativement meilleurs). Une approche particulière, qui sera détaillée dans ce document, veut fonder les décisions éducatives, "non seulement sur des connaissances théoriques, et/ou le jugement et l'expérience d'enseignants, mais également sur des preuves scientifiques (d'après :cite:`rey06`).

Pour reprendre le commentaire de Hattie :cite:`hattie09` toutes les pratiques pédagogiques semblent marcher. S'il est certain que chaque enseignant (et chaque élève) a un comportement ou une pratique propre, il peut être intéressant de se poser la question de connaître ceux qui marchent le mieux.

Bien évidemment, il ne s'agit pas de dire ici que *seules* des approches empiriquement fondées doivent être menées dans le milieu éducatif : la conviction du bien-fondé qu'une approche éducative peut effectivement suffire, et le constat de la preuve peut être laissée à des chercheurs. Cela a par exemple été le cas de certains aspects de la méthode Montessori (apprentissage multisensoriel), dont les preuves empiriques ont été réalisées bien plus tardivement :cite:`gentaz09` chap. 6.

 Il ne convient non plus de considérer un élément ci-dessous en l'incorporant directement dans une pratique d'enseignement ou d'apprentissage, sans réfléchir aux changements nécessaires pour que l'élément soit cohérent avec ces dernières. Pour reprendre un exemple (:cite:`hattie09` p. 4) : un résultat montre que les rétroactions données par l'enseignant ont un effet important sur l'apprentissage. Il ne suffit sans doute pas qu'un enseignant se mette à délivrer plus de rétroactions à ses élèves ; il faut sans doute aussi qu'il change sa conception de ce que sont les rétroactions et leur objet (ce sur quoi elles portent, quand et à qui elles se destinent, et même leur source) car ce ne sont pas *n'importe quelles* rétroactions qui sont bénéfiques à l'apprentissage.

Le champ de recherche "fondé sur les preuves" (*evidence-based*) est florissant dans les pays anglo-saxons, et est très utilisé dans certaines disciplines, comme la médecine. Il a certains effets :cite:`rey06,bruyckere15` :

* implications dans le domaine du choix des politiques éducatives ;
* améliorer la qualité scientifique de la recherche en éducation ;
* privilégier les méthodologies expérimentales ou fondées sur des méta-analyses ;
* débusquer certains "mythes" (c'est-à-dire contredits par des études contrôlées) parfois tenaces.

Le propos de ce document est moins de peser les avantages et inconvénients de ces recherches que d'expliquer leur possible utilisation à des fins de formation des enseignants, en se centrant sur les études recensant des résultats liés à la psychologie cognitive de l'éducation.

Ce que l'on sait
================

Principes sur l'apprentissage (Graesser)
----------------------------------------

Graesser :cite:`graesser09` est une excellente reformulation très synthétique de certains "principes d'action" à propos d'apprentissage. Il faut noter qu'ils ne sont pas fondés sur des méta-analyses, mais sont une formulation agrégée d'un grand nombre de recherches, dont beaucoup sont issues de la psychologie de l'apprentissage (un certain nombre venant directement de la Théorie de la charge cognitive, voir doc. SAPP :ref:`charge_cognitive`). Il a écrit "25 principes sur l'apprentissage" (*id*, p. 260)

.. topic:: **Les 25 principes sur l'apprentissage de Graesser.**

	#. *Effets de contiguïté*. Les idées qui nécessitent d'être associées entre elles doivent être présentées de manière contiguë dans l'espace et le temps.

	#. *Fondation perceptivo-motrices*. Les concepts bénéficient d'être fondés dans des expériences perceptivo-motrices, particulièrement dans les premières étapes.

	#. *Double codage et effets multimédia*. Les matériels présentés dans des formes verbale, visuelle et multimédia forment des représentations plus riches que lorsqu'ils sont présentés dans des média uniques.

	#. *Effet du test*. Le fait de tester les apprenants augmente l'apprentissage, particulièrement lorsque les tests sont à propos d'un contenu important.

	#. Effet de l'espacement*. La répartition du travail et des tests dans le temps entraîne une meilleure mémorisation qu'une unique session de travail ou de test.

	#. *Attentes liées à l'examen*. Les apprenants bénéficient plus de tests répétés quand ils sont dans l'attente d'un examen final.

	#. *Effet de production*. L'apprentissage est amélioré lorsque les apprenants formulent des réponses (production) plutôt qu'ils les sélectionnent dans une liste (reconnaissance).

	#. *Effet d'organisation*. Transformer l'information en plans, l'intégrer à d'autres informations ou la synthétiser génère un meilleur apprentissage que relire l'information ou employer des stratégies plus passives.

	#. *Effet de cohérence*.  Des matériels présentés sous la forme de multimédia devrait explicitement relier les idées présentées et minimiser les informations distractrices et non pertinentes.

	#. *Histoires et cas exemplaires*. Des histoires ou cas exemplaires sont mieux rappelés que des faits décontextualisés et des principes abstraits.

	#. *Exemples multiples*. La compréhension d'un concept abstrait est améliorée par la présentation d'exemples multiples et variés.

	#. *Effet des rétroactions (feedback)*. Les rétroactions dans une tâche d'apprentissage améliorent ce dernier, mais le délai optimal des rétroactions dépend de la nature de la tâche.

	#. *Effet des suggestions négatives*. Lorsque la rétroaction est immédiate, on a moins tendance à apprendre des informations erronées.

	#. *Difficultés attractives*. Les défis rendent l'apprentissage et le rappel plus ardus, ce qui améliore la mémorisation à long terme.

	#. *Prise en compte de la charge cognitive*. L'information présentée à l'apprenant ne devrait pas surcharger sa mémoire de travail.

	#. *Le principe de segmentation*. Une leçon complexe devrait être divisée en sous-parties plus accessibles.

	#. *Effet de l'explication*. Il est plus bénéfique d'apprendre en construisant des explications cohérentes et profondes du matériel plutôt que de mémoriser des faits isolés et de surface.

	#. *Questions profondes*. Il est plus bénéfique de formuler et de répondre à des questions profondes qui avancent des explications (p. ex., pourquoi, pourquoi pas, si-alors) que des questions de surface (p. ex., qui, quoi, quand, où).

	#. *Déséquilibre cognitif*. Les raisonnements et apprentissages profonds sont stimulés par des problèmes qui créent un déséquilibre cognitif, comme des obstacles à des buts, des contradictions, des conflits, des anomalies.

	#. *Flexibilité cognitive*. Lorsqu'on est confronté à de multiples points de vue qui relient faits, habiletés, procédures, et concepts profonds, notre flexibilité cognitive est entraînée et s'améliore.

	#. *Le principe de Boucle d'Or*. Les tâches ne devraient pas être trop faciles ou trop difficiles, mais à un niveau de difficulté adapté au niveau de connaissances et compétences de l'apprenant.

	#. *La métacognition imparfaite*. Les apprenants ont rarement une connaissance exacte de leur propre cognition. Donc on ne peut avoir une confiance totale en leur capacité à gérer leur compréhension, apprentissage, et mémoire.

	#. *L'apprentissage par la découverte*. La plupart des apprenants ont des difficultés à découvrir d'eux-mêmes des principes importants, sans une aide attentive, un étayage, ou des matériels avec des instructions bien pensées.

	#. *L'apprentissage auto-régulé*. La plupart des apprenants ont besoin qu'on les aide à entraîner leur manière d'auto-réguler leur apprentissage et leurs autres processus cognitifs.

	#. *L'apprentissage ancré*. L'apprentissage est plus profond et les apprenants plus motivés lorsque les matériels et les compétences sont ancrés dans des problèmes du monde réel qui concernent l'apprenant.


Les techniques d'apprentissage (Dunlosky)
-----------------------------------------

Dunlosky et ses collègues :cite:`dunlosky13a,dunlosky13b` ont réalisé une synthèse complète des méthodes d'apprentissage qui "marchent" en se fondant sur des résultats empiriques. Ils les recensent en recensant 10 techniques d'apprentissage et 4 catégories pouvant influer l'apprentissage (voir Tableaux 1 et 2 ci-dessous). Il est à noter qu'ils ne réalisent pas une méta-analyse pour justifier ces techniques, mais citent des résultats de recherche.

*Tableau 1** - Dix techniques influant l'apprentissage (:cite:`dunlosky13` p. 6)

+------------------------------+---------------------------------------------------+
| Technique                    | Description                                       |
+==============================+===================================================+
| 1. Auto-interrogation        | Expliquer pourquoi un fait ou un concept est vrai |
+------------------------------+---------------------------------------------------+
| 2. Auto-explication          | Expliquer comment une information nouvelle est    |
|                              | reliée à une information connue, ou expliquer les |
|                              | étapes d'une résolution de problèmes              |
+------------------------------+---------------------------------------------------+
| 3. Résumé                    | Ecrire des résumés (de tailles variées) de        |
|                              | matériel textuel à apprendre                      |
+------------------------------+---------------------------------------------------+
| 4. Surligner/souligner       | Marquer des portions potentiellement importantes  |
|                              | de matériel textuel à apprendre                   |
+------------------------------+---------------------------------------------------+
| 5. Mémorisation par mots-    | Utiliser des mots-clés ou des images mentales     |
| clés                         | pour associer du matériel textuel verbal          |
+------------------------------+---------------------------------------------------+
| 6. Imagerie textuelle        | Se forger des images mentales du matériel textuel |
|                              | à apprendre pendant sa lecture ou écoute          |
+------------------------------+---------------------------------------------------+
| 7. Relecture                 | Réétudier le matériel textuel à apprendre après   |
|                              | une lecture initiale                              |
+------------------------------+---------------------------------------------------+
| 8. Test                      | S'auto-tester ou s'exercer sur du matériel à      |
|                              | apprendre                                         |
+------------------------------+---------------------------------------------------+
| 9. Pratique distribuée       | Se créer un plan de travail qui distribue les     |
|                              | périodes d'étude dans le temps                    |
+------------------------------+---------------------------------------------------+
| 10. Pratique variable        | Se créer un plan de travail qui mixe différents   |
|                              | types de problèmes, de matériel, dans une session |
|                              | de travail unique                                 |
+------------------------------+---------------------------------------------------+

**Tableau 2** - Les catégories de variables influant l'apprentissage (:cite:`dunlosky13a` p. 6). Ce tableau se lit par colonnes (il n'y a pas de correspondance par ligne).

+-------------+----------------------------+-----------------------------+-----------------+
| Matériels   | Conditions d'apprentissage | Caractéristiques des élèves | Tâches-critères |
+=============+============================+=============================+=================+
| Vocabulaire | Dosage de la pratique      | Age                         | Rappel stimulé  |
+-------------+----------------------------+-----------------------------+-----------------+
| Mots        | Pratique avec livre ouvert | Connaissance pré-requise    | Rappel libre    |
| traduits    | vs. fermé                  |                             |                 |
+-------------+----------------------------+-----------------------------+-----------------+
| Contenu de  | Lire vs. écouter           | Capacité de mémoire de      | Reconnaissance  |
| cours       |                            | travail                     |                 |
+-------------+----------------------------+-----------------------------+-----------------+
| Définitions | Apprentissage incident vs. | Habileté verbale            | Résolution de   |
| en science  | intentionnel               |                             | problème        |
+-------------+----------------------------+-----------------------------+-----------------+
| Textes      | Enseignement direct        | Intérêt                     | Développement   |
| narratifs   |                            |                             | des arguments   |
+-------------+----------------------------+-----------------------------+-----------------+
| Textes      | Apprentissage par la       | Intelligence                | Ecrire des      |
| expositifs  | découverte                 |                             | essais          |
+-------------+----------------------------+-----------------------------+-----------------+
| Concepts ma | Délais de relecture        | Motivation                  | Création de     |
| thématiques |                            |                             | portfolios      |
+-------------+----------------------------+-----------------------------+-----------------+
| Cartes      | Types de tests             | Réussite pré-requise        | Tests de        |
|             |                            |                             | réussite        |
+-------------+----------------------------+-----------------------------+-----------------+
| Diagrammes  | Apprentissage par groupes  | Auto-efficacité             | Questionnaires  |
|             | vs. individuel             |                             |                 |
+-------------+----------------------------+-----------------------------+-----------------+


L'apprentissage visible (Hattie)
--------------------------------

Nous avons laissé pour la fin la tentative la plus complète de spécifier "ce qui marche en éducation", celle de Hattie :cite:`hattie09,hattie14`. C'est une recension de plus de 800 méta-analyses en éducation (elles-mêmes le reflet de plus de 50 000 études expérimentales, impliquant plusieurs millions d'élèves), qui peut être également récupérée sur le site `Hattie Ranking <http://visible-learning.org/hattie-ranking-influences-effect-sizes-learning-achievement/>`_

Le fondement des études rassemblées dans l'ouvrage de Hattie est la taille d'effet, c'est à dire une représentation de la difference entre deux moyennes représentant des performances d'apprentissage (p. ex., soit une différence entre les performances d'un groupe expérimental et celles d'un groupe-contrôle, soit une différence entre les performances d'un groupe après un traitement et celles avant ce traitement), en proportion d'écart-type. Le *d* de Cohen est une mesure de taille d'effet et se calcule ainsi :

.. math::

	d = (moyenne(g1) - moyenne(g2)) / ec


où *g1* est la variable performance du groupe 1, *g2* celle du groupe 2 (les 2 groupes pouvant être remplacés par 2 moments de mesure), et *ec* est l'écart-type agrégé des deux variables *g1* et *g2*).

Un effet dont la valeur est supérieure à 0,8 est considéré comme fort (et faible s'il est inférieur à 0,2). Une valeur négative signifie que le groupe-contrôle a une réussite meilleure que le groupe expérimental. Pour donner un exemple concret, un *d* de 1 signifie que 84 % des participants du groupe expérimental sera au-dessus du participant ayant la moyenne dans le groupe-contrôle, qu'il y a 76 % de chances qu'un participant pris au hasard dans le groupe expérimental aura un résultat supérieur à un participant pris au hasard dans le groupe-contrôle.
Le site http://rpsychologist.com/d3/cohend/ permet de se rendre compte visuellement des tailles d'effet.

Plusieurs enseignements généraux peuvent être tirés du travail d'Hattie. Tout d'abord, presque tous les traitements "marchent", c'est-à-dire que très peu de traitements ont des tailles d'effet négatives. Ensuite, la moyenne des tailles d'effet recensées est de 0,4, c'est-à-dire qu'il peut être intéressant de s'intéresser aux domaines d'influence dont la taille d'effet dépasse cette valeur. Le Tableau 3 suivant recense les 20 traitements ayant le plus important effet.

**Tableau 3** - Les 20 domaines d'influence par taille de leur effet sur l'apprentissage (d'après :cite:`hattie09`, Annexe B).

+------+--------------+---------------------------------+----------------+
| Rang | Domaine      | Type d'influence                | Taille d'effet |
+======+==============+=================================+================+
| 1    | Elève        | Auto-évaluation des élèves      | 1,44           |
+------+--------------+---------------------------------+----------------+
| 2    | Elève        | Programmes piagétiens           | 1,28           |
|      |              | d'évaluation des compétences    |                |
+------+--------------+---------------------------------+----------------+
| 3    | Enseignant   | Evaluation formative            | 0,90           |
+------+--------------+---------------------------------+----------------+
| 4    | Enseignant   | Micro-enseignement              | 0,88           |
+------+--------------+---------------------------------+----------------+
| 5    | Ecole        | Faire sauter une classe         | 0,88           |
+------+--------------+---------------------------------+----------------+
| 6    | Ecole        | Gestion du comportement des     | 0,80           |
|      |              | élèves                          |                |
+------+--------------+---------------------------------+----------------+
| 7    | Enseignement | Interventions pour élèves en    | 0,77           |
|      |              | difficultés d'apprentissage     |                |
+------+--------------+---------------------------------+----------------+
| 8    | Enseignant   | Clarté dans l'explication       | 0,75           |
+------+--------------+---------------------------------+----------------+
| 9    | Enseignement | Enseignement réciproque         | 0,74           |
|      |              | (dialogue ens./élève)           |                |
+------+--------------+---------------------------------+----------------+
| 10   | Enseignement | Rétroactions (feedback)         | 0,73           |
+------+--------------+---------------------------------+----------------+
| 11   | Enseignant   | Relations enseignant-élèves     | 0,72           |
+------+--------------+---------------------------------+----------------+
| 12   | Enseignement | Pratique distribuée vs. massée  | 0,71           |
+------+--------------+---------------------------------+----------------+
| 13   | Enseignement | Stratégies méta-cognitives      | 0,69           |
+------+--------------+---------------------------------+----------------+
| 14   | Elève        | Réussite antérieure de l'élève  | 0,67           |
+------+--------------+---------------------------------+----------------+
| 15   | Curriculum   | Programmes de vocabulaire       | 0,67           |
+------+--------------+---------------------------------+----------------+
| 16   | Curriculum   | Programmes de lecture répétée   | 0,67           |
+------+--------------+---------------------------------+----------------+
| 17   | Curriculum   | Programmes de créativité        | 0,65           |
+------+--------------+---------------------------------+----------------+
| 18   | Enseignement | Auto-explication/questionnement | 0,64           |
+------+--------------+---------------------------------+----------------+
| 19   | Enseignant   | Développement professionnel     | 0,62           |
+------+--------------+---------------------------------+----------------+
| 20   | Enseignement | Enseignement résolution de      | 0,61           |
|      |              | problèmes                       |                |
+------+--------------+---------------------------------+----------------+


Ce que l'on peut faire
======================

Les auteurs cités ci-dessus donnent de riches enseignements sur l'enseignement et l'apprentissage. Ces enseignements, à leur tour, pourraient nécessiter des synthèses, des analyses afin de voir leurs possibles recoupements, mais aussi contradictions. Ce n'est pas le but de ce document de les réaliser, mais d'inciter ses lecteurs à les opérer, avec les réflexions suivantes.

Réplication par des enseignants
-------------------------------

Chacune des informations ci-dessus peut donner lieu à mise en œuvre par des enseignants (qu'ils soient novices, en formation professionnelle, ou plus expérimentés, pour renouveler leur pratique). Il est possible de s'emparer d'une conclusion et de considérer sa réplication dans un contexte scolaire.

Ajout de technologies
---------------------

Une autre possibilité est de considérer l'usage du numérique comme pouvant faciliter telle ou telle pratique énoncée plus haut.

Réalisation d'une méta-méta-analyse
-----------------------------------

Les méta-méta-analyses sont encore rares en éducation, et il pourrait être intéressant de rassembler un ensemble de résultats pour en déterminer, d'une part, les principes communs et, d'autre part, les mécanismes cognitifs pouvant sous-tendre ces principes :cite:`dessus08,sweller03`. Pour cela, lisez les principes ci-dessus et dressez une liste de ceux qui sont concordants, et ceux qui sont possiblement contradictoires.

Débusquer les mythes
--------------------

Une dernière solution est de considérer certains "mythes" liés à l'éducation et de voir comment certains des principes ci-dessus peuvent être utilisés pour les débusquer. Il existe de nombreux ouvrages récents qui se sont attelés à cette tâche (voir notamment :cite:`bruyckere15,berliner14,baillargeon13`) et il est toujours salutaire de tenter de contredire des principes éducatifs très ancrés dans les pratique tout en reposant sur peu d'éléments testables, voire ayant été contredits empiriquement). Le Document :ref:`mythes_education` détaille ce sujet.

Quizz
=====

.. eqt:: Apprentissage-visible-1

	**Question 1. Parmi ces propositions, quelles sont celles qui correspondent aux principes de Graesser (2009) ?**

	A) :eqt:`I` `Imagerie textuelle ; auto-explication ; relecture"`
	B) :eqt:`C` `Attentes liée à l'examen ; Histoires et cas exemplaires ; métacognition imparfaite"`
	C) :eqt:`I` `Auto-interrogation ; Mémorisation par mots-clés ; Test`
	D) :eqt:`I` `Pratique distribuée ; Pratique variable ; Auto-explication"`

.. eqt:: Apprentissage-visible-2

	**Question 2. Parmi ces propositions, quelles sont celles qui correspondent à des techniques d'apprentissage de Dunloski (2013) ?**

	A) :eqt:`I` `Attentes liée à l'examen ; Histoires et cas exemplaires ; La métacognition imparfaite`
	B) :eqt:`I` `Effet d'organisation ; Effet de production ; Effet de cohérence`
	C) :eqt:`I` `Exemples multiples ; Fondation perceptivo-motrices ; L'apprentissage par la découverte`
	D) :eqt:`C` `Imagerie textuelle ; Auto-explication ; Relecture`

.. eqt:: Apprentissage-visible-3

	**Question 3. Parmi les stratégies suivantes, quelle est celle qui a le plus d'effet sur l'apprentissage ?**

	A) :eqt:`I` `Les stratégies méta-cognitives`
	B) :eqt:`I` `Les relations enseignants-élèves`
	C) :eqt:`C` `L'auto-évaluation des élèves`
	D) :eqt:`I` `L'enseignement de la résolution de problèmes`

.. eqt:: Apprentissage-visible-4

	**Question 4. Parmi les stratégies suivantes, quelle est celle qui à le moins d'effet sur l'apprentissage ?**

	A) :eqt:`I` `L'enseignement de la résolution de problème`
	B) :eqt:`I` `L'auto-évaluation des élèves`
	C) :eqt:`C` `L'évaluation formative`
	D) :eqt:`I` `Les rétroactions `



Analyse des pratiques
=====================

#. Trouvez un résultat ou principe ci-dessus qui ne correspond pas à vos pratiques actuelles. Essayez de comprendre ce qu'il faudrait changer dans vos pratiques pour l'intégrer, si toutefois cela vous paraît pertinent.

#. Essayez d'étendre le Tableau 2 en extrayant des informations de vos propres pratiques d'enseignement.

#. Les sites ci-dessous  peuvent aider à déterminer les pratiques les plus efficaces.

Sites
=====

* `Best evidence encyclopedia <http://bestevidence.org>`_
* `Best evidence in brief <https://beibindex.wordpress.com>`_
* `Teaching & learning toolkit <https://educationendowmentfoundation.org.uk/resources/teaching-learning-toolkit/>`_

Références
==========

.. bibliography::
	:cited:
    :style: apa
    :filter: docname in docnames
