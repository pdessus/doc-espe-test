(charge-cognitive)=

# La charge cognitive dans l'apprentissage

```{index} single: auteurs; Dessus, Philippe single: auteurs; Charroud, Christophe single: auteurs; Besse, Émilie
```

:::{admonition} Information
- **Auteurs** : Christophe Charroud, Inspé, univ. Grenoble Alpes & [Philippe Dessus](http://pdessus.fr/), LaRAC & Inspé, Univ. Grenoble Alpes. Le quizz a été réalisé par Emilie Besse, projet [ReflexPro](http://www.idefi-reflexpro.fr).
- **Date de création** : 2003, refondu en Janvier 2016.
- **Date de publication** : {sub-ref}`today`.
- **Statut du document** : Terminé.
- **Voir aussi** : Doc. SAPP {ref}`attention`.
- **Résumé** : La théorie de la charge cognitive de Sweller et Chandler est présentée. Elle permet, en prédisant leur charge cognitive, de comprendre l'apprentissage des élèves selon le type de matériel et les indications qui leur sont fournis.
- **Note** : Le précédent titre de ce document était "L'apprentissage de notions scientifiques : TP ou documents ?". Ce document a fait l'objet d'une recension par M.-A. Boudreault (2018, 12 juin). [La charge cognitive dans l'enseignement](http://rire.ctreq.qc.ca/2018/06/charge-cognitive-lenseignement/). RIRE/CTREQ. Nous remercions M. Martin-Michiellot pour nous avoir autorisé à reproduire les premières figures de ce document.
- **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.
- **Licence** : Document placé sous licence *Creative Commons* : [BY-NC-SA](http://creativecommons.org/licenses/by-nc-nd/3.0/fr/).
:::

% {Glass, 2018 #10627}

% https://www.cese.nsw.gov.au/images/stories/PDF/Cognitive_load_theory_practice_guide_AA.pdf

## Introduction

Il est communément admis que les élèves apprennent mieux lorsqu'ils ont à manipuler un matériel dans une situation réelle. Ainsi, les enseignant proposent à leurs élèves des séances de travaux pratiques, avec du matériel souvent sophistiqué (ordinateurs, microscopes, etc.). Pour autant, est-ce que la plus-value en terme d'apprentissage est aussi nette que cela ? Le temps dévolu à la manipulation, sans compter celui nécessaire à préparer la séance, à commander et récupérer le matériel, à rédiger les consignes, est-il un temps bien employé ? Et, si oui, y a-t-il des règles pour présenter le matériel et les consignes ? Ce chapitre, en présentant une théorie issue de recherches en psychologie, est dévolu à ces questions.

Avant de présenter la théorie de la charge cognitive telle qu'énoncée par Sweller et Chandler, en voici une simplification par {cite}`reif08` (p. 361-362). "La charge cognitive impliquée dans une tâche est l'effort cognitif (ou la quantité d'information traitée) requise par une personne pour réaliser cette tâche". Si cette charge devient trop importante, il n'est pas possible d'apprendre. Cela ne veut pas dire qu'elle doit être minimale, parce que dans ce cas la tâche devrait être découpée en de trop nombreuses petites étapes qui empêcheraient une vue d'ensemble et un apprentissage correct ; de plus, l'apprentissage pourrait être trop facile, donc trop ennuyeux et l'attention de l'apprenant dériverait vers des objets plus stimulants.

La charge cognitive, toujours selon Reif, dépend donc à la fois des exigences de la tâche et des ressources cognitives que l'apprenant peut mobiliser pour la réaliser. Il a à sa disposition deux types de ressources. Des ressources internes (connaissances, motivation, intérêt, stratégies d'apprentissage etc.), et des ressources externes (appareils numériques, mémos, livres, cahiers, etc.). Cela dit, passons à une version plus complète et complexe de cette question.

## Ce que l'on sait

### La théorie de la charge cognitive : Attention partagée et redondance

Nous présentons d'abord ici la théorie de la charge cognitive, élaborée par Sweller et Chandler {cite}`sweller94` (voir les présentations en français : {cite}`mendelsohn99,chanquoy07`). Cette théorie s'attache à observer et prescrire des manières d'organiser la présentation d'un matériel et des consignes associées. Ainsi, une telle présentation peut se faire via manipulation directe, ou bien lecture d'un manuel. Ce dernier peut contenir des schémas commentés ou des textes. Sweller et Chandler ont étudié de près les bénéfices de ces différents modes de présentation sur l'apprentissage d'élèves.

Dans des travaux pratiques, les élèves doivent souvent lire un document expliquant la procédure d'expérimentation. Ce document intègre généralement une ou plusieurs images, représentant le dispositif, ainsi que du texte explicitant la procédure. Il est reconnu que ce que l'on appelle "le double codage" (*dual coding*, ou codage multimédia), c'est-à-dire la présentation conjointe d'images et de textes présentant des informations à apprendre favorise mieux l'apprentissage que la présentation d'un texte seul {cite}`fletcher05,mayer05`, en raison d'un processus d'intégration entre le texte et les images, générateur de relations entre ces différents types d'éléments, et donc de compréhension.

Mais cela ne veut pas dire que n'importe quelle figure intégrant texte et image favorisera l'apprentissage. Un problème survient lorsque les informations sont redondantes.  Sweller et Chandler montrent qu'une information peut être redondante et, par là, gêner la compréhension. C'est l'objet de la deuxième hypothèse : lorsque l'élève est placé dans une situation où il doit traiter de l'information de manière redondante provenant de deux sources différentes (par exemple, un ordinateur et un manuel), cet élève apprend moins bien (voir plus bas la figure sur la circulation sanguine dans le cœur). Ces hypothèses sont fondées sur deux principes théoriques de psychologie :

- la mémoire que l'on utilise pour résoudre des problèmes sur-le-champ (la mémoire de travail) est de capacité limitée, ce qui contraint notre capacité à appréhender simultanément l'ensemble des paramètres d'un problème ;
- un mécanisme, l'acquisition de procédures automatisées, permet de résoudre en partie le premier principe.

Ces hypothèses ont été vérifiées dans de nombreuses expérimentations, qu'il serait trop long de résumer ici. Résumons simplement l'une d'entre elles {cite}`sweller94`. On a fait étudier à trois groupes d'élèves le fonctionnement d'un logiciel de Conception assistée par ordinateur. Voici les documents qu'ils avaient à leur disposition, selon le groupe :

- un mode d'emploi "classique" et la possibilité d'utiliser le logiciel sur ordinateur ;
- un mode d'emploi "intégré", qui intègre des copies d'écran du logiciel et les commentaires sur les fonctionnalités du logiciel, sans possibilité d'utiliser le logiciel ;
- le même mode d'emploi "intégré" et la possibilité d'utiliser le logiciel.

Voici des exemples de figures selon les deux modalités, classique et intégrée {cite}`martin98`

```{image} /images/tp-img1.jpg
:align: center
:scale: 100 %
```

Figures Classiques : Pour la figure de gauche, les numéros renvoient à la légende, séparée de l'image. Pour la figure de droite, le texte explicatif se réfère à diverses parties du graphique.

```{image} /images/tp-img2.jpg
:align: center
:scale: 100 %
```

Figures Intégrées : Pour la figure de gauche, la légende renvoie directement à chaque élément de la figure. Pour la figure de droite, des nombres entourés explicitent l'ordre de la procédure à suivre.

Voici un autre exemple de deux types de formats, en mathématiques, reproduit de Cooper (1998). La première dans un format classique, la seconde dans un format intégré.

```{image} /images/tp-img3.jpg
:align: center
:scale: 100 %
```

```{image} /images/tp-img4.jpg
:align: center
:scale: 100 %
```

Voici enfin une figure intégrant texte et image de manière redondante : le texte inséré dans l'image redit ce que l'image représente, ce qui engendre une surcharge d'attention pour lire le schéma.

```{image} /images/tp-img5.jpg
:align: center
:scale: 100 %
```

Les résultats montrent que les élèves qui apprennent le mieux sont ceux du groupe "manuel intégré" sans possibilité d'utiliser le logiciel. En effet, ils ne partagent pas leur attention entre deux sources d'informations, et cette information n'est pas présentée de manière redondante, comme dans le troisième groupe. En d'autres termes, l'élève va consacrer une attention importante à décoder l'image, attention qui ne sera pas utilisée à comprendre le matériel lui-même.

Ces résultats montrent clairement que la manipulation de matériel n'engendre pas obligatoirement un apprentissage meilleur que la lecture d'un document bien conçu. Ces résultats ont également été reproduits avec des contenus différents : électricité, biologie.

### Complexité de l'information

Nous détaillons ici un point plus précis de la théorie de Sweller et Chandler, souvent mis en avant (voir p. ex. {cite}`sweller94` pour une revue). Ils définissent ce qui fait qu'une information peut être difficile à comprendre.

Une information peut être difficile à apprendre à cause du degré de liaison (d'interactivité) des différents éléments de cette information. Par exemple (tiré de {cite}`sweller94`), il n'est pas difficile d'apprendre des listes de mots avec leur traduction dans une langue étrangère, car aucun de ces mots n'est lié aux autres et, par conséquent, chacun peut être appris indépendamment des autres. Ce type d'information a donc un faible niveau d'interactivité. En revanche, parler dans une langue seconde est beaucoup plus difficile, car produire des phrases correctes nécessite une connaissance, non seulement du vocabulaire, mais également de la syntaxe (ordre des mots dans la phrase), de la grammaire de cette langue, etc. Ainsi, parler dans une langue seconde est une activité à très haut degré d'interactivité, car cela dépend d'un grand nombre de sous-tâches dépendantes les unes des autres : par exemple l'ordre des mots d'une phrase dépend de la nature de ces mots.

Au cours de l'apprentissage, l'élève construit des schémas (parfois appelés schèmes, procédures de pensée) de plus en plus groupés, organisés et automatisés, lui permettant de faire face de la manière la plus économique possible aux tâches demandées, en dégageant de la charge cognitive. Par exemple, la compréhension du fonctionnement d'un circuit électrique nécessite l'acquisition de schémas de bas niveau concernant la compréhension des symboles qui le composent. Dans un deuxième niveau de compréhension, il est nécessaire d'avoir une compréhension de l'interaction de chaque composant avec les autres, ce qui va aussi nécessiter la construction de schémas de plus haut niveau, fondés sur les précédents, et pouvant à leur tour être considérés par l'apprenant comme des unités (p. ex., la capacité de détecter rapidement si un circuit est en série ou en parallèle) {cite}`kalyuga10`

### Les différents types de charge cognitive

Les auteurs de cette théorie {cite}`sweller94,sweller10` listent trois types de charge cognitive :

- *la charge cognitive intrinsèque* est liée aux caractéristiques du matériel appris et du niveau d'interactivité de ses éléments (voir section précédente). Un contenu a un haut niveau d'interactivité si les élements qui le composent (concepts, notions) ne peuvent être appris isolément. L'enseignant peut intervenir à ce niveau en segmentant le matériel en sous-parties, mais, de manière générale, il a une faible latitude.
- *la charge cognitive extrinsèque* est liée à la manière dont l'information est présentée, et donc dépend plus directement de l'enseignant, qui doit essayer de la limiter au maximum, notamment en évitant la redondance (voir section précédente).
- *la charge cognitive essentielle* (*germane*) est liée aux caractéristiques de l'élève : ses ressources cognitives (mémoire de travail), à motivation constante. Ce n'est donc pas une source indépendante de charge cognitive. Un élève alloue une quantité de charge cognitive essentielle pour prendre en compte la charge cognitive intrinsèque liée à l'interactivité du matériel.

Ces trois catégories de charge cognitive sont additives et ne peuvent excéder la capacité totale de mémoire de travail de l'élève. Les charges sont donc interreliées : si la charge cognitive intrinsèque est élevée et la charge cognitive extrinsèque est basse, une quantité importante de charge cognitive essentielle pourra être allouée à la tâche (voir Figure ci-dessous, mémoire libre). Lorsque la charge cognitive extrinsèque augmente, la charge essentielle sera moins importante (ces deux dernières charges sont donc complémentaires).

La Figure ci-dessous, d'après {cite}`sweller10,moreno10`, résume les relations entre ces types de charge cognitive.

```{image} /images/tp-img6.jpg
:align: center
:scale: 40 %
```

### Les niveaux d'interactivité entre éléments

Une des premières choses qu'un enseignant doit avoir à l'esprit, lorsqu'il présente un nouveau contenu, c'est le nombre d'éléments nécessaires à connaître pour réaliser une tâche, ainsi que leur niveau d'interactivité, c'est-à-dire si ces éléments doivent être connus simultanément ou consécutivement. Prenons un exemple, tiré de {cite}`sweller94`, pp. 190-191).

Pour apprendre à situer un point sur un repère à deux dimensions, il faut faire correspondre une notation algébrique de type $P(x,y)$ au point correspondant dans l'espace à deux dimensions, et vice versa. Voici les éléments nécessaires à connaître simultanément pour s'acquitter de cette tâche. Chacun des éléments suivants est simple, mais il n'est pas possible de comprendre l'un d'entre eux sans avoir compris les autres.

1. L'axe des abscisses est une ligne graduée, horizontale ; l'axe des ordonnées est une ligne graduée, verticale. Ces deux lignes se croisent à angle droit au point 0 des deux axes, appelé l'origine.
2. *P*, dans $P(x,y)$, est le nom du point.
3. *x*, dans $P(x,y)$, est la position *x* sur l'axe des abscisses.
4. *y*, dans $P(x,y)$, est la position y sur l'axe des ordonnées.
5. Tracer une ligne partant de *x* (axe des abscisses), parallèle à l'axe des ordonnées.
6. Trace une ligne partant de *y* (axe des ordonnées), parallèle à l'axe des abscisses.
7. Le point d'intersection de ces deux lignes est $P(x,y)$.

La connaissance de cette liste est triviale pour tout enseignant, ainsi que pour la plupart des élèves du secondaire. Pourtant, lors de l'apprentissage de ce contenu, ces personnes ont dû comprendre ces sept éléments. L'utilisation répétée de cette procédure les a amenés à construire ce que l'on appelle un schéma - ou cadre de pensée - qui automatise cette procédure et permet de la mettre en œuvre avec une attention minimale (ce qui permet également de traiter des problèmes bien plus complexes). On comprendra aussi que, pour cette même raison, l'élément 1 peut être considéré, pour certains élèves, comme un ensemble d'éléments et plus un seul. Formulé comme ci-dessus, l'élément 1 est lui-même devenu un schéma.

### Apprendre avec les animations

Certains travaux ont confirmé que des systèmes dynamiques (animations et vidéos) facilitent la compréhension en compensant certaines limites de notre système cognitif qui doit simuler des mouvements ou des processus dynamiques à partir d’information statiques. Cependant les travaux de Lowe et Boucheix {cite}`lowe11` montrent qu’apprendre à partir d’une animation implique différentes activités mentales comme sélectionner les informations dynamiques ou savoir quelle information est pertinente relativement aux autres.

Pour sélectionner et organiser les unités d’informations entre elles, les apprenants doivent disposer de ressources et d’habilités cognitives importantes {cite}`dekoning07`. La nature même des informations dynamiques, s’affichant puis disparaissant ou se transformant, induit une difficulté liée au maintien en mémoire des informations transitoires. Les apprenants peuvent oublier les informations précédentes, qui de ce fait ne sont plus accessibles {cite}`singh12,wong12`.

## Ce que l'on peut faire

### Implications pédagogiques générales

Voici quelques implications pédagogiques de cette théorie, implications qui devraient permettre une meilleure conception des documents de TP (voir {cite}`wilson96` et Smith, 2016).

- Analyser soigneusement la quantité d'attention requise par la mise en œuvre du TP. Il est possible de comptabiliser le nombre d'éléments différents de la cession d'apprentissage. Un trop grand nombre d'éléments différents gêne la compréhension de l'élève.
- Utiliser des représentations simples et cohérentes, qui permettent à l'élève de focaliser son attention plutôt que de la partager. Par exemple, préférer un diagramme dont la légende est intégrée à l'image, plutôt que séparée.
- Si la segmentation n'est pas possible, permettre un entraînement sur des composants plus simples et isolés, avant de présenter leur fonctionnement dans le système complet.
- Ralentir lors de l'explication de présentations mêlant mots et images.
- Éliminer autant que possible la redondance. De l'information redondante entre texte et image fait décroître l'apprentissage. Supprimer l'information qui n'est pas strictement utile à la compréhension (p. ex., musique de fond).
- Essayer de présenter ensemble les images et les mots écrits et encourager les élèves à faire des liens entre les deux. Avant la diffusion, les prévenir de se centrer sur tel ou tel moment de l'explication.
- Proposer des exemples résolus plutôt que des problèmes ouverts. Dans ce dernier cas, en effet, les élèves passent beaucoup trop de temps au but général du problème, et mettent en oeuvre des stratégies de résolution souvent peu adéquates, alors qu'ils ont surtout besoin de comprendre comment certains exemples fonctionnent. Les problèmes ouverts sont toutefois utiles, et peuvent être proposés en guise d'évaluation de l'apprentissage.

### Les exemples résolus (*worked examples*)

De nombreuses recherches ont montré que le fait de soumettre aux élèves des exemples résolus (*worked examples*) plutôt que des situations de résolution de problèmes ouverts amélioraient leur apprentissage. Voici la procédure pour introduire de tels exemples {cite}`cooper98` :

1. Présenter un nouveau sujet. Présenter les pré-requis, les principes, les règles générales.
2. Montrer, en utilisant un petit nombre d'exemples résolus, comment appliquer ces principes et règles.
3. Faire acquérir de l'expérience aux élèves, en leur proposant de résoudre de nombreux problèmes à but précis.

Voici un exercice sous la forme d'exemple résolu, en mathématiques (Cooper, 1998) : Commencez par comprendre l'exemple résolu 1), puis résolvez l'autre exercice.

Consigne : Pour chaque exercice, trouvez *a*.

$1) c(a + b) = f$

$(a + b) = f/c$

$a = f/c - b$

$2) g(a + m) = k$

### Des animations efficaces pour l'apprentissage

Cette section est un résumé de Amadieu et Tricot {cite}`amadieu14`. Voici quelques conseils pour concevoir ou utiliser des animations efficaces pour l'apprentissage.

#### Présenter de façon animée des informations elles-mêmes dynamiques

- Présenter des informations dynamiques n’a véritablement d’intérêt que si les contenus possèdent un caractère dynamique {cite}`betrancourt00`.
- L’utilisation d’animations sera pertinente pour faire étudier des connaissances procédurales et motrices plutôt que pour faire étudier des connaissances déclaratives ou des connaissances de résolution de problème {cite}`hoffler07`.

#### Animer l’essentiel et non les détails

- L’attention de l’apprenant peut être détournée des informations conceptuelles pertinentes par des détails ou des mouvements non pertinents pour comprendre le système étudié {cite}`lowe03`.
- Il est recommandé d’utiliser les dispositifs de guidage attentionnels dont le principe est d’orienter l’attention de l’apprenant vers les parties les plus pertinentes de l’animation {cite}`ayres07,betrancourt05`).
- Des signaux visuels qui mettent en avant ces informations à un temps *t* peuvent être utilisés {cite}`dekoning07`.

#### Limiter le nombre d’informations à maintenir en mémoire pendant le visionnage

- Le caractère transitoire des informations peut rendre difficile leur intégration en mémoire et entraver la compréhension du processus présenté par l’animation.
- Il est donc utile de maintenir affichées certaines informations pertinentes afin d’augmenter l’efficacité de l’animation {cite}`ng13`.

#### Segmenter les animations

- Fractionner une animation aide l’apprenant à se représenter la structure temporelle, c’est-à-dire les étapes qui composent le processus et leur longueur. Ainsi, les associations des informations en mémoire se feront plus facilement {cite}`spanjers12`.
- Pointer les étapes clés en présentant une image représentative de chaque étape est également efficace {cite}`arguel09`.

#### Un contrôle simple de la vidéo ou de l’animation

Lorsqu’un apprenant à la possibilité d'avoir un contrôle minimal sur le rythme de défilement d’une animation ou d’une vidéo (interrompre et reprendre la vidéo), cela permet de diminuer la charge cognitive et d’améliorer l’efficacité de l’apprentissage. Un niveau de contrôle trop complexe (interrompre, reprendre, changer de chapitre, changer la vitesse et la direction de la vidéo) induit une charge de gestion de la présentation augmentant la charge cognitive ce qui va finalement diminuer l’efficacité du traitement par rapport à un contrôle minimal {cite}`tabbers10`.

## Quizz

```{eval-rst}
.. eqt:: Chargecog-1

        **Question 1. Selon R. E. Mayer (2005), qu'est-ce que l'on appelle le "double codage"**

        A) :eqt:`I` `Le fait de répéter une deuxième fois l'information`
        B) :eqt:`C` `La présentation conjointe d’images et de textes présentant des informations à apprendre`
        C) :eqt:`I` `Expliquer une information de deux façons`
        D) :eqt:`I` `Se représenter visuellement l'information à apprendre`
```

```{eval-rst}
.. eqt:: Chargecog-2

        **Question 2. Selon Sweller et Chandler (1994), la charge cognitive intrinsèque est liée :**

        A) :eqt:`I` `À la manière dont l’information est présentée`
        B) :eqt:`I` `Aux caractéristiques de l’élève`
        C) :eqt:`I` `À la posture de l'enseignant`
        D) :eqt:`C` `Aux caractéristiques du matériel appris et du niveau d’interactivité de ses éléments`
```

```{eval-rst}
.. eqt:: Chargecog-3

        **Question 3. Selon J. Sweller et P. Chandler (1994), la charge cognitive extrinsèque est liée :**

        A) :eqt:`I` `Aux caractéristiques de l’élève`
        B) :eqt:`I` `Aux caractéristiques du matériel appris et du niveau d’interactivité de ses éléments`
        C) :eqt:`C` `À la manière dont l’information est présentée`
        D) :eqt:`I` `À la posture de l'enseignant`

```

## Analyse des pratiques

1. Prenez une activité d'apprentissage de votre choix dans votre discipline et exercez-vous à lister son nombre d'éléments.
2. Concevez, à propos du contenu de votre choix, une série d'exemples résolus.
3. Toujours à propos du contenu de votre choix, concevez un document de TP intégrant texte et image qui suive les prescriptions de Sweller et Chandler (pas d'attention partagée, non-redondance).

## Webographie

- Caviglio, O. [Poster sur la charge cognitive](https://extra.teachinghow2s.com/workspace/uploads/resources/pinpoint-cognitive-load.pdf)
- CESE (2018). [Cognitive load in practice](https://www.cese.nsw.gov.au/images/stories/PDF/Cognitive_load_theory_practice_guide_AA.pdf)
- Smith, M. (2016, 17 Nov.). *Dual coding: Can there be too much of a good thing?* Billet de blog accessible [ici](http://www.learningscientists.org/blog/2016/11/17-1).

## Références

```{bibliography}
:filter: docname in docnames
```
