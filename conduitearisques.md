(conduite-a-risques)=

# Prévenir les conduites à risques

```{index} single: auteurs; Dessus, Philippe single: auteurs; Besse, Émilie
```

:::{admonition} Information
- **Auteurs** : [Philippe Dessus](http://pdessus.fr/), LaRAC & Espé, Univ. Grenoble Alpes. Le quizz a été réalisé par Emilie Besse, projet [ReflexPro](http://www.idefi-reflexpro.fr).
- **Date de création** : Janvier 2006.
- **Date de publication** : {sub-ref}`today`.
- **Statut du document** : Terminé.
- **Résumé** : Ce document fait le point sur la notion de conduite à risques chez l'adolescent, et plus particulièrement sur la consommation de substances psychoactives (alcool, tabac, cannabis). Après avoir dressé un état des lieu de ce domaine en France, il donne des pistes de travail pour les prévenir.
- **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.
- **Licence** : Ce document est placé sous licence *Creative Commons* : [BY-NC-SA](http://creativecommons.org/licenses/by-nc-sa/3.0/fr/).
:::

## Introduction

L'adolescence est souvent présentée comme une période à problèmes spécifiques : suicide, diverses conduites à risques (alcool, relation sexuelles non protégées, sports dangereux, risques routiers, addiction à divers produits, etc.). Or, Desrichard (2004), en montrant d'une part que ces différents problèmes ne sont pas statistiquement plus élevés chez l'adolescent que chez l'adulte, et d'autre part que ces conduites diffèrent plus de la période précédente (enfance) que de la suivante (l'âge adulte), l'adolescent-e s'essayant, avec une liberté accrue, à différentes conduites qu'il ou elle poursuivra ou pas à l'âge adulte. Cela étant, parce que certaines conduites peuvent avoir des conséquences importantes en termes de santé ou d'insertion sociale (sida, échec scolaire, problèmes judiciaires), il convient de porter sur cette période une attention particulière, et les enseignants sont, de ce point de vue, très concernés.

Cependant, la notion même de "conduite à risques" est rarement définie précisément. Desrichard (2004) montre qu'on ne peut  simplement la définir comme une conduite qui  présente des risques pour le bien être physique et/ou mental car, dans ce cas, n'importe quelle activité de la vie quotidienne peut être ainsi qualifiée. Une définition plus précise amène à la définir en tant que conduite ayant plus d'une conséquence possible, au moins l'une d'entre elles pouvant être jugée désirable, et au moins l'une autre d'entre elles devant être jugée indésirable (*i.e.*, avec des conséquences néfastes). Cette définition implique que la personne prenant des risques est consciente de cela. Une extension de cette définition amène à parler de comportement-problème, dont les conséquences sont essentiellement sociales, et pas nécessairement liées à la santé (absentéisme, violence, délinquance, etc.).

Enfin, il s'agit aussi de comprendre de quel point de vue la conduite à risques est considérée. Desrichard (2004) montre que trois niveaux de conduites à risques peuvent être détaillées :

- celles sans sanctions sociales (ni chez les adultes, ni chez les  adolescents), comme la sédentarité, un régime à hautes calories ;
- celles sanctionnées uniquement par les adultes, (consommation de  tabac, d'alcool, de marijuana, les rapports sexuels non protégés,...)  ;
- celles sanctionnées par les adultes et les adolescents (consommation  d'héroïne, de crack, ...).

## Ce que l'on sait

Les articles L 627 et 628-1 du code de la santé publique interdisent et sanctionnent l'usage, l'incitation à l'usage, la détention, la cession ou la vente de substances psychotropes classées au tableau des stupéfiants illicites. L'article 227-18 du code pénal réprime le fait de provoquer directement un mineur à faire un usage illicite de stupéfiants, peine aggravée lorsqu'il s'agit d'un mineur de quinze ans.

La loi prévoit un an de prison et 3750 euros d'amende pour usage simple de stupéfiants, peine qui augmente en cas de trafic (jusqu'à 10 ans et 7 500 000 euros, voir Les Drogues et les lois, 2002). Mais seulement 1 % des usagers sont emprisonnés (Vanier & Le Pautremat, 2001).

### Quelques statistiques sur la consommation de substances psychoactives

Tout d'abord, il y a un lien étroit entre expérimentation (*i.e.*, en avoir consommé au moins une fois) d'alcool et de tabac : plus on boit, plus on fume, mais l'inverse n'est pas vrai. De plus, l'expérimentation de cannabis est toujours liée à celle d'alcool ou de tabac. Enfin, 45 % des 15-19 ans ne consomment régulièrement ni alcool, ni tabac (Source "À toi de juger").

Choquet *et al*. (2004) font état du volet français d'une enquête européenne sur la consommation de substances psychoactives chez les collégiens et lycéens (ESPAD, sur 30 pays européens, par questionnaire). Pour résumer, ils montrent que le tabac et l'alcool sont les substances les plus fréquemment expérimentées entre 12 et 18 ans, mais leur progression entre ces âges diffère : l'expérimentation d'alcool (*i.e.*, le fait d'avoir consommé au moins une fois de l'alcool) est déjà élevée à 12 ans, et croît peu alors que l'expérimentation du tabac (et aussi de cannabis) croît tout au long (voir tableau I). Il est à noter, aussi, que les garçons sont en général un peu plus nombreux que les filles à expérimenter des substances psychoactives, mais que cet écart s'amenuise tout au long de l'adolescence, pour même s'inverser pour le tabac. En revanche, l'expérimentation de tranquillisants et somnifères est est principalement féminine, et les autres substances illicites (LSD, cocaïne, ecstasy, amphétamines) sont peu expérimentées (majoritairement inférieures à 5 %), et leur usage n'est en général pas renouvelé.

Quant aux consommations régulières de ces substances, elles sont rares avant 14 ans et augmentent ensuite, le tabac étant largement plus consommé quotidiennement que les autres. Le pic de consommation régulière de cannabis (Vanier & Le Pautremat, 2001) survient plus tard (20-24 ans) Les garçons sont plus consommateurs réguliers de telles substances que les filles, hormis pour le tabac pour lequel ils sont à égalité.

de Peretti et Leselbaum (1995) ont en outre montré que le fait de connaître les différents risques liés à la consommation de substances psychoactives n'a pas forcément d'impact sur la consommation : c'est le cas pour l'alcool et le tabac, dont la communication sur les risques est maintenant large (bien que les risques de dépendance soient plus souvent signalés chez les gros fumeurs, et non chez les gros consommateurs d'alcool). Ce n'est pas, en revanche, le cas pour le cannabis : si les lycéens reconnaissent que "c'est mauvais pour la santé", ce sont ceux qui n'en fument pas qui signalent le plus les risques de dépendance.

Tableau I - Expérimentation et usage régulier de tabac, alcool et cannabis par sexe et âge, en France. Lire : 4 % des garçons de 12 ans ont consommé au moins une fois du cannabis, pour 66 % à 18 ans. Données ESPAD 2003, d'après Choquet *et al*., 2004, pp. 1 et 3.

| **Garçons**     |                    |                   |                      |
| --------------- | ------------------ | ----------------- | -------------------- |
| Usage           | Alcool (12-18 ans) | Tabac (12-18 ans) | Cannabis (12-18 ans) |
| Expérimentation | 70-91 %            | 22-78 %           | 4-66 %               |
| Usage régulier  | env. 1-37 %        | env. 1-22 %       | 0-21 %               |
| **Filles**      |                    |                   |                      |
| Expérimentation | 63-91 %            | 16-81 %           | 1-52 %               |
| Usage régulier  | 0-7 %              | 0-34 %            | 0-7 %                |

### Pourquoi consomment-ils de telles substances ?

de Peretti et Leselbaum (1996) classent les lycéens consommateurs en trois catégories :

- *consommation festive* : consommation ponctuelle, une manière de s'"éclater", pour faire comme les autres...
- *consommation instrumentalisée* : consommation répétée pour accéder à un bien-être.
- *consommation pour fuir les difficultés* : angoisse, solitude,  difficultés sociales, ...

### Les prédicteurs des conduites à risques

Jessor (1993, voir un résumé dans Desrichard, 2004) liste les facteurs liés -- positivement ou négativement -- à la conduite à risques chez l'adolescent (quelle qu'elle soit). Parmi tous ces facteurs, d'autres travaux ont montré que le facteur prédisant le mieux la prise de cannabis était la consommation par les pairs. Le tableau II ci-dessous les résume :

**Tableau II** - Les différents domaines agravant et atténuant les risques (d'après Jessor, 1993, p. 120 ; Desrichard, 2004).

| X                                                 | Facteur agravant les risques                                                                                                                                                                       | Facteurs atténuant les risques                                                       |
| ------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------ |
| Patrimoine biologique et génétique de la personne | Histoire familiale par rapport à l'alcoolisme                                                                                                                                                      | Intelligence                                                                         |
| Environnement social                              | Pauvreté, inégalités raciales, opportunités d'accès                                                                                                                                                | Qualité de l'environnement scolaire, Cohésion familiale, Voisinage, Entourage adulte |
| Perception de l'environnement                     | Modèles de comportements déviants, conflit  de normes entre parents et pairs, surestimation des bénéfices et sous-estimation des risques, illusion de contrôle ("je peux m'arrêter quand je veux") | Modèles de comportements acceptables, Contrôle élevé des comportements déviants      |
| Personnalité                                      | Estime de soi peu élevée, propension à la prise de risques, perception du risque, besoin de sensation                                                                                              | Valeur attribuée à la santé, à la réussite                                           |
| Comportements                                     | Faible travail scolaire                                                                                                                                                                            | Engagement dans le travail scolaire et clubs (sportifs, sociaux)                     |

## Ce que l'on peut faire

De Peretti et Leselbaum (1996) insistent sur le phénomène de la consommation de drogues est difficile à appréhender, à circonscrire, du fait :

- que certaines drogues sont socialement acceptées (alcool, tabac) et d'autres non (héroïne, crack, cannabis) ;
- que leur mode de consommation est très variable : si leur expérimentation est fréquente, leur consommation régulière (abus) est moindre, sans parler de ceux qui en sont dépendants (Beck *et al*., 2004).

Si les adolescents sont relativement bien informés des dangers de la consommation de ces produits (par la famille, les médias, les avertissements sur les emballages), les séances de prévention de ces conduites pourront avoir les objectifs suivants (d'après "À toi de juger, conseils et propositions d'activités") :

- faire débuter la prévention au plus jeune âge (école élémentaire,  Wyvill, 1999) ;
- faire acquérir des connaissances sur les mécanismes biologiques ou neurobiologiques induits par la consommation de telles substances.
- combattre certaines idées fausses, croyances : *e.g.*, "l'alcool réchauffe". À l'inverse, il est inutile de diaboliser certains produits, de dire qu'on va mourir si l'on prend du haschisch (Vanier & Le Pautremat, 2001). Avoir un langage clair sur les risques de chaque produit (voir tableau des produits dans le BO 1999).
- valoriser les images positives de comportements favorables à la santé, plutôt qu'avoir une conception hygiéniste, qui mène à interdire ce qui fait plaisir pour garder un corps sain. Ne pas nier que ces produits peuvent procurer des sensations agréables, mais mentionner que la recherche de ces sensations peut amener un état de dépendance. Questionner les élèves sur ce dont ils ont besoin pour se sentir bien dans leur corps, leur tête, avec les autres. Eviter d'amener les élèves à penser que la consommation de substances psychoactives leur permettra de surmonter certaines difficultés personnelles (humeur, communication, etc.). De plus, certaines campagnes de prévention nationales ne jouent pas leur rôle de prévention (Vanier & Le Pautremat, 2001). Dire "tu t'es vu quand t'as bu ?" ou "La drogue, c'est de la merde" n'a pas d'effet dissuasif : c'est précisément pour cela, pour ces effets, que les adolescents en prennent. Jouer uniquement sur la crainte ou la peur peut avoir l'effet inverse de celui escompté.
- avoir une attitude de compréhension et non de jugement face aux récits de telles conduites, se garder de leur donner valeur d'exemple. Notamment, comme la consommation d'alcool des jeunes est très liée à sa consommation dans leur famille, il peut être intéressant de prendre en compte l'attitude familiale à ce propos (*e.g.*, via questionnaire). Renforcer les relations avec l'adolescent, lui faire exprimer ce qui ne va pas, plutôt que de l'enfermer dans un statut de "drogué" dans lequel il risque de continuer à s'enfoncer :  "La tendance actuelle serait plutôt de le faire taire à coup de psychotropes avec la bonne conscience d'un alibi scientiste. En fin de compte, est-ce si différent de la position toxicomaniaque ?" (Vanier & Le Pautremat, 2001, interrogeant le Dr. Cornier, psychiatre).
- tous les membres de la communauté éducative doivent avoir la même attitude par rapport à ces problèmes de consommation, travailler ensemble (ensiegnants, assistante sociale, CPE, COP, etc.), et ce quelles que soient les opinions personnelles de chacun à ce propos.
- Signaler l'existence du service Drogue Info Service, anonyme, et accessible 24 h sur 24 au 0 800 23 13 13.

Plus particulièrement, une information transversale pourra être diffusée dans les différentes disciplines (Vanier & Le Pautremat, 2001) :

- Campagnes de prévention routière au collège : dangers de conduire en  état d'ivresse.
- SVT : influence des substances psychoactives sur l'organisme.
- ECJS : Travail sur les différentes lois, peines.
- Littérature : Le club des Haschichins, Théophile Gautier, Baudelaire.
- Histoire : L'expédition en Egypte de Napoléon, agressé par un soldat  sous l'emprise du cannabis, rendit le cannabis prohibé en Egypte  (1800).
- Géographie : Carte des routes de la drogue.
- Economie : Economie souterraine.
- TPE : peut être un lieu privilégié pour aborder ce thème de manière  transversale. Il existe d'ailleurs sur internet de nombreux sites  reportant de tels TPE. Consulter le site [Forum sciences  TPE](http://sciences-tpe.ens-cachan.fr/) pour des renseignements à  ce sujet.

Se référer au Comité d'éducation à la santé et à la citoyenneté (CESC) de l'établissement, s'il existe. Créé par la circulaire n ° 98-108 du 1{sup}`er` juillet 1998, c'est un dispositif qui constitue au niveau de l’établissement scolaire un cadre privilégié de définition et de mise en œuvre de l’éducation préventive en matière de conduites à risques, de dépendances, dans et hors l’école.

Mener avec ses élèves une discussion en abordant les points suivants (J.-P. Falcy, in Vanier & Le Pautremat, 2001, p. 150) :

- Qui se drogue (portrait robot) ;
- Pourquoi se drogue-t-on ?
- Y a-t-il du sens à faire des distinctions enter différents types de  drogues ?
- Quelles sont leurs conséquences sur l'insertion sociale et la santé ? (risques de marginalisation)
- Comment y remédier ?

## Analyse de pratiques

1. Partir d'exemples auxquels les enseignants ont été confrontés, soit  directement, soit indirectement. Le [BO du 4 nov.  99](http://www.education.gouv.fr/bo/1999/hs9/som.htm)contient  également des exemples de situations pouvant arriver (un élève fume  un joint, est dans un état d'ébriété, consommation de drogues le  week-end...).

## Quizz

```{eval-rst}
.. eqt:: Conduitearisques-1

        **Question 1. Selon Desrichard (2004), quelles sont les trois niveaux de conduites à risques possibles ?**

        A) :eqt:`C` `"Celles sans sanctions sociales ; Celles sanctionnées uniquement par les adultes ; Celles sanctionnées par les adultes et les adolescents"`
        B) :eqt:`I` `"Celles sans sanctions ; Celles avec sanction faibles ; Celles avec sanctions élevées"`
        C) :eqt:`I` `"Celles avec sanctions proportionnelles ; Celles avec sanctions non proportionnelles"`
        D) :eqt:`I` `"Celles avec sanctions ; Celles sans sanctions"`
```

```{eval-rst}
.. eqt:: Conduitearisques-2

        **Question 2. Selon De Peretti et Leselbaum (1996), quelles sont les trois catégories de lycéens consommateurs de substances psychoactives ?**

        A) :eqt:`I` `"À consommation journalière ; Consommation hebdomadaire ; Consommation mensuelle"`
        B) :eqt:`C` `"À Consommation festive ; Consommation instrumentalisée ; Consommation pour fuir les difficultés"`
        C) :eqt:`I` `"À Consommation régulière ; Consommation occasionnelle ; Consommation rare"`
        D) :eqt:`I` `"À Consommation de tabac ; Consommation d'alcool ; Consommation de cannabis"`
```

```{eval-rst}
.. eqt:: Conduitearisques-3

        **Question 3. Selon De Peretti et Leselbaum (1996), le phénomène de la consommation de drogues est difficile à appréhender car :**

        A) :eqt:`I` `Le nombre de drogues existant est élevé`
        B) :eqt:`I` `Le sujet est tabou donc il est difficile de récolter des témoignages`
        C) :eqt:`I` `Les lycéens les consomment en dehors du lycée`
        D) :eqt:`C` `Certaines drogues sont socialement acceptées et d'autres non`

```

## Références bibliographiques

- À toi de juger, site internet sur la prévention de la consommation de substances psychoactives (alcool, tabac et cannabis), [www.atoidejuger.com](http://www.atoidejuger.com)
- Beck, F., Legleye, S. & Spilka, S. (2004). Cannabis, alcool, tabac et autres drogues à la fin de l'adolescence : usages et évolutions récentes, ESCAPAD 2003. *Tendances*, *39*.
- Choquet, M., Beck, F., Hassler, C., Spilka, S., Morin, D. & Legleye, S. (2004). Les substances psychoactives chez les collégiens et lycéens : consommation en 2003 et évolutions depuis 10 ans. *Tendances*, *35*.
- Comité d'éducation à la santé et à la citoyenneté. Site internet du MEN accessible à <http://eduscol.education.fr/D0004/vtcacc01.htm>
- DESCO & ENS. Forum Sciences TPE accessible à <http://sciences-tpe.ens-cachan.fr/>
- Desrichard, O. (2004). Les conduites à risques chez les adolescents. In M.-C. Toczek & D. Martinot (Eds.), *Le défi éducatif, des situations pour réussir* (pp. 247-275). Paris: Colin.
- de Peretti, C., & Leselbaum, N. (1995). *Tabac, alcool, drogues illicites, opinions et consommations des lycéens*. Paris: I.N.R.P.
- de Peretti, C., & Leselbaum, N. (1996). Les jeunes et les drogues : réflexions pour la prévention. *Revue Française de Pédagogie, 114*, 29-43.
- Les drogues et les lois états des lieux, états du droit (2002). document accessible à <http://www.drogues.gouv.fr/fr/professionnels/FAQ/faq_loi/faq_loi.pdf>.
- MEN (1999). [Repères pour la prévention des conduites à risque](http://www.education.gouv.fr/bo/1999/hs9/som.htm). BO HS n° 9 du 4 novembre, accessible à <http://www.education.gouv.fr/bo/1999/hs9/som.htm>.
- Jessor, R. (1993). Successful adolescent development among youth in high-risk settings. *American Psychologist*, *48*(2), 117-126.
- Vanier, V., & Le Pautremat, F. (Eds.). (2001). *Drogues et toxicomanies chez les jeunes*. Paris: ADAPT.
- Wyvill, B. (1999). Drug education in England. *Drugs: Education, Prevention and Policy*, *6*(3), 353-360.
