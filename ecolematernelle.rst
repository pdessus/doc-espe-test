.. _ecole_maternelle:

****************************
L'école maternelle en France
****************************

.. index::
  single: auteurs; Dessus, Philippe
  single: auteurs; Deschaux, Joël
  single: auteurs; Besse, Émilie

.. admonition:: Informations

  * **Auteurs** : Joël Deschaux, Inspé, Univ. Grenoble Alpes & `Philippe Dessus <http://pdessus.fr/>`_, Inspé & LaRAC, Univ. Grenoble Alpes. Le quizz a été réalisé par Emilie Besse, projet `ReflexPro <http://www.idefi-reflexpro.fr>`_.

  * **Date de création** : Octobre 2008 ; mis à jour en Février 2017.

  * **Date de publication** : |today|.

  * **Statut du document** : Terminé.

  * **Résumé** : Ce document présente quelques  caractéristiques de l'école maternelle française, à la fois d'un point de vue historique, pratique, et évoque également quelques débats à son propos, comme la scolarisation précoce.

  * **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.

  * **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.


Introduction
============

Un niveau très fréquenté
------------------------

Bien que non obligatoire, l'école maternelle (EM) française est fréquentée par près de 100 % des enfants de 3 ans (pour seulement 36 % en 1960). Environ 11,8 % des 2-3 ans la fréquentent en 2014 ; ce dernier chiffre a de grandes disparités sur le territoire : rural ou urbain, régions d'outre-mer, éducation prioritaire ou non (BOEN, `2013 <http://www.education.gouv.fr/pid25535/bulletin_officiel.html?cid_bo=66627>`_).

L'accueil des élèves se fait dans ce cadre ou dans la mesure des places disponibles, alors que la scolarisation des 3 ans est un droit pour tout parent qui en fait la demande (`Art. L 113-1 du 8/07/2013 <https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071191&idArticle=LEGIARTI000027682617>`_ du Code de l’éducation). En 2014, la France comptait 15 079 écoles maternelles publiques et 137 privées qui scolarisaient 2 574 900 élèves (`source <http://www.education.gouv.fr/cid166/l-ecole-maternelle-organisation-programme-et-fonctionnement.html>`_).

Missions
--------

La `Loi d’orientation et de programmation pour la refondation de l’École de la République <https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000027677984&dateTexte=&categorieLien=id>`_ (JO du 9 juillet 2013), redéfinit les missions de l'EM :

  « en lui donnant une unité par la création d'un cycle unique (petite section, moyenne section et grande section) (rentrée 2014). Il ne s'agit pas de refermer l'EM sur elle-même, mais de lui permettre de préparer progressivement les enfants aux apprentissages fondamentaux dispensés à l'école élémentaire. Les enseignants de grande section de maternelle et de cours préparatoire d'un même secteur de recrutement continueront à se rencontrer de manière régulière afin d'échanger sur les acquis des élèves à l'issue de l'école maternelle et sur les besoins spécifiques des élèves bénéficiant d'aménagements particuliers de scolarité. »

  « Elle assure une première acquisition des principes de la vie en société et de l'égalité entre les filles et les garçons. La prévention des difficultés scolaires y est assurée par la stimulation et la structuration du langage oral et l'initiation à la culture écrite. »

Et son Article 44 décrit brièvement un programme d'enseignement :

  « La formation dispensée dans les classes enfantines et les écoles maternelles favorise l'éveil de la personnalité des enfants, stimule leur développement sensoriel, moteur, cognitif et social, développe l'estime de soi et des autres et concourt à leur épanouissement affectif. Cette formation s'attache à développer chez chaque enfant l'envie et le plaisir d'apprendre afin de lui permettre progressivement de devenir élève. Elle est adaptée aux besoins des élèves en situation de handicap pour permettre leur scolarisation. »

Ces éléments montrent l'importance de l'enjeu de l'école maternelle : rendre l'élève progressivement autonome, lui faire acquérir le langage oral et découvrir l'écrit, tout cela en prenant en compte le rythme propre de l'enfant, tout en le préparant à la scolarité élémentaire.

Programme
=========

Une école de « plein exercice » pour « permettre une première expérience réussie ». Ce qui suit détaille le programme des EM de 2015 (`BO spécial n° 2 du 26 mars 2015 <http://www.education.gouv.fr/pid25535/bulletin_officiel.html?cid_bo=86940>`_).

1. Ce qu'est l'EM et sa pédagogie
---------------------------------

La première partie du programme définit ce que doit être l'école maternelle et caractérise la pédagogie qu'il convient de mettre en œuvre (mise en avant du rôle de l'enseignant et de ses gestes professionnels).

L'EM est un cycle unique
````````````````````````
Ce cycle est de plus fondamental pour la réussite de tous. Ce temps de scolarité, bien que non obligatoire, établit les fondements éducatifs et pédagogiques sur lesquels s'appuient et se développent les futurs apprentissages des élèves pour l'ensemble de leur scolarité. La mission principale de l'EM est de donner envie aux enfants d'aller à l'école pour apprendre, affirmer et épanouir leur personnalité. L'EM :

* s'adapte aux jeunes enfants ;
* organise des modalités spécifiques d'apprentissage ;
* permet aux enfants d'apprendre ensemble et de vivre ensemble ;
* pense  l'évaluation  comme  un  processus.

Développons ce dernier point. L’évaluation est perçue comme un outil de régulation plutôt que comme un instrument  de  prédiction ni un jugement. Elle informe  sur  l'évolution  de l’élève : les  progrès  au  fil  de l’année *vs*. les  attendus  de fin  de  cycle (bilan de fin de cycle renseigné et remis aux familles à la fin du Cycle 1). Ce bilan reprend les cinq domaines des programmes et une rubrique « apprendre et vivre ensemble ». Au total, des items à renseigner selon trois degrés d’échelle : ne réussit pas encore/est en voie de réussite/réussit souvent. L’enseignant précise également les points de réussite et ceux à améliorer. C'est de plus un processus  continu sur les progrès :  observation  et  interprétation  au  quotidien  de ce que l'enfant  dit  ou fait  pour  mettre  en  valeur  son  cheminement  et  ses  progrès (mise en place d’un carnet de suivi des apprentissages avec des traces peuvent prendre des formes variées : photos, dessins, écrits, productions, enregistrements vidéo ou sonores, etc.).

Il est donc important d'être attentif  à ce que chacun peut faire  seul, avec l'aide  des  autres, ou avec le soutien de l'enseignant :

* l'élève doit pouvoir   identifier  ses  réussites, en  garder  des  traces, s'en souvenir en les évoquant, les revisiter afin de percevoir leur évolution ;
* les  enseignants rendent explicites pour  les  parents  les  démarches,  les attendus  et  les  modalités d'évaluation ;
* l’évaluation est un outil de travail essentiel au sein des équipes et un moyen de communication.

Au quotidien, cette évaluation positive et bienveillante :

* c'est encourager toutes les tentatives, mettre en évidence la plus petite des réussites ;
* c’est valoriser tous les « exploits » individuels, et en félicitant le groupe pour ses conquêtes, l’enseignant aide chacun au travers de nombreux tâtonnements à construire une plus grande estime de lui-même, à prendre confiance en lui et à désirer repousser ses limites.

Des gestes professionnels avec des modalités  spécifiques  d'apprentissage
``````````````````````````````````````````````````````````````````````````
**Apprendre en jouant**

  «L’équipe pédagogique aménage l'école (les salles de classe, les salles spécialisées, les espaces extérieurs…) afin d’offrir aux enfants un univers qui stimule leur curiosité, répond à leurs besoins notamment de jeu, de mouvement, de repos et de découvertes et multiplie les occasions d'expériences sensorielles, motrices, relationnelles, cognitives en sécurité. »  (BOEN, 2015).

Le  jeu  favorise  la  richesse  des  expériences vécues  des  enfants au niveau individuel et social, selon quatre principales catégories : exploration, symbolique, construction,  jeux à règles. On peut aller du jeu libre : plaisir de faire, de refaire, d’explorer, de découvrir, par tous les sens (toucher, vie, odorat, etc.), au  jeu structuré :  avec des objectifs d’enseignement pour acquérir explicitement des apprentissages.

De plus, le jeu (et plus largement l'activité) est l'un des moyens privilégiés de faire accéder à la plupart des champs de connaissance parmi lesquels le langage oral joue un rôle prioritaire y compris celui de l’enseignant qui se donne en spectacle, en verbalisant en direct ses procédures, ses choix cognitifs (par exemple, pour bien écrire, je vais choisir mon stylo noir fin et je vais m’installer bien assis en tenant avec ma main qui n’écrit pas ma feuille, ensuite je, etc.).

Le Programme de l'EM (BOEN,2015) signale deux dimensions du jeu :

* *Une dimension de transversalité* : « Le jeu favorise la richesse des expériences vécues des enfants dans l'ensemble des classes de l’école maternelle et alimente tous les domaines d’apprentissages. »
* *Une dimension de progressivité au fil du cycle* : « Ils constituent un répertoire commun de pratiques, d’objets et de matériels (matériels didactiques, jouets, livres, jeux) pour proposer au fil du cycle un choix de situations et d’univers culturels à la fois variés et cohérents. »

On aura donc un temps pour jouer dans la classe, avec une différenciation du matériel, des aides, etc. : jouer devant eux en utilisant du vocabulaire spécifique, jouer avec, jouer avec des pairs, jouer seul, etc., où on pourra aussi observer les élèves en train de faire.

**Apprendre  en  réfléchissant  et  en  résolvant  des problèmes**

* mettre  les  élèves  en  réflexion ;
* activités  cognitives   fondamentales : donner  aux  enfants  l'envie d'apprendre et  les  rendre autonomes  intellectuellement.

**Apprendre  en  s'exerçant**

* nombreuses  répétitions dans  des  situations  variées.
* possible  si  l'enfant comprend ce qu'il est en train d'apprendre et de  percevoir ses progrès.

**Apprendre  en  se  remémorant  et en mémorisant**

* l'enseignant  organise, catégorise,  aide  à  mémoriser, stabilise les informations  et  aide  à  se remémorer avec des outils (répertoire des albums lus, des comptines connues, l’album photo de vie de la classe, etc.).

On dépassera le « faire » pour parvenir à  « penser le faire » avec un  passage progressif, de l’action à la réflexion sur l’action, avec une articulation  autour de quatre mots clefs : « faire »,  « réussir »,  « penser », « comprendre ».

2. Les objectifs visés et leur progression
------------------------------------------

Voici ce qui est attendu des enfants en fin d'EM, distribués en cinq domaines d’apprentissage :

* mobiliser le langage dans toutes ses dimensions ;
* agir, s’exprimer, comprendre à travers l’activité physique ;
* agir, s’exprimer, comprendre à travers les activités artistiques ;
* construire les premiers outils pour structurer sa pensée ;
* explorer le monde.

Ils doivent  permettre une organisation des apprentissages par les enseignants. Le site `Eduscol <http://eduscol.education.fr/>`_ présente des ressources variées pour appliquer ce programme.

À retenir
---------

* L’EM est une école à part entière  avec, comme fondement éducatif, la réussite de tous les enfants (lutter contre les déterminismes sociaux).

* C’est une école bienveillante et adaptée dont la pédagogie s’organise autour du développement de l’enfant et qui accompagne les transitions avec une prise en compte des décalages dus à des différences de maturité entre élèves (jusqu’à 11 mois d’écart en terme d’âge, ce qui représente par exemple à 4 ans, 25 % de vie en plus).

* D'un point de vue éducatif et pédagogique, les enseignants d’EM sont tout spécialement attentifs aux rythmes des enfants, et considèrent que les différentes phases de la journée sont autant de moments à la fois éducatifs : accueil, pauses de collations, restauration scolaire, etc., et d’apprentissage (un important travail sur  le langage oral et écrit   peut être effectué lors de la collation : nommer/désigner des objets ; et aussi un travail en lien avec « explorer  le monde », etc.).

* Bautier :cite:`bautier08` note que les domaines sont en fait centrés sur l'acquisition de connaissances plus formelles et intellectuelles qu'on peut le penser (*cf*. les compétences qui leur correspondent), ce qui peut gêner le caractère d'initiation au métier d'élève (*cf*. le domaine « Devenir élève ») et à l'épanouissement que devrait peut-être avoir cette école, afin de lutter le mieux possible contre l'échec scolaire et les inégalités sociales. Pour cela, toute la professionnalité des enseignants d’école maternelle se manifestera dans les liens effectués entre ces  domaines et « les mondes des enfants » : ce qui intéresse tous les enfants de 3 ans, ou de 4 ans ou de 5 ans :cite:`boisseau05,brigaudiot00`, mais aussi dans le rapport à l’école et au savoir des parents. Dans ce domaine de compensation des inégalités sociales, leur sécurisation et la connaissance de ce qui de fait à l’école est un élément primordial.

* Il faut enfin comprendre la logique des parents qui s’appuient sur l’école maternelle  comme un mode de garde de leurs enfants, absolument nécessaire dans ce temps de famille parfois « atomisée », à proximité immédiate de leur habitat et souvent sans autre solution de garde possible (par exemple, très peu de crèches en milieu rural) : un lieu reconnu et gratuit, ce qui n’est pas un point de détail quand on connaît le nombre d’enfants issus de familles défavorisées.  Ainsi de plus en plus de parents demandent cette scolarisation considérant la fréquentation de l’EM comme une chance pour leurs enfants.

Ce que l'on sait
================

L'EM et la réduction des inégalités
-----------------------------------

Le rapport du Haut conseil de l’Education (HCE, 2007) insiste sur une évidence : « Il ne peut pas y avoir d’apprentissages fondamentaux (Cycle 2, GS-CE1) sans apprentissages premiers (Cycle 1), et il y a trop d’élèves qui arrivent en CP avec de trop grandes difficultés de maîtrise de langage oral. La responsabilité de l’EM à ce sujet, sans être unique, doit donc être évaluée. Le HCE montre un décalage entre les programmes (pédagogie par le jeu, importance des plans relationnels et affectifs) et la réalité des pratiques, avec certains enseignants de GS alignant leurs méthodes à celles de l’école élémentaire. Les raisons en sont multiples (manque de formation des personnels, pression des familles pour que l’EM ressemble à l’école élémentaire).

La majorité des travaux (assez rares, toutefois) à propos des effets de la scolarisation en école maternelle souligne que lorsqu'on compare les performances d'élèves scolarisés à temps plein *vs*. mi-temps en EM, les premiers réussissent mieux dans les matières « de base » (alphabétisation, lecture et maths), à court terme, et en particulier pour les enfants des milieux défavorisés. Les effets sont visibles au moins 2 ans après l'entrée en EM (INRP-VST, 2005).

D'après une enquête de panel, les élèves entrés au CP en septembre 2011 affichent des scores nettement supérieurs à ceux des élèves entrés au CP en 1997. Les progrès les plus importants sont enregistrés dans les domaines de la pré-lecture, de l’écriture et de la numération. Ces résultats s'expliquent en partie par l'évolution socio-économique des familles. Les progrès observés sont plus importants pour les élèves issus des catégories sociales les moins favorisées. Les inégalités sociales et scolaires ont ainsi tendance à se  réduire à l’issue de l’école maternelle (Andreu *et al*. :cite:`andreu14`).

Une brève histoire des EM françaises
------------------------------------

  « où l’aspect charitable doit s’atténuer au profit de l’aspect éducatif et de la nécessité d’un programme » (Arrêté du 28 avril 1848).

  « L’école est faite pour l’enfant et non l’enfant pour  l’école! Hélas, ces idées sont lentes à pénétrer les esprits. Depuis quelques années ceux qui aiment les enfants sont hantes par une inquiétude qui augmente chaque jour. Cette inquiétude est provoquée par le travail prématuré et disproportionné auquel sont soumis les jeunes intelligences… » (Pauline Kergomard, 1889)


Colnot :cite:`colnot` a réalisé un tableau historique de l'évolution de l'école maternelle française depuis sa création. En voici les principaux points dans le Tableau I ci-dessous. Comme le note Bautier :cite:`bautier08`, cette évolution est le reflet des conceptions du petit enfant, de ses besoins, de la professionnalisation des femmes, et surtout du rapport entre éducation familiale et scolaire.

**Tableau I - Bref historique de l'école maternelle française** (d'après :cite:`colnot`, ajouts de :cite:`collectif84,bautier08,rosenzweig97`.

+------------------+----------------------+------------------------+------------------------+
| Dates            | Modèles de pensée    | Elèves/enseignants     | Relation/familles      |
+==================+======================+========================+========================+
| 1825-30 Les      | Ancêtre de l'EM.     | El. = milieu           | Rapport de sujétion    |
| salles d'asile   | Privé,               | populaire, familles    | des familles, qui      |
|                  | confessionnel,       | dans le besoin. Ens.=  | n'ont pas leur mot à   |
|                  | charité.             | dames patronnesses et  | dire.                  |
|                  |                      | curés.                 |                        |
+------------------+----------------------+------------------------+------------------------+
| 1880 Naissance   | Laïcisation des      | El. = milieu           | "l'EM doit former le   |
| de l'EM (IIIe    | salles d'asile.      | populaire. 1ers        | passage de la famille  |
| rép.).           | Centration sur       | instituteurs =         | à l'école".            |
| Passerelle       | l'instruction et     | recrutés dans classes  | Passerelle. Pour       |
|                  | éducation morale.    | "méritantes".          | autant, l'EM n'a pas à |
|                  | Chaque classe avait  |                        | *préparer* à l'école   |
|                  | en moyenne 150       |                        | élémentaire, mais à    |
|                  | élèves.              |                        | l'apprentissage de la  |
|                  |                      |                        | vie sociale. Lien      |
|                  |                      |                        | école/famille          |
|                  |                      |                        | important              |
+------------------+----------------------+------------------------+------------------------+
| 1890-1910 -      | Abri social/garderie | EM = abri pour enfants | Remplacer la famille   |
| L'école asile –  | pour sauvegarder les | classe laborieuse et   | dans les classes       |
| Remplacement     | enfants des dangers  | indigente; structure   | populaires. "Imite les |
|                  | de la rue (P.        | de substitution        | procédures de la mère  |
|                  | Kergomard).          |                        | intelligente et        |
|                  | Hygiénisme. On       |                        | dévouée" (IO 1908)     |
|                  | apprend à lire en    |                        |                        |
|                  | fin d'EM, jusque     |                        |                        |
|                  | vers 1940.           |                        |                        |
+------------------+----------------------+------------------------+------------------------+
| 1921-1975 - L'EM | 3 missions :         | 1950: élargissement à  | Provenance et          |
| Tripolaire       | garderie,            | toutes les couches     | exigences de parents   |
|                  | préparation école    | sociales; puis aussi   | se diversifient.       |
|                  | élémentaire, basée   | les classes moyennes   | Demande de relations   |
|                  | sur les besoins de   | (1960).                | école/famille qui      |
|                  | l'enfant.            |                        | évoluent. L’EM devient |
|                  |                      |                        | valorisée par les      |
|                  |                      |                        | classes moyennes et    |
|                  |                      |                        | supérieurs après 1950. |
+------------------+----------------------+------------------------+------------------------+
| 1970-80 – EM et  | Ses 3 missions sont  | Instituteurs recrutés  | Rupture avec le modèle |
| les  théories    | réaffirmées, et      | dans les classes       | précédent              |
| psychologiques - | revisitées avec les  | moyennes. Moins de     | (garde+instruction ).  |
| Modèle           | principes de la      | lien école/famille     | Importance de          |
| expressif.       | psychologie du       |                        | l'expression/épano     |
|                  | développement, et la |                        | uissement. L'enfant    |
|                  | détection des        |                        | n'est plus à           |
|                  | handicaps.           |                        | "dresser". "Tout se    |
|                  | L'initiation à la    |                        | passe avant 6 ans »    |
|                  | lecture disparaît,   |                        | (cf. Dodson)           |
|                  | au profit de la mise |                        |                        |
|                  | en place des         |                        |                        |
|                  | *conditions*         |                        |                        |
|                  | d'acquisition de la  |                        |                        |
|                  | lecture (perception  |                        |                        |
|                  | rythme, sons,        |                        |                        |
|                  | formes, code).       |                        |                        |
+------------------+----------------------+------------------------+------------------------+
| Fin 1980 - l'EM  | Loi orientation 1989 | Recrutement niveau     | L’EM est               |
| est une école.   | : 1. Scolariser      | licence. Ecart entre   | complémentaire de      |
| Complément avec  | (découvrir le        | les enseignants et les | l'éducation familiale  |
| famille          | plaisir              | familles populaires.   | et elle prépare à la   |
|                  | d'apprendre), 2.     |                        | scolarité élémentaire. |
|                  | socialiser (coopérer |                        | Les parents sont des   |
|                  | avec d'autres) ; 3.  |                        | partenaires. La        |
|                  | faire apprendre et   |                        | fonction de garde      |
|                  | exercer (développer  |                        | disparaît.             |
|                  | ses capacités,       |                        |                        |
|                  | autonomie, enrichir  |                        |                        |
|                  | ses connaissances).  |                        |                        |
|                  | Apprentissages       |                        |                        |
|                  | structurés et        |                        |                        |
|                  | interventions        |                        |                        |
|                  | didactiques          |                        |                        |
|                  | rigoureuses.         |                        |                        |
+------------------+----------------------+------------------------+------------------------+
| Fin 1990-2000 -  | Circulaire de 1999   | Hétérogénéité accrue   | Confiance et           |
| L'école de la    | sur la maîtrise des  | des élèves Accueil des | information réciproque |
| parole -         | langages. Programme  | tout-petits.           | entre école-famille.   |
| Coéducation      | de 2002 : usage      |                        | Tend à valoriser les   |
|                  | efficace de la       |                        | classes moyennes et    |
|                  | langue parlée. « Le  |                        | supérieures : elle     |
|                  | langage est la       |                        | suppose un partage des |
|                  | priorité des         |                        | savoirs qui du coup ne |
|                  | priorités… Le        |                        | sont plus enseignés,   |
|                  | langage au cœur des  |                        | au profit de           |
|                  | apprentissages… »    |                        | l'autonomie, la        |
|                  |                      |                        | socialisation. Les     |
|                  |                      |                        | familles les moins     |
|                  |                      |                        | favorisées ne          |
|                  |                      |                        | participent pas autant |
|                  |                      |                        | que les autres à cette |
|                  |                      |                        | coéducation.           |
+------------------+----------------------+------------------------+------------------------+

L’EM est une *école* de plein exercice : bienveillante et exigeante (lien entre les fonctions maternelle et paternelle) qui doit concilier de multiples rôles parfois paradoxaux : être un mode de garde mais aussi effectuer de nombreux liens avec les familles, la santé des élèves en lien avec leur besoins physiologiques et la nécessité de dépistage, l’épanouissement et la nécessaire frustration (attendre son tour, partager, etc.), la mise en œuvre de principes éducatifs (règles, effort, aller au bout, projets, etc.), la découverte de l’altérité, l’acculturation, l’envie d’apprendre et apprendre.

La scolarisation des tout-petits
--------------------------------

Déjà avant la déclaration du ministre de l'éducation nationale Xavier Darcos à propos de la scolarisation des 2-3 ans : "Est-ce qu'il est vraiment logique [...] que nous fassions passer des concours bac +5 à des personnes dont la fonction va être essentiellement de faire faire des siestes à des enfants ou de leur changer les couches ? ", un débat assez important s'est tenu quant à l'intérêt de scolariser cette tranche d'âge. Il faut déjà noter que peu de pays européens scolarisent les enfants de cet âge (la Belgique, notamment). Florin :cite:`florin00` note que, d'une part, la possibilité d'un tel accueil existe depuis longtemps (1881), mais aussi qu'il n'a pas évolué depuis les années 1980 : 32 % des enfants scolarisés ont entre 2-3 ans en 2002, pour seulement 10 % en 1960. La loi d'orientation de 1989, en massifiant la scolarisation des enfants de 3 ans, indique  que "L'accueil des enfants de 2 ans est étendu en priorité dans les écoles situées dans un environnement social défavorisé, que ce soit dans les zones urbaines, rurales ou de montagne" (loi du 10/7/89). De plus, de très grandes disparités existent entre départements (de 15 % à 90 %, avec les plus forts taux dans l'ouest, le nord et le centre de la France, voir DEP 2003).

Un argument souvent avancé est celui qui montre que plus la scolarité pré-élémentaire est longue, moins les élèves redoublent ensuite (notamment au CE2) : 91 % des enfants scolarisés à 2 ans passent en CE2, contre seulement 77 % de ceux scolarisés à 4 ans et plus, mais 88 % de ceux scolarisés à 3 ans (DEP 2003). Florin :cite:`florin00` montre que ce gain de 2 % est minime, et qu'il ne prend pas en compte d'autres facteurs importants (quel mode de garde des enfants non scolarisés ? quel avis des parents sur la scolarisation précoce?), qui peuvent eux aussi influer sur ce taux d'accès.

Il existe de nombreuses études ayant évalué les effets de cette scolarisation (voir Florin :cite:`florin00` pour une synthèse). Elles ne concluent pas toujours sur des effets importants, mais peuvent montrer  des acquisitions langagières un peu meilleures. Ce sont les élèves des catégories sociales extrêmes (les plus et les moins favorisées) qui bénéficient le plus de la scolarisation précoce. Elle est aussi plus bénéfique dans des écoles classées ZEP (Zone d'éducation prioritaire) que hors ZEP (DEP 2003). Enfin, le fait que ce soient les enseignants qui scolarisent le plus leurs jeunes enfants en maternelle peut montrer qu'ils trouvent là une réponse à une demande d'éducation plus que de garde.

En outre, en 2011 comme en 1997, les élèves scolarisés à deux ans ont des performances supérieures en moyenne à celles des élèves scolarisés à trois ans, comprises entre 10 % et 20 % d’écart-type selon les dimensions. (Extrait de `Forte augmentation du niveau des acquis à l’entrée au CP en 2011 <http://www.education.gouv.fr/cid73252/forte-augmentation-du-niveau-des-acquis-des-eleves-a-l-entree-au-cp-entre-1997-et-2011.html>`_).

On précise cette scolarisation des tout-petits dans la Loi d’orientation et de programmation pour la refondation de l’École de la République mardi 9 juillet 2013 (JO, 2013). La scolarisation précoce d'un enfant de moins de 3 ans est une chance pour lui et sa famille lorsqu'elle est organisée dans des conditions adaptées à ses besoins. C'est en particulier un levier essentiel pour la réussite scolaire des enfants de milieux défavorisés.

La scolarisation des moins de 3 ans est très inégale selon les territoires et elle a fortement diminué ces dernières années. Pour faire de l'école maternelle un atout dans la lutte contre la difficulté scolaire, l'accueil des enfants de moins de 3 ans sera privilégié dans les secteurs de l'éducation prioritaire, dans les secteurs ruraux isolés et dans les départements et régions d'outre-mer.

  « Dans les classes enfantines ou les écoles maternelles, les enfants peuvent être accueillis dès l'âge de deux ans révolus dans des conditions éducatives et pédagogiques adaptées à leur âge visant leur développement moteur, sensoriel et cognitif, précisées par le ministre chargé de l'éducation nationale. Cet accueil donne lieu à un dialogue avec les familles. Il est organisé en priorité dans les écoles situées dans un environnement social défavorisé, que ce soit dans les zones urbaines, rurales ou de montagne et dans les régions d'outre-mer. » (Art. 8)

**Tableau II** – Lien entre scolarisation en EM et non redoublement (MEN, 2004).

+-------------------------+----------------------------------------+
| Nombre d’années à l’EM  | Scolarité ordinaire sans redoublement  |
+=========================+========================================+
| 4                       | 79,2 %    (+ 1,8 %)                    |
+-------------------------+----------------------------------------+
| 3                       | 77,4 %                                 |
+-------------------------+----------------------------------------+
| 2                       | 72 %                                   |
+-------------------------+----------------------------------------+
| 1 ou moins              | 63,1 %                                 |
+-------------------------+----------------------------------------+

**Tableau III** – **Effet de la scolarisation à 2 ans** sur les notes moyennes aux évaluations CP-CE1.

+-----------------+-----------------------------------------------------------+
| Evaluation      | Gains pour les élèves scolarisés à 2 ans sur 100 points   |
+=================+===========================================================+
| Début CP        | de 2,2 à 4,2                                              |
+-----------------+-----------------------------------------------------------+
| Fin CP          | de 3,2 à 3,8                                              |
+-----------------+-----------------------------------------------------------+
| Fin CE1         | de 3,2 à 4,2                                              |
+-----------------+-----------------------------------------------------------+

On montre des effets  positifs de la scolarisation à 2 ans : « quoique plus limités que ceux d’une scolarisation à 3 ans par rapport à une entrée à l’école à 4 ans dans le développement langagier, les apprentissages fondamentaux et les parcours scolaires sans redoublement [...] Les petits sont tout aussi sécurisés affectivement à l’école qu’ils le sont à la crèche [...] Les effets positifs sont surtout repérés pour les milieux défavorisés mais ils existent aussi dans les autres milieux [...]» :cite:`florin00,florin04`

Ce que l'on peut faire
======================

L’EM va effectuer des liens entre les besoins physiologiques, des principes éducatifs et des apprentissages spécifiques avec des temps structurés en ateliers, en demi groupes, en grand groupes. Il faut notamment faire attention au modèle dominant des ateliers, à n’utiliser que si les obstacles d’apprentissage sont nombreux (par exemple apprendre à parler ou à écrire, ou si des contraintes de matériel à disposition existent, en identifiant bien leur nature : - apprentissage ou réinvestissement ou transfert, - leur organisation matérielle et spatiale, - et avec qui ? - présence de l’ATSEM, - en groupes homogènes ou hétérogènes). Les rituels existent pour sécuriser en terme de rites mais ils évoluent aussi au fil de l’année scolaire et des années d’école maternelle afin de montrer que l’on devient grand...

L’idée forte c’est de faire que le tout-petit soit tout d’abord bien dans son école maternelle en ressentant l’idée que « mon école c’est la meilleure du monde » (:cite:`brigaudiot00`, p. 58). Pour cela l’enseignant va « suivre les mondes des enfants (*id*., page 41) en travaillant sur ce qui les concerne tous : les animaux, le jeu, la famille, la nourriture ». Idée reprise par Boisseau :cite:`boisseau05` où il lie les thèmes les plus populaires entre 3 et 5 ans avec les apprentissages langagiers. Un moment où se croisent la logique de l’enfant, celle de l’enseignant et celle des programmes. Le Tableau IV ci-dessous contient quelques préconisations, section par section.

**Tableau IV – Préconisations d'activités par section** d'après :cite:`collectif84`.

+---------------------------+------------------------------------------------------------------+
| Section/Besoins-Objectifs | Types d'activités possibles                                      |
+===========================+==================================================================+
| **Section des petits**    | **L'intérêt dominant est le jeu avec une activité sensori-       |
|                           | motrice intense: courir, se sauver, remplir, écraser, goûter,    |
|                           | serrer, emboîter… faire, re-faire encore et encore… Mais aussi   |
|                           | de calme et de repos… S’isoler aussi.**                          |
+---------------------------+------------------------------------------------------------------+
| Besoin de sécurité        | Importance de l’accueil et  création de différents espaces       |
|                           | identiques dans la durée. Importance des repères affectifs,      |
|                           | temporels (rituels) Construction de son  identité et             |
|                           | participation au devenir élève. Le doudou, un objet              |
|                           | intermédiaire entre le dedans et le dehors, entre le concret et  |
|                           | l'imaginaire « un objet transitionnel » souvent essentiel pour   |
|                           | pallier l’angoisse de la séparation.                             |
+---------------------------+------------------------------------------------------------------+
| Objectif de socialisation | Créer des coins jeux/ateliers pour favoriser des situations de   |
|                           | communication. Mener des manipulations et des découvertes.       |
|                           | Importance des grands jeux au sol. Faire avec, jouer avec  les   |
|                           | élèves et parler de ce qu’on a fait. Mimétisme,  imitation  avec |
|                           | des simulations de dormir ou manger par exemple: vers l'accès à  |
|                           | la fonction symbolique.                                          |
+---------------------------+------------------------------------------------------------------+
| Besoin de mouvement       | Importance de prendre en compte les besoins de mouvement, de     |
|                           | repos, affectifs, tous dans une grande sécurité.                 |
+---------------------------+------------------------------------------------------------------+
| **Section des moyens**    | **Etape de transition (consolider les acquis de la petite        |
|                           | section) : temps privilégié de l'expression de soi (langage,     |
|                           | corps, etc.)**                                                   |
+---------------------------+------------------------------------------------------------------+
| Sécurité                  | Evolution des rituels. Faire prendre en charge l’organisation de |
|                           | certains ateliers par les élèves. Donner à l’enfant des repères  |
|                           | sur La famille/ l’école, l e temps qui passe  et les lieux.  Le  |
|                           | doudou  est progressivement délaissé au profit du doudou         |
|                           | collectif… des jeux symboliques, des premiers  apprentissages    |
|                           | structurés...                                                    |
+---------------------------+------------------------------------------------------------------+
| Mouvement                 | Besoin de mouvement intensifié (aménagement de l’espace), mais   |
|                           | aussi restreint (meilleur contrôle).                             |
+---------------------------+------------------------------------------------------------------+
| Socialisation             | Regroupements riches, structurés. Lors de la socialisation       |
|                           | l’enfant se voit transmettre « les éléments socioculturels » et  |
|                           | il acquiert des «manières de faire, de penser, de ressentir».    |
|                           | Expliquer à ses parents ce qu’il fait à l’école.                 |
+---------------------------+------------------------------------------------------------------+
| Expression                | Chaque instant est temps de langage. Proposer des moyens pour    |
|                           | inciter les élèves à se parler. Importance accrue de             |
|                           | l'expression manuelle et corporelle.                             |
+---------------------------+------------------------------------------------------------------+
| **Section des grands**    | **Importance de la confrontation avec les pairs. Identification  |
|                           | à des modèles non parentaux. Vers des apprentissages             |
|                           | structurés**                                                     |
+---------------------------+------------------------------------------------------------------+
| Désir de grandir          | La sieste disparaît. Autonomie dans l’habillage/déshabillage.    |
|                           | Comportements autonomes en ateliers. Mener des expérimentations. |
|                           | Faire faire au P.E. Un être distinct, différencié de son         |
|                           | entourage. Intégrer le Non, la nécessité de l’acceptation de la  |
|                           | frustration pour grandir. On parle pour mieux agir, pour         |
|                           | comprendre, pour échanger des manières de faire, pour dégager    |
|                           | des règles d’action ou des conditions d’efficacité.              |
+---------------------------+------------------------------------------------------------------+
| Besoin d'ouverture aux    | Respect des normes (des règles explicites, qui orientent le      |
| autres et au monde        | comportement des individus: se laver les mains, manger avec des  |
|                           | couverts, arriver à l’heure, trier ses déchets…) et des valeurs  |
|                           | (des principes moraux, idéaux auxquels les membres d’une société |
|                           | adhèrent: effort, solidarité, honnêteté…) sociales (lecture).    |
|                           | Déplacements dans l'école et hors de l’école.                    |
+---------------------------+------------------------------------------------------------------+
| Besoin de s'organiser     | Prise en charge par les élèves  de certains rituels. Commencer à |
| dans le travail           | laisser un degré d’autonomie notamment matérielle aux élèves     |
|                           | dans l’organisation de leur travail. Auto-régulation et          |
|                           | évaluation formatrice.                                           |
+---------------------------+------------------------------------------------------------------+

Quizz
=====

.. eqt:: ecolematernelle-1

  **Question 1. En quelle année les écoles maternelles ont-elles vu le jour en France ?**

  A) :eqt:`C` `1881`
  B) :eqt:`I` `1830`
  C) :eqt:`I` `1890`
  D) :eqt:`I` `1921`

.. eqt:: ecolematernelle-2

  **Question 2.  De quelle année date la loi française redéfinissant les missions de l'école maternelle en instaurant un cycle unique ?**

  A) :eqt:`I` `2008`
  B) :eqt:`C` `2013`
  C) :eqt:`I` `2002`
  D) :eqt:`I` `1999`

.. eqt:: ecolematernelle-3

  **Question 3.  Quel est l'ancêtre de l'école maternelle ?**

  A) :eqt:`I` `L'abri social`
  B) :eqt:`I` `La crèche`
  C) :eqt:`C` `La salle d'asile`
  D) :eqt:`I` `La garderie`


Questions d'analyse de pratiques
================================

#. Lire attentivement les programmes de l'école maternelle. Mettre en évidence ce qui concerne : 1°) les besoins des enfants/élèves ; 2°) le cadre dans lequel il peuvent être satisfaits ; 3°) ce que cela implique comme activité de l'enseignant.

#. Concevoir un atelier en prenant en compte : leur organisation dans la classe (spatiale) et aussi humaine (comment accéder à un atelier) ; les aspects temporels ; la manière dont le matériel est rangé et accessible aux élèves.

Références
==========

Textes législatifs
------------------

* BOEN (2008). `Hors-série n° 3 du 19 juin 2008 : programme de l'école maternelle <http://www.education.gouv.fr/bo/2008/hs3/programme_maternelle.htm>`_.
* BOEN (2013). `Scolarisation des élèves de moins de 3 ans (15 janvier) <http://www.education.gouv.fr/pid25535/bulletin_officiel.html?cid_bo=66627>`_.
* BOEN (2015). `BO spécial n° 2, programme de l'EM (26 mars) <http://www.education.gouv.fr/cid87300/rentree-2015-le-nouveau-programme-de-l-ecole-maternelle.html>`_.
* Code de l'éducation (2013). `Art. L113-1 du Code de l'éducation <https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071191&idArticle=LEGIARTI000027682617>`_
* JO (2013). `Loi d’orientation et de programmation pour la refondation de l’École de la République <https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000027677984&dateTexte=&categorieLien=id>`_
* JO (2015). `Fixant les programmes d'enseignement des Cycles 2 à 4 (24 nov.) <https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000031518715&dateTexte=&categorieLien=id>`_

Autres références
-----------------

* HCE (2007). `L’école primaire, bilan des résultats de l’école <http://www.hce.education.fr/gallery_files/site/21/40.pdf>`_. Paris : HCE.
* INRP-VST (2005). `L'éducation avant 6 ans <Disponible à http://www.inrp.fr/vst/LettreVST/octobre2005.htm>`_. *La lettre d'information de la VST*,  11.
* `L'école maternelle (mis à jour en 2016) <http://www.education.gouv.fr/cid166/l-ecole-maternelle-organisation-programme-et-fonctionnement.html>`_
* `Textes de référence sur l'école maternelle <http://eduscol.education.fr/D0101/programmes-textes-reference.htm>`_.
* DEP (2003). `Faut-il développer la scolarisation à 2 ans ? <http://www.education.gouv.fr/cid5049/01.-faut-il-developper-la-scolarisation-a-deux-ans.html>`_ *Education et Formations*, *66*.
* Luc, J.-N. (Ed.) (1999). `L'école maternelle en Europe (XIXe-XXe siècles) <http://www.inrp.fr/publications/edition-electronique/histoire-education/RH082.pdf>`_. *Histoire de l'éducation*,  82 [Numéro spécial].
* MEN (2002). `Qu'apprend-on à l'école maternelle ? <http://www.cndp.fr/ecole/quapprend/pdf/755a0211.pdf>`_ Paris : CNDP-XO.
* MEN (2003). `Pour une scolarisation réussie des tout-petits <http://www2.cndp.fr/archivage/valid/43843/43843-7071-7029.pdf>`_. Paris : CNDP.
* MEN-DGES (2004). `L'école maternelle en France <http://eduscol.education.fr/D0039/ecole-maternelle.htm>`_. Paris : MEN.

Articles et ouvrages
--------------------

.. bibliography::
    :cited:
    :style: apa

