(peda-projet)=

# Projets et pédagogie de projet

```{index} single: auteurs; Campanale, Françoise single: auteurs; Charroud, Christophe single: auteurs; Dessus, Philippe single: auteurs; Besse, Émilie
```

:::{admonition} Information
- **Auteurs** : Françoise Campanale, Christophe Charroud, et [Philippe Dessus](http://pdessus.fr/), LaRAC & Inspé, Univ. Grenoble Alpes.  Le quizz a été réalisé par Emilie Besse, projet [ReFlexPro](http://www.idefi-reflexpro.fr).
- **Date de création** : Décembre 2006, mis à jour en Mars 2009, Février 2018 et Février 2020.
- **Date de publication** : {sub-ref}`today`.
- **Statut du document** : Terminé.
- **Résumé** : La référence à la notion de projet est omniprésente, que ce soit dans la société (projet d'entreprise, de carrière, de vie) ou dans l'école (projet d'école, de cycle...). Ce document présente quelques indications théoriques sur cette notion, et recense quelques exemples de mise en œuvre de projets à l'école (principalement primaire). Il distingue les projets institutionnels (d'école) et la pédagogie de projet (dans les classes).
- **Lire aussi** : Doc. {ref}`ens_explicite`.
- **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.
- **Licence** : Document placé sous licence *Creative Commons* : [BY-NC-SA](http://creativecommons.org/licenses/by-nc-sa/3.0/fr/).
:::

## Introduction

Le terme de {index}`projet` est récent, et n'est couramment employé qu'à partir du milieu du XX{sup}`e` siècle (voir {cite}`cros94`, pour un historique). Il a souvent une connotation positive par son assonance avec progrès. Boutinet {cite}`boutinet92` le définit comme « une anticipation opératoire, individuelle ou collective d’un futur désiré ».

Si l'on estime que toute activité d'enseignement, à l'école, est   intentionnelle, alors on peut dire qu'elle remplit en partie un   projet. Il existe, dans le discours contemporain, de très nombreuses   occurrences du mot “projet” {cite}`courtois97` : management par projet, projet d'entreprise, projet d'orientation professionnel, projet de vie, etc. Dans le domaine de l'éducation, et depuis la Loi d'orientation de 1989, chaque école doit se doter d'un projet (voir MEN, 1990), bien qu'il existe de nombreux antécédents (*voir plus bas*).

## Ce que l'on sait

### Les origines multiples du concept de projet

- *Dans le champ de l’architecture*, à la Renaissance, le projet est un aménagement de l’espace. Il est la référence commune négociée entre le maître d’ouvrage ou le commanditaire et le maître d’œuvre, celui qui réalise. Il est d’abord maquette, aménagement potentiel avant de devenir réalisation concrète. Cette approche met l’accent sur les deux dimensions virtuelle et réelle du projet, sur la distinction conception/réalisation et la dimension négociation.
- *Dans le champ de la philosophie*, à partir du XVIII{sup}`e` siècle, et surtout avec la philosophie existentialiste (Sartre : « c’est en se projetant hors de lui et en se perdant que l’homme fait exister l’homme ») : le projet est lié à l’exercice de la liberté, il est développement de l’être. Il traduit comment l’être se situe dans et par rapport au monde. Cette approche met l’accent sur la dimension relationnelle du projet.
- *Dans le champ de la psychologie américaine* (début XX{sup}`e` siècle) avec [Dewey](https://fr.wikipedia.org/wiki/John_Dewey) et [Kilpatrick](https://fr.wikipedia.org/wiki/William_H._Kilpatrick) (pères de la pédagogie du projet, voir plus bas, et {cite}`cros94`). Tout apprentissage est finalisé, c’est toujours un projet. L’activité des individus est stimulée et orientée par des buts. L’élève apprend en faisant quelque chose qu’il s’est proposé de réaliser. Cette approche met l’accent sur la motivation liée au projet et l’articulation des fins et des moyens. Il y a tension entre pédagogie de projet et projet pédagogique.
- *Dans le champ de la sociologie*. L’étude des organisations, dans les sociétés industrielles, met l’accent sur la volonté de contrôle collectif et l’exigence de créations personnelles, sur la diversité des projets, les conflits de projets, l’articulation difficile entre projet collectif et projets individuels.

### Le projet omniprésent, même dans l'éducation

Actuellement, le terme figure partout (projet de société, de vie, de vacances, de carrière…, voir {cite}`boutinet93` pour une revue des différents types de projets et {cite}`reverdy13` pour une synthèse). Le projet s’est fait méthodologie, particulièrement dans les champs de la gestion des entreprises et de la pédagogie, à la recherche de l’efficacité.

La pédagogie s’en est emparée, par volonté de mobiliser, motiver, accroître l’efficacité de l’acte éducatif : pédagogie de projet, à la suite du mouvement de l’Education nouvelle et notamment de Dewey. Dans les Instructions officielles, il fait son apparition dans les années 1970 (en 1973, les 10 %, en 1979, les PACTE, Projets d’action culturelle, technique et éducative, en 1981, les PAE, Projets d’action éducative, voir {cite}`bru91,cros94` pour une perspective historique). La Loi d'orientation de juillet 1989 institutionnalise le projet d'établissement. L’institution promeut non seulement les projets institutionnels, mais aussi la pédagogie de projet. En raccourci, un projet est un "acte de création dans le temps" {cite}`chen19` (p. 71). Elle se caractérise par ({cite}`legrand83,krajcik06`) :

- Un sujet d’étude motivant pour l’élève, démarrant par une question intéressante, un problème à résoudre ;
- un travail en équipes des élèves, assisté par l'enseignant, dans un environnement authentique ;
- une autonomie guidée des élèves dans la détermination du sujet, de la démarche et de la réalisation ;
- une démarche anticipée, en partie planifiée par l'enseignant ;
- une production concrète attendue, valorisée socialement, accessible au public.

L’ensemble de ces caractéristiques conduisent à un modèle d’enseignement où le projet est un élément central, et dans lequel sont mises en place des conditions favorables pour que les élèves construisent des connaissances (cf. Document {ref}`constr_conn` pour plus d'informations sur ce point), s'engagent, se questionnent, développent des ressources et recherchent des solutions à leurs problèmes {cite}`lafortune10`. Ce modèle implique un projet formulé précisément, et il est généralement destiné à un public plus large que la classe {cite}`leduc14`.

Il faut toutefois distinguer plusieurs types de pédagogie du projet ({cite}`bru91`, p. 329) :

- "celle qui organise dans une architecture de projet, les activités qu'elle fait exercer aux élèves. Le projet est un cadre de travail ;
- celle qui assure l'éducation des activités de projet au même titre que celle des activités mathématiques littéraires, historiques, etc. Le projet est un objet d'éducation ;
- celle qui part des projets des élèves pour organiser leurs activités d'apprentissage. Le projet est un mobile et une méthode de travail ;
- c'est (en un sens très étroit) la planification et la division des tâches pour la production d'un objet ou d'un effet objectif. Elle peut combiner deux ou trois des caractères précédents."

### Avantages et inconvénients

Depuis Dewey et Kilpatrick aux Etats-Unis {cite}`landsheere92`, ou bien Freinet, Decroly, ou Cousinet en Europe, les avantages d'une pédagogie de projets sont nombreux et bien connus. D'une part, cela permet aux élèves de s'engager dans une activité qui a souvent plus de sens (puisque les connaissances sont engagées dans une action) et qui est moins morcelée (possibilité de travail interdisciplinaire), que les enseignements classiques ({cite}`tochon90`). Plus récemment, cette démarche est devenue très employée pour l'enseignement des sciences ({cite}`krajcik06`). Elle est devenue une manière constructiviste de présenter une tâche à l'élève qui lui permette de mieux comprendre le contenu enseigné.

Toutefois, les travaux ayant essayé d'évaluer les effets réels de la   mise en place de projets sont rares cite:`thomas00,helle06`. La plupart d’entre eux se limitent à une description d’un enseignement innovant et on constate qu’il est souvent difficile d’évaluer rigoureusement l’efficacité de cette approche sur les apprentissages des élèves à l’issue du projet.

Quelques avantages de la pédagogie de projet (qu’elle soit au niveau de l’école ou de la classe) ont toutefois été identifiés :

- elle est motivante pour les enseignants et les élèves, qui la préfèrent généralement à une pédagogie plus "traditionnelle" ;
- elle permet une plus grande collaboration entre enseignants, et une meilleure attitude envers l'apprentissage pour les élèves ;
- elle permet d'obtenir d'un peu meilleurs résultats à des épreuves standardisées que les autres modes d'enseignement ;
- elle permet aux élèves d'avoir de meilleures capacités de résolution de problèmes complexes, et de transfert de leurs compétences ;

Pour autant, il existe des inconvénients à cette démarche (les trois premiers points sont issus de {cite}`romero10`) :

- difficultés à apprendre ensemble ;
- difficultés à s’organiser, se coordonner,  à planifier, à gérer le temps du projet (régulation entre le temps de travail individuel et le temps commun) ;
- difficultés à communiquer entre membres du groupe (perte d’informations)
- et parfois des difficultés à utiliser l’environnement censé soutenir la réalisation du projet (*e.g.*, une plate-forme de travail collaborative)
- les élèves peu autonomes rencontrent plus de difficultés dans de telles situations (quoi chercher, comment ? pour quels résultats ?). L'enseignant doit particulièrement soigner leur assistance ;
- "Les conditions du projet sont malgré tout tributaires des orientations de l'enseignant. L'idéologie de l'épanouissement peut sombrer dans le dirigisme dogmatique, la manipulation et l'endoctrinement. Il existe une charnière subtile entre l'idée d'autonomie (voir document sur l'autonomie) et l'action visant à développer l'autonomie chez autrui : préconiser l'autonomie n'est pas l'imposer. La pédagogie de projet agit sur la base d'une contradiction fondamentale puisque le meilleur projet sera toujours celui qui jaillira spontanément des apprenants sans interférence aucune ; mais avec quelle pédagogie ?" ({cite}`tochon90`, p. 63).

### L'apprentissage par problèmes vs. par projet

La distinction entre l'apprentissage par problèmes (voir {doc}`ens_explicite` pour plus d'informations) et l'apprentissage par projets est souvent ténue. Toutes deux impliquent des élèves dans un travail en petits groupes collaboratifs, dans des situations authentiques et peu structurées ({cite}`jensen15`), leur demandant d'investir des connaissances et compétences de haut niveau (plutôt que du simple rappel de faits).

Toutefois, comme l'indique {cite}`jensen15`, elles diffèrent : le but final de l'apprentissage par problèmes est de résoudre un problème et sa solution est le produit livré par les élèves ; dans le cas d'un apprentissage par projets, le but est la livraison d'un produit, événement, ou présentation à une audience.

### Comment évaluer un projet ?

La pédagogie de projet induit un changement de fonctionnement, à prendre en compte dès la conception d’un enseignement appuyé sur une telle démarche. Dans un cadre scolaire “traditionnel”, l’élève est seul face à un problème isolé, abstrait clairement défini (les données sont disponibles, et le problème est bien posé) et à ce problème correspond la plupart du temps une solution unique. Dans une démarche de projet, en revanche, les élèves sont en équipe avec des problèmes liés, concrets souvent mal définis et ayant de nombreuses solutions possibles ce qui rend plus difficile l'évaluation de ses effets.

### La pédagogie de projet, est-ce que cela marche ?

Il existe très peu de recherches visant à évaluer, de manière globale, les effets de la pédagogie de projet sur l'apprentissage des élèves. Récemment, Chen et Yang {cite}`chen19` ont synthétisé une cinquantaine de résultats impliquant au total plus de 12 000 élèves de près de 200 écoles dans 9 pays. Les résultats montrent que la taille d'effet moyenne pondérée d'une telle pédagogie est de 0,71 (i.e., un élève moyen du groupe pédagogie de projet a des résultats supérieurs à 76 % des élèves du groupe pédagogie "ordinaire"), ce qui montre qu'elle a un effet moyen à élevé, comparativement à une pédagogie "ordinaire". Jensen {cite}`Jensen15`, elle, obtient une taille d'effet moyenne de 0.59 en se centrant sur les études dans l'enseignement secondaire (i.e., un élève moyen du groupe pédagogie de projet a des résultats supérieurs à 72 % des élèves du groupe pédagogie "ordinaire").

Cette étude montre aussi que la pédagogie de projet a plus d'effet dans l'enseignement des sciences sociales (histoire, géographie, langues) que dans l'enseignement de l'ingénierie ou de la technologie (cela n'est pas corroboré par Jensen au niveau du secondaire, où les effets les plus importants sont dans l'enseignement des sciences, suivi par l'enseignement des maths, puis des sciences sociales). Le nombre d'heures dédiées au projet a également un effet : travailler plus de 2 h par semaine à un projet a plus d'effets, comparativement à une pédagogie "ordinaire" que lorsqu'on y travaille moins de 2 h. Enfin, l'utilisation du numérique a également un effet modérateur sur la pédagogie de projet (les séances de pédagogie de projet utilisant le numérique ont des effets meilleurs que celles "ordinaires", alors que celles ne l'utilisant pas se différencient pas de ces dernières). Enfin, la taille du groupe d'apprenants (binômes, trinômes, etc.) n'a pas d'effets.

## Ce que l'on peut faire

:::{admonition} Des exemples de projet
- Le Ministère de l’Éducation Nationale et de la jeunesse propose dans le cadre du programme des actions éducatives une revue de projets pluridisciplinaires qui ont pour objectif de favoriser les approches pédagogiques transversales.
- [Programme des actions éducatives 2019-2020 (Educscol)](https://eduscol.education.fr/cid47920/programme-des-actions-educatives-2018-2019.html)
:::

**Attention  : ce qui suit n'a pas été récemment mis à jour**

Nous l'avons souligné, il s'agit de bien faire la différence entre une pédagogie de projet, au niveau de la classe et des élèves et, à un   niveau plus général, les différents projets d'établissement, d'école, voire, au niveau national, les plans (*e.g.*, Plan Langevin-Wallon). Les seconds sont obligatoires depuis la première Loi d'orientation de 1989, et concernent principalement un mode d'organisation du travail   entre enseignants et partenaires éducatifs. Ils ne  rendent pas nécessaires qu'une pédagogie de projet soit menée par les enseignants,   au quotidien, dans leur propre classe. Comme toute école doit mettre en œuvre un projet, il est important de voir de quelle manière chaque enseignant va pouvoir s'y insérer. Ce qui suit récapitule brièvement les différents types de projets, leur niveau d'intervention, leur durée, et les protagonistes.

### Au niveau de la circonscription

**Projet de circonscription (3 ans)**
: - *Personnes impliquées* :  I.E.N., conseillers pédagogiques, enseignants
  - Fixe les principaux axes de travail au niveau d'une circonscription, entre l'équipe de circonscription (inspecteur et conseillers   pédagogiques) et les enseignants. But : animer, former, évaluer les   enseignants, installer une dynamique.

### Au niveau de l'école ou de plusieurs écoles

- **Projet de REP/ZEP**

  - But : Articulation et mise en cohérence des projets d'école et d'établissement dans la zone REP/ZEP concernée (voir le recadrage en Réseaux ambition réussite).

- **Contrat ambition réussite** [Circ. 2006-058, 30.03.06](http://www.education.gouv.fr/bo/2006/14/MENE0600995C.htm)

  - *Personnes impliquées* : Membres du REP.
  - *But* : Permet de préciser, et d'alléger, l'organisation en projet de certaines REP (249), nommées Réseaux ambition réussite, sur les zones ayant le plus de difficultés. Regroupement autour d'un collège. Dispose de 1000 enseignants et 3000 assistants pédagogiques supplémentaires.

- **Classes à Projet d'action culturelle et artistique** (PAC, 1 an) [Circ. 2001-104, 14.06.01](http://circulaire.legifrance.gouv.fr/pdf/2009/04/cir_1665.pdf)

  - *Personnes impliquées* : Equipe pédagogique et partenaires extérieurs et DRAC.
  - "Une classe à projet artistique et culturel organise une grande partie de l'activité d'une année scolaire, pour tout le groupe-classe, autour d'une réalisation artistique et culturelle. Ce projet constitue un prolongement et un enrichissement des enseignements : il s'appuie sur les programmes et s'inscrit dans les horaires habituels de la classe. \[...\]  Les pratiques mises en œuvre doivent permettre aux élèves d'accéder à une véritable culture artistique en les plaçant dans des situations où s'éclairent l'une par l'autre une activité spécifique, l'acquisition de notions et de techniques, la rencontre avec des créateurs ou des professionnels, la découverte d'œuvres, l'élaboration de points de vue et de jugements esthétiques, la réflexion à partir des pratiques, des rencontres ou des visites. L'authenticité de l'engagement et de l'activité des élèves doit primer sur la restitution."

- **Contrat éducatif local** (CEL) (3 ans). [Circ.  98-144,9.7.98](http://www.education.gouv.fr/botexte/bo980716/scob9801882c.htm)

  - *Personnes impliquées* : Equipe éducative & municipalité
  - "Les Contrats Educatifs Locaux sont nés de la volonté d'appréhender l'éducation des enfants et des jeunes dans sa totalité. Ces contrats ont été mis en place pour mettre en cohérence tous les temps, scolaire, péri et extra scolaires. Il s'agit d'aborder l'éducation dans sa globalité, avec l'ensemble des partenaires concernés par ce qui est désormais défini comme une mission partagée: familles, Etat, et en particulier les enseignants, milieu associatif, collectivités locales, pour parvenir à une réelle continuité éducative."

- **Projet d'école** (3 ans) [Circ. 90-939, 15.02.90](http://circulaire.legifrance.gouv.fr/pdf/2009/04/cir_870.pdf)

  - *Personnes impliquées* : Equipe pédagogique.
  - *Rôle pédagogique* : viser une meilleure réussite scolaire ; éducatif : placer l'enfant au centre du système éducatif ; institutionnel : coordonner l'action des différents partenaires éducatifs.

- **Projet de cycle** [BO n° 9 du 3.10.91](http://www4.ac-nancy-metz.fr/ia54-gtd/directeurs/sites/directeurs/IMG/pdf_bospecial-n9-03101991.pdf)

  - *Personnes impliquées* : Enseignants du cycle, Directeur.
  - "Le conseil des maîtres de l'école constitue pour chaque cycle un conseil des maîtres de cycle  \[...\]. Ce conseil de cycle, présidé par un membre choisi en son sein, arrête les modalités de la concertation et fixe les dispositions pédagogiques servant de cadre à son action, dans les conditions générales déterminées par les instructions du ministre chargé de l'Éducation. Il élabore notamment le projet pédagogique de cycle, veille à sa mise en oeuvre et assure son évaluation, en cohérence avec le projet d'école."

### Au niveau de la classe

- **Projet de classe** (1 an)

  - *Personne impliquée* : Enseignant.
  - Mise en œuvre au niveau de la classe d'une partie du projet d'école.

Pour les projets d'aide des élèves au niveau individuel (de type intégration scolaire) voir le Document {ref}`integration_scolaire`.

### Analyser un projet

Enfin, le cadre ci-dessous (Campanale, d'après {cite}`barbier91`, {cite}`roegiers97`, pp. 175-214) permet d'élaborer les grandes lignes d'un projet.

- 1. Analyse de la situation
  - 1.1. Identification  du problème

    - Constats ? À partir de quelles informations, recueillies auprès de qui, par qui et comment ?
    - Formulation du problème de la problématique et d’un questionnement

  - 1.2 Diagnostic

    - Quels savoirs existent sur ce sujet ?
    - Quelles hypothèses peut-on faire sur les causes du problème ?
    - Qu’est-ce qui peut aider à résoudre le problème ?

  - 1.3. Identification des ressources et des contraintes (ou freins)

    - Quelles sont les ressources  (humaines et matérielles) du contexte ?
    - Quelles en sont les contraintes humaines, institutionnelles,   matérielles ?
- 2. Détermination de la ou des visées / des objectifs généraux du projet
  - Quels objectifs privilégier qui combinent besoins des individus et orientations institutionnelles et tiennent compte de ressources et contraintes ?
- 3. Définition des actions et programmation (remplir le tableau I ci-dessous).
- 4. Dispositif d'évaluation prévu

**Tableau I -- Définition des différentes actions d'un projet.**

|                       | Action 1 | ... | Action  n |
| --------------------- | -------- | --- | --------- |
| Objectifs spécifiques |          |     |           |
| Produits éventuels    |          |     |           |
| Calendrier, durée     |          |     |           |
| Pilote et acteurs     |          |     |           |
| Moyens nécessaires    |          |     |           |
| Effets attendus       |          |     |           |

## Analyse de pratiques

- Lisez le projet de votre école ou établissement. Réfléchissez à des activités scolaires qui seraient compatibles avec ce dernier.

## Quizz

```{eval-rst}
.. eqt:: Pedaprojet-1

  **Question 1. Selon la Loi d'orientation de juillet 1989, les différents types de pédagogie du projet sont ?**

  A) :eqt:`I` `"Le projet est un sujet ; Le projet est un cadre ; Le projet est une finalité"`
  B) :eqt:`I` `"Le projet est une finalité ; Le projet est un courant ; Le projet est un objet"`
  C) :eqt:`C` `"Le projet est un cadre ; Le projet est un objet ; Le projet est une méthode"`
  D) :eqt:`I` `"Le projet est une méthode ; Le projet est une finalité ; Le projet est un courant"`
```

```{eval-rst}
.. eqt:: Pedaprojet-2

  **Question 2. Dans une pédagogie de projet au niveau d'un établissement les personnes impliquées seront :**

  A) :eqt:`I` `L'enseignant`
  B) :eqt:`C` `L'équipe pédagogique`
  C) :eqt:`I` `L'inspecteur de l'Éducation nationale`
  D) :eqt:`I` `Le conseiller pédagogique`
```

```{eval-rst}
.. eqt:: Pedaprojet-3

  **Question 3.   Selon Campanale, les grandes lignes d'un projet sont :**

  A) :eqt:`I` `"1° L'analyse de la situation ; 2° Définition des actions et des programmations ; 3° La détermination des objectifs du projet ; 4° Mise en place d'un dispositif d'évaluation"`
  B) :eqt:`I` `"1° Définition des actions et des programmations ; 2° L'analyse de la situation ; 3° La détermination des objectifs du projet ; 4° Mise en place d'un dispositif d'évaluation"`
  C) :eqt:`I` `"1° Définition des actions et des programmations ; 2° La détermination des objectifs du projet ; 3° Mise en place d'un dispositif d'évaluation ; 4° L'analyse de la situation"`
  D) :eqt:`C` `"1° L'analyse de la situation ; 2° La détermination des objectifs du projet ; 3° Définition des actions et des programmations ; 4° Mise en place d'un dispositif d'évaluation"`

```

## Textes réglementaires

- Eduscol. [Portail du programme des actions éducatives 2019-20](https://eduscol.education.fr/cid47920/programme-des-actions-educatives-2018-2019.html)

### Webographie

- [La trousse à projets (financement participatif)](https://trousseaprojets.fr)

### Réglementation ancienne

- MEN (1990). Le projet d'école. [Circ. n° 90-039 du 15.02.90 (B.O. n° 9 du 3 oct. 1991)](http://circulaire.legifrance.gouv.fr/pdf/2009/04/cir_870.pdf).
- MEN (1991). Organisation et fonctionnement des écoles maternelles et élémentaires. [BO du 3 oct. 1991](http://www4.ac-nancy-metz.fr/ia54-gtd/directeurs/sites/directeurs/IMG/pdf_bospecial-n9-03101991.pdf).
- MEN (2001). Les classes à projet artistique et culturel.  [Circ. n° 21-104 du 14 juin 2001](http://circulaire.legifrance.gouv.fr/pdf/2009/04/cir_1665.pdf).
- MEN (2003). Enfants et adolescents atteints de troubles de santé. [BO n° 34 du 18 sept. 2003](http://www.education.gouv.fr/bo/2003/34/MENE0300417C.htm) .
- MEN (2006). Education prioritaire. [Circ. n° 20-058 du 30 mars 2006](http://www.education.gouv.fr/bo/2006/14/MENE0600995C.htm).
- MEN (2006). Scolarisation des élèves handicapés. [Décret 2005-1752, BO n° 10 du 9 mars](http://www.education.gouv.fr/bo/2006/10/MENE0502666D.htm).

## Références

```{bibliography}
:filter: docname in docnames
```
