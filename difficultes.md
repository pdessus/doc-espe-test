(difficultes-ecrit)=

# Les troubles cognitifs de l'apprentissage de l'écrit

% todo:

% https://www.scienceshumaines.com/la-dyslexie-un-cerveau-a-remodeler_fr_37995.html

% http://www.ipubli.inserm.fr/bitstream/handle/10608/110/?sequence=21

% Fluss, J., Bertrand, D., Ziegler, J., & Billard, C. (2009). Troubles d'apprentissage de la lecture: rôle des facteurs cognitifs, comportementaux et socio-économiques. Développements(1), 21-33.

```{index} single: auteurs; Javellas, Renaud single: auteurs; Dessus, Philippe single: auteurs; Besse, Émilie
```

:::{admonition} Information
- **Auteurs** : Renaud Javellas, Espé, Univ. Grenoble Alpes & [Philippe Dessus](http://pdessus.fr/), Inspé & LaRAC, Univ. Grenoble Alpes. Le quizz a été réalisé par Émilie Besse, projet [ReflexPro](http://www.idefi-reflexpro.fr).
- **Date de création** : Novembre 2002, mis à jour en juillet 2017.
- **Date de publication** : {sub-ref}`today`.
- **Statut du document** : Terminé.
- **Résumé** :  Ce document fait le point sur les différents troubles cognitifs de l'apprentissage de l'écrit, notamment les dyslexies. Après en avoir défini les principales formes, il présente quelques conseils pour aider les élèves présentant ces troubles, en association avec les professionnels compétents.
- **Voir aussi** : Documents {ref}`integration_scolaire`, {ref}`peda_projet`, et {ref}`difficultes-TIC`.
- **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.
- **Licence** : Document placé sous licence *Creative Commons* : [BY-NC-SA](http://creativecommons.org/licenses/by-nc-sa/3.0/fr/).
:::

## Introduction

Résolvez ce problème en moins de 4 min (CSIU, 2003) :

:::{topic} Problème
- Monsieur etma damare novon deupari achameau nit. Ladisten cet deux 600 Km lavoix tureconsso me 10 litr rausan quil aumaître. Ilfocon thé 18€ deux pé âge d'aux taurou tet 8 € dere papour désjeu néleumidit. Les sens kou tes 1€ leli treu ilpar ta 8 eureh. Kélai laconso mas siondes sans ?
- Quélai ladaipan setota lepour levoiaje ?
:::

Vous avez mis plus de  4 minutes ? Alors c'est 0 en calcul ! On peut ainsi éprouver ce que ressent une personne en situation de dyslexie face à un exercice, qui obtient une mauvaise note en calcul à cause de ses difficultés à identifier les mots (ce [site](http://geon.github.io/programming/2016/03/03/dsxyliea) simule ce que pourrait être l'expérience d'un lecteur en situation de dyslexie). Analysons la mise en situation de grande difficulté éprouvée, sur différents axes {cite}`lapeyre04` :

- *les axes affectif et social* : comment ai-je vécu cette situation (comment me suis-je senti\[e\], que me suis-je dit) ? On constate fréquemment les sentiments :

  > - de pression subie (attente forte, situation dans le groupe, etc.),
  > - de déstabilisation face à la difficulté (difficulté de motivation, de mobilisation et de concentration, abandon, etc.),
  > - d’angoisse de l'échec, le tout renforçant une certaine mésestime de soi fréquemment exprimée.

- *les axes cognitif et instrumental* : quels étaient pour moi les obstacles à la réalisation de cette tâche ? On constate fréquemment les conséquences :

  > - de difficultés de lecture-décodage (mauvaise reconnaissance ou mauvaise référence mémorielle des mots écrits),
  > - d’un manque d’automatisation des actions dans la tâche (décodage lecture, calcul mental à partir des données de ce problème, etc.),
  > - d’un décodage très coûteux qui retarde la compréhension dans un premier temps (fort coût cognitif), et donc en conséquence d’une lenteur dans la réalisation de la tâche, d’une fatigue. Mais cet exercice ne présente pas d'obstacle mathématique pour l’élève porteur de troubles dyslexiques.

## Ce que l'on sait

Parmi tous les troubles cognitifs pouvant affecter les élèves, nous nous centrerons ici sur les troubles spécifiques du langage oral et écrit (TSLOE). Ils ont une influence sur les habiletés de base (lire, écrire, compter), et donc sur la scolarité des élèves affectés. En réalité, il n'est pas si facile de séparer ces troubles d'autres problèmes, qu'ils soient sociaux, liés à la perception ou à la motricité : un trouble léger de la perception (auditive, par exemple) peut avoir des répercussions importantes et durables sur son apprentissage {cite}`khomsi95`.

Bien souvent, il n'y a pas qu'un seul trouble cognitif affectant l'élève, mais des troubles associés. De plus, un échec scolaire (axes cognitif et instrumental) a lui-même un effet sur le rapport de l'élève à lui-même, aux autres, à la règle (axes affectif et social) qui, en retour, etc. Cette interaction entre les différents axes est souvent négligée {cite}`boimare04`. Il est donc extrêmement important d’évaluer le profil fin de chaque élève, sur les différents axes :

- *affectif*, le rapport à soi-même, au travers de sa sécurité et de son assurance, de l’affirmation de soi et de confiance, de son autonomie et de son indépendance,
- *social*, le rapport aux autres et au cadre scolaire, le rapport à la règle, au cadre et l’appartenance au groupe,
- *instrumental*, le rapport aux outils et techniques dont on dispose pour apprendre (lire, écrire, compter, se repérer dans le temps et l’espace, la motricité, la capacité mémorielle, etc.),
- *cognitif*, le rapport aux savoirs, les liens que l'on fait entre différents savoirs, le sens qu'on y met, le traitement et la compréhension de l’information, l’évaluation de la situation et la prise de décision.

Trois mots-clés définissent ce travail d’évaluation du profil de l’élève {cite}`lapeyre04` : *globalité*, ou ne pas se centrer que sur les seuls constats scolaires ou cognitifs, *singularité*, où chaque élève est unique et il ne peut y avoir de « recettes » ou d’approche catégorielle moyenne, et *universalité*, ce qui vaut pour l’élève en grande difficulté pourrait aussi aider d’autres élèves pas nécessairement identifiés comme tels.

Ici, nous tenterons de définir les différents troubles liés à l'apprentissage du langage écrit, qui concernent aussi les élèves du secondaire et laisserons de côté les troubles du langage oral, pourtant cruciaux à l'école primaire. Il nous faut aussi noter que dans l'enseignement secondaire, les élèves sont généralement déjà  identifiés pour ces troubles. Toutefois, certains élèves peuvent être repérés  dyslexiques tardivement, au collège voire au lycée {cite}`egaud01`, car ils ont employé, depuis le début de leur scolarité, des stratégies de compensation efficaces (par exemple, ils pourront mémoriser, ce qui est tout à fait contre-intuitif, que "tabac" rime comme "hamac" et que "royaume" rime comme "atome", {cite}`valdois00`).

On distingue au moins quatre troubles : la *dyslexie* (acquisition de la lecture), la *dysorthographie* (restitution, stabilisation de l’orthographe), la *dyscalculie-innumérisme* (numération, calcul, résolution de problèmes), ainsi que la *dysgraphie* (trouble de la qualité de l'écriture, lié à l'apprentissage de l'écriture). Définissons-les, non pas dans l'idée que les enseignants puissent réaliser eux-mêmes un diagnostic (du ressort des partenaires médicaux et paramédicaux), mais pour qu'ils puissent connaître ces différentes formes de troubles, afin d'alerter les personnes partenaires (en premier lieu les parents, mais aussi le médecin scolaire, le psychologue, l'orthophoniste, etc.) (d'après {cite}`khomsi95`). Il est à noter que ces troubles sont présents alors que l'élève n'a aucun déficit de perception notable (vision, audition) ni d'intelligence.

### La dyslexie

La dyslexie (ou plutôt les dyslexies) est un trouble de l'identification d'un mot écrit (en France, entre 3 et 5 % des élèves seraient dyslexiques, {cite}`ringard00`, sans que cela porte sur leur compréhension ou leur production à l'oral. Il faut souligner {cite}`sprenger03`) que tous les élèves ayant des difficultés de compréhension ne sont pas dyslexiques : des difficultés peuvent provenir d'une mauvaise maîtrise de la langue, d'une scolarité peu régulière, d'un déficit intellectuel. On distingue deux catégories principales de personnes en situation de dyslexie  :

- *les lecteurs dysphonétiques* (les plus courants), qui font appel à la forme globale d'un mot, en ayant du mal à convertir les graphèmes (plus petite unité du système graphique) en phonèmes (la plus petite unité de son capable de produire un changement de sens par commutation). Ils lisent donc bien les mots "existants" (réguliers ou irréguliers), mais ont du mal avec les pseudo-mots. Leur compréhension globale d'un texte est réduite.
- les lecteurs dyslexiques de surface, qui réalisent les stratégies inverses, en s'intéressant prioritairement aux phonèmes et non à la forme globale d'un mot. Les lecteurs dyslexiques de surface ont tendance à s'appuyer sur le contexte (les autres mots dans le paragraphe, le texte) pour identifier les mots écrits, et ont de la peine à lire des mots irréguliers, mais lisent correctement les mots réguliers ou les pseudo-mots. Leur accès au sens des mots est donc très perturbé. Les lecteurs dyslexiques de surface ont du mal à lire un texte, en sautant fréquemment des lignes, faisant de nombreux retours en arrière.

Dans la figure ci-dessous, les lecteurs dysphonétiques auront tendance à accepter comme bien orthographié le mot "binyclette" (forme globale semblable, mais « pseudomot »), alors qu'un lecteur dyslexique de surface le rejettera. En d'autres termes, un lecteur dyslexique phonétique produira des mots avec de nombreuses erreurs phonologiques (livre au lieu de libre), alors qu'un lecteur dyslexique de surface sera capable d'écrire le même mot de plusieurs manières différentes ("méson", "maizon", "meison"). Notons enfin qu'il existe des personnes porteuses de dyslexie "mixte"  (environ 30 % des lecteurs dyslexiques) qui ont à la fois des problèmes de décodage phonologique et lexical.

**Figure 1** - Exemples de réponses produites par des enfants dyslexiques dans la lecture de "binyclette" ({cite}`khomsi95`, p. 451).

- Réponses impliquant un traitement phonographique : "Ça prend un c à la place du n ; il faut mettre si à la place de ni ; on ne peut pas dire binyclette."
- Réponses impliquant un traitement iconique :  "Oui, c'est bicyclette ; j'aurais mis vélo, mais il y a bicyclette."
- Réponses impliquant un traitement contextuel : "Ça commence pas pareil ; je connais vélo, c'est plus petit, il n'y a pas le devant ; oui, c'est vélo qui est écrit."

```{image} /images/velo.jpg
:align: center
:scale: 100 %
```

### La dysorthographie

La dysorthographie est un trouble de la production orthographique, donc lié à l'écriture. Les distinctions entre les personnes dites dyslexiques s'appliquent aussi chez les scripteurs dysorthographiques (d'ailleurs, un lecteur dyslexique dysphonétique développe le même type de dysorthographie) :

- un lecteur dysphonétique aura tendance à écrire en utilisant une représentation orale des mots, mais en utilisant d'autres mots de la langue  (*e.g.*, "hors taux graphe"), et à faire également des inversions, ajouts ou omissions de syllabes. Toutefois, il arrivera à écrire la plupart des mots réguliers et irréguliers correctement ;
- un lecteur dysorthographe de surface a une connaissance du recodage phonologique, mais a du mal à se construire un lexique (c'est-à-dire un répertoire de mots correctement orthographiés). Il utilisera une orthographe "phonétique"  (*e.g.*, « ortograffe »), avec des difficultés de segmentation : ("il a rivala mais son").

### La dyscalculie

La dyscalculie est un trouble qui entraîne une difficulté dans l'apprentissage du nombre, du calcul, ainsi que des difficultés spatio-temporelles souvent associées. C’est un trouble des compétences numériques, géométriques et des habiletés arithmétiques, se manifestant chez des enfants d ’intelligence normale qui ne présentent pas de déficits neurologiques acquis {cite}`temple92`. Il y a beaucoup moins de travaux dans ce domaine que dans les précédents. La variété des manifestations de ce trouble est large. On distingue les difficultés de procédures (stratégies de résolution d’opérations simples, d’activité de dénombrement), de déclarations (mise en mémoire des faits arithmétiques, récupération en mémoire), de présentation sémantique ou analogique des nombres (notions des quantités, persistance du nombre ligne numérique), de transcodage et de lecture-écriture des chiffres ou des nombres {cite}`bosse09`).

Une ou plusieurs structures cognitives logiques peuvent être atteintes :  conservation (invariance quantitative), classification (détermination de critères), inclusion (hiérarchisation des classes), sériation (relations d’ordre), combinatoire (organisation).

Pour plus d'informations, se reporter à {cite}`dehaene10, fayol12`.

### La dysgraphie

La dysgraphie est un trouble de la qualité et la vitesse de l'écriture. Son origine est encore mal connue. Certains avancent qu'elle peut être liée à une stratégie de masquage des erreurs d'orthographe, d'autres qu'elle est liée à des troubles du langage écrit. Tout d'abord, elle peut accroître l'attention portée aux mots pendant leur écriture (orthographe, syntaxe), ce qui peut amener l'enfant à faire plus d'erreurs. Ensuite, elle peut être liée, plus généralement, à un problème de coordination motrice. Elle est souvent associée à une dyspraxie ou des difficultés visuo-attentionnelles.

## Ce que l'on peut faire

### Détecter les problèmes

Rappelons que l’identification d’un trouble, dyslexique ou autre, qu’il soit ensuite reconnu comme handicap ou non, est soumis à tout un ensemble d’examens portés par des spécialistes formés et outillés (médecin, orthophoniste, orthoptiste, psychologue, etc.). Comme indiqué dans l'introduction, il arrive que certains élèves dyslexiques parviennent au collège sans avoir été signalés. Notons une fois de plus que si l'enseignant n'a pas à établir de diagnostic, il doit être attentif à tous signaux d'alerte chez  les élèves. Comme signalé plus haut, l’une des caractéristiques communes des élèves ayant l'un des handicaps présentés ici est leur grande fatigabilité et une estime de soi dégradée.

Le groupe de diffusion [Cognisciences](http://www.cognisciences.com) a élaboré ROC (Repérage orthographique collectif), un test collectif (dictée et lecture) permettant de détecter les élèves de CM2, 6{sup}`e` et 5{sup}`e` en grande difficulté de lecture, et de les orienter ensuite vers les personnes compétentes. Il est disponible à la demande, par courrier électronique. Voici quelques signes d’alerte qui peuvent être associés à une dyslexie ({cite}`egaud01` p. 39), aucun de ces signes ne suffisant toutefois à détecter une dyslexie. L’élève :

- a des difficultés  à apprendre les leçons. Il y passe beaucoup de temps, se fatigue ;
- a du mal à recopier un cours, à prendre des notes ;
- se perd dans une page surchargée de textes ;
- a des difficultés à catégoriser, à classer ;
- manque  de repères organisationnels, p. ex. à organiser les différentes sections d’un classeur de cours ;
- a des difficultés  en orthographe, à lire un plan, à copier des figures géométriques ;
- a du mal à transcrire ses idées, il peine en expression écrite ;
- en revanche, est brillant à l'oral.

Ces différentes difficultés peuvent s’exprimer de différentes manières, à des degrés divers, avec plus ou moins de répercussions sur les apprentissages. L’observation en classe, plus particulièrement auprès de l’enfant jeune, est donc primordiale pour alerter les professionnels :

- En Grande section de maternelle (GS), l’enseignant sera attentif à repérer d’éventuelles difficultés de conscience phonologique pour les tâches préalables à l’acquisition de la lecture (comptage et segmentation syllabique, détection de rimes...).
- Au Cours préparatoire (CP), on s’attachera aux obstacles de mise en œuvre de cette conscience phonologique pour parvenir à la fusion syllabique, pour acquérir la combinatoire et les correspondances graphème-phonème (guide de l’enseignant La dyslexie, académie de Dijon, 2009). Notons aussi que certains élèves porteurs d’une dyslexie sont plus particulièrement en difficulté pour apprendre certaines langues vivantes, l’anglais notamment à cause de la multiplicité des correspondances phonème-graphème).

### Aider l'élève

L’école inclusive prône une logique de culture commune. Avant tout, on visera les mêmes objectifs que pour les autres élèves, à savoir la maîtrise du socle commun de connaissances et de compétences. Dans toute la mesure du possible, on essaye de travailler « différemment » plutôt que « différent ». Toutefois dans certains cas, il sera peut-être nécessaire de prioriser des objectifs si l’élève semble très éloigné des apprentissages. Cette décision doit être collective, plurielle, partagée et en lien avec le projet de vie de l’élève. Dans le cadre du handicap reconnu, on pourra mettre en œuvre une Programmation Adaptée des Objectifs d’Apprentissage (PAOA – Circ. du 08/08/2016).

Toute aide doit avant tout se fonder sur une évaluation précise, individuelle, des difficultés, sans oublier les points d’appui-facilités et la définition des besoins particuliers de l’élève {cite}`lapeyre04` ; Prairat, 2002_REF?). Bien souvent les points d’appui de l’élève (par ex. les compétences à l’oral) soutiennent les étayages. Pour déterminer les aides, on pourra s’inspirer notamment des différentes ressources d’accompagnement éducatif mises en ligne sur le site Éduscol du ministère de l’Éducation nationale, notamment les livrets sur les troubles des apprentissages de 2012 ou les propositions d’aménagements pédagogiques développés dans les annexes du Plan d’accompagnement pédagogique (PAP). Encore une fois, soulignons que les conseils donnés ici ne peuvent être mis en place en dehors de tout soutien par des spécialistes, d’un travail de partenariat et d’une approche individualisée. Ils concernent principalement les dyslexies (conseils tirés de Direction de l'enseignement scolaire, 2005; {cite}`egaud01,juhel98`). Le Document {ref}`integration_scolaire` contient plus d'informations à ce sujet. Voici quelques conseils généraux :

- *Collaborer avec les parents, les autres professionnels* : construire des relations professionnelles avec les familles en les associant au parcours de formation, connaître les différents acteurs et leurs cultures professionnelles respectives, coordonner des actions avec les membres de la communauté éducative pour la scolarisation et l’accompagnement des élèves (par exemple au travers de cahiers de liaison) , co-construire des enseignements, co-intervenir, etc. (BOEN n° 7 du 16 février 2017 - [Référentiel des compétences caractéristiques d’un enseignant spécialisé](http://cache.media.education.gouv.fr/file/7/87/9/BOEN7_16-2-2017_717879.pdf), Annexe I)
- *Aspect affectif et social* : Changer de regard sur l'élève en difficulté, se centrer sur la compréhension de son fonctionnement. Établir un climat de confiance (car un tel élève manque souvent de confiance en lui) ; éviter les situations perturbantes (comme faire lire à haute voix en classe). Limiter le temps de parole du professeur, afin que l'élève puisse assimiler, s'exprimer, et que l'enseignant puisse observer l'élève. Garder un contact visuel avec lui le plus souvent possible. Parler doucement et clairement.
- *Structurer le travail* : Stabiliser au maximum l’environnement de travail de l'élève pour lui faciliter son repérage dans le temps, l'espace et les règles. Donner du temps à l'élève, lui donner moins d'exercices qu'aux autres (attention toutefois de maintenir l’exigence), ou avec une échéance plus longue, et les lui donner à l'avance. Ouvrir les manuels à la page requise, fournir des repères chronologiques (en histoire). Inciter à travailler par fiches, en reportant les cours sur fiches, avec mots-clés. Donner un plan de cours au début du cours. Établir des projets très structurés et planifiés. Fractionner la tâche, ainsi donner les consignes étape par étape plutôt que groupées. Donner systématiquement un exemple. Vérifier que l'élève a compris avant d'avancer à la prochaine étape.
- *Enrôler l'élève dans la tâche* : Procéder à des activités multi-sensorielles, multimodales (symboles écrits + langage oral). Utiliser du matériel intéressant et motivant, car les élèves porteurs de troubles dyslexiques sont facilement distraits, notamment par le bruit.

### Conseils par activité

- *Lecture* : Oraliser les supports, lire les documents et consignes (enseignant, élève tuteur ou logiciel d’oralisation). Produire et proposer à l’élève des supports de cours soignés (polices de caractères plus grandes, textes fractionnés).
- *Écriture, rédaction* : Faire écrire l'élève en cursive (plus difficile d'intervertir des lettres qu'en script). Prendre note sous dictée de l’élève. Comme il a du mal à noter, lui fournir la leçon déjà écrite (photocopie). Si les conditions de travail le permettent (en classe comme à la maison), favoriser la saisie numérique et si besoin utiliser des logiciels de saisie vocale. De même, l'aider et l'inciter à inventer des moyens mnémotechniques (en lien avec le travail de l’orthophoniste) pour retenir l'orthographe des mots. Écrire au tableau les mots nouveaux ou avec une orthographe irrégulière. Lui permettre, quand c'est possible, d'exprimer ses connaissances par oral ou par QCM, plutôt que par dissertations. Pour ces dernières, lui laisser du temps supplémentaire. Dans tous les cas, ne pas évaluer l'orthographe de la même manière que pour les autres élèves, en se centrant sur le contenu.
- *Numérique* : Mettre à la disposition de l'élève des auxiliaires (ordinateur, dictaphone, etc.). Egaud {cite}`egaud01` mentionne que des élèves dyslexiques ont pu faire des études longues grâce à l'écoute d'enregistrements de cours. Les logiciels de synthèse vocale aident au déchiffrage, et il existe des logiciels de prédiction de mots. Certains correcteurs orthographiques sont spécialement adaptés pour les élèves dyslexiques (voir le site [Ceciaa](http://www.ceciaa.com/dyslexie/) et le Document {ref}`difficultes-TIC`).
- *Évaluations et examens* : Rendre plus explicites les critères de réussite. Évaluer avec des critères différents, en prenant plus en compte l'oral et en lui faisant mieux préparer les contrôles. Valoriser les progrès. Concernant les élèves notamment des classes à examen (ex. Brevet, baccalauréat), les conditions de passation d'examens peuvent être adaptées (circ. 03/08/2015), qu’il y ait ou non reconnaissance préalable d’un handicap. Toute difficulté importante, reconnue par le médecin conseil auprès du Recteur, peut déclencher des aménagements d’examen (temps majoré, aide d’un tiers, scripteur, matériel informatique…).  Voir <http://www.education.gouv.fr/pid285/bulletin_officiel.html?cid_bo=91832>.

## Quizz

```{eval-rst}
.. eqt:: Difficultes-1

        **Question 1. L'axe affectif correspond :**

        A) :eqt:`I` `Au rapport aux autres`
        B) :eqt:`C` `Au rapport à soi-même`
        C) :eqt:`I` `Au rapport aux savoirs`
        D) :eqt:`I` `Au rapport aux outils et techniques`
```

```{eval-rst}
.. eqt:: Difficultes-2

        **Question 2. L'axe cognitif correspond :**

        A) :eqt:`I` `Au rapport à soi-même`
        B) :eqt:`I` `Au rapport aux autres`
        C) :eqt:`I` `Au rapport aux outils et techniques`
        D) :eqt:`C` `Au rapport aux savoirs`
```

```{eval-rst}
.. eqt:: Difficultes-3

        **Question 3. La dysorthographie est un trouble :**

        A) :eqt:`I` `De l’identification d’un mot écrit`
        B) :eqt:`I` `Des compétences numériques, géométriques et des habiletés arithmétiques`
        C) :eqt:`C` `De la production orthographique`
        D) :eqt:`I` `De la qualité et la vitesse de l’écriture`
```

```{eval-rst}
.. eqt:: Difficultes-4

        **Question 4. La dysgraphie est un trouble :**

        A) :eqt:`C` `De la qualité et la vitesse de l’écriture`
        B) :eqt:`I` `De l’identification d’un mot écrit`
        C) :eqt:`I` `Des compétences numériques, géométriques et des habiletés arithmétiques`
        D) :eqt:`I` `De la production orthographique`
```

## Références

### Textes officiels

- BO (2002). [Mise en œuvre d'un plan d'action pour les enfants atteints d'un trouble spécifique du langage oral ou écrit](http://www.education.gouv.fr/botexte/bo020207/MENB0200174C.htm). *BOEN n° 6 du 7 février 2002*.
- BOEN (2015). [Examens et concours de l'enseignement scolaire](http://www.education.gouv.fr/pid285/bulletin_officiel.html?cid_bo=91832). Dispositions relatives à l’aménagement des épreuves des examens et concours de l'enseignement scolaire pour les élèves en situation de handicap. *BOEN n°31 du 27 août 2015*.
- BOEN (2016). [Scolarisation des élèves en situation de handicap - Parcours de formation des élèves en situation de handicap dans les établissements scolaires](http://www.education.gouv.fr/pid285/bulletin_officiel.html?cid_bo=105511). *BOEN n°30 du 25 août 2016*.
- Direction de l'enseignement scolaire (2005). [Prévenir l'illettrisme, apprendre à lire avec un trouble du langage](http://www.cndp.fr/bienlire/02-atelier/document/apprendre_lire.pdf) (note 2). Paris : MEN.
- BOEN n° 7 du 16 février 2017 - [Référentiel des compétences caractéristiques d’un enseignant spécialisé](http://cache.media.education.gouv.fr/file/7/87/9/BOEN7_16-2-2017_717879.pdf), Annexe I)

### Outils et ressources

- [Cognisciences, ressources gratuites sur l'évaluation des dys](http://www.cognisciences.com)
- [Corydis](http://www.coridys.fr)
- [Ceciaa, ressources commerciales sur la dyslexie](http://www.ceciaa.com/dyslexie/)
- Jacquier-Roux, M.. (1999-2000). Prise en compte des difficultés des élèves dyslexiques à l'entrée au collège. Grenoble : Laboratoire Cognisciences \[<http://www.grenoble.iufm.fr/recherch/cognisciences/pdf/LE%20TOUVET.pdf>\]
- ROC. Repérage orthographique collectif. Document à l'usage des enseignants, récupérable à  <http://www.cognisciences.com/article.php3?id_article=42>
- CSIU, Centre de Santé Interuniversitaire de Grenoble (2003). Dépêche spéciale. Grenoble : Grenoble Université Recherche, document polygraphié.

### Ouvrages et articles

```{bibliography}
:filter: docname in docnames
```
