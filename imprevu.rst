.. _imprevus:

====================================
Enseignement et gestion des imprévus
====================================

.. index::
	single: auteurs; Dessus, Philippe
	single: auteurs; Besse, Émilie

.. admonition:: Information

  * **Auteurs** : `Philippe Dessus <http://pdessus.fr/>`_, Inspé & LaRAC, Univ. Grenoble Alpes. Le quizz a été réalisé par Emilie Besse, projet `ReflexPro <http://www.idefi-reflexpro.fr>`_.

  * **Date de création** : Novembre 2001.

  * **Date de publication** : |today|.

  * **Statut du document** : Terminé.

  * **Résumé** : Une préparation de cours peut être aussi rigoureuse et   précise que possible. Il n'en reste pas moins que des événements "imprévus" peuvent survenir et perturber l'enseignant, s'il n'y est pas préparé. Ce document est dévolu à définir de plus près cette   notion d'imprévu et la manière d'y faire face.
  
  * **Note** : Certains éléments relatés ici sont repris de Dessus (2002).
  
  * **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.

  * **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.
  


Introduction
============

Des chercheurs en éducation ont travaillé sur la notion d'imprévu survenant pendant l'enseignement. Cet imprévu peut être considéré négativement, si l'on considère qu'il déstabilise l'enseignant et l'oblige à décider de mettre en œuvre une autre activité que celle prévue. Il peut être considéré positivement, si l'on considère qu'il peut être un indice de choses qui se passent dans la classe, notamment les mécompréhensions des élèves. De manière générale, cette gestion des imprévus est donc directement reliée à la prévision (planification, ou encore préparation) de l'enseignant, avant qu'il mène la séance devant ses élèves. 

Cette planification (`voir document sur la planification <planif.html>`_) prend des formes diverses (liste des compétences visées, liste des activités des élèves, des activités de l'enseignant, modalités d'évaluation, etc.), mais elle a pour but principal, justement, de réduire les imprévus pouvant survenir lorsque l'enseignant sera en face de ses élèves. Comme l'expriment Chautard et Huber (1999a), du point de vue des enseignants, il y aurait des imprévus inévitables (qui arrivent nécessairement, car on ne peut tout prévoir, donc des imprévus prévus)…, des imprévus nécessaires (que l'enseignant attend presque, et qui sont le témoin de l'intérêt des élèves) et des imprévus exploitables (que l'enseignant utilise pour décider de la suite à donner à la séance).

Ce que l'on sait
================

Etude des imprévus survenant en classe
--------------------------------------

Les imprévus survenant en classe ont, la plupart du temps, été étudiés ainsi : on demande à des enseignants de préparer une séance, puis on les filme et les observe (ainsi que leurs élèves) pendant la réalisation de cette séance. Ensuite, après la séance, on leur fait regarder le film en leur posant des questions sur les moments où il semble qu'un imprévu s'est produit. Les résultats sont divers.

Ils ont par exemple montré *des profils d'enseignants* (Bru, 1991) : certains enseignants préparent leurs cours en pensant à de nombreuses variables (par exemple, niveau des élèves, difficulté de la tâche, matériel, etc.) et en agissant, pendant leur travail en classe, sur autant de variables ; d'autres, en revanche, planifient sur de nombreuses variables et réduisent leur nombre pendant la séance en classe ; enfin, certains planifient et agissent sur un nombre réduit de variables. Ces profils ne sont pas liés à la réussite des élèves.

Ils ont également montré *des profils de leçon* (Chautard & Huber, 1999b, 2001) : des leçons avec peu d'incidents et d'imprévus ; des leçons avec des incidents parasites, sans véritables imprévus ; des leçons avec imprévus nécessitant un diagnostic non suivi d'une action spécifique et enfin des leçons avec imprévus nécessitant une décision. Ces auteurs proposent de plus une intéressante classification des imprévus, que nous exposons maintenant.

Une classification des imprévus
-------------------------------

Chautard et Huber (1999b) ont observé une dizaine d'enseignant de français en lycée. Ils ont relevé, avec leur aide, les incidents, ainsi que les "modes de gestion" des enseignants les plus fréquents. Les voici.

**Tableau I** - Les principaux incidents et les modes de gestion des enseignants (d'après Chautard & Huber, 1999b).

+---------------------------------------------------------------------------------------------------+
| **Incidents (classe de français, lycée)**                                                         |
+===================================================================================================+
| * Types de réponses : Inexacte, inattendue, imprécise, absente, saugrenue, inaudible, incomprise. |
| * Perte de repères (confusion).                                                                   |
| * Conflit socio-cognitif (confrontation de points de vue différents).                             |
| * Orthographe.                                                                                    |
+---------------------------------------------------------------------------------------------------+
| **Modes de gestion des enseignants**                                                              |
+---------------------------------------------------------------------------------------------------+
| * (re)questionner.                                                                                |
| * Proposer une solution : donner la réponse.                                                      |
| * Répéter sans commenter la réponse.                                                              |
| * Ignorer la réponse.                                                                             |
| * Récapituler/synthèse/mise au point                                                              |
| * Modifier son point de vue (acceptation de la réponse).                                          |
| * Repousser la réponse.                                                                           |
| * S'étonner.                                                                                      |
| * Renvoi au document.                                                                             |
| * Penser tout haut.                                                                               |
| * Différer (une réponse).                                                                         |
| * Faire un diagnostic.                                                                            |
| * Corriger une erreur.                                                                            |
| * Manier l'humour.                                                                                |
| * Gratifier un élève.                                                                             |
| * Activer un conflit socio-cognitif.                                                              |
| * Faire un commentaire désobligeant.                                                              |
+---------------------------------------------------------------------------------------------------+

Ce que l'on peut faire
======================

Contrôler les différents événements de la classe
------------------------------------------------

Par définition, il est difficile de se préserver de tous les imprévus pouvant survenir en classe. Il est même certain qu'un enseignant qui ferait en sorte de ne laisser s'exprimer aucun imprévu manquerait de ce fait des événements importants. Certaines recommandations sont utiles, issues des travaux de Kounin et reprises notamment par Archambault et Chouinard (1996) ou Crahay (2000). Comme elles contribuent nécessairement à la discipline, on pourra se reporter au `document sur la discipline <discipline.html>`_ :

* Être sensible à ce qui se passe dans la classe*. L'enseignant doit essayer d'être conscient de tout ce qui se passe dans sa classe et de le montrer aux élèves. Il supervise constamment le fonctionnement de sa classe, "il a des yeux tout autour de la tête", il peut faire face à des événements inattendus. Même si, réellement, on ne peut jamais être capable de tout superviser, l'enseignant doit montrer aux élèves que le moins possible de choses lui échappe.

* *Gérer un rythme de travail adéquat*. L'enseignant doit faire en sorte que les différents événements de sa classe se succèdent "en    douceur", de façon continue et selon une logique claire. Il doit éviter les hésitations et les délais entre les différentes activités. Une des caractéristiques les plus difficiles à appréhender, notamment pour les enseignants débutants, est le fait que différents événements se produisent en même temps (chevauchement) : par exemple, aider un    élève qui tente de résoudre un problème, jeter un coup d'œil sur un groupe, intervenir pour qu'un élève cesse de déranger les autres,  vérifier la durée d'une activité, etc. La seule manière de gérer    efficacement ces chevauchements est de bien préparer ces différentes activités.

Comprendre la cause des imprévus
--------------------------------

Lorsque un ou des élèves se comportent de manière imprévue (nous laissons ici de côté le cas où du matériel se comporte de telle manière), il est non seulement utile de savoir *comment* on va réagir, mais aussi de comprendre *pourquoi* cet imprévu est survenu. En effet, les élèves interprètent les situations (ou les consignes), ils ne les exécutent pas nécessairement comme l'enseignant l'attend toujours (voir Document :ref:`taches_ens_el`). La référence de cette interprétation se trouve bien souvent dans les habitudes de travail que les élèves acquièrent avec chaque enseignants. Voici un exemple :

Pendant un cours, les élèves sont en train de réaliser un exercice, qui prend manifestement plus de temps que prévu. L'enseignant décide de le donner à terminer à la maison, en devoir. Ce qu'il annonce aux élèves. Juste après l'annonce, l'enseignant voit avec surprise tous les élèves se mettre à ranger leurs affaires, alors qu'il restait un quart d'heure avant la fin du cours. C'est bien l'annonce de devoirs à la maison qui a "déclenché" cette action des élèves, car, d'habitude, elle se situait bien *juste avant* la fin du cours. Bien évidemment, il a été difficile, pour l'enseignant, de terminer son heure de cours.

Quizz
=====

.. eqt:: Imprevu-1

	**Question 1. Comment peut-on contrôler les événements de la classe, selon Archambault et Chouinard (1996), et Crahay (2000) ?**

	A) :eqt:`I` `En prévoyant tous les imprévus possibles`
	B) :eqt:`I` `En gérant les imprévus au dernier moment`
	C) :eqt:`I` `En planifiant l'ensemble de son cours`
	D) :eqt:`C` `En étant sensible à ce qu'il se passe dans la classe`

.. eqt:: Imprevu-2

	**Question 2. Quelle est l'une des caractéristiques les plus difficile à appréhender pour les enseignants ?**

	A) :eqt:`I` `Les bavardages des élèves`
	B) :eqt:`C` `Le chevauchement de plusieurs événements à gérer`
	C) :eqt:`I` `Le manque de temps pour les leçons`
	D) :eqt:`I` `Susciter l'intérêt des élèves`

.. eqt:: Imprevu-3

	**Question 3. L'interprétation des élèves par rapport aux cours de l'enseignant dépend souvent :**

	A) :eqt:`I` `de leur niveau scolaire`
	B) :eqt:`I` `de leur attention en classe`
	C) :eqt:`C` `de leurs habitudes de travail acquises avec l'enseignant`
	D) :eqt:`I` `de leur assiduité en classe`


Analyse des pratiques
=====================

#. Reprendre la distinction faite dans l'introduction (Chautard & Huber, 1999a) entre les imprévus inévitables, les imprévus nécessaires et les imprévus exploités. Trouvez, dans votre expérience, un événement qui peut se rattacher à chaque catégorie. Réfléchissez à votre manière de les régler (voir partie "classification des imprévus).
#. Prenez un de ces événements, essayez d'en comprendre les causes (partie comprendre les causes des imprévus).

Références bibliographiques
===========================

* Archambault, J., & Chouinard, R. (1996). *Vers une gestion éducative de la classe*. Montréal: Morin.
* Bru, M. (1991). *Les variations didactiques dans l'organisation des conditions d'apprentissage*. Toulouse: Editions Universitaires du Sud.
* Chautard, P., & Huber, M. (1999a). *La gestion des imprévus par l'exercice du jugement pédagogique*. Dijon: ENESAD, rapport de recherche.
* Chautard, P., & Huber, M. (1999b). Une recherche à double effet : de la conceptualisation du cours dialogué à l’auto-analyse, par   l’enseignant, de ses pratiques. *Éducation Permanente*, *139*,   165-184.
* Chautard, P., & Huber, M. (2001). *Les savoirs cachés des enseignants*. Paris: L'Harmattan.
* Crahay, M. (2000). *L'école peut-elle être juste et efficace ?* Bruxelles: De Boeck.
* Dessus, P. (2002). `Les effets de la planification sur l'activité de l'enseignant en classe <https://edutice.archives-ouvertes.fr/file/index/docid/1790/filename/Bressoux.pdf>`_. In P. Bressoux (Ed.), *Les stratégies de l'enseignant en situation d'interaction* (pp. 19–33). Grenoble : Université Pierre-Mendès-France, Note de synthèse longue école et sciences cognitives. 