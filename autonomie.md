(autonomie-eleves)=

# Favoriser l'autonomie des élèves

```{index} single: auteurs; Dessus, Philippe single: auteurs; Besse, Émilie
```

:::{admonition} Information
- **Auteurs** : [Philippe Dessus](http://pdessus.fr/), Espé & LaRAC, Univ. Grenoble Alpes.  Le quizz a été réalisé par Emilie Besse, projet [ReflexPro](http://www.idefi-reflexpro.fr).
- **Date de création** : Mars 2002, mis à jour en décembre 2002.
- **Date de publication** : {sub-ref}`today`.
- **Statut du document** : Terminé.
- **Résumé** : Ce document contient quelques outils pour favoriser l'autonomie des élèves, c'est-à-dire des moyens pour que chaque élèves organise son travail et évaluer en partie ses performances.
- **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.
- **Licence** : Document placé sous licence *Creative Commons* : [BY-NC-SA](http://creativecommons.org/licenses/by-nc-sa/3.0/fr/)
:::

## Exergue

L'idée que l'enseignant doit s'attacher à promouvoir l'autonomie des élèves semble aujourd'hui incontestable. Pourtant, elle est paradoxale, au moins à deux niveaux :

- au niveau des savoirs : "\[...\] le professeur se \[sent\] malheureux, malheureux de ne pas laisser sa pleine liberté à l'élève dans la construction de son savoir, mais malheureusement aussi lorsque, laissant toute sa liberté à l'élève, le contrat didactique n'est pas rempli" (Bkouche, 1999) ;
- au niveau social : des classes d'élèves totalement autonomes n'auraient pas besoin d'enseignants.

## Ce que l'on sait

L'autonomie des élèves passe par deux points principaux : la possibilité qu'ils réalisent eux-mêmes une partie des tâches de l'enseignant ; la possibilité qu’ils améliorent d’eux-mêmes leurs stratégies d’apprentissage. Parmi ces tâches figurent la planification de l'enseignement, l'analyse et l'aide des stratégies d'apprentissage, l'évaluation des performances. Ainsi, trouver les moyens d'aider les élèves à réaliser en partie ces tâches concourra à leur autonomie. C'est ce que Zimmerman, Bonner & Kovach, 2000) nomment l'apprentissage autorégulé. Ce type d'apprentissage se déroule dans les quatre phases suivantes :

1. *autoévaluation et autocontrôle, détermination du niveau* : autoévaluation du niveau des élèves concernant le contenu à  apprendre. Cette évaluation utilise les traces écrites des élèves (que l'enseignant doit favoriser). Ces traces écrites peuvent être un "journal de bord", des brouillons. Cette évaluation peut être reprise par l'enseignant.
2. *analyse de la tâche d'apprentissage* : les élèves se fixent des objectifs concernant le contenu à apprendre et préparent une    stratégie permettant de les réaliser.
3. *Application de la stratégie et contrôle* : les élèves réalisent le plan prévu, en prenant des notes concernant leur démarche.
4. *Contrôle des résultats* : Analyse des résultats en rapport avec la stratégie utilisée. L'élève pourra déterminer si la stratégie était    adéquate en fonction des objectifs poursuivis.

A ces quatre niveaux, l'enseignant peut apporter à l'élève les formes de soutien suivantes :

- montrer à l'élève comment contrôler les procédés difficiles pour lui,
- encourager l'élève plutôt que de les critiquer,
- aider l'élève à analyser et décomposer les tâches (voir Document {ref}`taches_ens_el`) de la stratégie adéquate,
- vérifier les résultats et aider à l'amélioration de la stratégie.

## Ce que l'on peut faire

Il est possible d'aider l'élève dans certaines activités, à l’aide de certains documents.

### Les devoirs à la maison

Le tableau ci-dessous (Zimmerman *et al*., 2000, p. 42) permet d'aider l'élève à se rendre compte de son rythme de travail, notamment pour les devoirs à la maison. Il faudra aussi : qu’ils se fixent des objectifs réalistes, qu’ils définissent la priorité des différentes tâches (notamment par rapport aux devoirs surveillés, ou la difficulté du contenu), qu’ils évitent progressivement les distractions, tout en récompensant leurs efforts.

```{eval-rst}
+--------+-------------+------------------------+---------+------+------------+---------------+--------------------------+
| Date   | Consignes   | Activité commencée à   | Durée   | Où   | Avec qui   | Distraction   | Sentiment d’efficacité   |
+--------+-------------+------------------------+---------+------+------------+---------------+--------------------------+
|        |             |                        |         |      |            |               |                          |
+--------+-------------+------------------------+---------+------+------------+---------------+--------------------------+
```

### Les devoirs surveillés

Le tableau ci-dessous (Zimmerman et al., 2000, p. 119) permet à l’élève de planifier ses révisions en vue d’un devoir surveillé.

```{eval-rst}
+------------+-------------------+----------------+------------------------------------+
| Date du DS | Révisions                          | Analyse des points omis            |
+============+===================+================+====================================+
|            | En fin de semaine | Après le cours | Repris dans les notes/Absents des  |
|            |                   |                | notes/Types de questions possibles |
+------------+-------------------+----------------+------------------------------------+
```

### Travailler en groupes autonomes

La fiche suivante pourra être utilisée par chaque groupe travaillant sur un projet.

```{eval-rst}
+-------------------------------+-----------------------------------+---------------------------+
| THEME                         | NOM des membres du groupe         | Date                      |
+-------------------------------+-----------------------------------+---------------------------+
|                               | Ce qui a été fait                 | Ce qui reste à faire      |
+-------------------------------+-----------------------------------+---------------------------+
|                               |                                   |                           |
+-------------------------------+-----------------------------------+---------------------------+
| Difficultés venant du thème   | Solutions auxquelles on a pensé   | Solutions du professeur   |
+-------------------------------+-----------------------------------+---------------------------+
| Des problèmes matériels       |                                   |                           |
+-------------------------------+-----------------------------------+---------------------------+
| De l’organisation             |                                   |                           |
+-------------------------------+-----------------------------------+---------------------------+
```

## Analyse des pratiques

1. Listez les différentes méthodes que vous mettez en place pour faire travailler les élèves de manière plus autonome.
2. Inspirez-vous d'un des documents ci-dessus pour réaliser une fiche d'aide au travail de l'élève en autonomie.
3. Utilisez une des méthodes d'analyse de la tâche pour rendre plus explicite une stratégie d'apprentissage d'élève, stratégie qui pourra alors être diffusée à ces derniers.

## Quizz

```{eval-rst}
.. eqt:: Autonomie-1

        **Question 1. En quoi l'idée, aujourd'hui incontestable, de promouvoir l'autonomie des élèves est paradoxale pour l'enseignant ?**

        A) :eqt:`I` `La plupart des élèves ne peuvent pas travailler en autonomie`
        B) :eqt:`C` `Si les élèves étaient totalement autonomes l'enseignants ne serait plus utile`
        C) :eqt:`I` `L'enseignant ne sait pas mettre ses élèves en autonomie`
        D) :eqt:`I` `Les salles de classes ne sont pas adaptées à ce genre de pratique`
```

```{eval-rst}
.. eqt:: Autonomie-2

        **Question 2. Selon  Zimmerman, Bonner et Kovach (2000), l'apprentissage autorégulé se déroule dans l'ordre suivant :**

        A) :eqt:`I` `"1° Analyse de la tâche d’apprentissage ; 2° Autoévaluation et autocontrôle, détermination du niveau ; 3° Application de la stratégie et contrôle ; 4° Contrôle des résultats"`
        B) :eqt:`I` `"1° Analyse de la tâche d’apprentissage ; 2° Autoévaluation et autocontrôle, détermination du niveau ; 3° Contrôle des résultats ; 4° Application de la stratégie et contrôle"`
        C) :eqt:`I` `"1° Analyse de la tâche d’apprentissage ; 2° Contrôle des résultats ; 3° Autoévaluation et autocontrôle, détermination du niveau ; 4° Application de la stratégie et contrôle"`
        D) :eqt:`C` `"1° Autoévaluation et autocontrôle, détermination du niveau ; 2° Analyse de la tâche d’apprentissage ; 3° Application de la stratégie et contrôle ; 4° Contrôle des résultats"`
```

```{eval-rst}
.. eqt:: Autonomie-3

        **Question 3.   Au niveau de la phase d'analyse de la tâche d'apprentissage, quelle aide l'enseignant peut-il apporter à l'élève ?**

        A) :eqt:`C` `Encourager l'élève plutôt que le critiquer`
        B) :eqt:`I` `Aider l'élève à analyser et décomposer les tâches de la stratégie`
        C) :eqt:`I` `L'aider à améliorer ses résultats`
        D) :eqt:`I` `Montrer à l'élève comment maîtriser les procédés difficiles pour lui`

```

## Références

- Bkouche, R. (1999). [De la transposition didactique](http://casemath.free.fr/divers/tribune/didactic.pdf). *Didactiques*, 4, IREM de Lorraine.
- Busser, F. (1987)(Ed.). *Guide méthodologique pour la pratique du travail autonome*. Besançon : CRDP de Besançon.
- Ravestein, J. (1999). *Autonomie de l'élève et régulation du système didactique*. Bruxelles: De Boeck.
- Zimmerman, B. J., Bonner, S., Kovach, R. (2000). *Des apprenants autonomes, autorégulation des apprentissages*. Bruxelles : De Boeck.
