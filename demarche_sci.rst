.. _demarche_sci:

===============================================
Enseigner la démarche expérimentale en sciences
===============================================

.. index::
    single: auteurs; Dessus, Philippe
    single: auteurs; Besse, Émilie

.. admonition:: Information

 * **Auteurs** : `Philippe Dessus <http://pdessus.fr/>`_, Inspé & LaRAC, Univ. Grenoble Alpes. Le quizz a été réalisé par Emilie Besse, projet `ReFlexPro <http://www.idefi-reflexpro.fr>`_.

 * **Date de création** : Septembre 2005.

 * **Date de publication** : |today|.

 * **Statut du document** : Terminé.

 * **Résumé** : Comment enseigner la démarche expérimentale en sciences ? Cette démarche est encouragée, notamment dans l'école primaire, depuis le plan de rénovation de l'enseignement des sciences et de la technologie. Ce document aborde certains de ses aspects historiques, épistomologiques, méthodologiques et cognitifs.

 * **Voir aussi** : Doc. SAPP :ref:`constr_conn`.

 * **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.

 * **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.


Introduction
============

Ce document fait une revue rapide de la manière d'enseigner la démarche expérimentale en sciences. Récemment, les programmes de l'enseignement élémentaire ont été "rénovés" :cite:`men00`, en insistant sur l'importance de l'expérimentation et de l'argumentation dans l'apprentissage de concepts scientifiques. Seront respectivement abordés les aspects historiques, épistémologiques, méthodologiques, et cognitifs.

Perspectives historiques
------------------------

Les sciences à l'école existent, sous une forme optionnelle, depuis la loi Guizot (1833), sont obligatoires depuis 1882 (lois Ferry), et figurent comme épreuve du certificat d'études depuis 1891 (ce qui suit est tiré de Mergnac :cite:`mergnac05` pp. 82-83, voir aussi Kahn :cite:`kahn02`). Appelées "Leçons de choses" (terme issu de l'anglais *Object Lesson*, voir Saltiel :cite:`saltiel01`), l'élève découvre des rudiments de géologie, cosmographie, botanique, biologie, hygiène. Mais, jusqu'en 1930, les leçons de choses se déroulent principalement en classe, avec du matériel spécifique (collections de spécimens), après cette date, les enseignants réalisent plus de sorties dans la nature.

Ces leçons de choses, de plus, sont surtout l'occasion de promouvoir le culte de la science toute puissante (où les savants, nécessairement Français, appliquent leurs théories plus qu'ils les expliquent, voir Boulanger :cite:`boulanger05`), de convaincre les paysans de rester cultiver la terre, en montrant qu'ils sont utiles à la nation, et de propager des idées hygiénistes (par rapport aux méfaits de l'alcool), et enfin d'enseigner la géographie, utile militairement. Kahn :cite:`kahn02` pp. 74 *et sq*.) montre aussi que les premiers manuels traitant de science sont narratifs (sur le ton de la causerie, ils racontent des histoires mettant en scène des thèmes scientifiques, *cf*. l'histoire naturelle), insistent sur la curiosité, voire le merveilleux (merveilles de la nature et de la science) et enfin, conséquence des deux premières, sont récréatifs, *i.e.*, instruisent en amusant.

Un peu d'épistémologie : inductivisme et falsificationnisme
------------------------------------------------------------

Chalmers :cite:`chalmers87` fait un bon descriptif des différentes manières d'acquérir un savoir scientifique. En voici deux brièvement décrites :

*L'empirisme*, ou le savoir issu des faits de l'expérience. La science (ses lois et théories) est formée, de façon rigoureuse, de faits issus de l'observation et de l'expérience, sans laisser de place aux opinions personnelles des chercheurs. Cette conception, qui remonte au XVII\ :sup:`e` siècle (Galilée, Newton), comprend certains problèmes. Tout d'abord, il s'agit de savoir combien d'observations permettront assurément de conclure à la véracité d'une loi : Observer, dans de nombreuses situations, qu'un grand nombre de corbeaux a la couleur noire ne permettra jamais de conclure "Tous les corbeaux sont noirs".

Un autre problème lié à l'empirisme est celui des situations : il est impossible de déterminer *a priori* quelles sont les situations qui pourront éventuellement jouer significativement sur le résultat. Par exemple, si l'on veut déterminer scientifiquement le point d'ébullition de l'eau, quels sont les paramètres qui pourront influencer cette mesure ? (pression, pureté de l'eau, température extérieure, heure du jour, couleur du récipient ? - seules les deux premiers paramètres sont signifiants). Ainsi, avoir des idées sur cette question reviendrait justement à dire que l'on possède une théorie avant toute observation, ce qui n'est pas compatible avec l'inductivisme, qui procède de l'observation vers la théorie. Enfin, un troisième argument est de signaler les problèmes posés par l'observation proprement dite : la perception humaine, on peut le tester aisément, n'est pas exempte de défauts, d'illusions (*e.g.*, illusions d'optique), et l'on n'est donc jamais sûr, par nos sens, d'observer le monde réel ; cette question est encore plus vive lorsqu'on fait intervenir des instruments (*e.g.*, microscope). La classique démarche OHERIC (Observation, Hypothèse, Expérience, Résultat, Interprétation, Conclusion) (voir par exemple :cite:`giordan99`).

*Le falsificationnisme*, ou le progrès par essais et erreurs. Les théories sont de simples conjectures , des suppositions, qui tentent de répondre aux questions non résolues par les théories précédentes. On les teste le plus rigoureusement possible, on les confronte à l'observation et l'expérience, et l'on élimine celles qui n'y résistent pas. Ainsi, on ne dira jamais qu'une théorie est vraie, mais seulement qu'elle est la meilleure possible à cette date. L'approche scientifique falsificationniste est donc différente de la précédente : ici, l'on formulera les théories de manière à ce qu'elles soient falsifiables (*i.e.*, qu'on puisse produire des énoncés ou des observations qui lui sont contradictoires). Par exemple, la proposition "On peut avoir de la chance dans les paris sportifs" reste vraie que la personne joue ou pas et, si elle joue, qu'elle gagne ou pas" ; et il suffira d'observer un seul corbeau blanc pour invalider l'assertion "Tous les corbeaux sont noirs". Cette approche darwinienne a été formulée par Popper :cite:`popper98`. La démarche Question, Hypothèse, Expérience :cite:`giordan99`, peut rentrer dans cette approche, du moment que la question soit falsifiable, et qu'on soit attentif au fait qu'aucune expérience ne réfute exactement l'hypothèse de départ.

Comment mettre en œuvre une démarche expérimentale d'investigation ?
--------------------------------------------------------------------

Un document d'application des programmes en sciences, du MEN :cite:`men02` contient un canevas d'une séquence (Le même document contient, à l'attention des cycles 3 (élémentaire), d'intéressantes mise en œuvre de situations : - Que deviennent les aliments que nous mangeons ? - Quelle heure est-il à Paris, Pékin ou Sydney ? - Le fonctionnement du levier. - Comment savoir d'où vient le vent ?)

#. Choix d'une situation de départ, compatible avec les programmes, le projet de cycle, les ressources disponibles, les centres d'intérêt locaux, l'intérêt des élèves.
#. Formulation du questionnement des élèves, guidée par l'enseignant qui reformule éventuellement les questions, les recentre sur le champ scientifique, favorise l'amélioration de l'expression orale des élèves ; émergence des conceptions initiales des élèves, confrontation des divergences de points de vue.
#. Elaboration des hypothèses et la conception de l'investigation (par groupes), formulation orale de l'hypothèse, élaboration éventuelle de protocoles pour (in)valider les hypothèses ; élaboration d'écrits et schémas précisant ces derniers ; prédiction du résultat de l'expérience ("que va-t-il se passer selon moi", "pour quelles raisons ?") ; communication orale à la classe des hypothèses et protocoles proposés dans les groupes.
#. L'investigation conduite par les élèves, débat par groupes pour déterminer les modalités de mise en œuvre de l'expérience ; contrôle de la variation des paramètres ; description de l'expérience (écrit + schémas), relevé précis des conditions de l'expérience (s'assurer de sa reproductibilité) ; traces écrites personnelles des élèves.
#. L'acquisition et la structuration des connaissances, comparaison et mise en relation des résultats des différentes expériences ; confrontation avec le savoir établi (documentation) ; recherche des causes d'éventuels désaccords, analyse critique des expériences, formulation écrite par les élèves avec l'aide du maître, des connaissances acquises ; réalisation de productions de communication des résultats.

On retrouve là clairement affiché les principes généraux mis au point par le programme Main à la pâte (*cf*. Texte de présentation) :

.. topic:: Encadré 1 - La démarche pédagogique du Programme Main à la Pâte (http://www.lamap.fr/?Page_Id=60)

 La démarche préconisée par *La main à la pâte* privilégie la *construction des connaissances par l'exploration, l'expérimentation et la discussion.* C'est une pratique de la science en tant qu'action, interrogation, investigation, expérimentation, construction collective qui est visée et non pas l'apprentissage d'énoncés figés à mémoriser. Les élèves réalisent eux-mêmes des expériences, pensées par eux, et discutent pour en comprendre l'apport. On apprend par l'action, en s'impliquant ; on apprend progressivement, en se trompant ; on apprend en interagissant avec ses pairs et avec de plus experts, en explicitant par écrit son point de vue, en l'exposant aux autres, en le confrontant à d'autres points de vue et aux résultats expérimentaux pour en tester la pertinence et la validité.

 L'enseignant propose, éventuellement à partir d'une question d'élève, (mais pas toujours) des situations permettant l'investigation raisonnée ; il guide les élèves sans faire à leur place ; il fait expliciter et discuter les points de vue, en accordant une grande attention à la maîtrise du langage ; il fait énoncer des conclusions valides par rapport aux résultats obtenus, les repère par rapport au savoir scientifique ; il gère des apprentissages progressifs.

 On trouve des éléments de cette démarche dans le cahier d'expériences, dans lequel se trouvent aussi bien des écrits personnels ou individuels que des écrits collectifs (d'un groupe, de toute la classe). Les séances de classe sont organisées autour de thèmes, de telle sorte que des progrès soient possibles en matière à la fois d'acquisition de connaissances, d'acquisition de démarches et d'acquisition du langage oral et écrit. *Un temps suffisamment long* doit être consacré à chaque thème pour permettre les reprises, les reformulations, la stabilisation des acquis.

Les démarches des élèves
------------------------

Pour autant, mettre en place une expérience pour trouver une réponse n'est pas forcément quelque chose que les élèves réalisent spontanément, et il peut arriver que ces derniers "manipulent" pour le seul plaisir de manipuler. Giordan :cite:`giordan99` distingue plusieurs types d'expérimentations que les élèves peuvent mettre en place :

* *Les expériences imitées* : trouvées dans des livres, des manuels et reproduites telles quelles. Elles ne permettent pas l'appropriation de savoirs, ni de compétences en rapport avec l'expérimentation.

* *Les expériences pour voir* : imaginées par un ou plusieurs élève(s), qui ont une intuition et réalisent une expérience pour voir ce qu'elle donne. Bien guidées par l'enseignant (qui demande à l'élève d'expliquer ce qu'il suppose, et pourquoi), elles peuvent mener à une démarche intéressante.

* *Les expériences pour vérifier* : dans ce type d'expériences, l'enseignant va demander à l'élève s'il est sûr de ce qu'il avance. Cela va l'amener à douter, chercher d'autres moyens de vérification, voire d'autres expériences ou interprétations pour se justifier.

Giordan (:cite:`giordan99`, p. 134-135) montre comment évolue la compétence à mettre en œuvre des expérimentations chez les élèves. C'est bien à l'enseignant de repérer, dans l'activité de l'élève, les signes d'évolution de ces différentes compétences.

*Approche de l'expérimentation*

#. Accepter tels quels les événements sans en chercher la cause, ou suggérer une raison sans la justifier.
#. Chercher une cause naturelle aux événements, sans tenter de les justifier.
#. Chercher la cause naturelle d'un événement, en l'analysant et le justifiant par tâtonnement.
#. Chercher la cause naturelle d'un événement, en essayant de l'infirmer en proposant des justifications provenant d'observations, ou une méthodologie expérimentale.

*Processus mis en œuvre pour produire une explication*

#. Ne tâtonne pas, ou uniquement au hasard.
#. Tâtonnement par contiguïté : mise en lien de 2 paramètres proches.
#. Tâtonnement en fonction d'un fil conducteur : mise en liaison en fonction de quelques paramètres.
#. Réalisation d'une combinatoire systématique avant d'expérimenter (tâtonnement accessoire). Opération abstraites. Formulation d'hypothèses à partir d'une observation suggérée. Réalisation de modèles opératoires (schémas).

Analyse des pratiques
=====================

* La démarche scientifique est-elle seulement utilisable dans l'enseignement des sciences ? Donnez des exemples.

Quizz
=====

.. eqt:: Demarche_sci-1

	**Question 1. Selon Chalmers (1987), le falsificationnisme signifie :**

	A) :eqt:`I` `L'embellissement d'un fait qui a réellement eu lieu`
	B) :eqt:`I` `Le savoir issu des faits de l'expérience`
	C) :eqt:`I` `Le fait de cacher une vérité`
	D) :eqt:`C` `Le progrès par essais et erreurs`

.. eqt:: Demarche_sci-2

	**Question 2. 	Selon Chalmers (1987), l'empirisme signifie :**

	A) :eqt:`I` `Le progrès par essais et erreurs`
	B) :eqt:`I` `Le fait d'imposer une vérité`
	C) :eqt:`C` `Le savoir issu des faits de l'expérience`
	D) :eqt:`I` `L'embellissement d'un fait`

.. eqt:: Demarche_sci-3

	**Question 3. 	Selon Giordan (1999), les types d'expérimentation que les élèves peuvent mettre en place sont :**

	A) :eqt:`C` `"Les expériences imitées ; Les expériences pour voir ; Les expériences pour vérifier"`
	B) :eqt:`I` `"Les expériences premières ; Les expériences secondaires ; Les expériences tertiaires"`
	C) :eqt:`I` `"Les expériences test ; Les expériences confirmées ; Les expériences finales"`
	D) :eqt:`I` `"Les expériences individuelles ; Les expériences collectives ; Les expériences globales"`


Références
==========

.. bibliography:: demarche_sci.bib
	:cited:
    :style: apa
