.. _sanction:

==================================
Utiliser des sanctions appropriées
==================================

.. index::
    single: auteurs; Dessus, Philippe
    single: auteurs; Besse, Émilie

.. admonition:: Information

   * **Auteurs** : `Philippe Dessus <http://pdessus.fr/>`_, LaRAC & Inspé, Univ. Grenoble Alpes (et Archambault & Chouinard, 1996). Le quizz a été réalisé par Emilie Besse, projet `ReflexPro <http://www.idefi-reflexpro.fr>`_.

   * **Date de création** : Octobre 2001, mis à jour en Janvier 2009.

   * **Date de publication** : |today|.

   * **Statut du document** : Terminé.

   * **Résumé** : Ce document permet de réfléchir à la manière d'appliquer des sanctions appropriées au comportement des élèves, et compatibles avec les règlements. Lire auparavant le Document :ref:`discipline`.

   * **Voir aussi** : Document :ref:`discipline`.
   
   * **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.

   * **Licence** : Ce document est placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.



Ce que l'on sait
================

Une fois qu'un comportement inadéquat a été remarqué par l'enseignant, ce dernier a le choix entre continuer son cours comme si de rien n'était, ou bien sanctionner ce comportement. Voyons ici comment déterminer cette sanction, en commençant par expliquer les inconvénients de punitions non directement liées au comportement inapproprié.

Les inconvénients de la punition
--------------------------------

Ce qui suit est tiré d'Archambault et Chouinard (1996). La majorité des enseignants appliquent directement des conséquences aux comportements inappropriés de leurs élèves, sans avoir essayé les quelques interventions qui auraient pu faire cesser ce comportement de façon plus économique (voir Document :ref:`discipline`). C'est dommage, car ces conséquences, la plupart du temps d'ordre punitif, présentent certains inconvénients.

* la punition a pour seul objectif de faire cesser le comportement inapproprié le plus rapidement possible, et non d'enseigner un comportement approprié qui le remplacerait. Faire copier des pages de lignes à un élève qui s'est battu avec un camarade n'informe pas l'élève sur ce qu'il aurait dû faire et comment il aurait dû le faire.
* les effets de la punition sont certes immédiats, mais *de courte durée*. Des recherches ont montré qu'en demandant à des enseignants de punir davantage les comportemens perturbateurs, ceux-ci se produisaient plus fréquemment, car les élèves perturbateurs recherchent à être l'objet de l'attention de l'enseignant.
* les punitions créent dans la classe *un climat hostile et malsain*, pas  favorable à l'apprentissage : les élèves deviennent plus anxieux,  sont plus facilement distraits et se mettent à déranger davantage la  classe. Les élèves ont tendance à se comporter correctement  uniquement sous la menace directe de l'enseignant, et manifestent des  comportements inadéquats dès qu'il a le dos tourné. De plus les  élèves punis auront tendance à réutiliser auprès de leurs camarades  ce type de comportement agressif.
* les punitions, en s'attaquant plus à la personne de l'élève qu'à son comportement, font que l'élève puni va avoir tendance à contester, nier la punition, ce qui est une attitude logique, puisque la punition met en péril l'estime que l'élève a de lui-même.

De la punition aux conséquences logiques
----------------------------------------

L'application de conséquences logiques aux comportements inappropriés des élèves devrait remplacer les punitions, car elle entretient un lien logique avec le comportement, et a un rapport avec la vie de tous les jours. Par exemple, récupérer le temps perdu en travaillant ultérieurement (pendant une récréation) est une situation que les adultes appliquent souvent. Il convient donc que les enseignants réfléchissent aux comportements inadéquats de leurs élèves ainsi qu'aux conséquences logiques qu'ils peuvent créer.

Il convient d'identifier les conséquences logiques appliquées aux élèves à la suite de leurs comportements inadéquats. La punition a pour fonction  de sanctionner quelqu'un, de lui donner une peine. Elle n'a pas pour but  d'enseigner un comportement approprié. Elle n'a pas de lien logique ou  naturel avec le comportement inadéquat. La conséquence logique poursuit un objectif d'apprentissage où un comportement approprié est lié de  façon logique et naturelle au comportement inadéquat (Archambault & Chouinard, 1996, p. 59).

Toutefois, comme le précisent Lec et Lelièvre (2007), cette sanction ne   peut consister en un travail humiliant pour l'élève. La ligne de partage  entre "réparation" et traitement dégradant est en effet subtile : est-il  dégradant de demander à un élève de nettoyer des toilettes qu'il aura     taggées ? Pour ces auteurs, "une sanction dégradante serait une sanction  inhabituelle par sa nature et par là même susceptible de signifier à      l'élève qu'il n'est pas l'égal de ses pairs".                            


Voici quelques principes à respecter pour appliquer des conséquences logiques (Archambault & Chouinard, 1996) : 

* *tendre à banir* la punition ;
* *faire suivre immédiatement* le comportement inadéquat de la conséquence logique ;
* *prévoir et annoncer dès le début de l'année* les conséquences  logiques et les relier aux règles de fonctionnement de la classe ou  de l'établissement ;
* *éviter d'appliquer une conséquence logique dans un moment d'émotivité* (dans un accès de colère, les risques de donner à un élève une punition disproportionnée au comportement sont plus  importants) ;
* *utiliser d'autres interventions* (félicitations, explications, etc.)  en parallèle à l'application des conséquences logiques. Le fait de  continuer à travailler dans des conditions les plus normales  possibles avec l'élève ayant un comportement inapproprié renforce  auprès de lui l'idée que c'est sont comportement et non sa personne  que l'enseignant réprouve. Il est donc important que l'enseignant se  comporte normalement auprès de lui ;
* *éviter de choisir des activités d'apprentissage en conséquence* (ou en punition) du comportement perturbateur, il vaut donc mieux éviter  de donner des copies ou devoirs supplémentaires à l'élève  perturbateur, qui, ainsi, apprend à haïr la punition (écrire, faire  des devoirs). Cela, au bout du compte, convainc les élèves que ces  activités sont négatives, désagréables.


Ce que l'on peut faire
======================

Compatibilité de la sanction avec le règlement intérieur
--------------------------------------------------------

Autre contrainte que doit respecter la sanction, la compatibilité avec le règlement intérieur de l'établissement (nous laissons le soin à l'enseignant de s'y reporter) ainsi qu'avec la réglementation de l'éducation nationale. Récemment, le BOEN n°8 du 13 juillet 2000  (MEN, 2000) reprenait cette réglementation. Nous la résumons ci-dessous :

* *Principe de la légalité des sanctions et des procédures* : elles doivent être fixées dans le règlement intérieur et peuvent faire l'objet d'un recours administratif interne.

* *Principe du contradictoire* : il est impératif d'instaurer un dialogue avec l'élève et d'entendre ses arguments. Les représentants légaux de l'élève sont informés et peuvent être entendus s'ils le souhaitent. Toute sanction doit être motivée et expliquée.

* *Principe de la proportionnalité de la sanction* : elle doit être graduée en fonction de la gravité du manquement à la règle, doit mettre l'élève en situation de s'interroger sur sa conduite.

* *Principe de l'individualisation des sanctions* : Toute sanction est individuelle, mais, dans certains cas, depuis la Circ.  du 19 octobre 2004 (MEN, 2004), peut être collective. On ne doit toutefois pas aller vers une "tarification des sanctions". Cette dernière circulaire précise dans quelles conditions :

  * "S’il est utile de souligner le principe d’individualisation de la  punition ou de la sanction, il faut rappeler qu’une punition peut être infligée pour sanctionner le comportement d’un groupe d’élèves    identifiés qui, par exemple, perturbe le fonctionnement de la classe. Par ailleurs, dans le cadre de l’autonomie pédagogique du professeur,  quand les circonstances l’exigent, *celui-ci peut donner un travail supplémentaire à l’ensemble des élèves*." (nous soulignons, MEN, 2004)

Ce document distingue la punition scolaire, qui est une réponse immédiate à un comportement inadéquat mineur, et qui est décidée par les personnels, de la sanction disciplinaire, concerne les atteintes aux personnes et aux biens et manquements graves, qui est du ressort du chef d'établissement ou du conseil de discipline. Voici des exemples :

* de punition (il n'est pas permis de baisser la note d'un devoir en raison du comportement d'un élève ; en revanche, un zéro ou une  retenue peuvent être donnés pour un devoir ou exercice non fait sans excuse valable),
* inscription sur le carnet de correspondance,
* excuse orale ou écrite,
* devoir supplémentaire assorti ou non d'une retenue,
* exclusion ponctuelle d'un cours,
* retenue pour faire un devoir ou un exercice non fait, sans excuse valable.

de sanction disciplinaire :

* avertissement,
* blâme,
* exclusion temporaire de l'établissement,
* exclusion définitive de l'établissement assortie ou non d'un sursis.
  
Quizz
=====

.. eqt:: Sanction-1

	**Question 1. 	Que faire lorsqu'un élève a un comportement innaproprié ?**

	A) :eqt:`I` `Lui donner des activités d'apprentissage supplémentaires`
	B) :eqt:`I` `Lui demander de recopier des lignes cent fois`
	C) :eqt:`C` `Utiliser d'autre interventions en parallèle des sanctions`
	D) :eqt:`I` `Ne plus accorder d'attention à l'élève`

.. eqt:: Sanction-2

	**Question 2. 	Que dit la réglementation de l'Éducation nationale à propos des sanctions ?**

	A) :eqt:`C` `Il est impératif d'instaurer un dialogue avec l'élève et d'entendre ses arguments`
	B) :eqt:`I` `Une sanction se doit d'être collective`
	C) :eqt:`I` `La sanction doit être une sanction liée à l'écriture`
	D) :eqt:`I` `Les représentants légaux de l'élève ne sont pas obligatoirement tenus informés des sanctions`

.. eqt:: Sanction-3

	**Question 3. 	Parmi ces sanctions laquelle est une sanction "disciplinaire" ?**

	A) :eqt:`I` `Devoirs supplémentaire`
	B) :eqt:`C` `Exclusion temporaire de l'établissement`
	C) :eqt:`I` `Excuse écrite ou orale`
	D) :eqt:`I` `Inscription sur le carnet de correspondance`



Références
==========

* Archambault, J., Chouinard, R. (1996). *Vers une gestion éducative de la classe*. Montréal : Morin.
* Lec, F., & Lelièvre, C. (2007). *Histoires vraies des violences à l'école*. Paris: Fayard.
* MEN (1998). `Lutte contre la violence en milieu scolaire <http://www.education.gouv.fr/bo/1998/archive.htm>`_, BOEN n° 11 du 15 oct. 1998.
* MEN (1999). `Repères pour la prévention des conduites à risques <http://www.education.gouv.fr/bo/1999/hs9/default.htm>`_, BOEN n° 9 du 4 nov. 1999.
* MEN (2000). `Procédures disciplinaires <http://www.education.gouv.fr/bo/2000/special8/proced.htm>`_ BOEN n° 8 du 13 juillet 2000.
* MEN (2004). `Procédures disciplinaires <http://www.education.gouv.fr/bo/2004/39/MENE0402340C.htm>`_, BOEN n° 39 du 28 octobre 2004.

