.. _interet_apprentissage:

===========================================
L'intérêt et ses liens avec l'apprentissage
===========================================

.. index::
	single: auteurs; Dessus, Philippe
	single: auteurs; Besse, Émilie

.. admonition:: Information

	* **Auteurs** : `Philippe Dessus <http://pdessus.fr/>`_, Inspé & LaRAC, Univ. Grenoble Alpes. Le quizz a été réalisé par Emilie Besse, projet `ReflexPro <http://www.idefi-reflexpro.fr>`_.

	* **Date de création** : Mars 2017.

	* **Date de publication** : |today|.

	* **Statut du document** : Terminé.

	* **Résumé** : Ce document s'intéresse aux liens entre intérêt et apprentissage.

	* **Note** : Ce document reprend des éléments présentés dans Dessus, P. (2006, 29 mars). Qu'est-ce qui rend certains contenus d'enseignement intéressants ? *Atelier du séminaire "À la rencontre des intelligences : diversité des apprentissages"*. Grenoble : IUFM & DAAF.

	* **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.

	* **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.


.. to do
.. http://www.sciencedirect.com/science/article/pii/S0361476X16300455
.. http://www.pyoudeyer.com/oudeyerGottliebLopesPBR16.pdf

Introduction
============

La notion d’intérêt dans l'apprentissage est à la fois simple et mystérieuse : c’est à la fois facile de ressentir que quelque chose ne nous intéresse pas, et difficile de savoir pourquoi. À l'école, les élèves se doivent d’être intéressés. Ils doivent notamment :

* s’engager dans l'apprentissage ;
* être attentifs ;
* maintenir leur travail avec une intensité suffisante et mener les activités à leur terme ;
* souvent, travailler aussi après les cours ;
* ne pas abandonner en cas de mauvaise note ;
* faire des efforts, etc.

Toutes ces activités devraient être plus aisément soutenues si un certain *intérêt individuel* pour le scolaire existe. Les élèves intéressés apprendraient mieux, se comporteraient mieux en classe. Le matériel intéressant serait mieux retenu et compris. L'intérêt, comme l'ont montré de nombreux psychologues, est l'un des ingrédients de la motivation à réaliser une tâche :cite:`murphy00`. Ce serait (*id*., p. 28) : "Les processus par lesquels les besoins et désirs des apprenants sont dynamisés".

Un autre type d'intérêt existe, l'*intérêt situationnel* :cite:`palmer16`, qu'on peut définir comme un intérêt *temporaire*, généré par certains aspects particuliers de la situation (d'apprentissage), donc externes à l'élève. Même s'il est impossible d'avoir une classe d'élèves tous intéressés individuellement par les sujets enseignés, il peut être intéressant de réfléchir à la manière de provoquer un intérêt situationnel. Cela, d'autant plus que le désengagement des étudiants dans les filières scientifiques a parfois été analysé par un manque d'intérêt pour ces filières. Depuis 2006, l'intérêt pour apprendre des sujets scientifiques figure même comme l'une des variables de l'enquête PISA :cite:`krapp11`, voir aussi :cite:`venturini04` pour une revue.

Des auteurs :cite:`hidi06` ont décrit les phases possibles du développement de l'intérêt, montrant que les premières sont suscitées par un intérêt situationnel qui, s'il se maintient, fait émerger un intérêt individuel. Le reste de ce document essaie de répondre à ces questions.

Ce que l'on sait
================

Une définition de l'intérêt
---------------------------

L'importance de la prise en compte de l'intérêt dans l'éducation est connue depuis longtemps. Des philosophes comme Comenius, Rousseau, et plus récemment Dewey l'ont montré. L'intérêt est une variable motivationnelle dont la recherche a montré qu'elle avait cinq grandes caractéristiques :cite:`renninger11` :

* *il est dépendant du contenu* : "réfère à une attention et/ou engagement d'un individu envers des événements et objets particuliers" (*id.*, p. 169) ;
* "il implique une *relation particulière entre une personne et un environnement* et est soutenu par l'interaction" (*ibid*.) ;
*  il a des *composantes affectives et cognitives* (qu'on ne peut séparer : l'intérêt n'est pas que prendre plaisir à faire quelque chose et on peut avoir un intérêt pour quelque chose sans l'apprécier), et la quantité de chacune dépend de la phase de développement de l'intérêt ;
*  *on n'est pas toujours conscient de son intérêt* quand on est engagé dans une activité : on peut être tellement absorbé qu'on n'est pas capable de réflexion métacognitive ;
*  *l'intérêt a des bases physiologiques et neurales*: l'activation du cerveau diffère selon qu'on a ou pas de l'intérêt pour l'activité engagée (impliquant, notamment, des mécanismes de récompense).

L'intérêt émotionnel
--------------------

Le premier type d'intérêt est lié à l'émotion, à la curiosité, au plaisir de lire et de découvrir de nouvelles connaissances. Il a été montré qu'ajouter, dans un texte, des phrases, des images non centrales pour la compréhension du texte, peuvent le rendre plus intéressant. Le lecteur serait content, donc plus attentif, il apprendrait donc plus et mieux. Les phrases intéressantes, qu’elles soient par ailleurs importantes ou non, seraient majoritairement rappelées. Les magazines "*people*", mais aussi la plupart des journaux de vulgarisation scientifique, utilisent ce genre de propositions.

Selon des auteurs :cite:`hidi86`, ces propositions sont formées attracteurs « absolus » : mort, danger, pouvoir, sexe, amour, etc. (c'est-à-dire, selon un psychologue évolutionnaire :cite:`boyer03`, des informations stratégiques, régulant les interactions sociales). Il y a aussi attracteurs relatifs, comme l'inattendu, les relations personnelles. Par exemple, l'énoncé ci-dessus est intéressant car il donne des informations surprenantes.

* La mort d’une personne de 82 ans d’une crise cardiaque
* … d’un coup de poing dans la figure…
* … donné par son petit-fils.

Il a été montré :cite:`hidi86` que la lecture de textes active des schémas, représentations d’événements sociaux. Lorsque des attentes liées à un schéma de compréhension ne sont pas remplies, ou modifiées ; lorsque de l’information reliée à un schéma ne sont pas présentes ; et/ou des attracteurs absolus sont présents, cela renforce l'intérêt émotionnel du lecteur ou de l'auditeur.

Ainsi, il est aisé d'introduire des *détails séduisants* (DS), c'est-à-dire  une information jugée intéressante pour le lecteur (détail nouveau, actif, concret, personnel, selon les critères ci-dessus), mais non pertinente pour comprendre le contenu du texte (non importante). Les DS facilitent-ils l'apprentissage ?

Des recherches :cite:`garner92` montrent que les idées importantes (qu’elles soient elles-mêmes intéressantes ou ennuyeuses) sont moins rappelées lorsqu’elles sont entourées de DS (que ces derniers soient ou non mis en valeur) : le lecteur se centre sur les DS. L’intérêt émotionnel de parties d’un texte entre en compétition avec l’importance d’autres : certains détails non importants peuvent être donc rappelés au détriment de parties plus importantes. Les DS peuvent donc gêner la sélection (attention), l’organisation (faire des connexions) ou l’intégration (liens avec connaissances antérieures) d’informations :cite:`harp98`, s’ils sont intégrés au texte (moins d’effet négatif lorsqu’ils sont placés après). Il s'agit donc de manipuler ces DS avec beaucoup de précaution.

L'intérêt cognitif
------------------

L'intérêt cognitif, lui, est lié aux interconnexions que l’on construit à la lecture, à la structure du texte. Il est relié à de très nombreuses variables, que nous n'explorerons pas toutes dans ce document :cite:`hidi86,tobias94,sadoski01` :

* *la connaissance que l’on a du sujet*, ou degré de familiarité : bas, quand notre niveau de connaissance est faible, haut, quand il est fort ;
* le degré d’incertitude généré par le texte, un peu d’incertitude peut augmenter l'intérêt du texte ;
* *la cohérence avec ce qui précède* : lorsque l’information lue peut être reliée aux parties précédentes du texte, sans être trop similaire, cela augmente l'intérêt du texte ;
* *le caractère concret du texte* : un texte évoquant des images concrètes, sans changer le sens principal du texte, est plus intéressant qu'un texte abstrait.

Les effets des détails séduisants
`````````````````````````````````

Peut-on dire que les textes "habillés" de détails séduisants sont lus plus volontiers, donc mieux mémorisés ? Une étude :cite:`harp97` a manipulé les types d'intérêt (cognitif vs. émotionnel) dans un texte scientifique explicatif à propos de la météorologie et les risques associés à la foudre. Des étudiants ont été placés dans 4 modalités :

* lire un texte présentant un *intérêt cognitif simple*, par exemple "L'éclair résulte de la différence de charge électrique entre le nuage et le sol" (nommé base) ;
* lire un texte présentant un *intérêt émotionnel*, soit le texte de base avec des détails séduisants (DS), censé rendre le texte plus intéressant, sous une forme textuelle ;
* lire le texte de base avec des *illustrations séduisantes* (c.-à-d., décoratives non directement reliées au contenu du texte, comme le sont nombre d'illustrations dans les manuels scolaires) ;
* lire le texte de base avec du texte *et* des illustrations séduisantes.

Leurs performances étaient mesurées selon trois paramètres : le rappel des idées importantes du texte (rappel), leur transfert dans des résolutions de problèmes (transfert), et l'intérêt des textes. Dans tous les cas, les étudiants du groupe "texte de base" (intérêt cognitif) ont de meilleures performances en rappel et transfert, le groupe ayant les performances les plus faibles en rappel et transfert étant le groupe ayant le texte et les illustrations séduisantes.

Selon les auteurs, il semble que les DS font chez le lecteur une sorte de diversion, en les faisant se centrer sur des éléments non pertinents (c'est-à-dire, annexes) de la situation. Ils construisent ainsi un réseau d'idées à partir des éléments séduisants, ratant ainsi les idées principales de base. Pour autant, les quatre types de matériel ont été évalués comme également intéressants par les étudiants.

Toutefois, le niveau de connaissances des élèves n'interviendrait-il pas dans l'intérêt ? On a intuitivement l'impression que lire des ouvrages de vulgarisation utilisant des narrations parfois "décoratives" n'empêche pas d'apprendre, pour peu qu'on ait un niveau de connaissances suffisant. Une étude :cite:`wang16` a demandé à un groupe lycéens de lire un texte de géologie comprenant des DS pendant qu'un autre groupe lisait un texte standard. Les étudiants avec un niveau d'intérêt maximal pour la biologie (comprenant un niveau de connaissances plus élevé que les autres) ont eu de meilleures performances dans le groupe SD dans le test de transfert de connaissances que ceux du groupe standard.

Une étude :cite:`newman12` montre justement qu'associer une image non informative à un contenu textuel permet de renforcer la croyance en ce contenu, en procurant au lecteur un contexte sémantique riche, et renforçant la véracité des faits présentés.

Les effets des détails concrets
```````````````````````````````

Une autre étude :cite:`sadoski93` montre que le fait qu’un texte présente des faits concrets (*i.e.*, activant des représentations de divers registres sensoriels) plutôt qu’abstraits le rend un peu mieux compréhensible et mémorisé que le fait qu’il soit intéressant. Voici deux exemples :

* *Texte concret* : "G. O’Keeffe était capable de détecter de l’art partout où elle se trouvait : elle a acheté une maison parce qu’elle aimait sa double porte au bout d’un long, long mur d’adobe."
* *Texte abstrait* : "La carrière de G. O’Keeffe se confond presque entièrement avec l’histoire américaine de l’art moderne, et cette dernière partage son monde intérieur avec les premiers peintres modernes."

Les auteurs de cette étude montrent que c'est moins la familiarité du texte que son caractère concret qui va jouer sur sa compréhensibilité, donc sur son intérêt.

La théorie développementale d'Egan
----------------------------------

Egan a exprimé une théorie développementale de l'intérêt chez l'enfant originale et pouvant être appliquée (:cite:`egan97`, voir une description en français dans :cite:`dessus03`, qui est reprise dans cette Section). Cette théorie originale  permet de rendre compte de la manière dont l’enfant comprend son environnement, notamment à travers l’enseignement.

Egan détaille cinq modes de compréhension (somatique, mythique, romantique, philosophique et ironique), étroitement calqués sur les quatre étapes de Donald :cite:`donald99`. Selon cet auteur, quatre étapes permettent de nouvelles formes de pensée et de culture : épisodique, mimétique, mythique et théorique ; qui émergent respectivement à l’aide des outils cognitifs suivants : les capacités mimétiques (imiter autrui, notamment les adultes), le langage oral, le langage écrit (symboles externes). Chacune de ces formes de pensée est donc liée à des outils intellectuels particuliers, principalement fondés sur le langage, oral ou écrit.

Selon Egan, être éduqué, c’est maîtriser l’un après l’autre ces cinq types de compréhension, étant donné que chacun incorpore les habiletés des précédents. Le nouveau-né et le nourrisson développent une compréhension somatique, puis l'enfant acquiert une compréhension du monde et de la société à travers les mythes, *via* le langage. Ensuite, au travers du mode de compréhension romantique, l’enfant borne la réalité, que la compréhension philosophique permet d’organiser en un tout cohérent. Enfin, la compréhension ironique autorise la prise en compte du doute, du fait que les théories comprennent souvent des exceptions.

Quelles conséquences peut-on tirer de cette théorie à propos de l'intérêt ? Par exemple, on peut l'utiliser pour analyser à quel type de compréhension fait appel tel ou tel document (p. ex., manuel, article de magazine). Voici un extrait d'un magazine de vulgarisation scientifique pour adolescents (Raffegeau, R. 2017, Comment explorer les fonds marins, *Sciences & Vie Junior*, 34-37 ; Peyrières, C., 2017, Quelle vie dans les fonds marins, *Sciences & Vie Junior*, 38-41).

* Le premier article débute par une question générale incitant à réfléchir aux limites de ce qu'on connaît (romantique) : "Vous pensez que l'on connaît notre planète dans ses moindres détails ? Détrompez-vous !" (*id*., p. 34).
* S'ensuit le détail de ce qu'on sait sur le fond des océans, les différents outils mis en œuvre pour cela (sous-marins), et les ressources minières accessibles.
* L'article se termine par une perspective critique, en lien avec la fragilité des fonds marins (compréhension ironique).
* Le second article commence lui aussi par mettre en évidence un décalage entre ce qu'on perçoit et la réalité : "Pourtant, plus on explore ce monde inhospitalier, plus on se rend compte que la vie est partout !" (*id*., p. 38).
* Après un exposé précis des différents types d'animaux rencontrés, l'auteure de l'article fait appel, pour clore l'article, à une compréhension mythique : "Mais nul n'a encore pu assister à ce choc de titans..." (*id*., p. 41).

Liens causaux entre intérêt et connaissance
-------------------------------------------

Intérêt et connaissance sont des phénomènes très intriqués, à tel point qu'il est difficile de dire si c'est l'intérêt qui permet de porter une meilleure attention à la connaissance, ou bien si c'est la réussite dans un domaine qui, en retour, cause de l'intérêt. Peu d'études ont tenté d'y voir plus clair sur ce sujet. Une récente recherche apporte quelques éléments :cite:`rotgans17`. Leurs auteurs ont testé des élèves de collège en géographie/géologie (tectonique des plaques), et on séparé les groupes selon leur niveau d'intérêt (bas vs. haut intérêt, évalué par questionnaire) et de connaissances (faible et élevé) pour le sujet. Les résultats montrent que les participants dans le groupe "connaissances élevées" ont un niveau d'intérêt plus élevé que les élèves du groupe "connaissances faibles", et que les différences d'intérêt sont dues à la différence de niveau de connaissances.

Ainsi, ce ne serait pas l'intérêt qui stimule l'acquisition de connaissances, mais le contraire. Ce résultat important a des implications pour l'enseignement. Il signifie que l'enseignant, s'il parvient à mettre ses élèves en situation de réussite en les guidant et leur procurant les rétroactions nécessaires, peut parvenir à les intéresser à un contenu, même s'ils ne le sont pas initialement.

L'ennui à l'école
-----------------

En plus des définitions sur l'intérêt, il peut aussi être fructueux, *a contrario*, de définir l'ennui à l'école. Là aussi, il existe assez peu de travaux sur la question  (voir toutefois :cite:`vincent03,ferriere09,leloup04`). Une étude sur l'ennui de l'écolier :cite:`reinhardt02` montre les résultats ci-dessous, non dépendants du niveau scolaire des élèves :

* 64 % des élèves (9-12 ans) déclarent ne pas s’ennuyer en classe ;
* 31 % un peu ;
* 5 % souvent ou toujours (l'enquête d'Asseman, voir webographie, montre qu'environ 10 % d'élèves de CM2 s'ennuient en classe).

Leloup :cite:`leloup04` a réalisé une enquête sur la représentation du cours ennuyeux auprès d'élèves et d'enseignants de lycée. Elle montre que les représentations du cours idéal vs. réel sont identiques chez les élèves et les enseignants -- les premiers insistant sur les aspects affectifs, les derniers sur le cognitif. Chacun fait le minimum :

* Les enseignants reconnaissent ne pas (toujours) passionner les élèves, mais les font travailler ;
* Les élèves reconnaissent suivre le cours, ou au moins prendre des notes.

L’ennui viendrait donc plus du décalage entre l’idéal et la réalité de chaque cours : il n'y aurait pas d’ennui si ce qui est fait en classe correspond à ce que l’on attend.


Ce que l'on peut faire
======================

Comment susciter l'intérêt ?
----------------------------

Nous l'avons vu plus haut, les enseignants ont un rôle à jouer pour susciter l'intérêt de leurs élèves :cite:`hidi06,renninger11` :

* tout d'abord, il convient ne ne pas se focaliser trop sur l'effet de nouveauté. D'une part, il n'est pas durable, et il est préférable d'ancrer l'intérêt des élèves dans les points suivants, plus durables et pouvant conduire à un intérêt individuel ;
* donner de l'autonomie aux élèves pour qu'ils choisissent des activités qui les intéressent, et leur procurent un sentiment de compétence ; cela nécessite de leur donner de la latitude dans leur travail, mais aussi du temps ;
* les aider à maintenir leur attention dans des tâches difficiles et complexes, en les aidant, leur procurant des rétroactions pour qu'ils puissent poser des questions, notamment sur l'intérêt et l'utilité de l'activité ;
* favoriser le travail en groupe permet une construction collaborative des connaissances, ce qui entraîne de l'intérêt, qui est négocié, modelé (voir doc. :ref:`constr_conn`) ;
* au fur et à mesure que leur intérêt se développe, passer d'une aide externe (par l'enseignant) à une aide plus interne, par l'élève lui-même. Ce passage est nécessaire pour passer d'un intérêt situationnel à un intérêt individuel.


Comment évaluer l'intérêt ?
---------------------------

L'intérêt, notion complexe, est donc difficile à évaluer. Il en existe de nombreuses mesures : (cite:`hidi06` pour une revue), questionnaires autorapportés (*i.e.*, par les apprenants), mesures comportementales, qu'elles soient analysées via observation ou vidéo, ou en direct, pendant une activité (*on-line*).

Le questionnaire suivant :cite:`shernoff10` permet d'évaluer quelques facettes de l'intérêt. Les élèves évaluent les aspects de l'activité par une échelle de lickert en 4 points (de "pas du tout" à "très"), inspiré de la notion de "*flow*" de Csikszentmihalyi :cite:`csik06` :

1. Conditions de l'engagement
	* As-tu utilisé tes compétences dans l'activité ?
	* Est-ce qu'il y avait un défi à relever ?
	* Cette activité était-elle importante pour toi ?

2. Expérience de l'engagement
	* Etais-tu très concentré-e dans l'activité ?
	* L'activité était-elle intéressante ?
	* Est-ce ce que cette activité t'a amusé-e ?

Un questionnaire plus long a été proposé par Schiefele et ses collègues (le `Student Interest Questionnaire <https://www.unibw.de/sowi1_1/interest/fields/siq>`_) : (répondre par un nombre entre 0-pas du tout vrai à 3-absolument vrai). Les questions précédées d'un astérisque sont à codage inversé (compter 3 pour une réponse 0 et 0 pour une réponse 3). On peut calculer un score pour chacun des 3 domaines en sommant les scores à chaque item, ainsi qu'un score global sommant les scores des 3 domaines (plus le score est élevé, plus l'élève est intéressé à la matière). On a choisi de traduire "*major*" par "matière principale". Une version courte de ce questionnaire peut être obtenue en sélectionnant seulement les items : 3, 5, 6, 8, 11, 12, 13, 16, 18 :

**A. Emotions**

1. \* Travailler sur le contenu et les problèmes de ma discipline principale n'est pas vraiment l'une de mes activités favorites.
2. \* Je n'aime pas trop parler des matières que j'étudie.
3. J'ai hâte de revenir à mes études après un long week-end ou des vacances.
4. Travailler à ma discipline principale me met de bonne humeur.
5. \* Je préfère parler de mes loisirs préférés que de ma matière principale.
6. Lorsque je suis dans une bibliothèque ou une librairie, j'aime bien feuilleter des livres ou magazines à propos de ma matière préférée.
7. \* Plusieurs champs de ma matière préférée n'ont aucun sens pour moi.

**B. Valeurs**

8. Être capable d'étudier ce sujet en particulier a été pour moi de la plus grande importance pour moi.
9. \* Pour être vraiment honnête, j'éprouve parfois de l'indifférence envers ma discipline principale.
10. \* Je n'ai en réalité pas besoin de ma matière principale pour m'épanouir.
11. \* À mes yeux, mes études on vraiment moins de valeur que d'autres choses que je trouve très importantes, comme mes loisirs préférés, ma vie sociale.
12. Travailler une matière est plus important pour moi qu'avoir des loisirs.
13. J'ai toujours accordé beaucoup d'importance à ma discipline princiaple, même avant d'avoir commencé mes études.
14. Je suis sûr-e que l'étude de ma discipline principale a une influence positive sur ma personnalité.

**C. Orientation intrinsèque**

15. Si j'avais du temps, j'étudierais plus certains aspects de mes cours, même si ce n'est pas demandé.
16. Je suis sûr d'avoir choisi la matière principale qui correspond à mes préférences.
17. Même avant l'université, j'ai beaucoup réfléchi à la discipline principale que j'allais choisir (en lisant des livres sur le sujet, allant à des cours, en discutant avec d'autres).
18. J'ai choisi ma discipline préférée parce qu'elle est intéressante.

Quizz
=====

.. eqt:: Interet_apprentissage-1

	**Question 1. Selon Palmer et al. (2016), comment peut-on définir l'intérêt situationnel ?**

	A) :eqt:`I` `Intérêt collectif`
	B) :eqt:`C` `Intérêt temporaire`
	C) :eqt:`I` `Intérêt global`
	D) :eqt:`I` `Intérêt personnel`

.. eqt:: Interet_apprentissage-2

	**Question 2. Que peut-on faire pour activer l'intérêt émotionnel des élèves ?**

	A) :eqt:`I` `Répéter plusieurs fois l'information`
	B) :eqt:`I` `Engager les élèves dans leur apprentissage`
	C) :eqt:`I` `Mettre les élèves en situation`
	D) :eqt:`C` `Introduire des détails séduisants`

.. eqt:: Interet_apprentissage-3

	**Question 3. Selon une étude de Sadoski et al. (1993), un texte est d'autant plus compréhensible et donc plus facilement mémorisable, qu'il est **

	A) :eqt:`I` `abstrait`
	B) :eqt:`I` `long`
	C) :eqt:`C` `concret`
	D) :eqt:`I` `court`


Analyse des pratiques
=====================

#. Analyser la littérature scientifique à destination des adolescents et analyser de quelle manière le contenu est présenté, "enrobé".
#. Partir d'une séquence pédagogique de votre domaine (ou de manuels scolaires) et l'habiller de détails concrets, mieux l'organiser pour susciter de l'intérêt situationnel.
#. Faire passer un questionnaire d'intêret ci-dessous à vos élèves. Vous servir des résultats et de ce document pour améliorer l'intérêt situationnel de vos cours.
#. Parcourir les magazines de vulgarisation scientifique (disponibles sur internet), présentant des sujets de manière narrative. Sélectionner un sujet et en faire une séquence de cours (en traduisant tout ou partie du document et en le scénarisant). Se servir de la liste ci-dessous.

	* Sites en anglais
		* `Aeon <https://aeon.co>`_
		* `Knowable <https://www.knowablemagazine.org>`_
		* `LateralMag <http://www.lateralmag.com>`_
		* `Matter <https://medium.com/matter>`_
		* `  Max Planck Research <https://www.mpg.de/mpresearch>`_
		* `MIT Tech review <https://www.technologyreview.com>`_
		* `Nautilus <http://nautil.us>`_
		* `Popular science <http://www.popsci.com>`_
		* `Quanta Magazine <https://www.quantamagazine.org>`_
		* `R&D Magazine <https://www.rdmag.com>`_
		* `The Verge <http://www.theverge.com>`_


	* Sites en français

	    * `Universcience (capsules vidéos en fr) <http://www.universcience.tv>`_
	    * `Athena <http://recherche-technologie.wallonie.be/fr/particulier/menu/revue-athena/par-numero/index.html>`_
	    * `Research.eu <http://cordis.europa.eu/research-eu/magazine_fr.html>`_
	    * `Research.eu focus (thématique) <http://cordis.europa.eu/research-eu/research-focus_fr.html>`_
	    * `Accromath <http://accromath.uqam.ca/archives/>`_
	    * `Géosciences/Géorama <http://www.brgm.fr/mediatheque/liste-journaux-revues?typejournal%5B%5D=6&typejournal%5B%5D=7&typejournal%5B%5D=286>`_
	    * `Simplyscience <https://www.simplyscience.ch/jeunes.html>`_

Webographie
===========

* Asseman, J. (2014). `L'ennui en contexte scolaire <https://dumas.ccsd.cnrs.fr/dumas-01087425/document>`_. Villeneuve d'Ascq : Espé de Lille, mémoire M2 MEEF.

Références
==========

.. bibliography::
    :cited:
    :style: apa
