.. meta::
	:author: Philippe Dessus
	:description: Ressources de cours en sciences de l'éducation
	:keywords: éducation, pédagogie, LaRAC, Université Grenoble Alpes, LSE, Espé, Inspé ressources, analyse de pratiques, psychologie, ReFlexPro, tutoriels, ateliers, cours
	:http-equiv=Content-Type: text/html; charset=UTF-8


.. _index:

==============================================
Ressources de cours en sciences de l'éducation
==============================================

.. http://www.cnesco.fr/fr/12-evaluations-du-cnesco-en-resume/

.. admonition:: Information

	* **Responsable éditorial** : `Philippe Dessus <http://pdessus.fr>`_, Inspé & LaRAC, Univ. Grenoble Alpes.

	* **Résumé** : Cette page indexe les différents documents de cours de sciences de l'éducation de la base SAPP (**Séminaires d'analyse de pratiques pédagogiques**), de l'Inspé de l'univ. Grenoble Alpes. Certains Documents, suivis d'un astérisque, contiennent des Quizzes (QCM) permettant d'évaluer leur compréhension. Chaque document ci-dessous est suivi de la date de sa dernière mise à jour principale. Six types de documents sont disponibles :

		* des **cours complets** (compilation ordonnée de plusieurs Documents de cours),
		* des **documents** de cours (théoriques),
		* des **ateliers** (travaux pratiques),
		* des **tutoriels** (décrivant pas à pas une procédure),
		* des listes de **ressources**, et
		* des **syllabi** de cours.

	* **Dernière mise à jour** : |today|.

	* **Nombre de Documents dans la base** : **169**.

	* **Téléchargements** : Liens vers les compilations de tous les documents ci-dessous en `PDF <http://espe-rtd-reflexpro.u-ga.fr/media/pdf/sciedu-general/latest/sciedu-general.pdf>`_ et `epub <http://espe-rtd-reflexpro.u-ga.fr/projects/sciedu-general/downloads/epub/latest/>`_.

	* **Note** : La réalisation de ce site a été en partie financée par le projet `ANR-IDEFI numérique ReflexPro <http://www.idefi-reflexpro.fr>`_. Les Documents dont les titres sont suivis d'un astérisque comprennent un Quiz, qui a été conçu par Émilie Besse, `ReFlexPro <http://www.idefi-reflexpro.fr>`_. Les Documents dont les titres sont suivis d'un ↗️ sont dans un autre groupement de Documents : faire “retour arrière” pour revenir dans ce groupement.

	* **Licence** : Sauf mention contraire spécifiée sur le document, tous les documents de ce site sont placés sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.


.. supprimés
.. http://webcom.upmf-grenoble.fr/sciedu/pdessus/sapea/comportement.html
.. http://webcom.upmf-grenoble.fr/sciedu/pdessus/sapea/evaluationcampeval.pdf
.. http://webcom.upmf-grenoble.fr/sciedu/pdessus/sapea/oralcrpe.html
.. http://webcom.upmf-grenoble.fr/sciedu/pdessus/sapea/orgEPLE.html



À propos
========

.. toctree::
	:maxdepth: 1

	La démarche des SAPP (1999) <demarche_sapp>

* :doc:`Projet ReFlexPro décrivant l'organisation du site (mai 2019) ↗️ <reflexpro:index>`


Cours complets
==============

**Note** : les Documents composant ces cours se retrouvent individuellement dans la suite de la liste, par thème.

* :doc:`Cours “Les relations enseignant-étudiants dans l’enseignement supérieur” (nov. 2018) ↗️ <ens-sup:index>`
* :doc:`Cours “Évaluer l'apprentissage par ordinateur”, MEEF-PIF, UGA (sept. 2018) ↗️ <qcm:index>`
* :doc:`Cours “Introduction à la recherche en éducation”, MEEF, UGA (sept. 2018) ↗️ <rech-educ:index>`
* :doc:`Cours introductif au Classroom Assessment Scoring System (juin 2015) ↗️ <cours-class:index>`

Documents
=========

Le milieu scolaire
------------------

.. toctree::
	:maxdepth: 1

	Aperçu des systèmes éducatifs européens* (déc. 2004) <euroeduc>
	L'école maternelle en France* (fév. 2009) <ecolematernelle>
	L'école, un lieu de vie ?* (oct. 2004) <ecolevie>
	Les droits des élèves dans la classe et dans l'établissement* (nov. 2001) <droitel>
	La violence à l'école (fév. 2009) <violence>
	Les relations parents-professeur* (fév. 2007) <parents>
	Les enseignants et le travail en équipe* (mars 2009) <equipe>
	L'orientation des élèves* (déc. 2005) <orientation>
	Le redoublement* (fév. 2006) <redoublement>


Enseigner
---------

Réfléchir à sa pratique
```````````````````````

.. toctree::
	:maxdepth: 1

	Réfléchir à ses principes éducatifs* (Janv. 2006) <education>
	Être un enseignant réflexif critique* (Janv. 2008) <ensreflexif>
	Quelques mythes dans la recherche en éducation* (Fév. 2017) <mythes_education>
	Enseignement et pensée critique (Juill. 2018) <pensee-critique>
	L'apprentissage visible : qu'est-ce qui marche en éducation ?* (Avr. 2015) <apprentissage-visible>
	L'éducation fondée par les preuves (Oct. 2020) <ens-preuves>
	Ressources -- L'éducation fondée par les preuves (Oct. 2020) <res-ens-preuves>
	Présentation des Ateliers profs-chercheurs (Janv. 2021) <ateliers-profs-chercheurs>

La qualité des interactions enseignant-élèves (CLASS)
`````````````````````````````````````````````````````

* :doc:`Une introduction au Classroom Assessment Scoring System (CLASS) (Janv. 2018) ↗️ <cours-class:CLASS-intro>`
* :doc:`Description du Classroom Assessment Scoring System (CLASS) (Janv. 2018) ↗️ <cours-class:CLASS-description>`
* :doc:`CLASS : le soutien émotionnel* (Fév. 2018) ↗️ <cours-class:CLASS_soutien_emo>`
* :doc:`CLASS : l'organisation de la classe* (Fév. 2018) ↗️ <cours-class:CLASS_org_classe>`
* :doc:`CLASS : le soutien à l'apprentissage* (Fév. 2018) ↗️ <cours-class:CLASS_soutien_appr>`
* :doc:`Aspects théoriques et techniques du CLASS (Juin 2015) ↗️ <cours-class:technique-CLASS>`
* :doc:`CLASS, une foire aux questions (Janv. 2022) ↗️ <cours-class:class-faq>`
* :doc:`Tutoriel -- Créer un climat de classe favorable aux apprentissages (CLASS) (Juin 2015) ↗️ <tuto_CLASS_que_faire>`
* :doc:`Présentation du CLASS niveau secondaire (Oct. 2016) ↗️ <cours-class:CLASS-secondary>`
* :doc:`Utilisation du CLASS en formation des enseignants (Juin 2015) ↗️ <cours-class:CLASS_formation>`

.. toctree::
	:maxdepth: 1

	Class-Card, un jeu de rôles pour simuler des situations d'enseignement (Fév. 2021) <class-card>


Les relations enseignant-étudiants dans l'enseignement supérieur
````````````````````````````````````````````````````````````````

* :doc:`Les interactions enseignant-étudiants dans l'enseignement supérieur (sept. 2018) ↗️ <peda-univ:interactions-sup>`
* :doc:`Tutoriel -- Évaluer le climat socio-émotionnel dans le supérieur (oct. 2018) ↗️ <peda-univ:tuto-emo-class-sup>`
* :doc:`Atelier -- Les autres dimensions du CLASS (nov. 2018) ↗️ <peda-univ:atelier-autres-dim-class>`
* :doc:`Atelier -- Méga-analyse des effets des variables dans l’enseignement supérieur (nov. 2018) ↗️ <peda-univ:atelier-mega-analyse-effets-variables-ES>`
* :doc:`Atelier -- Les effets de la comparaison sociale de connaissances (nov. 2018) ↗️ <peda-univ:atelier-connaissance-sociale>`
* :doc:`Atelier -- Aiguiser l'esprit critique des étudiants (oct. 2018) ↗️ <peda-univ:atelier-esprit-critique-sup>`
* :doc:`Atelier -- Quand les étudiants font de la résistance (nov. 2018) ↗️ <peda-univ:atelier-resistance-etudiants>`
* :doc:`Atelier -- La menace du stéréotype (nov. 2018) ↗️ <peda-univ:atelier-menace-stereotype>`
* :doc:`Atelier –- Le sexisme à l'université (sept. 2018) ↗️ <peda-univ:atelier-sexisme-univ>`
* :doc:`Atelier -- Le monde académique en littérature (sept. 2018) ↗️ <peda-univ:atelier-litt-acad>`


Pédagogie universitaire
```````````````````````

* :doc:`Enseignement en ligne : Principes et activités (Fév. 2021) ↗️ <peda-univ:ens-en-ligne>`
* :doc:`Évaluation formative des étudiants à distance (Janv. 2021) ↗️ <peda-univ:evaluation-online>`
* :doc:`Évaluation de l'enseignement par les étudiants (Juin 2021) ↗️ <peda-univ:eval-ens-etu>`
* :doc:`La surveillance numérique des étudiants lors des examens (Oct. 2021) <peda-univ:surveillance-etu>`

Gérer et organiser la classe
````````````````````````````
.. toctree::
	:maxdepth: 1

	Concevoir un enseignement* (Avr. 2015) <conception-cours>
	Enseignement et gestion des imprévus* (Nov. 2001) <imprevu>
	La discipline dans la classe* (Oct. 2002) <discipline>
	Utiliser des sanctions appropriées* (Janv. 2009) <sanction>
	Analyser les tâches de l’enseignant et des élèves* (Oct. 2001) <taches>
	Observer le travail individuel des élèves* (Avr. 2012) <activite>
	Organiser le travail en groupes : débattre et apprendre* (Déc. 2007) <techgroupe>
	Engager les élèves à apprendre* (Oct. 2005) <relationseleves>
	Favoriser l'autonomie des élèves* (Déc. 2002) <autonomie>

Approches pédagogiques
``````````````````````

.. toctree::
	:maxdepth: 1

	L'enseignement explicite vs. par la découverte (Mars 2015) <ens-explicite>
	Projets et pédagogie de projet* (Fév. 2018) <pedaprojet>
	Pédagogie différenciée : luxe ou nécessité ?* (Janv. 2017) <pedadiff>
	Enseigner la démarche expérimentale en sciences* (Sept. 2005) <demarche_sci>
	Prévenir les conduites à risques* (Janv. 2006) <conduitearisques>

.. Une revue des théories de l'apprentissage (janv. 2016) <apprendre>
.. Pensée critique et enseignement <pensee-critique>

Évaluer et rétroagir
```````````````````

* :doc:`Evaluation formative, ou pour l'apprentissage (janv. 2021) ↗️ <eval_formative>`
* :doc:`Des tâches pour évaluer les connaissances des élèves* (sept. 2015) ↗️ <qcm:tache_eval>`
* :doc:`Les rétroactions : définition et rôle dans l'apprentissage* (déc. 2015) ↗️ <qcm:retro_defs>`
* :doc:`Les différentes formes de rétroactions* (sept. 2015) ↗️ <qcm:retro_formes>`
* :doc:`Les rétroactions : leur réception par les élèves* (sept. 2019) ↗️ <qcm:reception-retro>`

.. toctree::
	:maxdepth: 1

	Des outils pour évaluer* (nov. 2002) <outilseval>

Apprendre (aspects cognitifs)
-----------------------------

.. toctree::
	:maxdepth: 1

	Littérature et formation humaine* (janv. 2008)* <litterature>
	La charge cognitive dans l'apprentissage (janv. 2016)* <chargecog>
	Construction sociale de connaissances et apprentissage* (avr. 2015) <constrconn>
	Les cartes de concepts pour évaluer l’apprentissage* (oct. 2001) <conceptmap>
	L'intérêt et ses liens avec l'apprentissage* (mars 2017) <interet_apprentissage>
	La motivation en milieu scolaire* (oct. 2001) <motivation>
	Apprendre à apprendre* (janv. 2005) <apprapprendre>
	Erreur et apprentissage* (janv. 2006) <erreur>
	Troubles cognitifs de l'apprentissage de l'écrit* (juil. 2017) <difficultes>
	Les dispositifs d'inclusion scolaire en France* (août 2017) <integration_scolaire>

Apprendre (aspects socio-émotionnels)
-------------------------------------

.. toctree::
	:maxdepth: 1

	Culture familiale et culture scolaire* (oct. 2004) <culturefamille>
	Métier d'élève et travail scolaire* (oct. 2004) <metiereleve>
	Socialisation par le groupe de pairs* (oct. 2001) <socialisation>
	Émotions en contexte scolaire (sept. 2018) <emotions-scolaire>
	La mixité à l'école* (mars 2007) <mixite>
	Le corps à l'école* (mars 2007) <corpsalecole>
	Réseaux d’affinités en classe, approche sociométrique* (oct. 2001) <sociometrie>


Usages éducatifs du numérique
-----------------------------

Généralités
```````````

* :doc:`Le cadre de référence des compétences numériques (sept. 2020) ↗️ <num:crcn>`
* :doc:`Faire des recherches sur l’usage du numérique* (sept. 2017) ↗️ <num:rech_info_TIC>`
* :doc:`Utiliser le numérique dans des situations scolaires* (janv. 2016) ↗️ <num:usageTIC>`
* :doc:`La distance dans l'enseignement* (mars 2015) ↗️ <num:distance>`
* :doc:`L’usage du numérique à l’école maternelle* (avr. 2012) ↗️ <num:ticeEM>`

Informatique et société
```````````````````````

* :doc:`Informatique et école : vers une éducation citoyenne ?* (juill. 2013) ↗️ <num:droitinfosoc>`
* :doc:`Les innovations en technologie éducative* (fév. 2015) ↗️ <num:innovation>`
* :doc:`Les nouvelles bidons (*fake news*) (sept. 2018) ↗️ <num:fake-news>`
* :doc:`Complotisme et internet (sept. 2020) ↗️ <num:complotisme>`

Numérique et contexte scolaire
``````````````````````````````

* :doc:`Numérique et prise en compte des difficultés des élèves (janv. 2016) ↗️ <num:difficultes-TIC>`
* :doc:`Photos, enregistrements vidéo et sonores à l'école : que dit la loi ?* ↗️ (avr. 2016) <num:loi-collecte-donnees>`

Numérique et enseignement
`````````````````````````

* :doc:`Évaluation formative des étudiants à distance (janv. 2021) ↗️ <peda-univ:evaluation-online>`
* :doc:`Les connaissances de l’enseignant à propos du numérique* (mars 2015) ↗️ <num:tpack>`
* :doc:`Concevoir des usages du numérique par la scénarisation* (nov. 2016) ↗️ <num:scenario-based-design>`

Effets du numérique sur l'apprentissage
```````````````````````````````````````

* :doc:`Les effets des médias sur l'apprentissage* (mars 2015) ↗️ <num:effet-medias>`
* :doc:`Numérique, addiction et attention (janv. 2018) ↗️ <num:media-attention>`
* :doc:`La littératie numérique (janv. 2016) ↗️ <num:litteratie_numerique>`

Numérique et médias
```````````````````

* :doc:`Le tableau blanc interactif et son utilisation en classe* (avr. 2012) ↗️ <num:tbi>`
* :doc:`L’apprentissage avec des surfaces mobiles interactives et tactiles (MIT) — Les tablettes (mars 2014) ↗️ <num:mitactile>`
* :doc:`Wikipédia et ses usages pédagogiques* (juill. 2016) ↗️ <num:wikipedia>`
* :doc:`Programmer un jeu en Scratch pour apprendre (janv. 2018) ↗️ <num:scratch>`

Impacts du numérique sur la santé
`````````````````````````````````

* :doc:`Numérique et environnement (Déc. 2018) ↗️ <num:cours_Environnement>`
* :doc:`Troubles musculosquelettiques (TMS) et usages des objets numériques (sept. 2017) ↗️ <num:cours_TMS>`
* :doc:`École numérique, écrans et santé ophtalmique : entretien avec Pr. Florent Aptel (juil. 2017) ↗️ <num:cours_Ophtalmo>`
* :doc:`Le numérique face aux troubles du déficit de l'attention et à l'Hyperactivité (juin 2016) ↗️ <num:cours_TDA-H>`
* :doc:`Numérique, wifi, téléphone, les ondes à l’école (mars 2018) ↗️ <num:cours_Ondes>`


Exerciseurs (QCM) et générateurs de cours
`````````````````````````````````````````

* :doc:`Utiliser des exerciseurs pour apprendre* (janv. 2016) ↗️ <num:exerciseurs>`
* :doc:`Atelier – Les activités cognitives engagées dans un QCM (Sept. 2017) ↗️ <qcm:atelier-qcm-cog>`
* :doc:`Les rétroactions informatisées* (Sept. 2015) ↗️ <qcm:retro_info>`
* :doc:`Les QCM : bref historique* (Sept. 2015) ↗️ <qcm:QCM_historique>`
* :doc:`Les QCM : définitions et critiques* (Sept. 2015) ↗️ <qcm:QCM_def>`
* :doc:`Répondre à un QCM : aspects cognitifs* (Sept. 2015) ↗️ <qcm:QCM_cog>`
* :doc:`Les différents formats de QCM* (Sept. 2015) ↗️ <qcm:QCM_formes>`
* :doc:`Un processus de conception de documents de cours avec QCM* (Sept. 2015) ↗️ <qcm:QCM_proc_conception>`
* :doc:`Rédiger des items de QCM* (Sept. 2015) ↗️ <qcm:QCM_principes>`
* :doc:`Tutoriel -- Analyser les items d'un questionnaire (Déc. 2015) ↗️ <qcm:tuto-analyse-items>`
* :doc:`Quelques usages pédagogiques des QCM* (Sept. 2015) ↗️ <qcm:usages-peda>`
* :doc:`Les systèmes de réponses immédiates, ou clickers (Déc. 2021) ↗️ <qcm:clickers>`
* :doc:`Écrire des questions d'examen à l'épreuve de la tricherie (Sept. 2021) ↗️ <qcm:exam-google>`
* :doc:`Ressources -- Générateurs de cours interactifs (Sept. 2015) ↗️ <qcm:res_generateurs_cours>`
* :doc:`Tutoriel -- Utiliser le générateur de cours interactifs eXe Learning (Sept. 2015) ↗️ <qcm:tuto_exe_learning>`
* :doc:`Tutoriel -- Présentation rapide de NetQuiz Pro (Sept. 2015) ↗️ <qcm:tuto_net_quiz_pro>`
* :doc:`Tutoriel -- Présentation du générateur d'explorations interactives Oppia (Sept. 2015) ↗️ <qcm:tuto_oppia>`

Banque de cas juridiques d'usage du numérique
`````````````````````````````````````````````

Niveau primaire
'''''''''''''''


* :doc:`Cas 1 : Protection des données personnelles dans un ENT ↗️ <num:cas1_PE>`
* :doc:`Cas 2 : Collecte de données personnelles d'élèves ↗️ <num:cas2_PE>`
* :doc:`Cas 3 : Protection des données personnelles : les photos ↗️ <num:cas3_PE>`
* :doc:`Cas 4 : Productions d'élèves et droit d'auteur ↗️ <num:cas4_PE>`
* :doc:`Cas 5 : Ressources pédagogiques et droit d'auteur ↗️ <num:cas5_PE>`
* :doc:`Cas 6 : Contenus inappropriés ↗️ <num:cas6_PE>`

Niveau secondaire (collèges et lycées)
''''''''''''''''''''''''''''''''''''''

* :doc:`Cas 1 : Protection des données personnelles dans un ENT ↗️ <num:cas1_SD>`
* :doc:`Cas 2 : Protection des données personnelles stockées par les enseignants ↗️ <num:cas2_SD>`
* :doc:`Cas 3 : Protection des données personnelles : les photos ↗️ <num:cas3_SD>`
* :doc:`Cas 4 : Productions d'élèves et droit d'auteur ↗️ <num:cas4_SD>`
* :doc:`Cas 5 : Ressources pédagogiques et droit d'auteur ↗️ <num:cas5_SD>`
* :doc:`Cas 6 : Cyber-harcèlement ↗️ <num:cas6_SD>`


Introduction à la recherche en éducation
----------------------------------------

Outils de veille
`````````````````

* :doc:`Veille pédagogique et académique : outils et stratégies (janv. 2012) ↗️ <rech-educ:veillepeda>`
* :doc:`Veille académique, une sélection de ressources (janv. 2016) ↗️ <rech-educ:veille-sci-ressources>`
* :doc:`Comprendre la littérature scientifique (mars 2017) ↗️ <rech-educ:litt_sci>`

.. Tutoriel : La recherche sur les ordinateurs 1:1 en classe <rech-effet-ordi-indiv>

Problématique et méthode
`````````````````````````

* :doc:`Tutoriel -- Lire un article de recherche (avril 2017) ↗️ <rech-educ:tuto-lire-article>`
* :doc:`Tutoriel -- Problématique et hypothèses de recherche (janv. 2015) ↗️ <rech-educ:tuto-problematique>`
* :doc:`Tutoriel -- Définir une méthode de recherche (avril 2017) ↗️ <rech-educ:tuto-methode-rech>`
* :doc:`Tutoriel -- Concevoir un questionnaire : des dimensions aux questions (Fév. 2019) ↗️ <rech-educ:tuto_concevoir_quest>`
* :doc:`Faire des recherches sur l’usage du numérique* (sept. 2017) ↗️ <num:rech_info_TIC>`
* :doc:`CONPA, un jeu de création et réflexion sur l'usage du numérique (nov. 2016) ↗️ <rech-educ:conpa-jeu>`

Recueil de données
```````````````````

* :doc:`Tutoriel -- Recueillir des données de recherche (janv. 2017) ↗️ <rech-educ:tuto-recueil-donnees>`
* :doc:`Tutoriel -- Mener un focus group (décembre 2018) ↗️ <rech-educ:tuto-focus-group>`
* :doc:`Tutoriel -- La captation de vidéos de classes (décembre 2018) ↗️ <rech-educ:tuto-captation-video-classe>`

Analyse de données
``````````````````

* :doc:`Outils d'analyse textométrique pour l'enseignement (Fév. 2021) ↗️ <rech-educ:textometrie-educ>`
* :doc:`Tutoriel –- Utiliser ReaderBench pour analyser automatiquement des discussions (Oct. 2020) ↗️ <rech-educ:tuto-rb-conpa>`

La rédaction
`````````````

* :doc:`Tutoriel -- Organiser la rédaction d'un mémoire (janv. 2015) ↗️ <rech-educ:tuto-organiser-memoire>`
* :doc:`Tutoriel -- La rédaction d'un mémoire (janv. 2015) ↗️ <rech-educ:tuto-redaction-memoire>`
* :doc:`Atelier -- Réaliser un poster scientifique (avril 2017) ↗️ <rech-educ:atelier_poster_sci>`
* :doc:`Tutoriel -- Les références APA (*American Psychological Association*) (juin 2018) ↗️ <rech-educ:tuto-references-apa>`
* :doc:`Tutoriel -- Tableaux et figures APA (*American Psychological Association*) ↗️ (juin 2018) <rech-educ:tuto-tab-fig-apa>`



Ateliers
========

Les Ateliers sont de courts documents dont les tâches permettent de mieux comprendre des éléments théoriques.

Ateliers sur les usages du numérique (PE & SD)
----------------------------------------------

Aspects sociaux
```````````````

* :doc:`Atelier -- Numérique et neutralité commerciale (Janv. 2021) ↗️ <sciedu-ateliers:atelier_neutralite_commerciale_num>`

Méthodes pédagogiques
`````````````````````

* :doc:`Atelier -- La classe inversée (sept. 2015) ↗️ <sciedu-ateliers:atelier_usages_CI>`
* :doc:`Atelier -- Litéracie scientifique (fév. 2017) ↗️ <sciedu-ateliers:atelier_sci_litteracie>`

Outils
``````

* :doc:`Atelier -- Les jeux sérieux (sept. 2016) ↗️ <sciedu-ateliers:atelier_usages_jeux_serieux>`
* :doc:`Atelier -- La recherche sur internet (sept. 2015) ↗️ <sciedu-ateliers:atelier_usages_recherche>`
* :doc:`Atelier -- Les cartes mentales (sept. 2015) ↗️ <sciedu-ateliers:atelier_usages_cartes_mentales>`
* :doc:`Atelier -- Travail collaboratif et numérique (sept. 2015) ↗️ <sciedu-ateliers:atelier_usages_collaboration>`
* :doc:`Atelier -- Exercices interactifs (sept. 2015) ↗️ <sciedu-ateliers:atelier_usages_exerciseurs>`
* :doc:`Atelier -- Les environnements numériques de travail (oct. 2015) ↗️ <sciedu-ateliers:atelier_usages_ent>`
* :doc:`Atelier -- Les portfolios (sept. 2016) ↗️ <sciedu-ateliers:atelier_usages_portfolio>`
* :doc:`Atelier -- Les réseaux sociaux en classe (sept. 2016) ↗️ <sciedu-ateliers:atelier_usages_reseau_soc>`
* :doc:`Atelier -- Les systèmes de vote (oct. 2015) ↗️ <sciedu-ateliers:atelier_usages_vote>`
* :doc:`Atelier -- Les tablettes tactiles (oct. 2015) ↗️ <sciedu-ateliers:atelier_usages_tablettes>`
* :doc:`Atelier -- Les tableaux interactifs (oct. 2015) ↗️ <sciedu-ateliers:atelier_usages_tbi>`


Ateliers sur les usages du numériques (PE)
------------------------------------------

* :doc:`Atelier -- Intégration scolaire et numérique (janv. 2016) ↗️ <sciedu-ateliers:atelier_ASH_PE>`
* :doc:`Atelier -- Numérique et école maternelle (janv. 2016) ↗️ <sciedu-ateliers:atelier_EM>`
* :doc:`Atelier -- Utiliser des wikis pour apprendre (janv. 2016) ↗️ <sciedu-ateliers:atelier_wiki-PE>`


Ateliers sur l'apprentissage (aspects cognitifs)
------------------------------------------------

* :doc:`Atelier – Attention des élèves et chronopsychologie (janv. 2000) ↗️ <sciedu-ateliers:atelier_attention>`
* :doc:`Atelier – Mémoriser et rappeler des informations (janv. 2000) ↗️ <sciedu-ateliers:atelier_memoire>`
* :doc:`Atelier – Comprendre et raisonner (janv. 2000) ↗️ <sciedu-ateliers:atelier_comprendre>`
* :doc:`Atelier – Les activités cognitives engagées dans un QCM (sept. 2017) ↗️ <sciedu-ateliers:atelier-qcm-cog>`

Ateliers sur l'enseignement
---------------------------

* :doc:`Atelier -- Réfléchir à ses principes éducatifs (2008) ↗️ <sciedu-ateliers:atelier-metier>`
* :doc:`Atelier -- Tester son opinion sur la discipline (mars 2002) ↗️ <sciedu-ateliers:atelier-testdiscipline>`


Tutoriels
=========

Seuls les tutoriels non repris dans un cours complet sont répertoriés ici.

Tutoriels sur le numérique
--------------------------

* :doc:`Tutoriel – La twictée (janv. 2017) ↗️ <sciedu-tutos:tuto-twictee>`
* :doc:`Tutoriel - Scénariser un cours avec le numérique (janv. 2017) ↗️ <sciedu-tutos:tuto_scenariser_numerique>`
* :doc:`Tutoriel – Logiciels éducatifs : Aspirateurs à données personnelles ? (Fév. 2020)  ↗️ <sciedu-tutos:tuto-donnees-perso>`
* :doc:`Tutoriel - Quelques outils de vérification de faits (*fact-checking*) (Janv. 2021) ↗️ <sciedu-tutos:tuto-fact-checking>`
* :doc:`Tutoriel - Politiques d'usage du numérique (Juillet 2021) ↗️ <num:tuto_politique_travail_num>`


Ressources
==========

* :doc:`Ressources -- Veille académique (janv. 2016) ↗️ <rech-educ:veille-sci-ressources>`
* :doc:`Ressources -- Générateurs de cours interactifs (sept. 2015) ↗️ <qcm:res_generateurs_cours>`

.. toctree::
	:maxdepth: 1

	Ressources -- Textes sur l'enseignement et l'apprentissage (déc. 2017) <res_apprendre>


Syllabi
=======

Année 2020-21
-------------

* :doc:`Syllabus du cours “UE Évaluer l'apprentissage par ordinateur”, MEEF-PIF, Espé-UGA (2020-21) ↗️ <qcm:syl-organisation-MEEFPIF>`
* :doc:`Syllabus du cours “Mémoire et écrit scientifique réflexif sur les interactions Enseignant-élèves", Espé-UGA (2020-21) ↗️ <cours-class:CLASS_espe>`
* :doc:`Syllabus du cours “Les relations enseignant-étudiants dans l'enseignement supérieur”, UGA (2018-19) ↗️ <ens-sup:syll-ens-sup>`
* :doc:`Syllabus du cours "UE Culture numérique et apprentissages SD", MEEF Inspé-UGA (2020-21) <num:syllabus-ue-cult-num_SD>`
* :doc:`Syllabus du cours "UE "Intégration du numérique dans l'enseignement : approfondissement" PE & SD, Inspé-UGA (2020-21) <num:syl-ue-cult-num-2>`
* :doc:`Syllabus du cours “UE Culture numérique et apprentissages PE”, MEEF Espé-UGA (2020-21) <num:syllabus-ue-cult-num-PE>`
* :doc:`Syllabus du cours "UE Recherche PE, Innovation & problématisation", MEEF Espé-UGA (2018-19) ↗️ <rech-educ:syl-ue-rech-prob-2>`

Années antérieures
------------------


* :doc:`Syllabus du cours “UE Culture numérique et apprentissages PE”, MEEF Espé-UGA (2016-17) <num:syllabus-ue-cult-num-PE>`
* :doc:`Syllabus du cours “UE Culture numérique et apprentissages PE”, MEEF Espé-UGA (2018-19) <num:syllabus-ue-cult-num-PE-19>`
* :doc:`Syllabus du cours "UE Culture numérique et apprentissages SD", MEEF Espé-UGA (2018-19) <num:syllabus-ue-cult-num-SD-18>`
* :doc:`Syllabus du cours “UE 206 Recherche PE, Innovation & problématisation”, MEEF Espé-UGA (2017-18) <rech-educ:syl-ue-rech-prob>`
* :doc:`Syllabus du cours "UE 800 Recherche SD" (2016-17) <rec-educ:syllabus-ue-recherche>`
* :doc:`Syllabus du cours du MARDIF (2016-17) <organisationMARDIF>`



Index et tables
===============

* :ref:`genindex`
* :ref:`search`
