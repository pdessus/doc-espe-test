(outils-eval)=

# Des outils pour évaluer

```{index} single: auteurs; Dessus, Philippe single: auteurs; Besse, Émilie
```

:::{admonition} Informations
- **Auteurs** : [Philippe Dessus](http://pdessus.fr/), Inspé & LaRAC, Univ. Grenoble Alpes. Le quizz a été réalisé par Emilie Besse, projet [ReflexPro](http://www.idefi-reflexpro.fr).
- **Date de création** : Novembre 2002.
- **Date de publication** : {sub-ref}`today`.
- **Statut du document** : Terminé.
- **Résumé** : L'objet de ce document est de préciser le type d'outils à la disposition des enseignants pour évaluer, que ce soit dans la phase de régulation de l'apprentissage, ou bien pour contrôler ce dernier.
- **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.
- **Licence** : Document placé sous licence *Creative Commons* : [BY-NC-SA](http://creativecommons.org/licenses/by-nc-sa/3.0/fr/).
:::

## Introduction

L'activité d'évaluation est une phase importante de l'activité de l'enseignant. Il  convient de faire en sorte que cette dernière soit à la fois utile à l'élève et à l'enseignant. Nous voyons ici quels outils utiliser et développer pour favoriser cette utilité, afin de répondre à la question : "comment évaluer ?". Il faut se poser au préalable ces questions :

- À quoi sert l'évaluation (réguler l'apprentissage, contrôler ?)
- À qui les résultats sont-ils destinés (aux élèves, à l'enseignant, à l'institution, aux parents ?)
- Qu'est-ce qui est évalué (des compétences, des stratégies ?)
- Qui évalue ? (les élèves, l'enseignant, l'équipe d'enseignants ?)
- Quand évaluer ? (avant, pendant, après l'apprentissage, à la fin d'un cycle ?)
- Comment ? (par des observations, des analyses, des tests ?)

Nous présentons ci-dessous quelques méthodes pour évaluer les apprentissages : afin de les réguler ou de les contrôler.

## Réguler

La régulation, comme l'indiquent Goupil et Lusignan (1993) permet de corriger, réorienter ou améliorer les conditions d'apprentissage. Elle peut être menée soit par l'élève, soit par l'enseignant. Il est possible de concevoir (ou de faire concevoir) des plans de régulation comme les deux suivants (tirés de Goupil & Lusignan, 1993, p. 274) :

**Tableau I – Tableaux d'évaluation des activités, par l'élève et l'enseignant.**

**Régulation par l'élève**

| Méthode                                                                                         | Résultat                                |
| ----------------------------------------------------------------------------------------------- | --------------------------------------- |
| J'apprends par cœur                                                                             | J'ai moins de 5/10. J'ai compris que... |
| Régulation                                                                                      |                                         |
| Je fais un résumé. Je classe les informations en ordre. J'utilise le questionnement réciproque. | J'ai 9/10. J'ai compris que...          |

**Régulation par l'enseignant**

| Méthodes                                                                                                                                                                                                                                                       | Résultat                                       |
| -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------- |
| J'ai fait un exposé. J'ai demandé de lire un texte. J'ai utilisé le manuel de base.                                                                                                                                                                            | 70 % des élèves sont sous le seuil de réussite |
| Régulation                                                                                                                                                                                                                                                     |                                                |
| Je place les élèves en sous-groupes de travail Je suggère plusieurs sources d'information.Je vérifie les plans de travail. Les élèves vérifient les hypothèses en laboratoire. Les élèves discutent des résultats entre eux. J'anime un retour en grand groupe | 40 % des élèves sont sous le seuil de réussite |

Il est également possible de mettre en place des fiches de suivi de l'activité, à l'usage de l'élève. Ces fiches permettent à l'élève de constater leurs progrès, sur un certain nombre d'activités d'un même domaine. Le contenu de ces fiches dépendent de la discipline enseignée. En voici un exemple tiré de Goupil et Lusignan (1993, p. 280). L'enseignant pourra reporter, sur une fiche, la manière dont ses élèves remplissent les différents critères.

**Tableau II - Fiche de suivi à l'usage de l'élève, à propos d'un travail d'expression écrite.**

```{eval-rst}
+--------------------+----------------------------------------------------------------------+
| Nom :              | Objectif :                                                           |
+====================+======================================================================+
| Critères           | Activités                                                            |
+--------------------+----------------------------------------+--------------+--------------+
|                    | Activité 1                             | Note obtenue | Commentaires |
+--------------------+----------------------------------------+--------------+--------------+
| Sujet clair        |                                        |              |              |
+--------------------+----------------------------------------+--------------+--------------+
| Argumentation      |                                        |              |              |
+--------------------+----------------------------------------+--------------+--------------+
| Vocabulaire        |                                        |              |              |
+--------------------+----------------------------------------+--------------+--------------+
| Ponctuation        |                                        |              |              |
+--------------------+----------------------------------------+--------------+--------------+
| Orthographe        |                                        |              |              |
+--------------------+----------------------------------------+--------------+--------------+
```

## Évaluer

Mais l'évaluation peut être plus précise, et centrée. Toujours dans le   domaine de la production écrite, voici une grille d'autoévaluation de   texte, permettant de passer chaque paragraphe au crible (adapté de Bereiter & Scardamalia, 1987). Les réponses en gras signalent des problèmes.

**Tableau III - Grille d'autoévaluation d'une production d'écrit (Bereiter & Scardamalia, 1987).**

| - Le professeur trouvera ce paragraphe important                 | oui\*\*non\*\* |
| ---------------------------------------------------------------- | -------------- |
| - Le professeur sera sensible à l’argumentation de ce paragraphe | oui\*\*non\*\* |
| - Le professeur sera intéressé par ce paragraphe                 | oui\*\*non\*\* |
| - Le professeur comprendra ce paragraphe                         | oui\*\*non\*\* |
| - Je pourrais donner un exemple                                  | **oui**non     |
| - Je pourrais en dire plus                                       | **oui**non     |
| - Je pourrais dire cela plus clairement                          | **oui**non     |
| - Je pourrais placer ce paragraphe ailleurs                      | **oui**non     |
| - Je fais un hors-sujet                                          | **oui**non     |

Passons maintenant à la recension de quelques instruments de mesure des performances des élèves. Nous ne citerons pas ici les classiques questionnaires à choix multiple, les phrases à compléter ou cases à cocher, pour décrire des pratiques de mesure moins courantes (Goupil et Lusignan, 1993, chap. 9).

- *Le portfolio* est une manière attractive et intéressante de rendre compte du travail et des compétences des élèves ([voir document sur l'orientation voir document sur l'\`orientation](orientation.md)).
- *Les listes de vérification* permettent le suivi d'une activité. Elles consistent en une liste chronologique de tâches à accomplir, suivies de trois colonnes à renseigner : "en cours d'exécution", "exécutées", "prévues". Ainsi, l'élève ou le groupe d'élèves sait où il en est dans son projet.
- *Les grilles d'observation* sont plus difficiles à concevoir, elles peuvent permettre à l'enseignant (ou à d'autres élèves) de rendre compte finement du travail des élèves. Plus largement Morissette (1993, p. 194) signale qu'on peut observer : les habiletés, les habitudes de travail, les attitudes sociales (respect des lois, etc.), les attitudes scientifiques, les intérêts et les jugements de    valeur des élèves. Elles peuvent aussi s'utiliser ponctuellement, à propos de quelques élèves en difficulté. Contrairement aux listes de vérification, prescriptives, ces grilles d'observations sont plus descriptives, bien que pouvant amener à des régulations (*voir plus haut*). Le lecteur trouvera dans Peretti (1985) une mine de grilles d'observation et de questionnaires pour un très grand nombre de    situations scolaires (réunions, travail en groupe, évaluation des attitudes, des professeurs, etc.).

Terminons par des grilles plus originales, qui peuvent apporter un peu de changement dans la procédure d'évaluation, sans modifier son objet (Peretti, 1985).

**Figure 1 - Cible d'évaluation du travail de groupe. Les secteurs de la cible sont coloriés selon l'appréciation.**

```{image} /images/outileval1.png
:align: center
:scale: 80 %
```

**Figure 2 - Profil d'élève en étoile. La surface de l'étoile est proportionnelle au niveau de l'élève.**

```{image} /images/outileval2.png
:align: center
:scale: 80 %
```

## Quizz

```{eval-rst}
.. eqt:: Outilseval-1

  **Question 1. Selon Goupil et Lusignan (1993), les listes de vérification permettent :**

  A) :eqt:`C` `Le suivi d'une activité`
  B) :eqt:`I` `De rendre compte du travail et des compétences de l'élève de manière attractive`
  C) :eqt:`I` `À l'enseignant de rendre compte finement du travail de l'élève`
  D) :eqt:`I` `De faire un feedback`
```

```{eval-rst}
.. eqt:: Outilseval-2

  **Question 2. Selon Goupil et Lusignan (1993), les portfolios permettent :**

  A) :eqt:`I` `De faire un feedback`
  B) :eqt:`C` `De rendre compte du travail et des compétences de l'élève de manière attractive`
  C) :eqt:`I` `Le suivi d'une activité`
  D) :eqt:`I` `À l'enseignant de rendre compte finement du travail de l'élève`
```

## Références

- Bereiter, C., & Scardamalia, M. (1987). *The psychology of written composition*. Illsdale: Erlbaum.
- Goupil, G., & Lusignan, G. (1993). *Apprentissage et enseignement en milieu scolaire*. Montréal: Gaëtan Morin.
- Morissette, D. (1993). *Les examens de rendement scolaire*. Sainte-Foy : Presses de l'Université Laval.
- Peretti, A. de (1985). *Recueil d'instruments et de processus d'évaluation formative (2 tomes)*. Paris : INRP.
