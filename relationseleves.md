(relations-eleves)=

# Comment l'enseignant engage-t-il les élèves à apprendre ?

```{index} single: auteurs; Dessus, Philippe single: auteurs; Besse, Émilie
```

:::{admonition} Information
- **Auteurs** : [Philippe Dessus](http://pdessus.fr/), Inspé & LaRAC, Univ. Grenoble Alpes. Le quizz a été réalisé par Emilie Besse, projet [ReflexPro](http://www.idefi-reflexpro.fr).
- **Date de création** : Octobre 2005.
- **Date de publication** : {sub-ref}`today`.
- **Statut du document** : Terminé.
- **Résumé** : Ce document, en se basant sur des théories de la motivation, permet de réfléchir aux manières d'engager les élèves dans un apprentissage.
- **Voir aussi** : [Les droits des élèves et les relations maître-élève](droitel.md)  et le Document {ref}`motivation`.
- **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.
- **Licence** : Document placé sous licence *Creative Commons* : [BY-NC-SA](http://creativecommons.org/licenses/by-nc-sa/3.0/fr/)
:::

## Introduction

"Tu comprendras plus tard à quoi servent les maths" est une phrase souvent entendue par les élèves, de la part de parents ou d'enseignants à court d'arguments. Beaucoup d'élèves s'engagent dans de difficiles tâches scolaires, sans avoir vraiment beaucoup de raisons concrètes, immédiates, de le faire : elles ne leur permettront d'avoir un (éventuel) métier que bien plus tard. Pour autant, ils sont motivés, et la question traitée ici est : pourquoi ? et surtout, quels moyens a l'enseignant de faire en sorte qu'ils le soient.

## Ce que l'on sait

Selon différentes théories de psychologie sociale de la motivation (voir Pelletier & Vallerand, 1993 ; Viau, 1997, pour des introductions), les raisons de l'engagement d'une personne (*e.g.*, un élève) peuvent dépendre (parmi de nombreux autres facteurs) :

- de la manière dont elle s'estime être la cause de ses propres actions  (motivation intrinsèque). Toute personne a besoin de se considérer comme autodéterminée, c'est-à-dire comme la cause première de ses  propres actions. Plus précisément, son degré d'autodétermination implique des niveaux de motivations différentes, que l'on peut situer  sur un continuum entre une motivation intrinsèque : on réalise une  activité pour le plaisir et la satisfaction qu'on en retire, donc  pour l'activité elle-même ; et une motivation extrinsèque, en  fonction de laquelle l'activité n'est pas réalisé pour elle-même,  mais soit pour en retirer quelque chose de plaisant, ou éviter  quelque chose de désagréable, une fois l'activité terminée.
- des buts par rapport à sa réussite (ici, dans l'apprentissage)  qu'elle se fixe. Toute personne se fixe, soit des buts liés à sa  propre performance (puis-je être meilleur que les autres ? comment l'enseignant me juge-t-il ?) ou à sa maîtrise d'un contenu (comment puis-je réaliser ce travail ? qu'est-ce que je dois apprendre).

Même si, fondamentalement, chaque élève peut avoir un certain degré de motivation intrinsèque, et avoir des buts d'apprentissage différents, l'enseignant a un rôle à jouer dans ces attitudes : il peut, sans forcément s'en rendre compte, insister plus sur des aspects compétitifs (centrés sur la performance), insister plus sur des récompenses (motivation extrinsèque). Il est donc important de réfléchir au climat que l'on instaure par rapport à l'engagement des élèves.

Ces théories permettent en outre de mieux comprendre de nombreux aspects contextuels ou relationnels au sein de la classe. Par exemple, quel type de contexte permet une motivation intrinsèque (récompenses, type de tâche, ou évaluation donnés aux élèves, Pelletier & Vallerand, 1993). Nous ne les traiterons pas ici, pour nous centrer sur les aspects relationnels. De plus, si les élèves se fixent des buts d'apprentissage élevés, ce n'est pas toujours uniquement pour des raisons académiques, mais aussi pour un principe de responsabilité sociale, notamment vis-à-vis de l'enseignant (Eccles & Wigfield, 2002).

Sur quels paramètres l'enseignant peut-il jouer pour engager l'élève dans l'apprentissage ? Tout d'abord, un enseignant maintenant un contrôle important sur ses élèves plutôt que de leur laisser une certaine autonomie aura des élèves avec une faible motivation intrinsèque. Ensuite, l'enseignant doit essayer de comprendre l'élève (ses intérêts, croyances, motivations), en clair : ce qui l'autodétermine. Dans cette perspective, un enseignant qui essaie de faire réussir ses élèves, éveille leur curiosité, leur créativité et encourage les relations interélèves élèvera leur motivation pour l'apprentissage. Enfin, il est important de faire en sorte que l'élève soit motivé par la maîtrise d'un contenu plus que par la performance elle-même. L'élève peut se préoccuper de "paraître plus intelligent" ou d' "être plus intelligent". Des études (voir Viau, 1997) montrent que les élèves centrés sur la maîtrise d'un contenu "s'engage plus en profondeur dans ses travaux, \[...\] il utilise des stratégies qui lui permettent d'apprendre mieux (*id*., p. 51). En revanche, un élève centré sur la performance utilise plus de stratégies pouvant être inefficace, voire masque son éventuelle incompétence.

## Ce que l'on peut faire

Il existe certains questionnaires (Sarrazin *et al*., 2006) qui permettent d'évaluer le niveau d'attitude de l'enseignant du point de vue de l'autodétermination de ses élèves, mais aussi le type de "climat" qu'il pense nécessaire d'instaurer dans sa classe. Dans le contexte de cette fiche, il est important de noter que ces questionnaires ne sont présentés qu'à des fins de discussion des pratiques, et non à des fins d'évaluation de ces dernières.

### Evaluer le climat d'une classe

Le questionnaire ci-dessous permet de déterminer le type de climat d'apprentissage que vous pensez installer dans votre classe. Cette version du *Learning Climate Questionnaire* a été traduite et adaptée en français par Sarrazin, Tessier, & Trouilloud (2006).

Pour chacune des questions suivantes, répondez selon la grille : 1. Jamais ; 2. Rarement ; 3. Difficile de choisir ; 4. Assez souvent ; 5. Tout le temps.

Il est important de répondre ce que vous pensez vraiment, et non ce qui vous paraît le plus convenable. La manière de traiter et interpréter chaque questionnaire est décrite plus bas.

**Questionnaire 1 - L'évaluation des attitudes à propos du climat de la classe (Sarrazin et al., 2006).**

```{eval-rst}
+-------------------------------------------------------------------------------------+-------------+
| 1. En classe, je laisse à mes élèves des possibilités de choisir certaines choses   | 1 2 3 4 5   |
| (comme les textes, à travailler, les livres à lire, quand faire telle ou telle      |             |
| chose...).                                                                          |             |
+-------------------------------------------------------------------------------------+-------------+
| 2. En classe, j'essaie de me mettre à la place de mes élèves, en particulier ceux   | 1 2 3 4 5   |
| qui ont des difficultés.                                                            |             |
+-------------------------------------------------------------------------------------+-------------+
| 3. En classe, quand je m'adresse à un élève, je fais en sorte que ce que je lui dis | 1 2 3 4 5   |
| soit de nature à renforcer sa confiance en lui dans la matière.                     |             |
+-------------------------------------------------------------------------------------+-------------+
| 4. En classe, j'accepte les élèves comme ils sont.                                  | 1 2 3 4 5   |
+-------------------------------------------------------------------------------------+-------------+
| 5. En classe, je vérifie que mes élèves ont bien compris ce qu'il fallait faire.    | 1 2 3 4 5   |
+-------------------------------------------------------------------------------------+-------------+
| 6. En classe, j'encourage les élèves à poser des questions.                         | 1 2 3 4 5   |
+-------------------------------------------------------------------------------------+-------------+
| 7. En classe, je fais tout pour que mes élèves aient confiance en eux dans la       | 1 2 3 4 5   |
| matière.                                                                            |             |
+-------------------------------------------------------------------------------------+-------------+
| 8. En classe, je réponds aux questions que me posent les élèves, en cherchant à     | 1 2 3 4 5   |
| être précis-e et compréhensible.                                                    |             |
+-------------------------------------------------------------------------------------+-------------+
| 9. En classe, je suis attentiv-e aux avis de mes élèves (sur les différents points  | 1 2 3 4 5   |
| du cours).                                                                          |             |
+-------------------------------------------------------------------------------------+-------------+
| 10. En classe, je sais très bien gérer les émotions de mes élèves (je suis sensible | 1 2 3 4 5   |
| à ceux qui peuvent avoir de la peine ; je sais bien m'occuper de ceux qui peuvent   |             |
| être agressifs ou en colère, etc.)                                                  |             |
+-------------------------------------------------------------------------------------+-------------+
| 11. En classe, je fais sentir à mes élèves que leur parole compte autant que la     | 1 2 3 4 5   |
| mienne.                                                                             |             |
+-------------------------------------------------------------------------------------+-------------+
| 12. En classe, il m'arrive d'être négatif-ve, voire cassant-e quand je m'adresse à  | 1 2 3 4 5   |
| un élève qui fait des erreurs.                                                      |             |
+-------------------------------------------------------------------------------------+-------------+
| 13. En classe, j'essaie de comprendre la manière dont les élèves voient les choses  | 1 2 3 4 5   |
| (leurs idées, opinions), avant de suggérer une nouvelle manière de faire.           |             |
+-------------------------------------------------------------------------------------+-------------+
| 14. Je fais sentir à mes élèves qu'ils peuvent me faire part de leurs sentiments ou | 1 2 3 4 5   |
| émotions.                                                                           |             |
+-------------------------------------------------------------------------------------+-------------+
```

### Evaluer ses priorités en termes d'aide à l'apprentissage

**Questionnaire 2 - Comment les enseignants pensent aider l'apprentissage de leurs élèves ? (Sarrazin et al., 2006)**

```{eval-rst}
+------------------------------------------------------------------------------------+-------------+
| 1. Je fais un effort tout particulier pour reconnaître les progrès individuels des | 1 2 3 4 5   |
| élèves, même s'ils sont en dessous de la moyenne.                                  |             |
+------------------------------------------------------------------------------------+-------------+
| 2. J'accorde des privilèges particuliers aux élèves qui ont fait le meilleur       | 1 2 3 4 5   |
| travail.                                                                           |             |
+------------------------------------------------------------------------------------+-------------+
| 3. Je prends en compte les progrès réalisés, quand je note un élève.               | 1 2 3 4 5   |
+------------------------------------------------------------------------------------+-------------+
| 4. J'essaie de donner aux élèves encore plusieurs exercices qui sont adaptés à     | 1 2 3 4 5   |
| leurs besoin et à leur niveau.                                                     |             |
+------------------------------------------------------------------------------------+-------------+
| 5. Je montre à la classe le travail des meilleurs élèves comme un exemple à        | 1 2 3 4 5   |
| suivre.                                                                            |             |
+------------------------------------------------------------------------------------+-------------+
| 6. Je mets en valeur les élèves qui ont obtenu un bon résultat, comme un modèle à  | 1 2 3 4 5   |
| suivre.                                                                            |             |
+------------------------------------------------------------------------------------+-------------+
| 7. Durant la classe, j'essaie d'offrir plusieurs activités différentes à faire,    | 1 2 3 4 5   |
| afin de permettre aux élèves de faire des choix (en fonction de leur niveau ou de  |             |
| leur intérêt).                                                                     |             |
+------------------------------------------------------------------------------------+-------------+
| 8. Je fais comprendre à chaque élève quel est son niveau, comparé à celui des      | 1 2 3 4 5   |
| autres élèves.                                                                     |             |
+------------------------------------------------------------------------------------+-------------+
| 9. J'encourage les élèves à entrer en "compétition" avec les autres.               | 1 2 3 4 5   |
+------------------------------------------------------------------------------------+-------------+
```

## Analyse des pratiques

1. Une fois l'un des questionnaires remplis et le score calculé, se mettre en petits groupes de scores différents, et justifiez votre conception du guidage de vos élèves. Vous pouvez reprendre chaque item d'un questionnaire, et vous demander quelles peut être les conséquences sur les élèves, à plus long terme, de l'attitude de l'enseignant décrite dans l'item.
2. Pour une activité d'apprentissage donnée, mentionnez les types de phrases que vous dites souvent à vos élèves. Essayez de déterminer si elles sont centrées maîtrise ou performance.

## Quizz

```{eval-rst}
.. eqt:: Relationseleves-1

        **Question 1. Selon Pelletier et Vallerand (1993), les raisons de l'engagement d'une personne peuvent dépendre :**

        A) :eqt:`I` `De l'obligation d'effectuer une action ou une tâche`
        B) :eqt:`C` `De la manière dont elle s'estime être la cause de ses propres actions`
        C) :eqt:`I` `De son humeur`
        D) :eqt:`I` `De sa motivation `
```

```{eval-rst}
.. eqt:: Relationseleves-2

        **Question 2. Sur quels paramètres un enseignant peut-il jouer pour engager l'élève dans l'apprentissage ?**

        A) :eqt:`I` `En effectuant un contrôle important sur ses élèves`
        B) :eqt:`I` `En minimisant les relations interélèves`
        C) :eqt:`C` `En laissant à ses élèves une certaine autonomie`
        D) :eqt:`I` `En instaurant un climat de compétitivité entre les élèves`
```

```{eval-rst}
.. eqt:: Relationseleves-3

        **Question 3. Sur quels paramètres un enseignant peut-il jouer pour engager l'élève dans l'apprentissage ?**

        A) :eqt:`C` `En essayant de comprendre ses élèves`
        B) :eqt:`I` `En favorisant la performance des élèves`
        C) :eqt:`I` `En favorisant le travail individuel`
        D) :eqt:`I` `En contrôlant de près le travail de ses élèves`


```

## Références

- Drique, E. (2015). [Style motivationnel instauré par l'enseignant et motivation des élèves](https://dumas.ccsd.cnrs.fr/dumas-01169801/document). Mémoire MEEF-1{sup}`er` degré. Univ. Lille.
- Eccles, J. S., & Wigfield, A. (2002). Motivational beliefs, values, and goals. *Annual Review of Psychology, 53*, 109-132.
- [Self-Determination Theory](http://selfdeterminationtheory.org)
- Pelletier, L. G., & Vallerand, R. J. (1993). Une perspective humaniste de la motivation : les théories de la compétence et de l'autodétermination. In R. J. Vallerand & E. E. Thill (Eds.), *Introduction à la psychologie de la motivation* (pp. 233-284). Laval : Etudes vivantes.
- Sarrazin, P., Tessier, D., & Trouilloud, D. (2006). Traduction du *Learning Climate Questionnaire* de Williams & Deci (1996).
- Viau, R. (1997). *La motivation en contexte scolaire*. Bruxelles : De Boeck.

## Traitement et interprétation des questionnaires

### Questionnaire 1

Faire la moyenne des différents scores obtenus pour chaque item, hormis pour la question 12, où vous inversez le score (*i.e.*, 1 devient 5, 2 devient 4, etc.). Plus la valeur est grande (proche de 5), plus l'enseignant pense qu'il laisse les élèves autonomes. Plus elle est proche de 0, plus l'enseignant pense les contrôler.

### Questionnaire 2

Faire deux scores totaux : le score M avec la moyenne des scores obtenus aux items 1, 3, 4, et 7, correspond à la centration de l'enseignant sur des buts de maîtrise ou d'apprentissage des élèves ; le score P, avec la moyenne des scores obtenus aux items 2, 5, 6, 8 et 9, correspond à une centration sur des buts de performance (voir texte).
