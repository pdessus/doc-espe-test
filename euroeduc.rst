.. _euro_educ:

=======================================
Aperçu des systèmes éducatifs européens
=======================================

.. index::
	single: auteurs; Dessus, Philippe
	single: auteurs; Besse, Émilie

.. admonition:: Information

  * **Auteurs** : `Philippe Dessus <http://pdessus.fr/>`_, Inspé & LaRAC, Univ. Grenoble Alpes. Le quizz a été réalisé par Emilie Besse, projet `ReflexPro <http://www.idefi-reflexpro.fr>`_.

  * **Date de création** : Décembre 2004.

  * **Date de publication** : |today|.

  * **Statut du document** : Terminé.

  * **Résumé** : Il peut être utile, pour un enseignant, d'avoir des moyens de comparer le système éducatif (centré ici sur le primaire) de son pays avec ceux d'autres pays, notamment européens. Il s'apercevra   qu'ils diffèrent notablement, selon les :index:`rythmes scolaires`, la sélectivité de l'orientation, les priorités en termes d'acquisition de connaissances.
  
  * **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.

  * **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_


Ce que l'on sait
================

Pour Legendre (1993, p. 1222), un système d'éducation est "un ensemble plus ou moins intégré d'institutions (système scolaire, famille, groupes religieux, médias, bibliothèques, musées, associations, etc.), de structures, de législations, de finalités, d'objectifs, de programmes, de méthodes, d'activités, de modes de fonctionnement ainsi que de ressources humaines, matérielles et financières dont se dote une société pour offrir à ses membres les services et les ressources nécessaires au développement de leurs habiletés et de leurs connaissances". 

Les types de systèmes scolaires
-------------------------------

Ainsi le système d'éducation est plus large que le système scolaire, et comprend toutes les manières parallèles de pourvoir à l'éducation des enfants dans une société. Il dépend de nombreux facteurs : économiques, géographiques, philosophiques, culturels, politiques ou même religieux. Nous allons nous intéresser ici aux systèmes scolaires. Pour Foerster (2000), il existe, en Europe, quatre types de systèmes scolaires :

* *L'école unique* des pays scandinaves (Suède, Norvège, Islande,  Danemark, Finlande), où tous les élèves, de 7 à 16 ans, suivent le  même cursus en primaire et collège, dans une école unique, la  *Folkeskole*, dans le même groupe-classe, avec le même professeur  principal, mais des enseignants différents dès le primaire. Le  redoublement est inconnu, et 95 %  des élèves obtiennent un diplôme  en dernière année de ce cycle.
* *Le type sélectif* des anglo-saxons (Grande Bretagne), cette fois, la continuité est plutôt recherchée dans le secondaire, et 10 % des élèves sont scolarisés dans des *Grammar Schools* (établissements privés), sélectives. Les anglo-saxons, comme les scandinaves, privilégient l'acquisition de l'autonomie à celle des connaissances (voir le système latin). Par exemple, ils auront tendance à mettre en avant les progrès des élèves indépendamment  de leur niveau initial (Abboudi, 1997).
* *Le type germanique*, différencié (Allemagne, Autriche, Suisse, Pays-Bas, Luxembourg), qui comprennent une orientation différenciée très tôt des élèves en trois filières : le *Gymnasium* (30 % des élèves), menant à des études universitaires, la *Realschule*, menant à des études supérieures non universitaires et une formation professionnelle courte, les *Hauptschulen*. Il faut noter toutefois que l'image sociale des élèves provenant de cette dernière filière est bien meilleure que celle équivalente des pays latins.
* *Le type latin*, privilégiant l'acquisition des connaissances (France, Italie, Espagne, Grèce), caractérisé par une attention plus importante à l'acquisition des savoirs et connaissances : ainsi, le système de contrôle des connaissances, des examens, des notes y a une part plus importante que dans les autres systèmes, ainsi que la présence du redoublement.

Rythmes scolaires
-----------------

Dans la plupart des pays européens (Foerster, 2000), les élèves de primaire fréquentent l'école de 5 à 6 jours par semaine, mais des différences importantes apparaissent sur la fréquentation à l'année en primaire : 600 h/an dans les pays scandinaves et en Grèce, pour plus de 800 dans les pays latins, en Belgique et Luxembourg (à 7 ans). Dans le secondaire, ces différences s'estompent. Il existe aussi des différences importantes dans le nombre de jours de classe par ans, et la France est l'un des pays où ce nombre est le plus bas (180 jours/an, pour près de 200 en Allemagne, Grande-Bretagne, Danemark, Italie).

Comment ces données peuvent influer sur les performances des élèves ? L'intéressante étude de Testu (1994) répond à cette question. Il  montre notamment que l'organisation du temps scolaire n'influe pas sur les niveaux d'attention des élèves d'élémentaire, et que les performances scolaires des élèves au cours de la journée évoluent de la même manière quel que soit leur pays. En revanche, la vigilance des élèves de 6-7 ans n'est pas la même selon les jours de la semaine :   elle croît tout au long de la semaine, avec un faible creux le jeudi pour les semaines de cinq jours, et avec un fort creux le vendredi   pour les semaines à la française (pas classe le mercredi et samedi   après-midi). Ces différences s'estompent pour les élèves de 10-11 ans.

Performances des élèves
-----------------------

Les performances des élèves français sont plutôt meilleures, comparativement à ceux d'autres pays, en mathématiques ; moyens en lecture et sciences. Cela étant, il existe peu d'études, hormis en lecture, à propos des élèves du primaire (DEP 2004). Concernant les performances en compréhension de l'écrit des élèves en fin d'élémentaire (âge moyen, 10;6 ans), la France se situe dans la moyenne des autres pays occidentaux, la Suède, les Pays-Bas, la Grande-Bretagne, les USA, l'Italie, l'Allemagne et la République Tchèque ayant des élèves aux performances statistiquement plus élevées (OCDE 2003). Concernant les performances en mathématiques et culture scientifique des élèves de 15 ans (OCDE, 2003 d'après PISA, 2000), les élèves français ont des performances qui les placent dans le groupe de tête, bien qu'à la dizième place.

Egalité des systèmes scolaires
------------------------------

Il faut aussi s'interroger sur le caractère plus ou moins égalitaire des systèmes scolaires. Les études sur ce sujet (voir Dupriez & Dumay, 2004) montrent que l'idée que sélection rime avec efficacité est un mythe : les systèmes différenciant plus tôt les cursus (Allemagne, Autriche, Suisse, Luxembourg, Pays Bas) sont à la fois moins efficaces   et plus inégalitaires que les autres (pays scandinaves). De plus, Dupriez et Dumay montrent que cette inégalité n'est pas liée à une   inégalité sociale, mais bien propre au système scolaire du pays.

Quizz
=====

.. eqt:: euroeduc-1

	**Question 1. Quel type de système scolaire privilégie l'acquisition de connaissances chez l'élève ?**

	A) :eqt:`I` `L'école unique des pays scandinaves.`
	B) :eqt:`I` `Le type sélectif des pays anglo-saxons.`
	C) :eqt:`I` `Le type germanique différencié.`
	D) :eqt:`C` `Le type latin.`

.. eqt:: euroeduc-2

	**Question 2. Parmi les pays suivants, lequel a le nombre de jours de classe par an le plus élevé ?**

	A) :eqt:`I` `l'Allemagne`
	B) :eqt:`C` `la France`
	C) :eqt:`I` `l'Italie`
	D) :eqt:`I` `le Danemark`

.. eqt:: euroeduc-3

	**Question 3. Dans lequel des pays suivants le système scolaire est-il le plus inégalitaire ?**

	A) :eqt:`C` `les Pays-Bas`
	B) :eqt:`I` `la Norvège`
	C) :eqt:`I` `la Suède`
	D) :eqt:`I` `l'Islande`



Références
==========

* Abboudi, M.-C. (1997). Enquête internationale sur le fonctionnement des écoles élémentaires. Paris :  DEP, coll. Les dossiers d'éducation et formations, *85*.
* DEP (2004). *Regards sur le système éducatif français*. http://www.education.gouv.fr/stateval/regards/index.htm.
* Dupriez, V. & Dumay, X. (2004). L'égalité dans les systèmes scolaires : effet école ou effet société ? *Les Cahiers du GIRSEF*, *31*. http://www.girsef.ucl.ac.be/Cahiers_CREF/031cahier.pdf
* Foerster, C. (2000). Étude comparée des systèmes éducatifs européens : approche pédago­gique, enjeux communs et particularités. *Actes du colloque « Les systèmes éducatifs en Europe : Approche juridique et financière »* (pp. 15-42). Barcelone : AAC.
* Legendre, R. (1993). *Dictionnaire actuel de l'éducation*. Montréal/Paris: Guérin/Eska.
* OCDE (2003). *Regards sur l'éducation. Les indicateurs de l'OCDE*. http://www.sourceocde.org/data/cm/00011536/9603062E.pdf.
* Testu, F. (1994). *Étude des rythmes scolaires en Europe*. Paris : D.E.P., coll. Les dossiers d'éducation et formations, *46*.