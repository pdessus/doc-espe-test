(taches-ens-el)=

# Analyser les tâches de l'enseignant et des élèves

```{index} single: auteurs; Dessus, Philippe single: auteurs; Besse, Émilie
```

:::{admonition} Information
- **Auteurs** : [Philippe Dessus](http://pdessus.fr/), LSE & Inspé, Univ. Grenoble Alpes. Le quizz a été réalisé par Emilie Besse, projet [ReFlexPro](http://www.idefi-reflexpro.fr).
- **Date de création** : octobre 2001, doc. créé en octobre 2000.
- **Date de publication** : {sub-ref}`today`.
- **Statut du document** : Terminé.
- **Résumé** : Prendre connaissance des méthodes d'analyse de la tâche. La tâche -- ce que l'élève ou l'enseignant a à faire -- est intéressante à analyser car on peut la comparer à l'activité réelle de l'élève ou l'enseignant. Les éventuels écarts permettre de mettre en valeur l'adaptation de ces derniers à la situation. Ce chapitre propose trois méthodes - théorie de l'activité, analyse en algorithme ou arbre de tâches - pour analyser et représenter les tâches dévolues aux élèves et enseignants.
- **Voir aussi** : Les documents {ref}`activite` et {ref}`tache`.
- **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.
- **Licence** : Document placé sous licence *Creative Commons* : [BY-NC-SA](http://creativecommons.org/licenses/by-nc-sa/3.0/fr/).
:::

## Introduction

Une des activités importantes de l'enseignant est de donner des tâches aux élèves (des choses à faire). Il est ici question de deux manières d'analyser et de représenter ces tâches. Nous montrerons, de plus, qu'il est nécessaire que l'élève transforme la tâche qui lui est allouée, car elle ne peut pas être totalement explicitée (cela prendrait trop de temps, et nécessiterait bien trop de papier ou d'explications orales). De plus, il peut être utile pour l'enseignant de décomposer, de son point de vue, la tâche que l'élève pourrait mettre en oeuvre lors de son travail. Ensuite, il pourra utiliser des techniques d'observation de l'activité (voir doc. SAPP {ref}`activite`).

## Ce que l'on sait

### Tâche prescrite et tâche effective

La tâche est ce qu'un sujet a à faire et l'activité est le comportement (physique et mental) qu'il met en oeuvre pour réaliser cette tâche {cite}`durand96`. On peut distinguer

- *la tâche prescrite*, celle qui est donnée par les prescripteurs (supérieurs hiérarchiques, manuels, instructions officielles) de la tâche effective, celle que le sujet (élève) effectue réellement lorsqu'il est dans une situation de travail précise. On peut transposer dans l'étude de l'enseignement et de l'apprentissage de telles notions. La tâche prescrite de l'enseignant comprend donc une combinaison de prescriptions de son inspecteur, des instructions officielles et de divers manuels scolaires ;
- *la tâche effective* -- pouvant différer de la tâche presrite -- étant ce qu'il met réellement en œuvre dans sa propre classe, avec ses élèves. Il est intéressant de noter que ses élèves, eux-mêmes, considèrent la consigne que l'enseignant leur propose comme une tâche prescrite, pouvant donc être différente de leur tâche réelle, qu'ils effectuent véritablement dans leur environnement.

Ces différentes transpositions doivent nous sembler normales. Le fait de changer sa tâche prescrite en tâche effective n'est pas obligatoirement signe d'erreur. Il suffit de penser que la grève du zèle (qui est, chez les travailleurs, la stricte application des consignes du supérieur hiérarchique, sans aucune adaptation à la situation) n'est pas une façon normale de travailler. L'enseignant - et l'élève - sont donc des interpréteurs de tâches et une partie de leur travail est de l'adapter à sa situation, voire à leurs capacités. L'enseignant ne réalise pas en classe exactement ce qu'il avait prévu dans sa préparation ; l'élève ne se conforme pas exactement aux consignes de l'enseignant.

### La théorie de l'activité

Jonassen et ses collègues {cite}`jonassen99` ont repris et appliqué la {index}`théorie de l’activité` au domaine de l'éducation avec le numérique. Cette théorie, issue du courant historico-culturel soviétique (Vygotski, Luria, et Leontiev), peut être qualifiée de constructiviste en ce que toute activité d’apprentissage prend bien place dans un contexte, qu’il soit social ou matériel. Posant, elle aussi, la question fondamentale du niveau d’analyse de l’activité, elle part du principe que l’apprentissage émerge de l’activité plutôt qu’il en est un préalable. Popularisée en Europe par l’école de psychologie finlandaise animée par Engeström {cite}`engestrom99`, cette théorie est maintenant répandue et appliquée à de nombreux domaines éducatifs (p. ex. {cite}`linard01,trouche03`).

Toute activité, selon cette théorie, est organisée en systèmes dont la Figure 1 ci-dessous rend compte, et ce triplet (Sujet, Objet, Outil) est le cadre minimal d’analyse de l’activité. Le sujet de l’activité peut être aussi bien une personne qu’un groupe. L’objet de l’activité est le produit mental ou physique recherché. Les outils peuvent être aussi bien matériels que cognitifs, et modifient autant l’activité qu’ils peuvent être modifiés par elle. Autre distinction fondamentale, celle entre activité, qui émerge au niveau du groupe, et action, qui est décrite au niveau du sujet.

Ce cadre a été appliqué {cite}`jonassen99` dans le design d’environnements d’apprentissage informatisés. Leurs auteurs l’ont détaillé en six étapes, dont nous ne détaillons ici faute de place que les quatre premières. Même si ces auteurs estiment qu’il s’agit d’une méthode de conception, il faut reconnaître que ce qu’ils évoquent dans leur article montre plutôt une méthode d’analyse de systèmes d’enseignement.

Les différents éléments impliqués dans cette théorie sont les suivants. Un sujet, à l’aide d’outils, atteint un objectif et peut accepter des règles de travail d’une communauté dans laquelle une division du travail permet de contribuer à l’objectif.

Les différents éléments peuvent être représentés par la Figure 1 suivante :

Figure 1 - Un système d'activité, d'après {cite}`engestrom99,engestrom10`.

```{image} /images/taches-img1.jpg
:align: center
:scale: 40 %
```

Le "sujet" est l'individu ou le sous-groupe dont le point de vue est choisi dans l'analyse. L'"objet" réfère à l'espace dans lequel l'activité est dirigée. L'objet est transformé en résultats avec l'aide d'instruments (outils, signes). La communauté comprend les différents individus et sous-groupes partageant le même objet. La division du travail réfère à la division des tâches, du travail, des statuts. Les règles réfèrent aux manières implicites et explicites de régulation, convention, standards qui contraignent les actions dans le système considéré {cite}`engestrom10` p. 6.

Plusieurs sous-systèmes peuvent être détaillés dans l'analyse (par triades) {cite}`jonassen00` :

- *production d'objet* (sujet-outil-objet) : décrit comment un objet est produit par un sujet ;
- *consommation* (sujet-communauté-objet) : décrit comment le sujet et sa communauté collabore pour agir sur l'objet ;
- *distribution* (objet-communauté-division du travail) : décrit comment les activités sont distribuées, partagées, en fonction des lois ou attentes sociales ;
- *échange* (sujet-règles-communauté) : décrit comment l'activité d'un sujet est régulée par différentes règles admises dans la communauté ;
- *activité* (le triangle entier et ses sous-systèmes), explique de quelle manière les sujets mettent en œuvre leur activité dirigées par des buts, dans une communauté, avec des règles, etc.

La Figure 2 ci-dessous est un exemple d'utilisation de ce cadre {cite}`sharples07`, dans le domaine de la visite d'un musée.

```{image} /images/taches-img2.jpg
:align: center
:scale: 40 %
```

Figure 2 - Un système d'activité : utilisation d'appareils portables dans des musées {cite}`sharples07`.

Tableau 1 - Extraits d’une méthode de conception et d'analyse d'environnements scolaires appliquant la théorie de l’activité {cite}`jonassen99`.

| 1.Clarifier l'utilisation du système d'activité 1.1 Comprendre le(s) contextes(s) pertinents dans le(s)quel(s) l’activité est inscrite | Lister une série de problèmes typiquement rencontrés par les sujets. Quand et où ces problèmes surviennent-ils d’habitude ? Examiner des communications qui ont lieu pendant l’activité                                                                                                                                                                                                                                                                                                                                     |
| -------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1.2 Comprendre le sujet, ses motivations et sa vision des contradictions du système                                                    | Lister exhaustivement les motifs et buts de chaque groupe de sujets. Quelles attentes ont-ils ? Qui les initie ? Comment contribuer à la dynamique de la situation observée ? Interroger des personnes (in-) directement associées à l’activité, afin de comprendre les contradictions, les facteurs généraux qui influent l’activité                                                                                                                                                                                       |
| 2. Analyser le système d’activité 2.1 Définir le sujet 2.2 Définir la ou les communauté(s) 2.3 Définir l’objet                         | Qui sont les participants du système ? Quels sont leurs rôles, leurs croyances ? Quels sont les résultats attendus de l’activité ? Sur quels critères seront-ils évalués ? Quels sont les règles et rôles de chaque membre du groupe ? Quels sont les buts et motifs de l’activité et comment sont- ils reliés aux buts et motifs des autres et de la société ? Quelle est la division du travail dans le système d’activité ? Quelle récompense est perçue lorsqu’un but est accompli ?\[…\]                               |
| 3. Analyser la structure de l’activité 3.1 Définir l’activité elle-même                                                                | Comment le travail est réalisé ? Quelles sont les activités auxquelles les sujets participent ? \[…\] Quelles sont les normes, règles, procédures à documenter ? Quelles formes de pensée, de « types rationnels », ou de présupposés théoriques ont motivé le travail ? comment ont-ils évolué ? Qu’est-ce que les sujets pensent d’eux- mêmes ? \[…\]                                                                                                                                                                     |
| 3.2 Décomposer l’activité en ses sous-actions et opérations                                                                            | Pour chaque activité (gouvernée par un but), observer et analyser les actions mises en œuvre, et par qui. Pour chaque action (de plus bas niveau), observer et analyser les opérations mises en œuvre, et par qui.                                                                                                                                                                                                                                                                                                          |
| 4. Analyser les outils médiateurs 4.1 Outils médiateurs et médiation                                                                   | Quels outils peuvent être utilisés dans cette activité ? Sont-ils d’emblée accessibles aux participants ? Quels sont les outils matériels (instruments, machines) ou cognitifs (signes, procédures, méthodes, formalismes, lois) utilisés pour mettre en œuvre des activités dans différents contextes et interactivités (projets). Comment ces outils ont-ils évolué dans le temps ? Quels sont les modèles, théories, méthodes standardisées guidant cette activité ? Comment les participants les utilisent- ils ? \[…\] |
| 4.2 Règles et rôles médiateurs                                                                                                         | Quelles sont les règles informelles guidant les activités ? Comment ces règles ont-elles évolué dans le temps ? \[…\] Qui a traditionnellement assumé les différents rôles ? Comment ces rôles évoluent-ils dans le temps ?                                                                                                                                                                                                                                                                                                 |

Il peut être donc intéressant d'étudier les différentes transpositions d'une tâche jusqu'à l'activité, c'est-à-dire la mise en œuvre de cette dernière dans la réalité d'une situation. Nous avons besoin pour cela de méthodes de représentation de la tâche et de méthodes d'analyse de l'activité. Décrivons-les.

## Ce que l'on peut faire

Il est possible, lorsqu'on est confronté au but de comprendre les tâches et l'activité d'une personne, de réaliser une analyse de ses tâches, c'est-à-dire, par observation ou questionnement, découper ses tâches en sous-parties cohérentes.

Nous pouvons distinguer, très sommairement, deux méthodes visuelles de représentation de tâches. La représentation en arbre et la représentation en algorithmes. Un arbre est une représentation hiérarchisée de tâches. Une dimension (*e.g.*, la hauteur) étant dévolue à la durée, c'est-à-dire qu'une tâche précède une autre si elle est située plus haut ; l'autre dimension (*e.g.*, la largeur) étant dévolue à la hiérarchie, ou décomposition en sous-tâches, c'est-à-dire qu'une tâche est une décomposition d'une autre si elle est située plus à droite. Mais on peut adopter la disposition inverse. Il importe aussi de comprendre que l'on a jamais achevé une décomposition hiérarchique de tâches ; on s'arrêtera lorsqu'on aura l'impression que la description de la tâche permet sa mise en œuvre sans trop de difficultés.

À titre d'exemple, voici deux arbres de tâches différents, le premier (Tableau 1) décrivant une tâche d'enseignant, le deuxième (Figure 2) une tâche d'élève. Quant à l'algorithme (Figure 3), il indique, sous une forme séquentielle les différentes étapes soumises à décision, choix. On aura intérêt à choisir une description sous cette forme dès lors que les tâches à mettre en oeuvre dépendent de nombreux choix et/ou s'il y a des retours en arrière dans la mise en oeuvre de tâches. Pour finir (Figure 4), un algorithme à propos de l'accord des adjectifs. Noter que le losange représente un choix.

Tableau 1 - La préparation d'une dictée, arbre de tâche {cite}`bonnaire95`

| Tâche niveau 1      | Tâche niveau 2                                          | Tâche niveau 3                                                                                                                                                                                                                                                       |
| ------------------- | ------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Préparer une dictée | Mobiliser l'attention                                   | -consigne de silence -annoncer qu'on prépare la dictée -demander aux élèves d'anticiper la réussite                                                                                                                                                                  |
|                     | Faire découvrir le texte                                | -lecture oralisée -faire reconnaître le type de texte - faire repérer le temps des verbes                                                                                                                                                                            |
|                     | Faire comprendre le texte                               | -expliciter les marques anaphoriques -repérer les connecteurs - reformuler le vocabulaire, les tournures syntaxiques difficiles                                                                                                                                      |
|                     | Repérer les difficultés grammaticales et de conjugaison | - faire repérer chaque verbe et son infinitif - faireretrouver son sujet - faire dire la personne - marquer les désinences de l'accord verbal - faire repérer chaque adjectif - faire dire le nom auquel il se rapporte - marquer les désinences de l'accord nominal |
|                     | Repérer les difficultés d'orthographe d'usage           | - Pour les cas typiques, faire rappeler la règle,marquer les lettres concernées en bleu - pour les cas particuliers, consigne d'évocation                                                                                                                            |
|                     | Faire copier le texte                                   | - Consigne d'application sur la calligraphie -Consigne de relecture - Contrôle des copies - Correction des erreurs de copie - Etude silencieuse du texte                                                                                                             |

Figure 3 - Un arbre de tâches pour la rédaction de résumé de texte en anglais {cite}`rafal95`.

```{image} /images/taches-img3.jpg
:align: center
:scale: 80 %
```

Figure 4 - Un algorithme d'accord en nombre de l'adjectif {cite}`lebrun91`.

```{image} /images/taches-img4.jpg
:align: center
:scale: 100 %
```

## Quizz

```{eval-rst}
.. eqt:: Taches-1

        **Question 1. Qu'est-ce qu'une tache effective ?**

        A) :eqt:`I` `Une tâche donnée par un prescripteur`
        B) :eqt:`I` `Une tâche que se donne à faire une personne`
        C) :eqt:`C` `une tâche réellement effectuée par la personne`
        D) :eqt:`I` `Une tâche comprenant une échéance`
```

```{eval-rst}
.. eqt:: Taches-2

        **Question 2. Une tâche effective et une tâche prescrite ont pour particularité :**

        A) :eqt:`C` `De pouvoir être différentes`
        B) :eqt:`I` `De toujours être identiques`
        C) :eqt:`I` `De toujours être fondamentalement différentes`
        D) :eqt:`I` `De toujours être fondamentalement identiques`
```

```{eval-rst}
.. eqt:: Taches-3

        **Question 3. Le triplet de la théorie de l'activité est le suivant :**

        A) :eqt:`I` `Sujet, Règle, Communauté de travail`
        B) :eqt:`I` `Règle, Division du travail, Objet`
        C) :eqt:`I` `Objet, Production, Communauté de travail`
        D) :eqt:`C` `Sujet, Objet, Outil`

```

## Références

```{bibliography}
:filter: docname in docnames
```
