.. _droit_eleves:

============================================================
Les droits des élèves dans la classe et dans l'établissement
============================================================

.. index::
  single: auteurs; Dessus, Philippe
  single: auteurs; Besse, Émilie


.. admonition:: Information

  * **Auteurs** : `Philippe Dessus <http://pdessus.fr/>`_, Inspé & LaRAC, Univ. Grenoble Alpes. Le quizz a été réalisé par Emilie Besse, projet `ReFlexPro <http://www.idefi-reflexpro.fr>`_. (Document très fortement inspiré de P. Merle, 2001. Les droits des élèves. *Rev. Fr. Sociol*., 42-1, 81-115.).

  * **Date de création** : Novembre 2001.

  * **Date de publication** : |today|.

  * **Statut du document** : Terminé. 

  * **Résumé** : Ce document fait le point sur la manière dont les droits des élèves sont établis et respectés dans les établissements et les classes. On voit aussi comment ces droits peuvent influer sur les   relations maître-élèves.

  * **Voir aussi** : `Document sur les sanctions <sanction.html>`_.
  
  * **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.

  * **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.


Introduction
============

L'enseignant entretient avec ses élèves des relations qui sont influencées par un grand nombre d'éléments : le règlement intérieur en est un parmi les plus visibles, mais il y a aussi les règles de vie qu'il peut instituer dans sa classe, les textes réglementaires nationaux, etc. À une époque dans laquelle l'apprentissage de la citoyenneté devient une des priorité de l'Education nationale et dans laquelle les questions de violence, d'incivilité sont plus que jamais en avant, il convient de réfléchir aux droits des élèves. Ainsi, en tant qu'enseignant, on pourra être plus attentif à ces droits, et donc aux élèves eux-mêmes.


Ce que l'on sait
================

La Loi du 10 juillet 1989, et le Décret du 18 février 1991 instituent les droits des élèves au sein de l'Education nationale. Cette loi est liée à la Convention internationale des droits de l'enfant, qui stipule que les États-membres "garantissent à l'enfant qui est capable de discernement le droit d'exprimer librement son opinion sur toute question l'intéressant, les opinions de l'enfant étant dûment prises en considération eu égard à son âge et à son degré de maturité" (cité par Merle, 2001, p. 83). Voici pour les aspects législatifs, mais qu'en est-il de la manière dont ces droits sont perçus par les élèves ?

Rapport des élèves au droit
---------------------------

Comme le dit Merle (2001), les élèves n'ont pas la même perception de leurs droits selon le niveau d'enseignement dans lequel ils sont. Les élèves du primaire ont une notion du droit ancrée dans l'expérience et peu différenciée de celle d'obligations ("J'ai le droit de travailler, d'aller en récréation, d'aller aux toilettes, mais pas pendant qu'on travaille", Merle, 2001, p. 84).

Dans le secondaire (collège ou lycée), en revanche, les élèves ont clairement conscience de leurs obligations, et parallèlement, ont un sentiment général d'injustice, car d'une part ils déclarent ne pas savoir quels peuvent être ces droits, et d'autre part, que ces droits sont plus largement des droits des enfants, plutôt que des élèves (*i.e.*, "Ne pas se faire taper par le prof.). Certains élèves relient ces droits au seul règlement intérieur. Enfin, ces droits sont directement liés aux relations maître-élève au sein de la classe :

.. epigraph::

  "On n'a pas beaucoup de droits en cours. On ne peut pas toujours   dire ce qu'on pense. Si on dit se qu'on pense du prof c'est pas trop   bien vu. Il y a des profs qui peuvent dire : 'Tu n'y arriveras   jamais, t'es con'. Je ne sais pas s'ils ont le droit, mais en tous   cas, ils le font. C'est pas normal puisque nous on n'a pas le droit   de dire ça à eux. Le droit ? Répondre au prof s'il nous provoque."   (Merle, 2001, p. 86)

Dans certaines filières (*i.e.*, technologiques), les élèves ont une conception détournée du droit : c'est ce qu'ils peuvent faire pour transgresser le règlement : "Des droits ? Je vois pas [...] Le droit de s'amuser, le droit de faire chier les profs, le droit de pas écouter, je le regarde pas, j'écoute pas, j'ai un chewing-gum, je fais rien". (*ibid*.)

Le règlement intérieur : une asymétrie des droits
-------------------------------------------------

Toujours selon Merle, les règlements intérieurs des collèges et lycées ne diffèrent que sur un point : la liberté d'association, autorisée dans ces derniers seulement. Hormis cela, les règlements intérieurs définissent les droits des élèves en termes d'obligations : les paragraphes sur les droits, lorsqu'ils existent, sont bien moins développés que ceux sur les obligations. Pour donner un exemple, le respect des personnes figure très souvent en tant qu'obligation, rarement en tant que droit : "les élèves respectent l'ensemble des membres de la communauté éducative tant dans leur personne que dans leur bien" (*ibid.*) n'a pas souvent sont équivalent du point de vue du personnel éducatif. Ainsi, les devoirs de "tenue correcte" ou de "ponctualité" devraient normalement concerner tous les personnels de l'établissement. 

De plus, cette réglementation n'a qu'une portée limitée sans possibilité pour les élèves de porter plainte, même si, informellement, les CPE ou d'autres enseignants peuvent avoir un rôle d'écoute des plaintes d'élèves. Le seul recours pour l'élève est que ses parents défendent sa cause auprès de l'enseignant qu'il juge avoir porté atteinte à ses droits.

Les droits des élèves, des conceptions concurrentes 
---------------------------------------------------

Lorsqu'on questionne les élèves du secondaire sur leurs droits, émerge une position "syndicale", qui comporte un certain nombre de revendications, qui sont des moyens de défendre leur :index:`métier d'élève` (repris de Merle, 2001, p. 98 *et sq*. et de Dubet, 1999, p. 181 *et sq*.) (voir aussi Document :ref:`metier-eleve`) :

* *Avoir des bons profs, le droit de ne pas savoir* : ce point est  lié au droit de poser des questions lorsqu'on n'a pas compris,  d'avoir des explications. Plus largement, ce point est lié au droit à  l'éducation de la `Loi d'orientation de 1989 <http://daniel.calin.free.fr/textoff/code_education.html>`_.

* *L'égalité de traitement, une notation ajustée au mérite* : Le  favoritisme, la notation "à la tête du client", sont des sujets  souvent abordés par les élèves, et sont rarement pris en compte par  les enseignants, qui pensent que leur notes sont non discutables. Les  élèves ressentent comme une grande injustice le fait que le travail  fourni ne soit pas noté en conséquence. Pourtant, les travaux en  docimologie  montrent combien les notes "objectives" sont difficiles à produire. Toutefois, quelques procédures simples permettent de régler  quelques-uns de ces problèmes : double correction pour les contrôles  importants, barème précis, évaluer seulement ce qui a fait l'objet  d'un enseignement… En bref, exercer une juste notation.

* *L'égalité de traitement* : traiter tous les élèves de la même  manière* : Comme l'observe Dubet (1999), prendre en compte le mérite  les élèves peut être incompatible avec un principe égalitaire, qui  serait de traiter tous les élèves de la même manière. On peut  toutefois convenir que l'Ecole doit traiter tous les élèves de la  même manière, indépendamment de ses performances. La compétition mise  en place par les notes ne peut donc être continue, afin de préserver  une cohésion entre élèves.

* *Le droit d'expression* : faire attention à ce qu'on dit* : C'est une des revendications les plus fortes des élèves, que ce soit à l'intérieur de la classe (pouvoir s'exprimer sur la quantité de travail demandé, sur l'échéancier des contrôles…) ou à l'extérieur (rôle des délégués de classe, des parents d'élèves au conseil de classe).

Ce que l'on peut faire
======================

Nous l'avons déjà signalé, l'expression et le respect des droits des élèves influence fortement les relations entre enseignants et élèves. Il est donc utile de réfléchir aux points suivants :

* *Quelle conception de leurs droits ont mes élèves ?* Permettre de faire s'exprimer les droits et devoirs des élèves, ce n'est pas seulement leur faire élaborer et signer une "charte", des règles de vie qui sont, la plupart du temps une série d'interdictions qui ne seront pas ou peu respectées. C'est peut-être en s'autorisant une écoute, une prise en compte de leurs problèmes plus grande que celle habituellement consacrée dans les établissements (voir ci-dessous les droits imprescriptibles vus par Perrenoud). 

* *Le règlement intérieur de mon établissement*. Le règlement intérieur est le principal repère de mes élèves. Il importe donc de le lire attentivement, et de vérifier -- sa conformité avec les textes officiels, -- quels usages vous allez pouvoir en faire.

* *Les principales revendications de mes élèves*. Les trois points principaux, mentionnés ci-dessus, montrent les principales revendications des élèves. Il s'agira donc de tenter de les rendre minimales.

.. only:: html

  Quizz
  =====

  .. eqt:: Droitel-1

    **Question 1. Quel document sert souvent de référence aux élèves pour connaître leurs droits ?**

    A) :eqt:`I` `Le décret du 18 février 1991`
    B) :eqt:`I` `La loi du 10 juillet 1989`
    C) :eqt:`C` `Le règlement intérieur de l'établissement`
    D) :eqt:`I` `Le texte de Perrenoud sur les droits de l'apprenant`


  .. eqt:: Droitel-2

    **Question 2. D'après les travaux de Merle (2001), les élèves du primaire :**

    A) :eqt:`I` `Pensent que leurs droits sont ceux d'enfants plus que d'élèves`
    B) :eqt:`I` `Ont la même perception de leurs droits que les élèves du secondaire`
    C) :eqt:`C` `Ne distinguent pas bien leurs droits de leurs obligations`
    D) :eqt:`I` `Se réfèrent au règlement intérieur pour faire valoir leurs droits`

  .. eqt:: Droitel-3

    **Question 3. Dans quel niveau d'enseignement les élèves ont le plus souvent un sentiment d'injustice vis-à-vis de leurs droits et obligations ?**

    A) :eqt:`I` `Dans le primaire`
    B) :eqt:`C` `Dans le secondaire`
    C) :eqt:`I` `À l'université`
    D) :eqt:`I` `Dans les classes préparatoires`


Analyse de pratiques
====================

* Décrire un événement vécu dans lequel interviennent la notion de droit des élèves.

* Relire le règlement intérieur de votre établissement : les droits des élèves y sont-ils définis autrement qu'en termes d'obligations ? Donnez des exemples.

* Que pouvez-vous vous permettre de dire à vos élèves en classe ?

* Qu'est-ce que vos élèves peuvent-ils se permettre de vous dire en classe ?

* Dans ma classe, qu'est-ce que les élèves ont le droit de faire ? pas le droit de faire ?

* Comment ces droits sont-ils énoncés ? Implicitement, explicitement ?

* À votre avis, de quelle manière ces droits jouent-ils sur la relation que vous entretenez avec vos élèves ? Comment s'y prennent-ils pour vous manifester leur désaccord lorsqu'il leur semble que vous "allez trop loin" ?

* `Perrenoud (1995) <http://www.unige.ch/fapse/SSE/teachers/perrenoud/php_main/php_1995/1995_11.html>`_ a établi la liste des droits imprescriptibles des élèves, sur le modèle de ceux du lecteur (Pennac). Quels sont les droits à ajouter ? Quels sont ceux que vous supprimeriez de la liste ? Pourquoi ?

  #. Le droit de ne pas être constamment attentif
  #. Le droit à son for intérieur
  #. Le droit de n'apprendre que ce qui a du sens
  #. Le droit de ne pas obéir six à huit heures par jour
  #. Le droit de bouger
  #. Le droit de ne pas tenir toutes ses promesses
  #. Le droit de ne pas aimer l'école et de le dire
  #. Le droit de choisir avec qui l'on veut travailler
  #. Le droit de ne pas coopérer à son propre procès (c'est-à-dire le droit de ne pas coopérer à sa propre évaluation, en ne donnant pas le bâton pour se faire battre, en se montrant à son meilleur avantage, etc.)
  #. Le droit d'exister comme personne

 
Références
==========

* Code de l'éducation, *Journal Officiel* Numéro 143 du 22 juin 2000, Mis en ligne par Daniel Calin (http://daniel.calin.free.fr/textoff/code_education.html)

* Dubet, F. (1999). Sentiments et jugements de justice dans l'expérience scolaire. In D. Meuret (Ed.), *La justice du système éducatif*. Bruxelles: De Boeck, pp. 177-193.

* Merle, P. (2001). Les droits des élèves, droits formels et quotidien scolaire des élèves dans l'institution. *Rev. Fr. Sociol*., 42-1, 81-115.

* Perrenoud, P. (1995). `Les droits imprescriptibles de l'apprenant ou comment rendre le métier d'élève plus vivable <http://www.unige.ch/fapse/SSE/teachers/perrenoud/php_main/php_1995/1995_11.html>`_. *Éducations*, 1, 56-62.