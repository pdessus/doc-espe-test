(eval-formative)=

# Évaluation formative, ou pour l'apprentissage

% p. 8 {Ministère de l'éducation de l'Ontario, 2020 #22697}

```{index} single: Auteurs; Dessus, Philippe
```

:::{admonition} Information
- **Auteur** : [Philippe Dessus](http://pdessus.fr), LaRAC & Inspé, Univ. Grenoble Alpes.
- **Date de création** : Janvier 2021.
- **Date de modification** : {sub-ref}`today`.
- **Statut** : En cours.
- **Résumé** : Ce document donne quelques éléments à propos d'évaluation formative, c'est-à-dire le processus “d'évaluer pour (faire) apprendre”.
- **Voir aussi** : Le Document {ref}`peda-univ:`eval_online`.
- **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.
- **Licence** : Document placé sous licence *Creative Commons* : [BY-NC-SA](http://creativecommons.org/licenses/by-nc-sa/3.0/fr/).
:::

## Introduction

L'évaluation tient une place importante dans l'activité d'enseignement. Un enseignant des premier/second degré travaille, en moyenne, environ 50 h par semaine {cite}`bryant20`. La moitié de ce temps se passe en présence directe des élèves et 11 % environ du temps est consacré à l'évaluation (rubriques évaluation et conseil aux élèves).

```{image} /images/hebdo-ens.jpg
:align: center
:scale: 80 %
```

**Figure 1** - Allocation hebdomadaire du travail d'un enseignant (Briant *et al*., 2020, p. 3).

## Quelques définitions

Commençons par quelques rappels rapides à propos de l'évaluation. L’évaluation en éducation est le processus par lequel **on délimite, obtient, et fournit des informations utiles permettant de juger des décisions possibles**”. ({cite}`stufflebeam80` p. 48). C'est donc un *processus* (dynamique, se déroulant dans le temps et l'espace) qu'on *prépare* (délimite), pour observer, et recueillir des informations qu'on synthétise pour les diffuser. Enfin, l'évaluation est orientée vers des buts précis (on n'évalue pas pour évaluer, mais pour agir, prendre des décisions à propos des étudiants).

C'est un fait connu que les types d'évaluation varient (doivent varier) en fonction du moment de l'apprentissage, du support, des processus cognitifs scrutés, du contexte, des décisions à prendre. Pour reprendre une métaphore célèbre :

> ”Lorsque le cuisinier goûte la soupe, c'est formatif ; lorsque les invités goûtent la soupe, c'est sommatif” (R. Stake cité par {cite}`scriven91` p. 169)

Nous avons donc l'évaluation *de* l'apprentissage (sommative), **évaluer en testant**, où des inférences sont faites entre l'évaluation de réponse à un test et les compétences de l'élève (on goûte une soupe pour évaluer la qualité du cuisinier). Et l'évaluation *pour* l'apprentissage (formative), **évaluer en jugeant**, où la qualité de la production est *directement* évaluée par l'élève, aidé par l'enseignant. Il ne s'agit pas de dire que l'une de ces deux est meilleure que l'autre, les deux sont nécessaires.

L'approche "évaluer en testant" va  proposer aux apprenants divers exercices (p. ex., de type choix multiple) les amenant à activer, reformuler leurs connaissances. L'approche "évaluer en jugeant" va proposer aux apprenants des activités plus complexes et authentiques (portfolios, jeux de rôles, test OverAll).

## Évaluer pour l'apprentissage : les rétroactions

Les rétroactions, c'est-à-dire les informations que l'enseignant donne en retour à l'apprenant sont des éléments importants du processus d'évaluation. Trois types de rétroactions, informantes pour l'apprenant, sont à formuler : ({cite}`hattie07,lepareur16`).

- Où est-ce que je vais ? (quels sont mes buts ?)
- Comment est-ce que j'y vais  ? (quels sont les progrès que j'ai faits vers ces buts ?)
- Que faire ensuite ? (quelles tâches dois-je réaliser pour mieux y arriver ?)

## Évaluer pour l'apprentissage : le processus

Des auteurs ({cite}`wiliam07` cités par {cite}`lepareur16`) ont retracé le processus lié à évaluer pour l'apprentissage :

1. Clarifier, partager et faire comprendre les **intentions d’apprentissage et les critères de réussite**
2. Organiser de véritables **discussions, activités et tâches** qui produisent des preuves sur les apprentissages
3. Donner un **feedback** qui fait progresser les élèves
4. Inciter les élèves à être des **personnes-ressources** pour leurs pairs
5. Inciter les élèves à **s’approprier leurs apprentissages**

{cite}`pellegrino13`, p. 380) montre qu'il y a trois moments interconnectés dans ce processus.

- Quelles **connaissances et compétences précises** voulez-vous voir acquises par les élèves et *comment* voulez-vous qu'ils les acquièrent ?
- Quelles **preuves** d'acquisition de ces connaissances et compétences accepterez-vous comme valables ? Comment les analyserez-vous et interpréterez-vous ?
- Quelles **tâches** les élèves *réaliseront* pour communiquer leurs connaissances et compétences ?

Il est à noter que ces différentes questions mettent en avant les buts de maîtrise des apprenants plutôt que des buts de performance.

### Ce que l'on peut faire

Il existe de nombreuses manières d'évaluer les apprenants {cite}`tinoca14` p. 6, notamment en les amenant à travailler dans les activités suivantes :

- Étude de cas
- Présentation de travaux d'étudiants
- Construction d'un artefact (blog, images, cartes de concepts, poster, vidéo, site internet, jeu, etc.)
- Commentaires critiques
- Débat
- Ecriture de dissertations
- Panel d'experts
- Portfolios \[…\]
- Projet
- Rapport (d'un travail empirique)
- Jeu de rôle
- Simulations

### Références

```{bibliography}
:filter: docname in docnames
```
