(redoublement)=

# Le redoublement

```{index} single: auteurs; Campanale, Françoise single: auteurs; Besse, Émilie
```

:::{admonition} Information
- **Auteurs** : Françoise Campanale Espé & LSE, Univ. Grenoble Alpes. Le quizz a été réalisé par Emilie Besse, projet [ReFlexPro](http://www.idefi-reflexpro.fr).
- **Date de création** : Février 2006.
- **Date de publication** : {sub-ref}`today`.
- **Statut du document** : Terminé.
- **Résumé** :  La France reste encore un des pays d’Europe où l’on redouble le plus. Ce document donne quelques statistiques sur les pratiques de redoublement en France, en primaire et secondaire, puis s'interroge sur son efficacité.
- **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.
- **Licence** : Document placé sous licence *Creative Commons* : [BY-NC-SA](http://creativecommons.org/licenses/by-nc-sa/3.0/fr/).
:::

## Ce que l'on sait

### Quelques chiffres

D'une façon générale, le redoublement a diminué ces dernières années mais le redoublement est encore très installé dans les pratiques et les mentalités comme un moyen d’aide aux élèves en difficulté scolaire voire comme un moyen de ne pas freiner ceux qui suivent (comme palliatif à une trop grande hétérogénéité).

« Entre 1990 et 1995, le redoublement au primaire augmente pour les deux classes de fin de cycle (CE1 et CM2) \[6 % au CE1 en 95 et 2,5% en CM2 en 95\], mais il reste paradoxalement fermement installé dans les deux classes desquelles il aurait dû disparaître (CE2 \[2,7%\] et CM1 \[1,9%\]) » (Paul, 1996, p. 21). Les chiffres qui suivent sont tirés du même auteur. Un élève sur 5 redouble en primaire. 14 % des élèves intègrent la 6{sup}`e` avec 1 an de retard et 37 % de ceux qui l’intègrent avec 2 ans de retard quittent le collège avant la 3{sup}`e`. Il en ressort que ceux qui redoublent ont moins de chances de réussite scolaire ultérieure que les élèves à l’heure.

Au collège, aujourd’hui, environ un élève sur 3 doit recommencer une année entre la 6{sup}`e` et la 3{sup}`e`. Ceux qui arrivent au collège avec un an de retard risquent davantage de redoubler que ceux qui arrivent à l’heure (44 % contre 34 %). Au collège les enfants d’ouvriers ou employés redoublent plus que les enfants des cadres supérieurs (40 % contre 19 %). Au lycée, le taux de redoublement en seconde est assez stable ; on y redouble plus qu’en première et terminale. Il y a plus de redoublement en seconde générale et technologique (16,8%) qu’en seconde professionnelle (6,3 %).

Deux tiers des enseignants sont favorables au redoublement. Trois quarts d’entre eux pensent que c’est une chance supplémentaire pour l’élève et ils proposent le redoublement en fonction d’un profil. Or quand on leur demande les motifs pour lesquels ils proposent le redoublement, en premier vient le manque de travail souvent associé à une situation familiale peu favorable à la réussite scolaire (manque d’aide, éclatement de la cellule familiale). On peut se demander si le redoublement peut changer cette situation.

### Le redoublement est-il équitable ?

Pour proposer le redoublement, les enseignants s’appuient sur leurs notes, sans tenir compte du niveau général de la classe issu de tests standardisés. Ainsi un élève moyen dans une 6{sup}`e` forte peut avoir des notes inférieures à la moyenne et se voir proposer un redoublement alors qu’un élève équivalent dans une 6{sup}`e` plus faible aura des notes supérieures à la moyenne et passera en 5{sup}`e`. Des recherches, en France et en Belgique, ont montré des écarts importants entre les résultas scolaires des élèves et ceux obtenus à des tests standardisés réalisés par des chercheurs. Outre l’effet du niveau général de la classe, les enseignants, comme l’ont montré les docimologues n’accordent pas la même pondération aux différents critères (pour certains l’orthographe est très importante, pour d’autres moins), diffèrent au niveau des indicateurs retenus pour un même critère, et leur notation est soumise à différents effets parasites qui agissent à leur insu (Merle, 1998). On peut donc en conclure qu’une grande part d’arbitraire intervient dans les décisions de redoublement.

De plus les familles ne sont pas à égalité face à une proposition de redoublement, certaines sauront argumenter en faveur du passage, d’autres accepteront comme fatalité  le redoublement ou l’orientation vers une filière professionnelle. « Un enfant de famille ouvrière doit avoir une note de 14 en 5{sup}`e` pour que sa famille ait à son égard la même demande en matière de redoublement qu’une famille de cadre supérieur pour un enfant qui n’aurait qu’une note de 8 à ce niveau » (Paul, 1996, p. 90). Les familles de cadre supérieur étant beaucoup plus persévérantes. On peut en conclure que le redoublement n’est pas équitable.

### Le redoublement est-il efficace ?

En fait les statistiques montrent que le redoublement n’est pas efficace puisqu’il ne permet pas la poursuite d’un cursus long et qu’il pénalise surtout les élèves travailleurs en difficulté moyenne.

A l’école primaire, les redoublants progressent moins vite que les faibles qui n’ont pas redoublé. Le redoublement n’est pas efficace. Au collège, en analysant les résultats des évaluations nationales (6:sup:`e` et 5{sup}`e`), on constate que les élèves faibles qui ne redoublent pas ont réalisé les mêmes acquisitions que ceux qui font le cycle d’observation en 3 ans. Toutefois, des élèves peu en difficulté en 5{sup}`e` et qui redoublent obtiennent ensuite des résultats sensiblement meilleurs que leurs homologues qui n’ont pas redoublé.

« L’impact d’une année de redoublement en termes de progression des acquisitions est donc rarement positif. Il est négatif (en 6{sup}`e`) et neutre (en 5{sup}`e`) pour les élèves les plus faibles ; il est neutre (en 6{sup}`e`) et légèrement positif (en 5{sup}`e`) pour les élèves les moins handicapés ». (Paul, 1996, p. 86).  Le redoublement pèse négativement sur les décisions d’orientation scolaire. « L’ensemble des élèves dont on a décidé le redoublement en 5{sup}`e`aurait eu de sérieuses chances de connaître une 4{sup}`e` sanctionnée positivement » (id. p. 87). C’est au niveau de la 3{sup}`e` que le redoublement apparaît le plus rentable.

Ajoutons que le redoublement affecte négativement l’image que les élèves ont d’eux-mêmes et que leur famille leur renvoie. L’effet ponctuellement positif d’un redoublement s’efface avec le temps, et ceux qui ont un an de retard, seront jugés plus sévèrement par les enseignants ultérieurs (l’effet d’étiquetage et effet Pygmalion).

Les enquêtes internationales (PISA 2000) montrent que les élèves de 15 ans d’Europe du nord (notamment la Finlande, les Pays Bas, la Norvège, la Suède, le Danemark, Royaume Uni et Irlande) obtiennent des performances en lecture, mais aussi en sciences, égales (pour le Danemark en lecture) ou supérieures à la moyenne internationale, alors que ces pays ne pratiquent pas le redoublement (Crahay, 2004).

« Le redoublement se révèle d’une efficacité douteuse en termes d’acquisitions scolaires ou d’orientation » (Paul, 1996, p. 96).

« Il semble établi désormais que le fait de répéter une année et, partant, de recommencer la totalité d’un programme de cours n’aide pas les élèves en difficulté à surmonter les obstacle qui les empêchent de réussir honorablement à l’école. (…) Si le redoublement ne constitue pas un moyen pour venir en aide aux élèves en difficulté, il paraît opportun de chercher d’autres moyens pour résoudre cet important problème » (Crahay, 2004, p. 22).

Le redoublement est un indicateur d’un niveau d’acquisitions insuffisant et non un remède pédagogique efficace. Il a un coût et son financement pourrait être utilisé différemment, pour du soutien différencié par exemple.

## Ce que l'on peut faire

### Faire du redoublement une mesure exceptionnelle

- Développer dans les établissements une culture de l’évaluation et une connaissance par les enseignants des travaux de recherche sur l'évaluation.
- Demander aux enseignants qu’en début d’année, ils prennent en compte les résultats des évaluations institutionnelles (6:sup:`e`, 5{sup}`e`) concernant leur classe et les autres de même niveau, ou qu’ils organisent une évaluation diagnostique à partir de tests    élaborés en commun et corrigés en mêlant les copies des classes
- Au conseil de classe du premier trimestre demander l’identification précise de la nature des difficultés et chercher des moyens d’aide    individualisée.

Remarque : Pour les activités de soutien : Une étude de D. Bonora et J. Hornemann (1994) citée par Paul (1996) montre que les dispositifs d’aide récemment instaurés sont surtout bénéfiques dans les collèges défavorisés. Les plus efficaces sont surtout le soutien individualisé concernant les méthodes de travail (tenue du cahier de textes, apprentissage des leçons, compréhension d’un énoncé, présentation des travaux écrits, utilisation du dictionnaire) et l’incitation à la lecture. Finalement la recommandation des chercheurs est de viser dans ces séances des objectifs scolaires très traditionnels.

- Si le redoublement est prononcé au dernier trimestre, l’accompagner d’une analyse des difficultés pour qu’au début de l’année suivante    une aide individualisée puisse être mise en œuvre avec un suivi relationnel par le prof principal qui vise à positiver la situation    de l’élève redoublant.
- Plutôt un cycle aménagé en 3 ans qu’un redoublement.

## Conclusion

Penser l'établissement comme une "organisation apprenante" qui non seulement permette aux élèves d’apprendre mais joue aussi ce rôle pour ses personnels, notamment les enseignants.

## Quizz

```{eval-rst}
.. eqt:: Redoublement-1

        **Question 1.   Depuis quelques années, le redoublement**

        A) :eqt:`I` `A été supprimé`
        B) :eqt:`I` `A augmenté`
        C) :eqt:`C` `A diminué`
        D) :eqt:`I` `Est resté stable`
```

```{eval-rst}
.. eqt:: Redoublement-2

        **Question 2.   Selon Paul (1996)**

        A) :eqt:`C` `Les élèves ayant redoublé ont moins de chances de réussite scolaire ultérieure`
        B) :eqt:`I` `Les élèves ayant redoublé ont autant de chance de réussite scolaire ultérieure`
        C) :eqt:`I` `Les élèves ayant redoublé sont autant issus de familles de cadres supérieurs que de familles d'ouvriers`
        D) :eqt:`I` `Les élèves ayant redoublé en seconde proviennent plus souvent de filières professionnelles`
```

```{eval-rst}
.. eqt:: Redoublement-3

        **Question 3.   L'impact d'une année de redoublement est**

        A) :eqt:`I` `Positif en 6e pour les élèves les plus faibles`
        B) :eqt:`I` `Négatif en 6e pour les élèves les plus forts`
        C) :eqt:`I` `Positif en 5e pour les élèves les plus faibles`
        D) :eqt:`C` `Positif en 5e pour les élèves les plus forts`
```

```{eval-rst}
.. eqt:: Redoublement-4

        **Question 4.   Le redoublement apparaît  le plus rentable au niveau de**

        A) :eqt:`I` `La 6e`
        B) :eqt:`I` `La 5e`
        C) :eqt:`I` `La 4e`
        D) :eqt:`C` `La 3e`

```

## Références bibliographiques

- Bonora, D., Hornemann, J. (1994). Influence des modes de   fonctionnement du collège sur la progression et l’orientation des   élèves au cycle d’observation. *Les dossiers Education et Formations*,   40, mars, 83-103.
- Crahay Marcel (2004). Peut-on conclure à propos des effets du redoublement ? *Revue Française de Pédagogie*,  148, 11-23.
- Crahay Marcel (2004). L’école en Europe, des conceptions divergentes. *Sciences Humaines*, n° 152 bis, 6-8.
- Merle Pierre (1998). *Sociologie de l’évaluation scolaire.* Paris : PUF, Que sais-je ? n° 3278.
- Paul Jean-Jacques (1996). *Le redoublement : pour ou contre ?*.Paris : ESF.
