.. _index_qcm_mardif:

****************************************************
Bienvenue dans l'atelier TICE d'avril 2017 du MARDIF
****************************************************


.. Pour compiler l'index mettre ce nom de fichier dans conf.py>master_doc

.. TO.DO:
.. AJOUTER

.. Zimbardi, K., Colthorpe, K., Dekker, A., Engstrom, C., Bugarcic, A., Worthy, P., . . . Long, P. (2016). Are they using my feedback? The extent of students’ feedback use has a large impact on subsequent academic performance. Assessment & Evaluation in Higher Education, 1-20. doi:10.1080/02602938.2016.1174187



.. index::
	single: auteurs; Dessus, Philippe
	single: auteurs; Tarbouriech, Nelly
	single: auteurs; de Flaugergues, Marie

.. admonition:: Information

	* **Auteurs** : `Philippe Dessus <http://pdessus.fr/>`_, LaRAC & Inspé, Univ. Grenoble Alpes, avec la participation de Nelly Tarbouriech, Rectorat de l'académie de Grenoble (document :ref:`usages-peda`) et de Marie de Flaugergues, Espé, Univ. Grenoble Alpes (document :ref:`net_quiz_pro`).

	* **Date de création** : Septembre 2015, dernière mise à jour : Mars 2017

	* **Date de publication** : |today|.

	* **Statut du document** : Archive : cours non maintenu.
	
 	* **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.

	* **Licence** : Document placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_


Cours complet
=============

Attention, ces documents ci-dessous peuvent être un peu moins à jour que le cours décrit dans ce site. Il faudra considérer que la version ci-dessous prévaut sur la version du site. Tout autre lecteur considérera le contraire.


Propos du cours
===============

Ce cours  a pour but de présenter des éléments théoriques et pratiques liés à l’évaluation de connaissances et les rétroactions nécessaires à un guidage optimal, principalement *via* questionnaires à choix multiple (:index:`QCM`). Les QCM sont de plus en plus utilisés dans des contextes d’e-learning ou en classe *via* des boîtiers de vote. S’ils sont souvent utilisés à des fins sommatives, ils peuvent également être adaptés à une pratique formative de l’évaluation.

Ce cours est donné dans deux institutions (Univ. Grenoble Alpes et Univ. de Rouen-CNED) et avec des modalités différentes. Ce document est celui du cours du MARDIF, univ. Rouen-CNED), et on se reportera à :ref:`organisation_cours-MARDIF` pour le détail de son organisation.

Remerciements
=============

L’auteur remercie vivement Christophe Charroud, Olivier Cosnefroy, Cédric D'Ham, Hussein Atta Salem et Emmanuelle Villiot-Leclercq pour leurs commentaires d’une version antérieure de ce cours. Olivier Cosnefroy et Thierry Rocher, par leurs commentaires vigilants, ont fait en sorte que la section sur l'analyse psychométrique soit plus claire et surtout moins ambiguë. Merci également à Ghislain Laloup et Samuel Labadie (Univ. Rouen) pour leur aide très efficace dans l'organisation de cet atelier, qui est même allée jusqu'à devancer mes questions et demandes. Selon la formule, les erreurs qui peuvent subsister dans le cours et son organisation (choisir la bonne réponse) :

a : sont entièrement de la faute des collègues précédemment cités 

**X b : sont de la pleine responsabilité de l’auteur !**


Notes sur les versions antérieures du cours
===========================================

Ce cours en est à sa 7\ :sup:`e` édition. Les 5 premières éditions de ce cours concernaient seulement la formation CNED-MARDIF (Master 2 à distance francophone). Voici les principaux changements des différentes versions, édition par édition du cours : 

* 7 (2016-17). Actualisations mineures. Intégration du cours par sections dans la base SAPP.
* 6 (2015-16). Portage sous `Sphinx <http://www.sphinx-doc.org/en/stable/>`_, ajout d'une page sur les usages pédagogiques, améliorations diverses.
* 5 (2014-15). Avec des actualisations mineures, essentiellement sur la section 7, sur les aspects cognitifs. Cette version a été également déposée sous HAL [`Référence <https://hal.archives-ouvertes.fr/cel-01074748>`_]
* 4 (2013-14). Ajout principal dans la § 9\ :sup:`e` Section (la présentation d’`Oppia <https://www.oppia.org/>`_). 
* 3 (2012-13). Légèrement augmentée (le calcul de l’indice *B* et ajout de quelques ressources). 
* 2 (2011-12). Refonte complète (une douzaine de pages supplémentaires) et en partie refondue (3\ :sup:`e` partie), avec l’ajout de la Section 10 (psychométrie). 


.. Sert à produire la file de documents 
.. penser à ajouter :numbered: avant production du doc (sinon, ajoute des numérotations dans l'index général)

.. toctree::
	:maxdepth: 3
	
	organisationMARDIF
	tache_eval
	retro_defs
	retro_formes
	retro_info
	QCM_historique
	QCM_def
	QCM_cog
	QCM_formes
	QCM_proc_conception
	QCM_principes
	tuto-analyse-items
	usages-peda
	res_generateurs_cours
	tuto_exe_learning
	tuto_net_quiz_pro
	tuto_oppia


Index et tables
===============

* :ref:`genindex`
* :ref:`search`