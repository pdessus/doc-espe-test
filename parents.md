(parents-profs)=

# Les relations parents-professeurs

```{index} single: auteurs; Campanale, Françoise single: auteurs; Besse, Émilie
```

:::{admonition} Informations
- **Auteurs** : Françoise Campanale, IUFM Grenoble. Le quizz a été réalisé par Emilie Besse, projet [ReflexPro](http://www.idefi-reflexpro.fr).
- **Date de création** : Nov. 2001 ; mis à jour en Février 2007.
- **Date de publication** : {sub-ref}`today`.
- **Statut du document** : Terminé.
- **Résumé** : Les relations parents-professeurs se construisent à travers des communications directes, lors de rencontres parents-professeurs organisées par les établissements, lors de rencontres à l’initiative des enseignants ou des parents (comme l'entretien individuel), et par des communications indirectes transmises par l’intermédiaire de   l’enfant-élève. Sont abordés des conseils pour intervenir dans la rencontre parents-professeurs du début d'année, dans les rencontres individuelles par niveau de classe, et enfin dans les rencontres individuelles enseignant-parents au cours de l'année.
- **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.
- **Licence** : Document placé sous licence *Creative Commons* : [BY-NC-SA](http://creativecommons.org/licenses/by-nc-sa/3.0/fr/)
:::

## Ce que l'on sait

### L’école transforme les familles en parents d’élèves

Les écoles, qui accueillent des populations socio-culturellement favorisées, parlent " des parents", celles situées en ZEP, plutôt "des familles" (Glasman, 1992) marquant par là une certaine indétermination des interlocuteurs avec qui elles pourront engager un dialogue et un plus   grand éloignement de ces familles par rapport à l’univers scolaire. C’est se désintéresser des rapports effectifs, et qui peuvent être différents au sein d’une même catégorie sociale, que ces familles/parents peuvent vouloir entretenir avec l’école.

### L’école désorganise et réorganise la vie de la cellule familiale

Emploi de temps de la famille, organisation de l’espace familial, ventilation du budget, répartition des tâches   (encadrement du travail scolaire, représentation aux rencontres   organisées par l’établissement et aux entretiens individuels,   formalités etc.) (Montandon & Perrenoud, 1994). L'école attend que les parents prennent en charge ce qu’elle ne peut faire, mais n’en attend ni propositions, ni   participation importante à son fonctionnement. À travers son action   d’instruction et d’éducation à l’égard des élèves, l’école cherche à agir sur l’éducation réalisée au sein de la famille. Elle pèse aussi sur le contrôle social que les parents exercent sur leurs enfants, sur   leur action éducative, sur les projets du devenir des enfants, sur ses stratégies face au piège scolaire. Enseignants et familles se renvoient la balle sur la question de l’autorité.

### L'école touche l’image de la famille

En fonction des évaluations scolaires qu’elle renvoie, à sa vie privée en fonction des   informations qu’elle demande, à son insertion sociale dans divers réseaux de relations. (Montandon & Perrenoud, 1994, pp. 133-134). L’école stigmatise l’échec, oriente par défaut. Face aux parents d’élèves en difficulté, les enseignants oscillent entre accusation et compassion.

### Les familles populaires font confiance au système scolaire

Les familles attendent que le système scolaire fasse réussir leurs enfants. Pour elles, les   enseignants connaissent leur métier et elles ne se sentent pas les   compétences pour intervenir, ni même éventuellement dialoguer. Aussi,   elles restent en retrait, ce que les enseignants interprètent comme de   la négligence. Quand l’enfant échoue, c’est la famille qui ressent l’échec. Quand il réussit, la famille populaire se sent quelque peu dépassée, voire dépossédée. Ces familles redoutent les rencontres avec les enseignants, qui se font sur le mode de la convocation, et   qui à travers les jugements portés sur leurs enfants risquent de les   renvoyer à leur propres difficultés scolaires passées, à leur   ignorance du fonctionnement du système. Les familles populaires jugent l’enseignant sur la relation qu’il établit avec l’enfant. L’enfant est élevé entre deux univers : l’un où l’on se ressource, l’autre où règne la compétition, éventuellement la violence. Il est souvent chargé de la communication entre ces deux univers qui peuvent être très   différents. Aussi, il interprète, utilise les stratégies qui lui conviennent le mieux.

Perrenoud (1996) nomme l’enfant le "*go-between*" (le messager), parce que, comme le jeune garçon du [film](<https://fr.wikipedia.org/wiki/Le_Messager_(film,_1971)>) de Joseph Losey du même nom, l'enfant va, et vient *entre* deux mondes. De l'une à l'autre, il fait passer *tout* ce qu'un être humain peut capter, filtrer, restituer en fonction de son équation   personnelle mais aussi de ses stratégies. Les parents ont peu de moyens de savoir exactement ce que leur enfant dit et fait en classe,   et à l'inverse le maître est réduit à des suppositions sur son   attitude à la maison. \[…\]  La façon dont l'enfant "trahit" l'intimité   de sa famille ou de son groupe-classe dépend non seulement de la   situation, mais des stratégies qu'il mène." …

Dans les rencontres, les familles populaires témoignent pour les raisons invoquées ci-dessus d’une certaine passivité. " Tous les enseignants ne manifestent pas, dans les rapports avec les parents, la disponibilité, l'aisance, la simplicité, l'ouverture qui pourraient faciliter les choses. Il n'est pas rare que l'enseignant soit aussi   tendu ou embarrassé que ses interlocuteurs. Ce que ces derniers ne   soupçonnent pas, mettant sur le compte d'une attitude arrogante ou   fermée ce qui n'est en réalité qu'une certaine angoisse devant un   groupe de parents. Aussi, la mise en place de médiateurs entre l’école   et les familles " éloignées " de l’école, comme le proposent Bouveau et *al.* (1999) peut être un moyen efficace pour gérer ces relations   entre les deux univers.

## Ce que l'on peut faire

### Les relations parents-professeurs

Ces relations se construisent à travers des communications directes, lors de rencontres parents-professeurs organisées par les établissements, lors de rencontres à l’initiative des enseignants ou des parents, et par des communications indirectes transmises par l’intermédiaire de l’enfant-élève. La récente [circulaire du 25 août 2006](http://www.education.gouv.fr/bo/2006/31/MENE0602215C.htm) reprécise et renforce certains points concernant ces relations : d'une part au niveau des informations des parents (réunions d'informations, rencontres parents-enseignants deux fois par an et par classe, informations dès que difficultés, bulletins scolaires, communication informatisée), d'autre part au niveau de l'exercice des associations de parents d'élèves (diffusion de documents d'information).

### La rencontre Parents–Professeurs du début de l’année

En début d’année, les parents sont invités à venir dans l’établissement rencontrer l’équipe pédagogique de la classe de leur enfant. Cette réunion se tient en général en fin d’après-midi. Cette réunion est importante car les parents, à travers les interventions des enseignants, vont se forger une opinion sur eux et ils interpréteront probablement par la suite ce que dira leur enfant en fonction de cette opinion. D’une étude concernant "la particpation des parents au fonctionnements des établissements d’enseignement secondaire " menée par J. Migeot-Alvarado et A. Henriot-van Zanten, à la demande de la Direction des lycées et collèges, il ressort que les parents participent massivement à ces rencontres et les jugent positivement. Néanmoins, les parents souhaitent qu’y soient inclues des informations précises sur l’organisation pédagogique, l’orientation et les résultats aux examens ". ils souhaiteraient aussi qu’y soient abordée la vie des élèves dans l’établissement et des problèmes liés à la sécurité et à la drogue (Migeot-Alvardo, 1995).

#### Qu’ont envie de savoir les parents ?

Les parents, qui s'assoient lors de ces réunions à la place de leurs enfants, ont pour but :
\* de faire connaissance avec les différents professeurs,
\* de s’informer sur les grandes lignes du programme traité dans chaque discipline,
\* de savoir quel matériel est demandé aux élèves,
\* de savoir ce que les élèves devront faire à la maison comme travail,
\* d’avoir quelques indications sur comment ils pourront suivre le   travail de leur enfant et comment ils pourront éventuellement l’aider.

#### Préparer la réunion

Cette réunion devrait être préparée par le professeur principal de la classe avec les collègues de l’équipe pédagogique. L’équipe peut ainsi harmoniser les demandes de fournitures (une liste globale est plus   simple à gérer pour les parents que des listes distinctes par   discipline, qui souvent se recoupent). L’équipe peut aussi équilibrer le travail demandé à la maison sur les différents soirs de la semaine   et en fonction de l’emploi du temps quotidien des élèves.

#### Le déroulement de la réunion

Les parents sont des interlocuteurs adultes et il est important de   manifester dès l’entrée une volonté de collaboration éducative. Les accueillir avec beaucoup de convivialité et non comme si c’était une   corvée imposée ; au moins les remercier d’être venus. Le professeur principal est en général chargé de conduire la réunion. Il annonce le déroulement de la réunion, présente la classe, l’équipe pédagogique, rappelle le règlement intérieur de l’établissement et   déclare les exigences de l’équipe pédagogique en matière de fournitures scolaires, de règles de vie générales dans la classe… Il présente aussi les outils au travers desquels parents et enseignants   pourront communiquer.
Ensuite chaque enseignant prend à tour de rôle la parole. Et enfin, soit les parents posent des questions à chaque enseignant, soit la dernière partie de la réunion peut être consacrée à des   échanges parents enseignants (le problème étant que des enseignants interviennent dans d’autres autres classes et doivent passer dans une   autre salle pour rencontrer d’autres parents).

#### Que dire quand c’est à son tour de parler ?

- Présenter sa discipline : quels en sont les concepts clés ? Quelles sont les capacités qu’elle sollicite ?
- Annoncer les grandes parties du programme et éventuellement leur programmation,
- Dire ce qu’on attend des élèves au niveau de l’évaluation, ce qui est le plus important pour les élèves cette année, ce qui est nouveau, les types de tâches qu’ils auront à réaliser dans l’année…
- Annoncer le type de travail qu’ils auront à faire le soir chez eux et la durée moyenne à consacrer à ce travail
- Nommer les fournitures spécifiques dont les élèves devront être pourvus
- Expliciter les règles de vie qu’on entend faire fonctionner dans la classe
- Dialoguer sur ce que les parents peuvent faire pour favoriser les    apprentissages, l’aide qu’ils peuvent éventuellement leur apporter.

#### Instaurer le dialogue

- Ne pas oublier de donner la parole aux parents et de les écouter (l’image du prof se joue dans l’attitude adoptée).
- On peut rester debout pour tous les premiers points et s’asseoir (comme ils le sont eux) pour manifester qu’on sollicite le dialogue.
- Eventuellement, leur demander ce qu’ils attendent eux.

### Les rencontres individuelles par niveau de classe enseignant-parents

Elles succèdent en cours d’année à la rencontre collective de début d’année et sont en général organisées en fin de trimestre, pour faire le point sur le travail des élèves et engager le projet d’orientation. De la même étude (citée plus haut), il ressort "qu’elles sont bien investies par les parents des classes moyennes …, mais souvent boudées   par les parents ouvriers non qualifiés", de même que par des parents d’origine étrangère. Ces réunions sont jugées plutôt mal organisées,   les parents attendent leur tour, quelquefois longtemps et les entretiens leur paraissent trop courts et superficiels.
Ces réunions sont difficiles à organiser et représentent une épreuve   pour l’enseignant qui doit successivement s’entretenir avec les   parents des élèves de toute une classe, du moins avec ceux qui sont là   et qui peuvent rester. Aussi, une préparation précise concernant   chaque élève doit être faite.
C’est aussi pour les parents un parcours d’obstacles. Ils sont censés rencontrer dans une soirée la totalité des enseignants de la classe,   répartis dans des salles différentes, et ils ne savent pas toujours où   se trouve tel ou tel enseignant, où se trouve telle ou telle salle. Aussi, les parents rompus au milieu scolaire se débrouillent-ils mieux   que d’autres.
Quelquefois, les parents sont convoqués successivement à des rendez-vous planifiés au cours desquels leur est remis le bulletin scolaire de leur enfant. Ces réunions devraient s’inscrire dans un processus d’évaluation-régulation :

- Bilan de fin de trimestre organisé par chaque prof avec ses élèves (quelques questions écrites aux élèves sur ce qui a marché, ce pour quoi ils ont des difficultés, ce qui les aiderait à mieux apprendre,    les suggestions qu’ils proposent…, dont les réponses sont résumées par le prof à l’ensemble de la classe, synthèse qui donne lieu à un    échange collectif de quelques minutes)
- Préparation du conseil de classe par le prof principal avec la classe
- Concertation de l’équipe pédagogique
- Conseil de classe
- Réunion parents-professeurs.

#### Déroulement de la rencontre

Le professeur principal peut donner collectivement au début de la réunion des informations générales sur le travail de la classe, les projets et le comportement des élèves. Les entretiens qui suivent, avec les parents de chaque élève, doivent   permettre d’expliciter ce qui figure sur le bulletin scolaire.

Il est important d’organiser la succession des entretiens, de préciser   leur but et leur durée. Ils sont nécessairement courts (5 min quand il n’y a pas de problème, 10 ou 15 si nécessaire). Il est possible de   donner des rendez-vous ultérieurs aux parents qui ne peuvent rester, avec lesquels l’enseignant prévoit un entretien plus long.

#### Que peuvent attendre les parents ?

- Un bilan des résultats et du travail du trimestre pour leur enfant
- L’identification des difficultés rencontrées par leur enfant et des conseils sur les aides qu’ils peuvent fournir
- Des explications sur les orientations possibles

#### Que peuvent attendre les enseignants ?

Des informations :

- sur la façon dont l’élève perçoit la discipline enseignée,
- sur sa façon de travailler à la maison, sur les aides qu’il demande et que les parents apportent,
- sur la façon dont l’élève assure la transmission des informations entre parents et enseignants.
- Il peut être utile de demander aux parents de se munir du carnet de correspondance, du bulletin scolaire.

### L’entretien individuel à l’initiative des enseignants et des parents

Ce type de rencontre a lieu parce qu’un problème se pose. Elle a lieu dans l’établissement scolaire. L’enseignant doit avoir prévu le   déroulement et les participants. L’élève assiste-t-il à l’entretien ? À tout l’entretien  ou à quel moment ? Le CPE est-il convié ? Il est   important que l’élève soit impliqué au moins dans la dernière partie   de l’entretien, pour éviter qu’il se sente exclu du problème et   laissent les adultes s’arranger entre eux.

La même étude mentionne que les parents accordent à ces contacts une   grande importance, mais si les cadres moyens ou employés les sollicitent ou répondent positivement à une demande de rendez-vous proposé par l’enseignant, les ouvriers qualifiés ou non qualifiés déclarent avoir eu peu d’entretiens de ce type et les parents immigrés   auraient tendance à les éviter. Suivant leur origine socio-culturelle,   leur passé scolaire, les parents ont des attitudes et attentes   différentes face à ces entretiens.

#### Le déroulement de l'entretien

- Préambule de l’enseignant sur l’objet de l’entretien, le statut de chacun dans l’entretien, le déroulement (l’élève participera à tel moment…).
- Explicitation la plus objective possible des faits, identification du problème posé par rapport à l’apprentissage, par rapport aux règles de vie dans la classe et dans l’établissement …
- La façon dont chacun ressent, interprète les faits
- Comment ces faits ont été rapportés par l’élève à la maison, avec quels commentaires sur la discipline enseignée, sur les problèmes que    rencontre l’élève dans le déroulement de la classe, dans la vie de l’établissement, dans son travail à la maison…
- Quelle est l’ambition des parents par rapport à l’avenir scolaire de leur enfant ? Quelle est leur stratégie ?
- Comment expliquer le dysfonctionnement ?
- Quelles attitudes, actions engagées par l’enseignant et les parents    peuvent aider l’élève à résoudre le problème / à rétablir un fonctionnement conforme aux règles de vie de l’établissement et de la classe ?
- À la fin de l’entretien, l’enseignant doit faire une synthèse de    l’échange, rappeler les décisions et engagements pris, leurs échéances et effets attendus.

## Quizz

```{eval-rst}
.. eqt:: Parents-1

        **Question 1.   Les écoles situées dans des REP utilisent plus souvent le terme**

        A) :eqt:`I` `Parents`
        B) :eqt:`C` `Famille`
        C) :eqt:`I` `Proches`
        D) :eqt:`I` `Responsables légaux`
```

```{eval-rst}
.. eqt:: Parents-2

        **Question 2.   Les écoles accueillant des populations favorisées parlent plutôt des**

        A) :eqt:`C` `Parents`
        B) :eqt:`I` `Famille`
        C) :eqt:`I` `Proches`
        D) :eqt:`I` `Responsables légaux`
```

```{eval-rst}
.. eqt:: Parents-3

        **Question 3.   Les familles populaires appréhendent les rencontres avec les enseignants car**

        A) :eqt:`I` `Elles n'ont pas souvent confiance aux enseignants`
        B) :eqt:`I` `Elles ne sont pas toujours d'accord avec le système scolaire`
        C) :eqt:`C` `Cela peut les renvoyer à leurs échecs scolaires passés`
        D) :eqt:`I` `Ils sont parfois désintéressés du parcours scolaire de leurs enfants`
```

```{eval-rst}
.. eqt:: Parents-4

        **Question 4.   Quel est la principale difficulté lors des réunions parents-enseignants ?**

        A) :eqt:`I` `La disponibilité des parents`
        B) :eqt:`I` `La non-préparation du professeur principal`
        C) :eqt:`C` `La multiplicité des classes transformant les réunions en jeu de piste`
        D) :eqt:`I` `La non-communication de convocation à la réunion`
```

## Références

- B.O. (2006). [Le rôle et la place des parents à l'école](http://www.education.gouv.fr/bo/2006/31/MENE0602215C.htm). Circulaire 2006-137 du 25 août (BO 31 du 31/8).
- Bouveau P. & Cousin O. & Favre J. (1999). *L’école face aux parents-Analyse d’une pratique de médiation*. Paris : ESF.
- Glasman, D., (1992), "parents" ou "familles" : critique d'un   vocabulaire générique" ", *Revue Française de pédagogie*, 100, 19-33.
- Glasman, D. (1995). Les avatars de l'implication. *Cahiers Pédagogiques*, 339, 13-14.
- Meirieu P. (2000). (dir.), *L’école et les parents-La grande explication*. Paris : Plon.
- MEN. [Site internet d'information : Les parents d'élèves](http://www.education.gouv.fr/cid2659/les-parents.html).
- Migeot-Alvarado, J. (1995), Les parents dans l'école ou Pourquoi ils ne participent pas assez. *Cahiers Pédagogiques*, 339, 15-17.
- Montandon C. & Perrenoud P. (1994). *Entre parents et enseignants : un   dialogue impossible ?* Berne : Peter Lang.
- Perrenoud, P. (1996). *Métier d’élève et sens du travail scolaire*. Paris : ESF.
