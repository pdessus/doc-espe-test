.. _peda_differenciee:

********************************************
Pédagogie différenciée : luxe ou nécessité ?
********************************************

.. index::
    single: auteurs; Dessus, Philippe
    single: auteurs; Campanale, Françoise
    single: auteurs; Besse, Émilie


.. admonition:: Information

   * **Auteurs** : Françoise Campanale & `Philippe Dessus <http://pdessus.fr/>`_, LaRAC & Inspé, Univ. Grenoble Alpes.  Le quizz a été réalisé par Emilie Besse, projet `ReflexPro <http://www.idefi-reflexpro.fr>`_.

   * **Date de création** : Juin 2005, doc. remanié en janvier 2017.

   * **Date de publication** : |today|.

   * **Statut du document** : En travaux.

   * **Résumé** : La pédagogie différenciée, née avec la massification de l'enseignement secondaire, se veut une réponse à l'hétérogénéité des classes, un moyen de lutter contre l'échec scolaire. Comment traiter les différences entre élèves ? Comment ne pas transformer les différences individuelles en inégalités de réussite scolaire reproduisant des inégalités sociales ?

   * **Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.

   * **Licence** : Ce document est placé sous licence *Creative Commons* : `BY-NC-SA <http://creativecommons.org/licenses/by-nc-sa/3.0/fr/>`_.


Introduction
============

Les différences sont naturelles et nécessaires à l’évolution.  Diversité et diversification sont des caractéristiques du vivant. Le problème de l’école républicaine, compte tenu de ses valeurs, est de donner à chacun le maximum de chances de réussite. La pédagogie différenciée est née avec la massification de l’enseignement et le collège unique des années 1970. Elle se veut une réponse à l’hétérogénéité des classes, un moyen de lutter contre l’échec scolaire. Comment traiter les différences entre élèves ? Comment ne pas transformer les différences individuelles en inégalités de réussite scolaire reproduisant des inégalités sociales ? Ce document traite de ces questions


Ce que l'on sait
================

Le traitement des différences à l’école : de la ségrégation à la différenciation
--------------------------------------------------------------------------------

Jusqu'aux années 1970, avec le plein emploi, l'école a la mission de ventiler les élèves à des niveaux de qualification différents. On note, on classe. La gestion des différences entre élèves se fait par ségrégation dans des classes différentes, avec des passerelles peu empruntées. La pédagogie officielle pratique la leçon, les exercices d'application, les compositions et le corrigé collectif.  Elle est transmissive, à orientation normative. La notion d’égalité signifie mettre les élèves dans les mêmes conditions de passation d’épreuves. Il y a ceux qui peuvent, ceux qui ne peuvent pas, et personne n'est choqué.

Après 1970, l'évolution du contexte économique et social amène la nécessité d'élever le niveau de qualification de l'ensemble de la population, et une exigence de démocratisation de l'école.  L'idée de l'école unique, la même école pour tous, garante d'un enseignement plus démocratique, née après 1918, se réalise en 1975 avec la réforme Haby et la mise en place du collège unique. Le traitement des différences doit alors se faire dans la classe. À un traitement  institutionnel, succède un traitement pédagogique.

Ce nouveau mode de traitement des différences ne signifie pas que le précédent a complètement disparu, mais la ségrégation est repoussée, se fait de façon plus insidieuse. La loi d'orientation de juillet 1989 organise la scolarité en cycles.  Elle promeut l'égalité des chances, et la notion d’équité apparaît (ne plus traiter chacun de la même façon, mais en fonction de ses différences), concurrençant l’égalité de traitement : "Pour assurer l'égalité et la réussite des élèves, l'enseignement est adapté à leur diversité" (L. Jospin, Préface au livret "Les cycles à l'école primaire", CNDP, Hachette, Ecoles, 1991).

Qu’est-ce que différencier ?
----------------------------

En prévision de la réforme Haby, L. Legrand, en 1971, avait lancé un programme de recherche à l'INRP sur la pédagogie différenciée. La pédagogie différenciée se développe contre les idées suivantes, et installe des principes pour chacune :

* l'idée d'uniformité "Cette uniformité est désastreuse.  Elle engendre d'abord la monotonie et l'ennui, dans une société où l'intérêt est sans cesse relancé" (Prost, 1985), d'où le principe de variété.
* l'utopie de l'égalitarisme par un traitement identique pour chacun des élèves, d'où le principe de diversification et d’adaptation (travaux différents, contrats individualisés, modalités d'évaluation différentes).
* les méthodes faites par un expert et qui seraient valables pour tous, d'où le principe de construction de références évolutives.
* l'évaluation normative comparative qui classe les élèves les uns par rapport aux autres et traduit les différences en termes d’inégalités, d'où le principe d’évaluation formative.
* l'idée que les erreurs sont des dysfonctionnements aberrants qui ne devraient pas être, d'où le principe d’intelligence et d’éducabilité de tous les individus.

La différenciation pédagogique s’inscrit dans la lignée des travaux de psychologie différentielle, qui mettent en évidence  la diversité des fonctionnements individuels. Elle se réfère aux théories socio-constructivistes de l'apprentissage (l’apprentissage se construit à partir ou contre des représentations  existantes, sur un déjà là, qui peut évoluer par confrontations avec d’autres représentations et avec des situations-problèmes qui invalident des représentations naïves.

La différenciation pédagogique peut se définir comme une démarche, une volonté, des stratégies, non comme une nouvelle méthode. Elle suppose travail en groupes, aide individualisée. Les verbes forts sont : varier/diversifier, adapter, négocier.

En voici deux définitions :

* C’est une "démarche qui cherche à mettre en œuvre un ensemble diversifié de moyens, de procédures d'enseignement et d'apprentissage, afin de permettre à des élèves d'âges, d'aptitudes, de comportements, de savoir-faire hétérogènes mais regroupés dans une même division, d'atteindre, par des voies différentes, des objectifs communs, ou en partie communs". (Raymond, 1987, *Cahiers Pédagogiques, numéro "Différencier la pédagogie"*, p. 47).

* "Différencier, c'est avoir le souci de la personne sans renoncer à celui de la collectivité... être en quête d'une médiation toujours plus efficace entre l'élève et le savoir... C'est pourquoi, il ne faut pas parler de la "pédagogie différenciée" comme d'un nouveau système pédagogique, mais bien plutôt comme d'une dynamique à insuffler à tout acte pédagogique... un moment nécessaire dans tout enseignement... celui où s'insinue la personne dans le système..." (Meirieu, 1987, *Cahiers Pédagogiques, numéro "Différencier la pédagogie"*, introduction).

Ce que l'on peut faire
======================

Cela nécessite une centration non oppressante sur les démarches des élèves (de l’intérêt, non du contrôle).

Distinguer différences quantitatives et qualitatives
----------------------------------------------------

Si l’on considère que, quel que soit le milieu social, le sens du développement intellectuel et de la personnalité doit être le même dans une société donnée, les différences individuelles constatées sont conçues comme des retards de développement, des lacunes dans les apprentissages de la prime enfance, des défauts dans la formation de la personnalité, des manques.  En fonction des stimulations plus ou moins riches dont ils ont bénéficié dans leur environnement, les enfants seraient plus ou moins avancés, développés, rapides, adaptés, cultivés, plus ou moins "intelligents".  Cette interprétation quantitative amène un traitement *compensatoire* des différences, en termes de remédiations au sens de remèdes, de soutien. 

A cette interprétation quantitative s'oppose une interprétation *qualitative* des différences.  Les diverses classes sociales ont leur culture propre, leurs propres valeurs, leur propre mode de relation, leur propre rapport au temps, au travail, au savoir etc...  L'école définit à travers ses normes une culture parente de celle développée dans les classes dominantes de la société.  Les cultures des différents groupes sociaux sont à une distance plus ou moins grande de cette culture de référence qui détermine la norme d'excellence scolaire :cite:`bautier00`. Les différences témoignent alors de diversités et non de manques. 

Il est dans l'ordre des choses qu'il y ait une culture commune à un ensemble territorial, mais ignorer les cultures d'origine, c'est créer des handicaps. D'autant plus qu'à la distance culturelle s'ajoute souvent un difficile décodage des implicites pédagogiques, générateur d'angoisse et de conduites aberrantes. Le traitement des différences suppose alors ouverture au pluriculturel, diversité des médiations et des médiateurs, implication de l’élève dans l’évaluation (l'enfant doit pouvoir se saisir des normes scolaires qui ne sont pas nécessairement celles du milieu familial), valorisation de l'expérience personnelle, absence de jugements de valeur

Prendre en compte ces deux interprétations suppose de varier, Prendre en compte ces deux interprétations suppose de varier, diversifier, personnaliser.

Que différencier ?  Formes de différenciation
---------------------------------------------

On peut imaginer que différencier, c'est s'adapter aux variables des apprenants : modes de pensée privilégiés (inductif, déductif...), manières d’entrée privilégiées avec certaines informations (symboliques, graphiques, chiffrées, écrites, orales...), mobiles profonds, attitudes, à l'objet d'apprentissage, attributions du contrôle (externe/interne), aux rythmes biologiques, structures cognitives (représentations, ...), cultures familiales, ... C'est une mission impossible : l'enseignement en classe n'est pas le préceptorat.  Mais entre l'individualisation totale et l'uniformisation, on peut jouer.

Sur quoi individualiser ?
`````````````````````````

* les modes de présentation du savoir (montrer, expliquer, faire rechercher, faire trouver/expérimenter...)
* les circonstances (les types de tâches, les supports, les formes de groupements, les rythmes, les modalités d'évaluation, les modalités de remédiations...)
* les types de relations/les modes de communication avec l'élève

Comment individualiser ?
````````````````````````

* par différenciation successive compte tenu des informations fournies par l'évaluation instrumentée ou intuitive sur l'ensemble du groupe-classe
* par différenciation simultanée :

   * présentation de la tâche sous des formes variées, comportant des étayages plus ou moins importants, au choix de l’élève ;
   * ateliers en fonction de groupes de besoins (remédiations et    renforcement), contrats individualisés à partir des résultats de l’évaluation formative.

* par des aides individualisées, "à chaud", au cours des travaux, aide méthodologique, incitation à la métacognition, ...

Tout cela demande du temps, de l'organisation, d'avoir dépassé les premières difficultés de l'enseignant débutant concernant la planification des séquences, la gestion du groupe-classe. Cela demande aussi de maîtriser les contenus enseignés et des connaissances en didactiques des disciplines.

Que faire "à peu de frais" ?
----------------------------

*A minima*, on peut varier au cours du temps ses modes d'intervention, les modalités de travail, les types de tâches et les supports.

* Varier dans le même temps : ateliers sur des tâches différentes visant un même objectif,avec possibilité de choix par les élèves,
* Permettre à l’élève de gérer sa différence au sein d’un groupe de travail (ex. pour la correction d’un devoir, organiser des groupes ayant pour tâche de présenter la correction d’une partie du devoir, en expliquant et évaluant les diverses démarches possibles, en faisant état des erreurs et si possible des raisonnements responsables de ces erreurs) ;
* Personnaliser au moins les annotations, le questionnement oral. Ecrire sur les travaux des élèves des annotations utiles à  l'apprentissage (identification des réussites, des erreurs,    questionnement/propositions) et non seulement des estimations de niveaux de réussite ou des remarques stéréotypées, engager par l’intermédiaire de la copie un dialogue avec l’élève.

La différenciation n’est pas l’individualisation. Il n’est pas nécessaire d’engager le dialogue avec chacun, ni d’annoter finement chaque copie à chaque devoir.

Les protocoles de Réponse à l'intervention
------------------------------------------

Un protocole a été mis en place par des chercheurs dans différents établissements et districts éducatifs (principalement d'Amérique du Nord), le protocole "à trois niveaux" (nommé encore RAI pour "Réponse à l'intervention", en anglais : "*Three-tier model*" et "*Response to intervention model*"). Décrivons-le rapidement (voir :cite:`lane12`).

Il part du principe que l'on peut régler la plupart des problèmes liés à l'apprentissage (*e.g.*, en lecture, écriture) ou au comportement des élèves en réalisant trois vagues successives d'interventions :

#. *Intervention primaire, dite "intervention universelle"*. Ce niveau s'adresse à tous les élèves. L'enseignant leur propose des séances qui ont montré un niveau d'efficacité certain (notamment, ayant fait l'objet d'évaluations sérieuses). Des mesures évaluatives permettent de déterminer son niveau de succès. Il est attendu, en moyenne, qu'environ 15 à 20 % des élèves n'ont pas des performances (comportementales ou cognitives) au niveau attendu. Ces élèves font l'objet des préventions des niveaux suivants.
#. *Intervention secondaire, dite "intervention ciblée"*. Les élèves sélectionnés sont répartis en petits groupes et font l'objet d'un enseignement centré sur leurs difficultés, identifiées à l'étape 1, et se déroulent pendant le temps scolaire. Les élèves qui ne progressent pas suffisamment à l'aide de ce second niveau sont sélectionnés pour le troisième.
#. *Intervention tertiaire, dite "intervention intensive"*. Les quelques élèves restants (en moyenne, 5-7 % des élèves) font l'objet d'entraînements très ciblés et intensifs (plus systématique que lors du 2\ :sup:`e` niveau), sur leurs difficultés, et leurs résultats tout au long de cette intervention sont analysés de près. Elles sont en général multipartenaires (enseignants, parents, psychologues) et font l'objet de plans personnalisés. Elles se déroulent en général en dehors du temps de classe.

Interventions individualisées sur le plan comportemental
--------------------------------------------------------

Voici ci-dessous une liste de ressources évaluatives pour évaluer les élèves dans le domaine comportemental (provenant, pour les réf. en anglais, de  :cite:`lane12`, p. 562) :

* `Malette numérique sur les troubles de comportement <https://fr.m.wikiversity.org/wiki/Mallette_pedagogique_Troubles_du_comportement>`_.
* `Systematic Screening for Behavior Disorders <http://nhcebis.seresc.net/universal_ssbd>`_
*  `Student risk screening <https://miblsi.org/evaluation/student-assessments/student-risk-screening-scale>`_
* `Strengths and difficulties questionnaire <http://www.sdqinfo.com>`_
* `BASC2--Behavioral and emotional screening system <http://www.pearsonclinical.com/education/products/100000661/basc-2-behavioral-and-emotional-screening-system-basc-2-bess.html>`_
* `SSiS--Social skills improvement system <http://www.pearsonclinical.com/education/products/100000356/social-skills-improvement-system-ssis-performance-screening-guide.html>`_, comprenant le "classwide intervention program", programme d'intervention dans le domaine des habiletés sociales.

Quizz
=====

.. eqt:: Pedadiff-1

	**Question 1. Parmi les idées suivantes, quelle est celle que la pédagogie différenciée rejette ?**

	A) :eqt:`C` `Le principe de l'égalitarisme`
	B) :eqt:`I` `Le principe de variété`
	C) :eqt:`I` `Le principe de diversification`
	D) :eqt:`I` `Le principe de construction de références évolutives`

.. eqt:: Pedadiff-2

	**Question 2. Parmi les idées suivantes, quelle est celle qui correspond à la pédagogie différenciée ?**

	A) :eqt:`I` `Le principe de l'évaluation normative comparative`
	B) :eqt:`I` `Le principe d'uniformité`
	C) :eqt:`C` `Le principe d'évaluation formative`
	D) :eqt:`I` `Le principe de l'égalitarisme`

.. eqt:: Pedadiff-3

	**Question 3. Selon le protocole de la Réponse à l'intervention, quelles sont les trois vagues d'intervention successives ?**

	A) :eqt:`I` `"1° Intervention individualisée ; 2° Intervention collective ; 3° Intervention globale"`
	B) :eqt:`C` `"1° Intervention universelle ; 2° Intervention ciblée ; 3° Intervention Intensive"`
	C) :eqt:`I` `"1° Intervention directe ; 2° Intervention indirecte ; 3° Intervention finale"`
	D) :eqt:`I` `"1° Intervention collective ; 2° Intervention individuelle ; 3° Intervention finale"`


Ressources
==========

* `Malette numérique sur la différenciation <https://fr.m.wikiversity.org/wiki/Definitions_de_la_differenciation>`_.
* `Cours sur le modèle RAI de S. Bouchard, TELUQ, Canada <http://edu1014.teluq.ca/mes-actions/modele-rai/>`_.
* `Cadre de référence en psychoéducation, Commission scolaire Marguerite-Bourgeoys, Canada <https://levissauve.ecoleverdun.com/wp-content/uploads/2016/05/Cadre-de-reference-psychoeducation.pdf>`_.

 
Références
==========

.. bibliography::
 :cited:
 :style: apa
